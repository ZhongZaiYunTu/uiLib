package com.zz.yt.test.flow.time;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.test.R;


/**
 * @author qf
 * @version 2023/4/24
 **/
public class TimeDelegate extends LatteDelegate implements View.OnClickListener {

    private TextView idTextValue;

    @Override
    public Object setLayout() {
        return R.layout.activity_date;
    }

    @SuppressLint("SetTextI18n")
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        idTextValue = rootView.findViewById(R.id.id_text_value);

        setOnClickListener(R.id.id_btn_single, v -> {
            //时间单选择(只能选择今天及以后时间)
            onClickTimeSingle(true, (startTime, endTime) -> idTextValue.setText(startTime + " 至 " + endTime));
        });
        setOnClickListener(R.id.id_btn_single2, v -> {
            //时间单选择
            onClickTimeSingle((startTime, endTime) -> idTextValue.setText(startTime + " 至 " + endTime));
        });

        getButton(R.id.id_btn_easy).setOnClickListener(this);

        setOnClickListener(R.id.id_btn_month, view -> {
            //月份段选择
            onClickDate(true, (startTime, endTime) -> idTextValue.setText(startTime + " 至 " + endTime));
        });

        setOnClickListener(R.id.id_btn_year, view -> {
            //年份段选择
            onClickDate(false, (startTime, endTime) -> idTextValue.setText(startTime + " 至 " + endTime));
        });

        setOnClickListener(R.id.id_btn_city, view -> {
            //城市选择
            onClickCity(index -> idTextValue.setText(index));
        });

    }


    @SuppressLint("SetTextI18n")
    public void onClick(View v) {
        onClickTimeMultiple(true, (startTime, endTime) -> idTextValue.setText(startTime + " 至 " + endTime));

    }

}
