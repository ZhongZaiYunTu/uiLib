package com.zz.yt.test.flow;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.test.R;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/4/24
 **/
public class InputDelegate extends LatteDelegate {

    @Override
    public Object setLayout() {
        return R.layout.activity_input;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        EditLayoutBar mEditPhone = rootView.findViewById(R.id.id_edit_name);

        getButton(R.id.id_btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditPhone.isString()) {
                    return;
                }
                if (!Patterns.PHONE.matcher(mEditPhone.toString()).matches()) {
                    ToastUtils.showShort("错误的手机号码格式");
                    return;
                }
                if (!RegexUtils.isMobileExact(mEditPhone.toString())) {
                    ToastUtils.showShort("错误的手机号码格式");
                    return;
                }
                LatteLogger.e(mEditPhone.toString() + "");
            }
        });
    }
}
