package com.zz.yt.test.delegate;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.delegate.authentication.personal.AuthenticationDelegate;
import com.zz.yt.lib.ui.delegate.cipher.CipherDelegate;
import com.zz.yt.lib.ui.delegate.edition.EditionDelegate;
import com.zz.yt.lib.ui.delegate.feedback.FeedbackDelegate;
import com.zz.yt.lib.ui.delegate.scanner.ScannerDelegate;
import com.zz.yt.test.R;
import com.zz.yt.test.flow.auto.AutoTextDelegate;
import com.zz.yt.test.flow.autograph.AutographDelegate;
import com.zz.yt.test.flow.search.SearchDelegate;

import me.yokeyword.fragmentation.ISupportFragment;

/***
 * 系统集成界面
 *
 * @author qf
 * @version 1.0
 */
public class PageDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {

    private final String[] titles = {
            "密码管理", "意见反馈", "搜索控件",
            "身份证界面", "扫码界面",
            "版本信息",
            "电子签名", "自动翻页"
    };

    private final ISupportFragment[] delegate = {
            new CipherDelegate(), new FeedbackDelegate(), new SearchDelegate(),
            new AuthenticationDelegate(), new ScannerDelegate(),
            new EditionDelegate(),
            AutographDelegate.create(), AutoTextDelegate.create()
    };

    @NonNull
    public static PageDelegate create() {
        final Bundle args = new Bundle();

        final PageDelegate fragment = new PageDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("系统集成界面");
    }

    @Override
    public Object setLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        start(delegate[position]);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        ListView mListView = findViewById(R.id.id_list_view);
        if (mListView != null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getProxyActivity(),
                    android.R.layout.simple_list_item_1,
                    titles);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(this);
        }
    }

}
