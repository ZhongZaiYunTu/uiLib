package com.zz.yt.test.adapter;

import android.view.View;
import androidx.annotation.NonNull;
import com.zz.yt.lib.ui.recycler.adapter.MultipleRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;


import java.util.ArrayList;
import java.util.List;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/8/14
 **/
public class ExampleAdapter extends MultipleRecyclerAdapter {

    private ExampleAdapter(List<MultipleItemEntity> data) {
        super(data);
    }

    @NonNull
    public static ExampleAdapter create() {
        return new ExampleAdapter(new ArrayList<>());
    }

    @NonNull
    public static ExampleAdapter create(List<MultipleItemEntity> data) {
        return new ExampleAdapter(data);
    }

    @NonNull
    public static ExampleAdapter create(@NonNull BaseDataConverter converter) {
        return new ExampleAdapter(converter.convert());
    }

    @NonNull
    @Override
    protected MultipleViewHolder createBaseViewHolder(@NonNull View view) {
        return MultipleViewHolder.create(view);
    }

}
