package com.zz.yt.test.flow;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.base.builder.ItemTabBuilder;
import com.zz.yt.lib.ui.base.ui.tab.LatteTopTabDelegate;
import com.zz.yt.lib.ui.recycler.data.TabEntity;

import java.util.LinkedHashMap;

public class TabDelegate extends LatteTopTabDelegate {

    @Override
    public LinkedHashMap<TabEntity, LatteDelegate> setItems(@NonNull ItemTabBuilder builder) {
        return builder.build();
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {

    }
}
