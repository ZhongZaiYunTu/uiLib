package com.zz.yt.test.flow.details;

import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.ButtonListEntity;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;
import com.zz.yt.test.R;

import java.util.ArrayList;

final class DetailsConverter extends BaseDataConverter {
    @Override
    public ArrayList<MultipleItemEntity> convert() {
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.INFORMATION_BAR)
                .setField(MultipleFields.SPAN_SIZE, 1)
                .setField(MultipleFields.ERROR_IMAGE, R.drawable.arrow_right_black)
                .setField(MultipleFields.TEXT, new ButtonListEntity("流程编号：", "NO.12345"))
                .build());
        final int size = 5;
        for (int i = 0; i < size; i++) {
            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.INFORMATION_BAR)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.IS_TITLE, true)
                    .setField(MultipleFields.ERROR_IMAGE, R.drawable.arrow_right_black)
                    .setField(MultipleFields.TEXT, new ButtonListEntity("流程编号：", "NO.12345"))
                    .build());
            setTitle("图片：");
            setMultiple("被检查单位", "collectiongetEnterpriseNames.getEnterpriseNames()");
            setDetail("被检查单位", "collection.getEnterpriseNames()");
            setDetails("地址", "collection.getInspectAddress()");
        }
        return ENTITIES;
    }
}
