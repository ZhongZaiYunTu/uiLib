package com.zz.yt.test.flow;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.FlowLayout;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.test.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 标签布局
 *
 * @author qf
 */
public class FlowDelegate extends LatteDelegate {

    private final List<String> list = new ArrayList<>();

    @Override
    public Object setLayout() {
        return R.layout.activity_flow;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        FlowLayout flowLayout = findViewById(R.id.id_layout_flow);
        list.add("Android");
        list.add("Java");
        list.add("IOS");
        list.add("python");
        flowLayout.setData(list);
    }

}
