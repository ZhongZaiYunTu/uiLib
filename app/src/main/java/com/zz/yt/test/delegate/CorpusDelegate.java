package com.zz.yt.test.delegate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.contrarywind.interfaces.IPickerViewData;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.popup.utils.picker.PickerPopupUtils;
import com.zz.yt.lib.ui.popup.view.VehiclePopup;
import com.zz.yt.lib.ui.utils.DateKeysUtils;
import com.zz.yt.lib.ui.utils.DatesUtils;
import com.zz.yt.test.R;

import java.util.ArrayList;
import java.util.List;

/***
 * 系统集成弹框
 * @author qf
 * @version 1.0
 */
public class CorpusDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {

    private final String[] titles = {
            "扫二维码", "扫二维码2", "附件选择",
            "车牌输入", "定位", "通用弹框",
            "选择时间", "选择时间-规定范围", "选择日期",
            "选择城市", "省市区选择",
    };

    @NonNull
    public static CorpusDelegate create() {
        final Bundle args = new Bundle();

        final CorpusDelegate fragment = new CorpusDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if ("扫二维码".equals(titles[position])) {
            startScanWithCheck(this);
        } else if ("扫二维码2".equals(titles[position])) {
            startScanWithCheck();
        } else if ("附件选择".equals(titles[position])) {
            startFilePickerWithCheck(this);
        } else if ("车牌输入".equals(titles[position])) {
            VehiclePopup.create(getActivity(), null);
        } else if ("定位".equals(titles[position])) {
            starClickWithCheck(index -> {
                LatteLogger.i("dw");
                ToastUtils.showShort("d定位权限成功");
            });
        } else if ("通用弹框".equals(titles[position])) {
            List<Bean> a = new ArrayList<>();
            a.add(new Bean("选项一"));
            a.add(new Bean("选项二"));
            a.add(new Bean("选项三"));
            PickerPopupUtils.showPickerData(context, "测试弹框", a, 0, null);
        } else if ("选择时间".equals(titles[position])) {
            onClickTime(null);
        } else if ("选择时间-规定范围".equals(titles[position])) {
            DatesUtils.create()
                    .setType(DateKeysUtils.DATE)
                    .currentDate(2024, 1, 4)
                    .startDate(2024, 1, 4)
                    .endDate(2024, 1, 2)
                    .onClick();
        } else if ("选择日期".equals(titles[position])) {
            onClickTimeSingle(null);
        } else if ("选择城市".equals(titles[position])) {
            onClickCity(null);
        } else if ("省市区选择".equals(titles[position])) {
            onClickAreas(null);
        }
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("系统集成弹框");
    }

    @Override
    public Object setLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        ListView mListView = findViewById(R.id.id_list_view);
        if (mListView != null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getProxyActivity(),
                    android.R.layout.simple_list_item_1,
                    titles);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setScanResult(requestCode, data, result -> ToastUtils.showShort("解析结果:" + result));
    }


    private static class Bean implements IPickerViewData {

        final String name;

        public Bean(String name) {
            this.name = name;
        }

        @Override
        public String getPickerViewText() {
            return name;
        }
    }

}
