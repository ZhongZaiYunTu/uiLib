package com.zz.yt.test.flow;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.test.R;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/5/28
 **/
public class SwitchListDelegate extends LatteDelegate {

    @Override
    public Object setLayout() {
        return R.layout.activity_list_switch;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {

    }
}
