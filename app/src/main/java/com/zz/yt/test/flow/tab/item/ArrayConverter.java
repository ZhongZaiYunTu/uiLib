package com.zz.yt.test.flow.tab.item;

import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.data.OrderFormEntity;
import com.zz.yt.lib.ui.recycler.data.StatusEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;

import java.util.ArrayList;

final class ArrayConverter extends BaseDataConverter {
    @Override
    public ArrayList<MultipleItemEntity> convert() {
        final int size = 5;
        for (int i = 0; i < size; i++) {
            setMultiple("被检查单位", "collectiongetEnterpriseNames.getEnterpriseNames()");
            setDetail("被检查单位", "collection.getEnterpriseNames()");
            setDetails("地址", "collection.getInspectAddress()");
            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.IMAGE_RECORD_NAME2)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.TITLE, " bean.getApplyRealName() bean.getProcessName()")
                    .setField(MultipleFields.IMAGE, "bean.getAvatar()")
                    .setField(MultipleFields.STATUS, new StatusEntity("审批中"))
                    .setField(MultipleFields.TEXT, new OrderFormEntity("流程编号：", "NO.12345"))
                    .setField(MultipleFields.TEXT_STATUS, new StatusEntity("流程编号状态"))
                    .setField(MultipleFields.NAME, new OrderFormEntity("申请时间：", "1592.03.24"))
                    .build());

            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.IMAGE_RECORD_NAME3)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.TITLE, " bean.getApplyRealName() bean.getProcessName()")
                    .setField(MultipleFields.IMAGE, "bean.getAvatar()")
                    .setField(MultipleFields.STATUS, new StatusEntity("审批中"))
                    .setField(MultipleFields.TEXT, new OrderFormEntity("流程编号：", "NO.12345"))
                    .setField(MultipleFields.TEXT_STATUS, new StatusEntity("流程编号状态"))
                    .setField(MultipleFields.NAME, new OrderFormEntity("申请时间：", "1592.03.24"))
                    .setField(MultipleFields.VALUE, new OrderFormEntity("申请人：", "张三"))
                    .setField(MultipleFields.VALUE_STATUS, new StatusEntity("申请人状态"))
                    .build());

            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.IMAGE_RECORD_NAME4)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.TITLE, " bean.getApplyRealName() bean.getProcessName()")
                    .setField(MultipleFields.IMAGE, "bean.getAvatar()")
                    .setField(MultipleFields.STATUS, new StatusEntity("审批中"))
                    .setField(MultipleFields.TEXT, new OrderFormEntity("流程编号：", "NO.12345"))
                    .setField(MultipleFields.TEXT_STATUS, new StatusEntity("流程编号状态"))
                    .setField(MultipleFields.NAME, new OrderFormEntity("申请时间：", "1592.03.24"))
                    .setField(MultipleFields.VALUE, new OrderFormEntity("申请人：", "张三"))
                    .setField(MultipleFields.TIME, new OrderFormEntity("申请部门：", "财务"))
                    .setField(MultipleFields.TIME_STATUS, new StatusEntity("财务状态"))
                    .build());

        }
        return ENTITIES;
    }
}
