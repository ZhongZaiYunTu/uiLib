package com.zz.yt.test.flow.circle;

import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.data.OrderFormEntity;
import com.zz.yt.lib.ui.recycler.data.StatusEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;

import java.util.ArrayList;

final class CircleConverter extends BaseDataConverter {
    @Override
    public ArrayList<MultipleItemEntity> convert() {
        setDetail("地址", "collection.getInspectAddress()");
        setMultiple("地址", "collection.getInspectAddress()");
        setDetails("地址", "collection.getInspectAddress()");
        final int size = 5;
        for (int i = 0; i < size; i++) {
            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.IMAGE_RECORD_NAME2)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.TITLE, " bean.getApplyRealName() bean.getProcessName()")
                    .setField(MultipleFields.IMAGE, "http://image.biaobaiju.com/uploads/20181007/16/1538902328-aSbPWXFQNh.jpg")
                    .setField(MultipleFields.STATUS, new StatusEntity("审批中"))
                    .setField(MultipleFields.TEXT, new OrderFormEntity("流程编号：", "NO.12345"))
                    .setField(MultipleFields.TEXT_STATUS, new StatusEntity("流程编号状态"))
                    .setField(MultipleFields.NAME, new OrderFormEntity("申请时间：", "1592.03.24"))
                    .build());


        }
        return ENTITIES;
    }
}
