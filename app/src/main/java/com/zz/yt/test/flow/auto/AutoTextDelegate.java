package com.zz.yt.test.flow.auto;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.view.auto.AutoTextView;
import com.zz.yt.test.R;

import java.util.ArrayList;



public class AutoTextDelegate extends LatteTitleDelegate {

    @NonNull
    public static AutoTextDelegate create() {

        Bundle args = new Bundle();

        AutoTextDelegate fragment = new AutoTextDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {

    }

    @Override
    protected Object setLayout() {
        return R.layout.delegate_auto;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState,
                           @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);

        AutoTextView autoTextView = rootView.findViewById(R.id.id_main_switcher);

        // 数据
        ArrayList<String> mStringArray = new ArrayList<>();
        mStringArray.add("137****1111 获得一等奖");
        mStringArray.add("137****2222 获得二等奖");
        mStringArray.add("137****3333 获得三等奖");
        mStringArray.add("137****4444 获得四等奖");
        autoTextView.setResources(mStringArray);


    }
}
