package com.zz.yt.test.activity;

import com.whf.android.jar.base.delegate.BaseDelegate;
import com.zz.yt.lib.ui.base.BaseExampleActivity;
import com.zz.yt.test.flow.index.AiIndexDelegate;



/**
 * 首页
 *
 * @author qf
 */
public class Main1Activity extends BaseExampleActivity  {


    @Override
    public BaseDelegate setRootDelegate() {
        return new AiIndexDelegate();
    }


}
