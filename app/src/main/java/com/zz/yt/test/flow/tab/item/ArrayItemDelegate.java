package com.zz.yt.test.flow.tab.item;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.base.ui.array.LatteItemArrayDelegate;
import com.zz.yt.lib.ui.recycler.adapter.MultipleRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.view.ScreenListView;


public class ArrayItemDelegate extends LatteItemArrayDelegate implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView = null;


    @Override
    protected BaseDataConverter setDataConverter() {
        return new ArrayConverter();
    }

    @Override
    protected void initRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(Latte.getActivity()));
    }

    @Override
    protected void initRefreshHandler(@NonNull SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        swipeRefreshLayout.setEnabled(false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initScreenView(ScreenListView screenView) {
        super.initScreenView(screenView);
        screenView.setVisibility(View.VISIBLE);
        screenView.setData("开始时间");
        screenView.setData("结束时间");
        screenView.setOnClickIntCheckListener((code, check) -> onClickTimeSingle(false,
                (startTime, endTime) -> check.setText(startTime)));
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        onRefresh();
    }

    @Override
    public void onRefresh() {
        recyclerView.setAdapter(MultipleRecyclerAdapter.create(setDataConverter().setJsonData("")));
    }
}
