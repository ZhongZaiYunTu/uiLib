package com.zz.yt.test.flow;


import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.popup.utils.date.TimeDateUtils;
import com.zz.yt.lib.ui.view.TransactionFiguresView;
import com.zz.yt.test.R;

/**
 * 标题
 *
 * @author oa
 */
public class TitleDelegate extends LatteTitleDelegate {

    @Override
    public Object setLayout() {
        return R.layout.activity_title;
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {

    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        TransactionFiguresView transactionFiguresView = findViewById(R.id.id_layout);
        TimeDateUtils.create(transactionFiguresView, transactionFiguresView.getFiguresTitle())
                .setMonth()
                .init(2021 + "");
        setOnClickListener(R.id.id_text, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inFile("http://183.66.214.218:9000/management/1646704832500-" +
                        "42a61ec1bc591b014daee3c33c14fb1c%288258139%29.avi");
//                inFile("http://vjs.zencdn.net/v/oceans.mp4");
            }
        });

    }
}
