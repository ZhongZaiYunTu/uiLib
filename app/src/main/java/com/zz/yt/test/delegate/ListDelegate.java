package com.zz.yt.test.delegate;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.test.R;
import com.zz.yt.test.flow.array.ArrayDelegate;
import com.zz.yt.test.flow.banner.BannerDelegate;
import com.zz.yt.test.flow.circle.CircleDelegate;
import com.zz.yt.test.flow.details.DetailsDelegate;
import com.zz.yt.test.flow.list.ListFragment;
import com.zz.yt.test.flow.tab.ArrayTabDelegate;

import me.yokeyword.fragmentation.ISupportFragment;

/***
 * 列表界面
 *
 * @author qf
 * @version 1.0
 */
public class ListDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {

    private final String[] titles = {
            "罗的列表", "圆形图片",

            "刷新与加载更多",
            "刷新与tab",
            "详情可修改界面",
            "Banner",
    };

    private final ISupportFragment[] delegate = {
            new ListFragment(), new CircleDelegate(),

            new ArrayDelegate(),
            new ArrayTabDelegate(),
            new DetailsDelegate(),
            new BannerDelegate(),
    };

    @NonNull
    public static ListDelegate create() {
        final Bundle args = new Bundle();

        final ListDelegate fragment = new ListDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("列表界面");
    }

    @Override
    public Object setLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        start(delegate[position]);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        ListView mListView = findViewById(R.id.id_list_view);
        if (mListView != null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getProxyActivity(),
                    android.R.layout.simple_list_item_1,
                    titles);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(this);
        }
    }

}
