package com.zz.yt.test.flow;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.util.callback.CallbackManager;
import com.whf.android.jar.util.callback.CallbackType;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.delegate.web.videos.VideosDelegate;
import com.zz.yt.test.R;
import com.zz.yt.test.delegate.ActivityDelegate;
import com.zz.yt.test.delegate.CorpusDelegate;
import com.zz.yt.test.delegate.ListDelegate;
import com.zz.yt.test.delegate.PageDelegate;
import com.zz.yt.test.flow.time.TimeDelegate;


import me.yokeyword.fragmentation.ISupportFragment;

/**
 * 首页
 *
 * @author qf
 * @version 1.0
 */
public class MainDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {

    String url = "https://devmeet.zhongzaiyuntu.com/a124";
//    String url = "https://tongxun.xn--fiq6ix5fi0h.com/zoom/zoom.html?room=21507&username=ad";

    private ListView mListView;
    private final String[] titles = {

            "系统集成弹框", "Activity首页类",
            "系统集成界面", "列表界面",

            "表格",
            "标签控件", "列表按钮控件", "列表开关控件",
            "标题控件", "输入控件", "时间选择",

            "选择", "视频",
    };

    private final ISupportFragment[] delegate = {
            CorpusDelegate.create(), ActivityDelegate.create(),
            PageDelegate.create(), ListDelegate.create(),

            new TableDelegate(),
            new FlowDelegate(), new ButtonListDelegate(), new SwitchListDelegate(),
            new TitleDelegate(), new InputDelegate(), new TimeDelegate(),

            new ChoiceDelegate(), VideosDelegate.create(null, url),
    };


    @Override
    public Object setLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
//        titleBar.setText("sz");
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        mListView = findViewById(R.id.id_list_view);
        CallbackManager.getInstance().addCallback(CallbackType.ON_SCAN, args -> {
            LatteLogger.i("a" + args);
            ToastUtils.showShort("a" + args);
        });
        initView();
    }

    private void initView() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getProxyActivity(),
                android.R.layout.simple_list_item_1,
                titles);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        start(delegate[position]);
    }

}
