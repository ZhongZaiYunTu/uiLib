package com.zz.yt.test.app;

import android.app.Application;

import com.blankj.utilcode.util.BarUtils;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.HttpCode;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.test.BuildConfig;

/**
 * @author qf
 */
public class TestApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        //全局初始化
        Latte.init(this)
                .withLoaderDelayed(1000)
                .withAdaptation(360f)
                .withFtpHost("https://wwww.baidu.com")
                .withWebHost("https://wwww.baidu.com")
                .withApiHost("https://wwww.baidu.com")
                .withAppId("3b1dffaec121205d3b50b86170813159")
                .withApplicationId(BuildConfig.APPLICATION_ID)
                .withJavascriptInterface("http://www.baidu.com")
                .withVersionCode(BuildConfig.VERSION_CODE)
                .withVersionName(BuildConfig.VERSION_NAME)
                .withSignExpiration(HttpCode.CODE_403)
                .withLoggable(BuildConfig.DEBUG)
                .withHttpLogging()
                .configure();
    }

    public static void initApp() {
        LatteLogger.i("高度" + BarUtils.getStatusBarHeight());
        ZXingLibrary.initDisplayOpinion(Latte.getApplicationContext());
    }


}

