package com.zz.yt.test.flow.details;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.GsonUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.array.LatteDetailsArrayDelegate;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.recycler.adapter.MultipleRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;

/**
 * 详情可修改界面
 *
 * @author qf
 * @version 1.0.2
 */
public class DetailsDelegate extends LatteDetailsArrayDelegate implements OnClickIntObjListener {

    @Override
    protected BaseDataConverter setDataConverter() {
        return new DetailsConverter();
    }

    @Override
    protected BaseDataConverter setData2Converter() {
        return null;
    }

    @Override
    protected void initRecycler2View(@NonNull RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(Latte.getActivity()));
        MultipleRecyclerAdapter adapter = MultipleRecyclerAdapter.create(setDataConverter().setJsonData(""));
        adapter.setOnClickIntObjListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void setSearchView(@NonNull SearchView search) {
        super.setSearchView(search);
        search.setVisibility(View.VISIBLE);
    }

    @Override
    protected void initRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(Latte.getActivity()));
        MultipleRecyclerAdapter adapter = MultipleRecyclerAdapter.create(setDataConverter().setJsonData(""));
        adapter.setOnClickIntObjListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void initRefreshHandler(@NonNull SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {

    }

    @Override
    public void onClick(int code, Object index) {
        LatteLogger.json("code" + code, GsonUtils.toJson(index));
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);

    }
}
