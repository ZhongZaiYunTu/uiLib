package com.zz.yt.test.delegate;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.test.R;
import com.zz.yt.test.activity.Main1Activity;
import com.zz.yt.test.activity.Main2Activity;

/***
 * Activity首页类
 * @author qf
 * @version 1.0
 */
public class ActivityDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {

    private final String[] titles = {
            "首页类A", "首页类B"
    };

    @NonNull
    public static ActivityDelegate create() {
        final Bundle args = new Bundle();

        final ActivityDelegate fragment = new ActivityDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
         if ("首页类A".equals(titles[position])) {
            ActivityUtils.startActivity(Main1Activity.class);
        } else if ("首页类B".equals(titles[position])) {
             ActivityUtils.startActivity(Main2Activity.class);
        }
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("Activity首页类");
    }

    @Override
    public Object setLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        ListView mListView = findViewById(R.id.id_list_view);
        if (mListView != null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getProxyActivity(),
                    android.R.layout.simple_list_item_1,
                    titles);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(this);
        }
    }
}
