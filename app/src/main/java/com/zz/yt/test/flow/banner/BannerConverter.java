package com.zz.yt.test.flow.banner;

import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.data.OrderFormEntity;
import com.zz.yt.lib.ui.recycler.data.StatusEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;

import java.util.ArrayList;

final class BannerConverter extends BaseDataConverter {
    @Override
    public ArrayList<MultipleItemEntity> convert() {
        ArrayList<String> objectList = new ArrayList<>();
        objectList.add("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fyouimg1.c-ctrip.com%2Ftarget%2Ftg%2F004%2F531%2F381%2F4339f96900344574a0c8ca272a7b8f27.jpg&refer=http%3A%2F%2Fyouimg1.c-ctrip.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1626510456&t=c8056d5d0649d2f62b7bfc3c8cf6789d");
        objectList.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598592802972&di=3987ba74ffdddb1274f16b9bc4168b5e&imgtype=0&src=http%3A%2F%2Fa3.att.hudong.com%2F14%2F75%2F01300000164186121366756803686.jpg");
        objectList.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598592802971&di=a704829619c87cbbe6934dd21b5e9186&imgtype=0&src=http%3A%2F%2Fa0.att.hudong.com%2F56%2F12%2F01300000164151121576126282411.jpg");
        objectList.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598593430218&di=954462a126c0d347f6e3d865fdddb38f&imgtype=0&src=http%3A%2F%2Fd.paper.i4.cn%2Fmax%2F2016%2F12%2F01%2F10%2F1480558471363_838559.jpg");

        ArrayList<String> objectTitle = new ArrayList<>();
        objectTitle.add(null);
        objectTitle.add("2");
        objectTitle.add("3");
        objectTitle.add("4");
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.BANNER)
                .setField(MultipleFields.SPAN_SIZE, 2)
                .setField(MultipleFields.CLICK, -1)
                .setField(MultipleFields.TITLE_ARRAY, objectTitle)
                .setField(MultipleFields.BANNERS, objectList)
                .build());

        setDetail("地址", "collection.getInspectAddress()");
        setMultiple("地址", "collection.getInspectAddress()");
        setDetails("地址", "collection.getInspectAddress()");
        final int size = 5;
        for (int i = 0; i < size; i++) {
            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.IMAGE_RECORD_NAME2)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.TITLE, " bean.getApplyRealName() bean.getProcessName()")
                    .setField(MultipleFields.IMAGE, "http://image.biaobaiju.com/uploads/20181007/16/1538902328-aSbPWXFQNh.jpg")
                    .setField(MultipleFields.STATUS, new StatusEntity("审批中"))
                    .setField(MultipleFields.TEXT, new OrderFormEntity("流程编号：", "NO.12345"))
                    .setField(MultipleFields.TEXT_STATUS, new StatusEntity("流程编号状态"))
                    .setField(MultipleFields.NAME, new OrderFormEntity("申请时间：", "1592.03.24"))
                    .build());


        }
        return ENTITIES;
    }
}
