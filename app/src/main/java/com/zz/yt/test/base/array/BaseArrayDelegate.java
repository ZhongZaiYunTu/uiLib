package com.zz.yt.test.base.array;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zz.yt.lib.ui.base.ui.array.LatteArrayDelegate;
import com.zz.yt.test.base.RefreshHandler;

/**
 * 纯列表类处理
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/4/24
 **/
public abstract class BaseArrayDelegate extends LatteArrayDelegate {

    protected RefreshHandler mRefreshHandler = null;

    /**
     * 设置加载
     *
     * @param refreshHandler:数据处理
     */
    protected abstract void setRefreshHandler(RefreshHandler refreshHandler);

    @Override
    protected void initRefreshHandler(SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {
        mRefreshHandler = RefreshHandler.create(getParentDelegate(), swipeRefreshLayout, recyclerView, setDataConverter());
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        setRefreshHandler(mRefreshHandler);
    }

    @Override
    public void responseSubmit() {
        mRefreshHandler.onRefresh();
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode) {
            mRefreshHandler.onRefresh();
        }
    }

}
