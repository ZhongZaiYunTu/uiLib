package com.zz.yt.test.flow.index;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.BarUtils;
import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;
import com.zz.yt.test.R;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 **/
public class BDelegate extends BaseBottomItemDelegate {

    @Override
    public Object setLayout() {
        return R.layout.activity_title;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        Button button = findViewById(R.id.button);
        if (button != null) {
            BarUtils.addMarginTopEqualStatusBarHeight(button);
        }
    }
}
