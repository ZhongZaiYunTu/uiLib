package com.zz.yt.test.flow.tab;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;

import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.base.builder.ItemTabBuilder;
import com.zz.yt.lib.ui.base.ui.tab.LatteTopTabDelegate;
import com.zz.yt.lib.ui.recycler.data.TabEntity;
import com.zz.yt.test.flow.tab.item.ArrayItemDelegate;
import com.zz.yt.test.flow.tab.item.ArrayTab2Delegate;


import java.util.LinkedHashMap;

public class ArrayTabDelegate extends LatteTopTabDelegate {


    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.getTitleBarTitle().setText("刷新与加载更多");
    }

    @Override
    protected void setSearchView(@NonNull SearchView search) {
        super.setSearchView(search);
        search.setVisibility(View.VISIBLE);
    }

    @Override
    public LinkedHashMap<TabEntity, LatteDelegate> setItems(@NonNull ItemTabBuilder builder) {
        return builder
                .addItem(new TabEntity("s"), new ArrayItemDelegate())
                .addItem(new TabEntity("s"), new ArrayTab2Delegate())
                .build();
    }

}
