package com.zz.yt.test.flow;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whf.android.jar.base.delegate.BaseDelegate;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.entity.SelectEntry;
import com.zz.yt.lib.ui.view.SelectArrayView;
import com.zz.yt.test.R;

import java.util.ArrayList;
import java.util.List;

public class ChoiceDelegate extends LatteTitleDelegate {
    @Override
    protected Object setLayout() {
        return R.layout.delegate_choice;
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {

    }

//    @Override
//    protected void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
//        SelectArrayView arrayView = findViewById(R.id.id_sav);
//        List<SelectEntry> selectBeanList = new ArrayList<>();
//        selectBeanList.add(new SelectEntry("123"));
//        selectBeanList.add(new SelectEntry("123"));
//        selectBeanList.add(new SelectEntry("123"));
//        selectBeanList.add(new SelectEntry("123"));
//        arrayView.setData(selectBeanList);
//
//        SelectArrayView arrayView2 = findViewById(R.id.id_sav_b);
//        List<SelectEntry> selectBeanList2 = new ArrayList<>();
//        selectBeanList2.add(new SelectEntry("123"));
//        selectBeanList2.add(new SelectEntry("123"));
//        selectBeanList2.add(new SelectEntry("123"));
//        selectBeanList2.add(new SelectEntry("123"));
//        arrayView2.setData(selectBeanList2);
//        arrayView2.setEnabled(false);
//    }
}
