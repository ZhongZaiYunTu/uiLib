package com.zz.yt.test.flow.list;

import androidx.annotation.NonNull;

import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.OrderFormEntity;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;
import com.zz.yt.test.R;

import java.util.ArrayList;
import java.util.Map;

public class ListAdapter extends BaseMvpRecyclerAdapter<String> {

    public ListAdapter() {
        super(R.layout.hai_item_multiple_record_2, new ArrayList<>());
    }

    @Override
    protected void convert(@NonNull MultipleViewHolder holder, @NonNull String entity) {
        super.convert(holder, entity);
        holder.setImageResource(R.id.image_icon, R.mipmap.iv_default_head_image);
        holder.setText(R.id.text_title, "entity, MultipleFields.TITLE" + holder.position())
                .setOrderForm(R.id.text_text, new OrderFormEntity("sss", "11111"))
                .setOrderForm(R.id.text_name, new OrderFormEntity("dddd", "2222***" + holder.position()));
    }
}
