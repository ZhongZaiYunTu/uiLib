package com.zz.yt.test.activity;

import android.os.Bundle;
import android.view.View;


import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.base.delegate.BaseDelegate;
import com.zz.yt.lib.ui.base.BaseExampleActivity;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.popup.view.TipsPopup;
import com.zz.yt.test.app.TestApp;
import com.zz.yt.test.flow.MainDelegate;

import java.util.WeakHashMap;


/**
 * 首页
 *
 * @author qf
 */
public class MainActivity extends BaseExampleActivity implements View.OnClickListener {

    private final String url = "01";
    private final String url2 = "02";

    @Override
    public BaseDelegate setRootDelegate() {
        return new MainDelegate();
    }

    @Override
    public void onClick(View v) {
        TipsPopup.create()
                .setTitle("是否允许安装应用？")
                .setDate("设备和个人信息数据容易受到未知来源应用的攻击，点击“允许”表示您同意承担由此带来的风险。")
                .setText("允许")
                .setOnClickListener(new OnClickDecisionListener() {
                    @Override
                    public void onConfirm(Object decision) {
                        ToastUtils.showShort("ok");
                        WeakHashMap<String, Object> params = new WeakHashMap<>();
                        loginStart(url, params);
                    }

                    @Override
                    public void onCancel() {
                        ToastUtils.showShort("no");
                        WeakHashMap<String, Object> params = new WeakHashMap<>();
                        loginStart(url2, params);
                    }
                })
                .popup();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestApp.initApp();
    }
}
