package com.zz.yt.test.flow.banner;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.listener.OnClickBannerListener;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.test.base.RefreshHandler;
import com.zz.yt.test.base.array.BaseArrayDelegate;


public class BannerDelegate extends BaseArrayDelegate implements OnClickBannerListener {


    @Override
    protected BaseDataConverter setDataConverter() {
        return new BannerConverter();
    }

    @Override
    protected void initRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new GridLayoutManager(Latte.getActivity(), 2));

    }

    @Override
    protected void setRefreshHandler(@NonNull RefreshHandler refreshHandler) {
        refreshHandler.setOnBannerListener(this);
        refreshHandler.firstPage("");
    }

    @Override
    protected void initRefreshHandler(SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {
        super.initRefreshHandler(swipeRefreshLayout, recyclerView);
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("");
    }


    @Override
    public void OnBannerClick(int position) {
        ToastUtils.showShort("" + position);
    }
}
