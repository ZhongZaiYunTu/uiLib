package com.zz.yt.test.base;



import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.delegate.record.BaseLatteRefreshHandler;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.test.adapter.ExampleAdapter;

import java.util.WeakHashMap;


/**
 * @author qf
 * @version 1.0
 */
public class RefreshHandler extends BaseLatteRefreshHandler<ExampleAdapter> {


    private RefreshHandler(LatteDelegate delegate,
                           SwipeRefreshLayout swipeRefreshLayout,
                           RecyclerView recyclerView,
                           BaseDataConverter converter) {
        super(delegate, swipeRefreshLayout, recyclerView, converter);
    }

    public static RefreshHandler create(LatteDelegate delegate,
                                        SwipeRefreshLayout swipeRefreshLayout,
                                        RecyclerView recyclerView,
                                        BaseDataConverter converter) {
        return new RefreshHandler(delegate, swipeRefreshLayout, recyclerView, converter);
    }

    public void firstPage(String url) {
        super.firstPage(url);
    }

    public void firstPage(String url, WeakHashMap<String, Object> params) {
        super.firstPage(url, params);
    }

    @Override
    public WeakHashMap<String, Object> params() {
        return super.params();
    }

    @Override
    protected void refresh(String url, WeakHashMap<String, Object> params, boolean refresh) {
        super.refresh(url, params, refresh);
    }

    @Override
    protected ExampleAdapter getNewAdapter() {
        return ExampleAdapter.create();
    }


}

