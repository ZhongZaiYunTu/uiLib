package com.zz.yt.test.flow.index;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ColorUtils;
import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;
import com.zz.yt.lib.ui.base.bottom.tab.BaseBottomTabDelegate;
import com.zz.yt.lib.ui.base.builder.HomeTabBuilder;
import com.zz.yt.lib.ui.recycler.data.TabEntity;
import com.zz.yt.test.R;


import java.util.LinkedHashMap;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 **/
public class AiIndexDelegate extends BaseBottomTabDelegate {

    @Override
    public LinkedHashMap<TabEntity, BaseBottomItemDelegate> setItems(@NonNull HomeTabBuilder builder) {
        return builder.addItem(new TabEntity("首页"), new ADelegate())
                .addItem(new TabEntity("首页"), new BDelegate())
                .addItem(new TabEntity("我的"), new ADelegate())
                .build();
    }

    @Override
    public int setIndexDelegate() {
        return 0;
    }

    @Override
    public int setClickedColor() {
        return ColorUtils.getColor(R.color.colorAccent);
    }
}
