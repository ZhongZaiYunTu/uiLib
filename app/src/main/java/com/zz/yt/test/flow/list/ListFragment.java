package com.zz.yt.test.flow.list;

import android.view.View;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.whf.android.jar.constants.Status;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.fragment.LatteListFragment;

import java.util.WeakHashMap;


public class ListFragment extends LatteListFragment<ListAdapter, String> {

    @Override
    protected ListAdapter getAdapter() {
        return new ListAdapter();
    }

    @Override
    public void firstPage() {
        firstPage("");
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("罗的适配器");
    }

    @Override
    public void firstPage(String url, WeakHashMap<String, Object> params) {
        close();
        if (mStatus == Status.REFRESH) {
            mAdapter.setList(data());
        } else {
            mAdapter.addData(data());
        }
    }


    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

    }
}
