package com.zz.yt.test.flow;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.listener.OnClickTableListener;
import com.zz.yt.lib.ui.table.TableView;
import com.zz.yt.lib.ui.table.entity.TableEntity;
import com.zz.yt.test.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/8/5
 **/
public class TableDelegate extends LatteDelegate implements OnClickTableListener {

    @Override
    public Object setLayout() {
        return R.layout.hai_item_multiple_tabel;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        final TableView tabLayout = rootView.findViewById(R.id.tv_table_view);
        tabLayout.setWeightTotal(6);
        tabLayout.setTableAdapter(additive(0, true));
        tabLayout.addData(additive(1, false));
        tabLayout.addData(additive(2, false));
        tabLayout.setOnClickTableListener(this);
    }

    private List<TableEntity> additive(int index, boolean title) {
        boolean isColor = index % 2 == 0;
        final int color = isColor ? ColorUtils.getColor(R.color.white_smoke) : ColorUtils.getColor(R.color.white);
        final List<TableEntity> entityList = new ArrayList<>();
        entityList.add(new TableEntity(title ? "序号" : "" + index, 1, color));
        entityList.add(new TableEntity(title ? "添加剂" : "添加剂" + index, 2, color).setOther(12));
        entityList.add(new TableEntity(title ? "重量" : "152.21", 1, color));
        entityList.add(new TableEntity(title ? "单位" : "公斤", 1, color));
        entityList.add(new TableEntity(title ? "操作" : "X", 1, color));
        return entityList;
    }

    @Override
    public void onClickTable(int position, int column, @NonNull List<TableEntity> text) {
        final boolean is = position % column == 4;
        if (is) {
            ToastUtils.showShort(text.get(position).getName());
        } else {
            String title = text.get(position % column).getName();
            ToastUtils.showShort(title + text.get(position).getName());
        }
    }
}