# uiLib 版本
[![](https://jitpack.io/v/com.gitee.ZhongZaiYunTu/uiLib.svg)](https://jitpack.io/#com.gitee.ZhongZaiYunTu/uiLib)

#### 介绍
界面自定义ui控件


#### 使用说明

1.  使用

            public class MainDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {
    
                String url = "https://devmeet.zhongzaiyuntu.com/a124";
            //    String url = "https://tongxun.xn--fiq6ix5fi0h.com/zoom/zoom.html?room=21507&username=ad";
            
                private ListView mListView;
                private final String[] titles = {
            
                        "系统集成弹框", "Activity首页类",
                        "系统集成界面", "列表界面",
            
                        "表格",
                        "标签控件", "列表按钮控件", "列表开关控件",
                        "标题控件", "输入控件", "时间选择",
            
                        "选择", "视频",
                };
            
                private final ISupportFragment[] delegate = {
                        CorpusDelegate.create(), ActivityDelegate.create(),
                        PageDelegate.create(), ListDelegate.create(),
            
                        new TableDelegate(),
                        new FlowDelegate(), new ButtonListDelegate(), new SwitchListDelegate(),
                        new TitleDelegate(), new InputDelegate(), new TimeDelegate(),
            
                        new ChoiceDelegate(), VideosDelegate.create(null, url),
                };
            
            
                @Override
                public Object setLayout() {
                    return R.layout.activity_main;
                }
            
                @Override
                protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
            //        titleBar.setText("sz");
                }
            
                @Override
                public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
                    super.onBindView(savedInstanceState, rootView);
                    mListView = findViewById(R.id.id_list_view);
                    CallbackManager.getInstance().addCallback(CallbackType.ON_SCAN, args -> {
                        LatteLogger.i("a" + args);
                        ToastUtils.showShort("a" + args);
                    });
                    initView();
                }
            
                private void initView() {
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(
                            getProxyActivity(),
                            android.R.layout.simple_list_item_1,
                            titles);
                    mListView.setAdapter(adapter);
                    mListView.setOnItemClickListener(this);
                }
            
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    start(delegate[position]);
                }
            
            }
2.  系统集成弹框

            public class CorpusDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {
            
                private final String[] titles = {
                        "扫二维码", "扫二维码2", "附件选择",
                        "车牌输入", "定位", "通用弹框",
                        "选择时间", "选择日期", "选择城市",
                        "省市区选择",
                };
            
                @NonNull
                public static CorpusDelegate create() {
                    final Bundle args = new Bundle();
            
                    final CorpusDelegate fragment = new CorpusDelegate();
                    fragment.setArguments(args);
                    return fragment;
                }
            
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if ("扫二维码".equals(titles[position])) {
                        startScanWithCheck(this);
                    } else if ("扫二维码2".equals(titles[position])) {
                        startScanWithCheck();
                    } else if ("附件选择".equals(titles[position])) {
                        startFilePickerWithCheck(this);
                    } else if ("车牌输入".equals(titles[position])) {
                        VehiclePopup.create(getActivity(), null);
                    } else if ("定位".equals(titles[position])) {
                        starClickWithCheck(index -> {
                            LatteLogger.i("dw");
                            ToastUtils.showShort("d定位权限成功");
                        });
                    } else if ("通用弹框".equals(titles[position])) {
                        List<Bean> a = new ArrayList<>();
                        a.add(new Bean("选项一"));
                        a.add(new Bean("选项二"));
                        a.add(new Bean("选项三"));
                        PickerPopupUtils.showPickerData(context, "测试弹框", a, 0, null);
                    } else if ("选择时间".equals(titles[position])) {
                        onClickTime(null);
                    } else if ("选择日期".equals(titles[position])) {
                        onClickTimeSingle(null);
                    } else if ("选择城市".equals(titles[position])) {
                        onClickCity(null);
                    } else if ("省市区选择".equals(titles[position])) {
                        onClickAreas(null);
                    }
                }
            
                @Override
                protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
                    titleBar.setText("系统集成弹框");
                }
            
                @Override
                public Object setLayout() {
                    return R.layout.activity_main;
                }
            
                @Override
                public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
                    super.onBindView(savedInstanceState, rootView);
                    ListView mListView = findViewById(R.id.id_list_view);
                    if (mListView != null) {
            
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                                getProxyActivity(),
                                android.R.layout.simple_list_item_1,
                                titles);
                        mListView.setAdapter(adapter);
                        mListView.setOnItemClickListener(this);
                    }
                }
            
                @Override
                public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
                    super.onActivityResult(requestCode, resultCode, data);
                    setScanResult(requestCode, data, result -> ToastUtils.showShort("解析结果:" + result));
                }
            
            
                private static class Bean implements IPickerViewData {
            
                    final String name;
            
                    public Bean(String name) {
                        this.name = name;
                    }
            
                    @Override
                    public String getPickerViewText() {
                        return name;
                    }
                }
            
            }

3.  系统集成界面

            public class PageDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {
            
                private final String[] titles = {
                        "密码管理", "意见反馈", "搜索控件",
                        "身份证界面", "扫码界面",
                         "版本信息", "电子签名", "自动翻页"
                };
            
                private final ISupportFragment[] delegate = {
                        new CipherDelegate(), new FeedbackDelegate(), new SearchDelegate(),
                        new AuthenticationDelegate(), new ScannerDelegate(),
                        new EditionDelegate(),
                        new SignatureDelegate(), AutoTextDelegate.create()
                };
            
                @NonNull
                public static PageDelegate create() {
                    final Bundle args = new Bundle();
            
                    final PageDelegate fragment = new PageDelegate();
                    fragment.setArguments(args);
                    return fragment;
                }
            
                @Override
                protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
                    titleBar.setText("系统集成界面");
                }
            
                @Override
                public Object setLayout() {
                    return R.layout.activity_main;
                }
            
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    start(delegate[position]);
                }
            
                @Override
                public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
                    super.onBindView(savedInstanceState, rootView);
                    ListView mListView = findViewById(R.id.id_list_view);
                    if (mListView != null) {
            
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                                getProxyActivity(),
                                android.R.layout.simple_list_item_1,
                                titles);
                        mListView.setAdapter(adapter);
                        mListView.setOnItemClickListener(this);
                    }
                }
            
            }

#### 参与贡献

