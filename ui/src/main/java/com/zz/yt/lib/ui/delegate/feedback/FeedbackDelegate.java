package com.zz.yt.lib.ui.delegate.feedback;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.picture.PictureView;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;

import java.util.List;


/**
 * 意见反馈
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/4
 **/
public class FeedbackDelegate extends LatteTitleDelegate implements OnClickPictureCallback,
        RadioGroup.OnCheckedChangeListener {

    public int mRadio = -1;
    private RadioGroup mRadioGroup = null;
    private EditText mProblem = null;
    private PictureView mPictureView = null;
    private Button mButtonSubmit = null;
    private List<String> selectList = null;

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_feedback;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("意见反馈");
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        mRadioGroup = rootView.findViewById(R.id.rg_opinion);
        mProblem = rootView.findViewById(R.id.ed_problem);
        mPictureView = rootView.findViewById(R.id.rc_icons);
        mButtonSubmit = rootView.findViewById(R.id.id_btn_submit);
        initView();
    }

    private void initView() {
        mRadioGroup.setOnCheckedChangeListener(this);
        mPictureView.setClickPictureCallback(this);
        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProblem != null) {
                    final String problem = mProblem.toString();
                    if (TextUtils.isEmpty(problem)) {
                        ToastUtils.showShort(mProblem.getHint());
                        return;
                    }
                    if (mRadio < 1) {
                        ToastUtils.showShort("请选择反馈问题点");
                        return;
                    }
                    itSubmit(problem, selectList);
                }
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rb_type_one) {
            mRadio = 1;
        } else if (checkedId == R.id.rb_type_two) {
            mRadio = 2;
        } else if (checkedId == R.id.rb_type_three) {
            mRadio = 3;
        }
    }

    protected void itSubmit(String problem, List<String> selectList) {
        LatteLogger.json(problem, GsonUtils.toJson(selectList));
    }

    @Override
    public void onCallback(List<String> selectList) {
        this.selectList = selectList;
    }

    @Override
    public void remove(int position) {
        if (selectList != null && selectList.size() > position) {
            selectList.remove(position);
        }
    }
}
