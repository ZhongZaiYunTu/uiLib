package com.zz.yt.lib.ui.popup.view.base;


import android.content.Context;

import androidx.annotation.NonNull;

import com.whf.android.jar.popup.LattePopupCenter;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.listener.OnClickStringsListener;


/**
 * 弹框基层
 *
 * @author qf
 * @version 1.0.3
 **/
public abstract class BaseCenterPopup extends LattePopupCenter {

    protected OnClickIntObjListener mClickIntObjListener = null;
    protected OnClickStringListener mClickStringListener = null;
    protected OnClickStringsListener mClickStringsListener = null;

    public BaseCenterPopup(@NonNull Context context) {
        super(context);
    }

    /**
     * @param listener：添加事件
     */
    public BaseCenterPopup setOnClickIntObjListener(OnClickIntObjListener listener) {
        this.mClickIntObjListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseCenterPopup setOnClickListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseCenterPopup setOnClickListener(OnClickStringsListener listener) {
        this.mClickStringsListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseCenterPopup setOnClickListener(OnClickDecisionListener listener) {
        this.mClickDecisionListener = listener;
        return this;
    }


}
