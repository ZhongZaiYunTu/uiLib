package com.zz.yt.lib.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickIntCheckListener;
import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.adapter.MultipleRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;

import java.util.ArrayList;
import java.util.List;

public class ScreenListView extends RelativeLayout implements OnClickIntCheckListener {

    private final RecyclerView screenLeft;
    private final MultipleRecyclerAdapter mAdapter;

    private OnClickIntCheckListener mClickIntCheckListener;

    public ScreenListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_view_screen_list, this, true);
        screenLeft = findViewById(R.id.screen_left);
        //设置布局管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        screenLeft.setLayoutManager(linearLayoutManager);
        //设置适配器
        mAdapter = MultipleRecyclerAdapter.create(new ArrayList<MultipleItemEntity>());
        mAdapter.setOnClickIntCheckListener(this);
        screenLeft.setAdapter(mAdapter);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ScreenListView);

        attributes.recycle();
    }


    public ScreenListView setOnClickIntCheckListener(OnClickIntCheckListener listener) {
        this.mClickIntCheckListener = listener;
        return this;
    }

    public RecyclerView getScreenList() {
        return screenLeft;
    }

    public void setData(String text) {
        if (mAdapter != null) {
            mAdapter.addData(MultipleItemEntity.builder()
                    .setItemType(ItemType.SCREEN)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.TEXT, text)
                    .build());
        }
    }

    public void setData(List<String> text) {
        if (text != null) {
            for (int i = 0; i < text.size(); i++) {
                String te = text.get(i);
                setData(te);
            }
        }
    }

    public MultipleRecyclerAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void onClick(int code, CheckBox check) {
        if (mClickIntCheckListener != null) {
            mClickIntCheckListener.onClick(code, check);
        }
//        PickerPopupUtils.showPickerData(mContext, title, Items, options, (code1, index) -> {
//
//        });
    }
}
