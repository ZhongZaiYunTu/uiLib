package com.zz.yt.lib.ui.delegate.release;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;

/**
 * 空白界面
 *
 * @author qf
 * @version 1.0
 **/
public class WhiteDelegate extends LatteDelegate {

    @NonNull
    public static WhiteDelegate create() {
        Bundle args = new Bundle();
        WhiteDelegate fragment = new WhiteDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_delegate_ui_null;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState,
                           @NonNull View rootView) {


    }
}
