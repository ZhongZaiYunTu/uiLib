package com.zz.yt.lib.ui.base.ui.tab;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.base.builder.ItemTabBuilder;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.recycler.data.TabEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import me.yokeyword.fragmentation.ISupportFragment;

/**
 * Created by 傅令杰
 *
 * @author 傅令杰
 */

public abstract class LatteBottomTabDelegate extends LatteTitleDelegate implements OnTabSelectListener {

    private final ArrayList<CustomTabEntity> TAB_BEANS = new ArrayList<>();
    private final ArrayList<LatteDelegate> ITEM_DELEGATES = new ArrayList<>();
    private final LinkedHashMap<CustomTabEntity, LatteDelegate> ITEMS = new LinkedHashMap<>();
    private int mCurrentDelegate = 0;
    private int mIndexDelegate = 0;
    private int mClickedColor = Color.RED;

    protected CommonTabLayout mCommonTabBar = null;

    /**
     * 初始化界面
     *
     * @param builder:
     * @return 界面
     */
    public abstract LinkedHashMap<TabEntity, LatteDelegate> setItems(ItemTabBuilder builder);

    public ArrayList<LatteDelegate> getItemDelegates() {
        return ITEM_DELEGATES;
    }

    @Override
    public Object setLayout() {
        return R.layout.a_ui_base_latte_botton_layout;
    }

    /**
     * 第一次默认显示的 tab
     *
     * @return 0
     */
    public abstract int setIndexDelegate();

    /**
     * 选项的颜色
     *
     * @return ss
     */
    @ColorInt
    public abstract int setClickedColor();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIndexDelegate = setIndexDelegate();
        if (setClickedColor() != 0) {
            mClickedColor = setClickedColor();
        }

        final ItemTabBuilder builder = ItemTabBuilder.builder();
        final LinkedHashMap<TabEntity, LatteDelegate> items = setItems(builder);
        ITEMS.putAll(items);
        for (Map.Entry<CustomTabEntity, LatteDelegate> item : ITEMS.entrySet()) {
            final CustomTabEntity key = item.getKey();
            final LatteDelegate value = item.getValue();
            TAB_BEANS.add(key);
            ITEM_DELEGATES.add(value);
        }
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        final LinearLayoutCompat bottomBar = findViewById(R.id.bottom_bar);
        inBottomBar(bottomBar);
        LayoutInflater.from(getContext()).inflate(R.layout.bottom_item_common_tab_layout, bottomBar);
        mCommonTabBar = bottomBar.findViewById(R.id.common_tab_bar);
        if (mCommonTabBar != null) {
            mCommonTabBar.setTabData(TAB_BEANS);
            mCommonTabBar.setOnTabSelectListener(this);
            mCommonTabBar.setCurrentTab(mIndexDelegate);
            mCommonTabBar.setTextSelectColor(mClickedColor);
            initCommonTabBar(mCommonTabBar);
        }
        final int size = ITEMS.size();
        final ISupportFragment[] delegateArray = ITEM_DELEGATES.toArray(new ISupportFragment[size]);
        getSupportDelegate().loadMultipleRootFragment(R.id.bottom_bar_delegate_container, mIndexDelegate, delegateArray);
        mCurrentDelegate = mIndexDelegate;
    }

    public void initCommonTabBar(CommonTabLayout commonTabBar) {

    }

    protected void inBottomBar(LinearLayoutCompat bottomBar) {

    }

    @Override
    public void onTabSelect(int position) {
        if (onSelect(position)) {
            return;
        }
        mCommonTabBar.setCurrentTab(position);
        getSupportDelegate().showHideFragment(ITEM_DELEGATES.get(position), ITEM_DELEGATES.get(mCurrentDelegate));
        //注意先后顺序
        mCurrentDelegate = position;
    }

    public int getCurrentDelegate() {
        return mCurrentDelegate;
    }

    @Override
    public void onTabReselect(int position) {

    }

    /**
     * 是否有凸出按钮
     *
     * @param position：
     * @return true
     */
    protected boolean onSelect(int position) {
        return false;
    }
}
