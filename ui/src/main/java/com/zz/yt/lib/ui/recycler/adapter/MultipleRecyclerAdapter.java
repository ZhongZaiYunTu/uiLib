package com.zz.yt.lib.ui.recycler.adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;

import com.blankj.utilcode.util.ColorUtils;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.GridSpanSizeLookup;
import com.chad.library.adapter.base.module.BaseLoadMoreModule;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.number.DoubleUtils;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import com.zz.yt.lib.annex.document.DocumentView;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.picture.PictureShowView;
import com.zz.yt.lib.ui.ButtonListLayout;
import com.zz.yt.lib.ui.R;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.listener.OnClickIntBoolListener;
import com.zz.yt.lib.ui.listener.OnClickIntCheckListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickStrObjListener;
import com.zz.yt.lib.ui.listener.OnClickTableListener;
import com.zz.yt.lib.ui.listener.OnClickIntListener;
import com.zz.yt.lib.ui.listener.OnClickIntStrListener;
import com.zz.yt.lib.ui.listener.OnClickObjectListener;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.banner.BannerCreator;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.ButtonListEntity;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.data.OrderFormEntity;
import com.zz.yt.lib.ui.recycler.data.TabEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;
import com.zz.yt.lib.ui.table.TableView;
import com.zz.yt.lib.ui.table.entity.TableEntity;


import java.util.ArrayList;
import java.util.List;

/**
 * 多类型布局
 *
 * @author qf
 * @version 1.0
 */
public class MultipleRecyclerAdapter extends BaseMultiItemQuickAdapter<MultipleItemEntity, MultipleViewHolder>
        implements ItemType, GridSpanSizeLookup, IAdapterClickListener, LoadMoreModule {

    /**
     * 确保初始化一次Banner，防止重复Item加载
     */
    private boolean mIsInitBanner = false;
    protected OnBannerListener mBannerListener = null;
    protected OnTabSelectListener mTabSelectListener = null;
    protected OnClickIntListener mClickIntListener = null;
    protected OnClickIntBoolListener mClickIntBoolListener = null;
    protected OnClickIntObjListener mClickIntObjListener = null;
    protected OnClickIntStrListener mClickIntStrListener = null;
    protected OnClickObjectListener mClickObjectListener = null;
    protected OnClickStringListener mClickStringListener = null;
    protected OnClickStrObjListener mClickStrObjListener = null;
    protected OnClickTableListener mClickTableListener = null;
    protected OnClickIntCheckListener mClickIntCheckListener = null;
    protected OnClickDecisionListener mClickDecisionListener = null;

    protected MultipleRecyclerAdapter(List<MultipleItemEntity> data) {
        super(data);
        init();
        //宽度监听
        setGridSpanSizeLookup(this);
        //设置动画
        setAnimationEnable(true);
    }

    @NonNull
    public static MultipleRecyclerAdapter create() {
        return new MultipleRecyclerAdapter(new ArrayList<MultipleItemEntity>());
    }

    @NonNull
    public static MultipleRecyclerAdapter create(List<MultipleItemEntity> data) {
        return new MultipleRecyclerAdapter(data);
    }

    @NonNull
    public static MultipleRecyclerAdapter create(@NonNull BaseDataConverter converter) {
        return new MultipleRecyclerAdapter(converter.convert());
    }

    /*** 设置不同的item布局 */
    protected void init() {
        //设置不同的item布局
        addItemType(TEXT, R.layout.hai_item_multiple_text);
        addItemType(LABEL, R.layout.hai_item_multiple_label);
        addItemType(TEXT_TEXT, R.layout.hai_item_multiple_text);
        addItemType(TEXT_MORE, R.layout.hai_item_multiple_text_more);
        addItemType(TEXT_TEXTS, R.layout.hai_item_multiple_texts);
        addItemType(TEXT_MULTIPLE, R.layout.hai_item_multiple_text_text);
        addItemType(TITLE, R.layout.hai_item_multiple_title);
        addItemType(BRANCH, R.layout.hai_item_multiple_branch);
        addItemType(ICON, R.layout.hai_item_multiple_image);
        addItemType(IMAGE, R.layout.hai_item_multiple_image);
        addItemType(BANNER, R.layout.hai_item_multiple_banner);
        addItemType(INFORMATION_BAR, R.layout.hai_item_multiple_information_bar);
        addItemType(ICON_TEXT, R.layout.hai_item_multiple_text_image);
        addItemType(IMAGE_TEXT, R.layout.hai_item_multiple_text_image);
        addItemType(TEXT_IMAGE, R.layout.hai_item_multiple_image_text);
        //
        addItemType(ICON_FONT_NAME, R.layout.hai_item_multiple_menu_font);
        addItemType(ICON_NAME, R.layout.hai_item_multiple_menu);
        addItemType(ICON_MAX_NAME, R.layout.hai_item_multiple_menu_max);
        addItemType(ICON_NUM_NAME, R.layout.hai_item_multiple_menu_num);
        addItemType(IMAGE_CIRCLE_NAME, R.layout.hai_item_multiple_menu);
        addItemType(IMAGE_NAME, R.layout.hai_item_multiple_menu);
        addItemType(IMAGE_MAX_NAME, R.layout.hai_item_multiple_menu_max);
        addItemType(IMAGE_NUM_NAME, R.layout.hai_item_multiple_menu_num);
        //
        addItemType(ICON_TEXT_TEXT, R.layout.hai_item_multiple_image_text_text);
        addItemType(IMAGE_TEXT_TEXT, R.layout.hai_item_multiple_image_text_text);
        addItemType(IMAGE_CIRCLE_TEXT_TEXT, R.layout.hai_item_multiple_image_text_text);
        addItemType(ICON_INFORMATION, R.layout.hai_item_multiple_information);
        addItemType(IMAGE_INFORMATION, R.layout.hai_item_multiple_information);
        addItemType(IMAGE_CIRCLE_INFORMATION, R.layout.hai_item_multiple_information);
        addItemType(ICON_INFORMATION_COUNT, R.layout.hai_item_multiple_information_count);
        addItemType(IMAGE_INFORMATION_COUNT, R.layout.hai_item_multiple_information_count);
        addItemType(INFORMATION_MSG, R.layout.hai_item_multiple_msg);
        //
        addItemType(ICON_RECORD_NAME2, R.layout.hai_item_multiple_record_2);
        addItemType(IMAGE_RECORD_NAME2, R.layout.hai_item_multiple_record_2);
        addItemType(ICON_RECORD_NAME3, R.layout.hai_item_multiple_record_3);
        addItemType(IMAGE_RECORD_NAME3, R.layout.hai_item_multiple_record_3);
        addItemType(ICON_RECORD_NAME4, R.layout.hai_item_multiple_record_4);
        addItemType(IMAGE_RECORD_NAME4, R.layout.hai_item_multiple_record_4);
        ///
        addItemType(TASK_NAME2, R.layout.hai_item_multiple_task_2);
        addItemType(TASK_NAME3, R.layout.hai_item_multiple_task_3);
        addItemType(TASK_NAME4, R.layout.hai_item_multiple_task_4);
        addItemType(TASK_NAME5, R.layout.hai_item_multiple_task_5);
        addItemType(TASK_NAME6, R.layout.hai_item_multiple_task_6);
        ///
        addItemType(TASK_CHECK2, R.layout.hai_item_multiple_task_check_2);
        addItemType(TASK_CHECK3, R.layout.hai_item_multiple_task_check_3);
        addItemType(TASK_CHECK4, R.layout.hai_item_multiple_task_check_4);
        ///
        addItemType(CLASSIFICATION, R.layout.hai_item_multiple_classification);
        addItemType(FUNCTION, R.layout.hai_item_multiple_function);
        addItemType(TABLE, R.layout.hai_item_multiple_tabel);
        addItemType(ANNEX, R.layout.hai_item_multiple_annex);
        addItemType(ANNEX_PHOTO, R.layout.hai_item_multiple_annex_photo);
        addItemType(EDITION_RECORD, R.layout.hai_item_multiple_edition_record);
        addItemType(SCREEN, R.layout.hai_item_multiple_screen);
        addItemType(CHARGE_TYPE, R.layout.hai_item_multiple_charge_type);
    }

    @NonNull
    @Override
    protected MultipleViewHolder createBaseViewHolder(@NonNull View view) {
        return MultipleViewHolder.create(view);
    }

    @Override
    protected void convert(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final OrderFormEntity orderForm;
        switch (entity.getItemType()) {
            case TEXT:
            case TITLE:
                holder.setText(R.id.text_single, entity, MultipleFields.TEXT);
                break;
            case LABEL:
                holder.setText(R.id.text_single, entity, MultipleFields.TEXT);
                LatteLogger.i("LABEL");
                break;
            case TEXT_TEXT:
                orderForm = entity.getField(MultipleFields.TEXT_TEXT);
                holder.setTextColor(R.id.text_single, ColorUtils.getColor(R.color.textColor_45))
                        .setOrderForm(R.id.text_single, orderForm);
                holder.setBackgroundColor(orderForm.getBackdropColor());
                break;
            case TEXT_MORE:
                holder.setText(R.id.text_single, entity, MultipleFields.TEXT);
                LatteLogger.i("textMore");
                break;
            case TEXT_TEXTS:
                orderForm = entity.getField(MultipleFields.TEXT_TEXT);
                holder.setText(R.id.text_name, orderForm.getText())
                        .setTextColor(R.id.text_name, orderForm.getTextColor())
                        .setText(R.id.text_value, orderForm.getValue())
                        .setTextColor(R.id.text_value, ColorUtils.getColor(
                                orderForm.isValueColor() ? R.color.colorAccent : R.color.textColor_85));
                holder.setBackgroundColor(orderForm.getBackdropColor());
                break;
            case TEXT_MULTIPLE:
                orderForm = entity.getField(MultipleFields.TEXT_TEXT);
                holder.setText(R.id.text_single, orderForm.getText())
                        .setText(R.id.text_multiple, orderForm.getValue());
                holder.setBackgroundColor(orderForm.getBackdropColor());
                break;
            case BRANCH:
                orderForm = entity.getField(MultipleFields.TEXT_TEXT);
                holder.setText(R.id.text_single, orderForm.getValue())
                        .setTextColor(R.id.text_single, ColorUtils.getColor(
                                orderForm.isValueColor() ? R.color.textColor_85 : R.color.colorAccent));
                holder.setBackgroundColor(orderForm.getBackdropColor());
                break;
            case ICON:
                holder.setGlideIcon(R.id.img_single, entity);
                break;
            case IMAGE:
                holder.setGlideUrl(R.id.img_single, entity);
                break;
            case BANNER:
                if (!mIsInitBanner && mBannerListener != null) {
                    final ArrayList<String> bannerTitles = entity.getField(MultipleFields.TITLE_ARRAY);
                    final ArrayList<String> bannerImages = entity.getField(MultipleFields.BANNERS);
                    final Banner convenientBanner = holder.getView(R.id.banner_recycler_item);
                    BannerCreator.createDefault(convenientBanner, bannerTitles, bannerImages, mBannerListener);
                    mIsInitBanner = true;
                }
                break;
            case INFORMATION_BAR:
                informationBar(holder, entity);
                break;
            case ICON_TEXT:
                holder.setText(R.id.tv_multiple, entity, MultipleFields.TEXT)
                        .setGlideIcon(R.id.img_multiple, entity);
                break;
            case IMAGE_TEXT:
            case TEXT_IMAGE:
                holder.setText(R.id.tv_multiple, entity, MultipleFields.TEXT)
                        .setGlideUrl(R.id.img_multiple, entity);
                break;
            default:
                inImageIcon(holder, entity);
                break;
        }
    }

    /***
     * 信息栏
     * @param holder:
     * @param entity:
     */
    private void informationBar(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        boolean titleIs = true;
        try {
            boolean title = entity.getField(MultipleFields.IS_TITLE);
            titleIs = !title;
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
        final ButtonListLayout mBll = holder.getView(R.id.bll_text);
        holder.setGlideCircleUrl(mBll.getImageRightBtn(), entity)
                .setGone(R.id.bll_top, titleIs);

        final ButtonListEntity title = entity.getField(MultipleFields.TEXT);
        if (title == null) {
            throw new NullPointerException("ButtonListEntity is null ! ");
        }
        mBll.getImageRightBtn().setVisibility(title.isImage() ? View.VISIBLE : View.GONE);
        mBll.getTextLeftBtn().setText(title.getText());
        mBll.getTextRightBtn().setText(title.getValue());
        //修改点击事件
        mBll.getImageRightBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickIntObjListener != null && mBll.getImageRightBtn().getVisibility() == View.VISIBLE) {
                    mClickIntObjListener.onClick(holder.position(), title);
                }
            }
        });
        //扩大点击范围
        mBll.getTextRightBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickIntObjListener != null && mBll.getImageRightBtn().getVisibility() == View.VISIBLE) {
                    mClickIntObjListener.onClick(holder.position(), title);
                }
            }
        });
    }

    private void inImageIcon(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        switch (entity.getItemType()) {
            case ICON_FONT_NAME:
                holder.setTextIcon(R.id.text_icon, entity, MultipleFields.IMAGE, MultipleFields.COLOR)
                        .setText(R.id.text_value, entity, MultipleFields.TEXT);
                if (null != entity.getField(MultipleFields.TIPS)) {
                    int num = DoubleUtils.objToInt(entity.getField(MultipleFields.TIPS));
                    holder.setGone(R.id.text_tips, num < 1)
                            .setText(R.id.text_tips, num + "");
                }
                break;
            case ICON_NAME:
            case ICON_MAX_NAME:
                holder.setGlideIcon(R.id.id_image_icon, entity)
                        .setText(R.id.id_text_value, entity, MultipleFields.TEXT);
                break;
            case ICON_NUM_NAME:
                holder.setGlideIcon(R.id.id_image_icon, entity);
                inImageNumName(holder, entity);
                break;
            case IMAGE_CIRCLE_NAME:
                holder.setGlideCircleUrl(R.id.id_image_icon, entity)
                        .setText(R.id.id_text_value, entity, MultipleFields.TEXT);
                break;
            case IMAGE_NAME:
            case IMAGE_MAX_NAME:
                holder.setGlideUrl(R.id.id_image_icon, entity)
                        .setText(R.id.id_text_value, entity, MultipleFields.TEXT);
                break;
            case IMAGE_NUM_NAME:
                holder.setGlideUrl(R.id.id_image_icon, entity);
                inImageNumName(holder, entity);
                break;
            default:
                inTidings(holder, entity);
                break;
        }
    }

    /***
     * @param holder:
     * @param entity:
     */
    protected void inImageNumName(@NonNull final MultipleViewHolder holder,
                                  @NonNull final MultipleItemEntity entity) {
        int number = 0;
        try {
            number = entity.getField(MultipleFields.NUMBER);
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
        holder.setGone(R.id.id_text_num, number == 0)
                .setText(R.id.id_text_num, number + "")
                .setText(R.id.id_text_value, entity, MultipleFields.TEXT);
    }

    private void inTidings(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        switch (entity.getItemType()) {
            case ICON_TEXT_TEXT:
                holder.setGlideIcon(R.id.img_single, entity)
                        .setText(R.id.tv_title, entity, MultipleFields.TITLE)
                        .setOrderForm(R.id.tv_text, entity, MultipleFields.TEXT)
                        .setOrderFormGone(R.id.image_click, entity, MultipleFields.CLICK);
                break;
            case IMAGE_TEXT_TEXT:
                holder.setGlideUrl(R.id.img_single, entity)
                        .setText(R.id.tv_title, entity, MultipleFields.TITLE)
                        .setOrderForm(R.id.tv_text, entity, MultipleFields.TEXT)
                        .setOrderFormGone(R.id.image_click, entity, MultipleFields.CLICK);
                break;
            case IMAGE_CIRCLE_TEXT_TEXT:
                holder.setGlideCircleUrl(R.id.img_single, entity)
                        .setText(R.id.tv_title, entity, MultipleFields.TITLE)
                        .setOrderForm(R.id.tv_text, entity, MultipleFields.TEXT)
                        .setOrderFormGone(R.id.image_click, entity, MultipleFields.CLICK);
                break;
            case ICON_INFORMATION:
                holder.setGlideIcon(R.id.img_single, entity);
                inInformation(holder, entity);
                break;
            case IMAGE_INFORMATION:
                holder.setGlideUrl(R.id.img_single, entity);
                inInformation(holder, entity);
                break;
            case IMAGE_CIRCLE_INFORMATION:
                holder.setGlideCircleUrl(R.id.img_single, entity);
                inInformation(holder, entity);
                break;
            case ICON_INFORMATION_COUNT:
                holder.setGlideIcon(R.id.img_single, entity);
                inInformationCount(holder, entity);
                break;
            case IMAGE_INFORMATION_COUNT:
                holder.setGlideUrl(R.id.img_single, entity);
                inInformationCount(holder, entity);
                break;
            case INFORMATION_MSG:
                holder.setText(R.id.tv_title, entity, MultipleFields.TITLE)
                        .setText(R.id.tv_content, entity, MultipleFields.TEXT)
                        .setText(R.id.tv_time, entity, MultipleFields.TIME);
                break;
            default:
                inRecord(holder, entity);
                break;
        }

    }

    private void inInformation(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final String title = entity.getField(MultipleFields.TITLE);
        final String text = entity.getField(MultipleFields.TEXT);
        final String time = entity.getField(MultipleFields.TIME);
        final boolean read = entity.getField(MultipleFields.READ);
        holder.setText(R.id.tv_title, title)
                .setText(R.id.tv_content, text)
                .setText(R.id.tv_time, time)
                .setGone(R.id.tv_read, !read);
    }

    private void inInformationCount(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final String title = entity.getField(MultipleFields.TITLE);
        final String text = entity.getField(MultipleFields.TEXT);
        final String time = entity.getField(MultipleFields.TIME);
        final int read = entity.getField(MultipleFields.READ);
        holder.setText(R.id.tv_title, title)
                .setText(R.id.tv_content, text)
                .setText(R.id.tv_time, time)
                .setText(R.id.tv_read, read > 99 ? "99+" : read + "")
                .setGone(R.id.tv_read, read < 1);
    }

    private void inFunction(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final ArrayList<CustomTabEntity> customTabEntities = new ArrayList<>();
        final int[] icons = entity.getField(MultipleFields.ICON_ARRAY);
        final String[] titles = entity.getField(MultipleFields.TITLE_ARRAY);
        if (titles != null && icons != null && titles.length == icons.length) {
            final int size = icons.length;
            for (int i = 0; i < size; i++) {
                customTabEntities.add(new TabEntity(titles[i], icons[i], icons[i]));
            }
        } else {
            throw new NullPointerException("String[] titles or int[] icons is null");
        }
        final CommonTabLayout tabLayout = holder.findView(R.id.id_tab_layout);
        if (tabLayout != null) {
            tabLayout.setTabData(customTabEntities);
            if (mTabSelectListener != null) {
                tabLayout.setOnTabSelectListener(mTabSelectListener);
            }
        }
    }


    /**
     * 记录类
     *
     * @param holder：
     * @param entity：
     */
    private void inRecord(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        switch (entity.getItemType()) {
            case ICON_RECORD_NAME2:
                holder.setGlideIcon(R.id.image_icon, entity);
                inRecord2(holder, entity);
                break;
            case IMAGE_RECORD_NAME2:
                holder.setGlideCircleUrl(R.id.image_icon, entity);
                inRecord2(holder, entity);
                break;
            case ICON_RECORD_NAME3:
                holder.setGlideIcon(R.id.image_icon, entity);
                inRecord3(holder, entity);
                break;
            case IMAGE_RECORD_NAME3:
                holder.setGlideCircleUrl(R.id.image_icon, entity);
                inRecord3(holder, entity);
                break;
            case ICON_RECORD_NAME4:
                holder.setGlideIcon(R.id.image_icon, entity);
                inRecord4(holder, entity);
                break;
            case IMAGE_RECORD_NAME4:
                holder.setGlideCircleUrl(R.id.image_icon, entity);
                inRecord4(holder, entity);
                break;
            default:
                inTask(holder, entity);
                break;
        }
    }

    /***
     * 记录类2
     * @param holder：
     * @param entity：
     */
    protected void inRecord2(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        holder.setText(R.id.text_title, entity, MultipleFields.TITLE)
                .setStatus(R.id.text_status, entity)
                .setOrderForm(R.id.text_text, entity, MultipleFields.TEXT)
                .setOrderForm(R.id.text_name, entity, MultipleFields.NAME)
                .setStatus(R.id.text_text_status, entity, MultipleFields.TEXT_STATUS)
                .setStatus(R.id.text_name_status, entity, MultipleFields.NAME_STATUS);
    }

    /***
     * 记录类3
     * @param holder：
     * @param entity：
     */
    protected void inRecord3(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        inRecord2(holder, entity);
        holder.setOrderForm(R.id.text_value, entity, MultipleFields.VALUE)
                .setStatus(R.id.text_value_status, entity, MultipleFields.VALUE_STATUS);
    }

    /***
     * 记录类4
     * @param holder：
     * @param entity：
     */
    protected void inRecord4(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        inRecord3(holder, entity);
        holder.setOrderForm(R.id.text_time, entity, MultipleFields.TIME)
                .setStatus(R.id.text_time_status, entity, MultipleFields.TIME_STATUS);
    }

    /**
     * 任务类
     *
     * @param holder：
     * @param entity：
     */
    private void inTask(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        switch (entity.getItemType()) {
            case TASK_NAME2:
                holder.setStatus(R.id.text_status, entity)
                        .setGone(R.id.text_subordinate, entity, MultipleFields.GONE)
                        .setOrderForm(R.id.text_text, entity, MultipleFields.TEXT)
                        .setOrderForm(R.id.text_name, entity, MultipleFields.NAME);
                break;
            case TASK_NAME3:
                holder.setStatus(R.id.text_status, entity)
                        .setGone(R.id.text_subordinate, entity, MultipleFields.GONE)
                        .setOrderForm(R.id.text_text, entity, MultipleFields.TEXT)
                        .setOrderForm(R.id.text_name, entity, MultipleFields.NAME)
                        .setOrderForm(R.id.text_value, entity, MultipleFields.VALUE);
                break;
            case TASK_NAME4:
                holder.setStatus(R.id.text_status, entity)
                        .setGone(R.id.text_subordinate, entity, MultipleFields.GONE)
                        .setOrderForm(R.id.text_text, entity, MultipleFields.TEXT)
                        .setOrderForm(R.id.text_name, entity, MultipleFields.NAME)
                        .setOrderForm(R.id.text_value, entity, MultipleFields.VALUE)
                        .setOrderForm(R.id.text_time, entity, MultipleFields.TIME);
                break;
            case TASK_NAME5:
                holder.setStatus(R.id.text_status, entity)
                        .setGone(R.id.text_subordinate, entity, MultipleFields.GONE)
                        .setOrderForm(R.id.text_text, entity, MultipleFields.TEXT)
                        .setOrderForm(R.id.text_name, entity, MultipleFields.NAME)
                        .setOrderForm(R.id.text_value, entity, MultipleFields.VALUE)
                        .setOrderForm(R.id.text_time, entity, MultipleFields.TIME)
                        .setOrderForm(R.id.text_tag, entity, MultipleFields.TAG);
                break;
            case TASK_NAME6:
                holder.setStatus(R.id.text_status, entity)
                        .setGone(R.id.text_subordinate, entity, MultipleFields.GONE)
                        .setOrderForm(R.id.text_text, entity, MultipleFields.TEXT)
                        .setOrderForm(R.id.text_name, entity, MultipleFields.NAME)
                        .setOrderForm(R.id.text_value, entity, MultipleFields.VALUE)
                        .setOrderForm(R.id.text_time, entity, MultipleFields.TIME)
                        .setOrderForm(R.id.text_tag, entity, MultipleFields.TAG)
                        .setOrderForm(R.id.text_type, entity, MultipleFields.TYPE);
                break;
            default:
                inTaskCheck(holder, entity);
                break;
        }
    }

    /**
     * 任务类
     *
     * @param holder：
     * @param entity：
     */
    private void inTaskCheck(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        switch (entity.getItemType()) {
            case TASK_CHECK2:
                inTaskCheck2(holder, entity);
                break;
            case TASK_CHECK3:
                inTaskCheck3(holder, entity);
                break;
            case TASK_CHECK4:
                inTaskCheck4(holder, entity);
                break;
            default:
                inOther(holder, entity);
                break;
        }
    }

    /***
     * 任务类2
     * @param holder：
     * @param entity：
     */
    protected void inTaskCheck2(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        holder.setText(R.id.text_title, entity, MultipleFields.TITLE)
                .setStatus(R.id.text_status, entity)
                .setGone(R.id.text_subordinate, entity, MultipleFields.GONE)
                .setOrderForm(R.id.text_text, entity, MultipleFields.TEXT)
                .setOrderForm(R.id.text_name, entity, MultipleFields.NAME);
    }

    /***
     * 任务类3
     * @param holder：
     * @param entity：
     */
    protected void inTaskCheck3(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        inTaskCheck2(holder, entity);
        holder.setOrderForm(R.id.text_value, entity, MultipleFields.VALUE)
                .setOrderForm(R.id.text_time, entity, MultipleFields.TIME);
    }

    /***
     * 任务类4
     * @param holder：
     * @param entity：
     */
    protected void inTaskCheck4(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        inTaskCheck3(holder, entity);
        holder.setOrderForm(R.id.text_tag, entity, MultipleFields.TAG)
                .setOrderForm(R.id.text_type, entity, MultipleFields.TYPE);
    }

    /**
     * 其他类
     *
     * @param holder：
     * @param entity：
     */
    private void inOther(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        switch (entity.getItemType()) {
            case CLASSIFICATION:
                final int color = entity.getField(MultipleFields.COLOR);
                holder.setText(R.id.tv_text, entity, MultipleFields.TEXT)
                        .setBackgroundColor(R.id.tv_title, color);
                break;
            case FUNCTION:
                inFunction(holder, entity);
                break;
            case TABLE:
                inTable(holder, entity);
                break;
            case ANNEX:
                inAnnex(holder, entity);
                break;
            case ANNEX_PHOTO:
                inAnnexPhoto(holder, entity);
                break;
            case EDITION_RECORD:
                inEditionRecord(holder, entity);
                break;
            case SCREEN:
                inScreen(holder, entity);
                break;
            case CHARGE_TYPE:
                inChargeType(holder, entity);
                break;
            default:
                break;
        }
    }

    private void inTable(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final int weightTotal = entity.getField(MultipleFields.NUMBER);
        final List<TableEntity> titles = entity.getField(MultipleFields.TITLE_ARRAY);
        final List<TableEntity> texts = entity.getField(MultipleFields.TEXT_ARRAY);

        final TableView tabLayout = holder.findView(R.id.tv_table_view);
        if (tabLayout != null) {
            tabLayout.setWeightTotal(weightTotal);
            tabLayout.setOnClickTableListener(new OnClickTableListener() {
                @Override
                public void onClickTable(int index, int column, List<TableEntity> text) {
                    if (mClickTableListener != null) {
                        mClickTableListener.onClickTable(index, column, text);
                    }
                }
            });
            tabLayout.getLayoutIncrease().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mClickIntListener != null) {
                        mClickIntListener.onClick(holder.position());
                    }
                }
            });
            tabLayout.setTableAdapter(titles);
            tabLayout.addData(texts);
            try {
                final String text = entity.getField(MultipleFields.TEXT);
                tabLayout.getTextSingle().setText(text);
            } catch (Exception e) {
                LatteLogger.e(e.getMessage());
            }
            try {
                final int icon = entity.getField(MultipleFields.ICON_IMAGE);
                tabLayout.getImageIncrease().setImageResource(icon);
            } catch (Exception e) {
                LatteLogger.e(e.getMessage());
            }
        }
    }

    /***
     * 附件
     * @param holder：
     * @param entity：
     */
    private void inAnnex(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final List<AnnexBean> text = entity.getField(MultipleFields.ANNEX);
        final DocumentView documentView = holder.findView(R.id.dv_annex);
        if (documentView != null) {
            documentView.setData(text);
            try {
                final boolean click = entity.getField(MultipleFields.CLICK);
                if (click) {
                    final String hint = entity.getField(MultipleFields.HINT);
                    documentView.setPreview(hint);
                }
            } catch (Exception e) {
                LatteLogger.e(e.getMessage());
            }
        }
    }

    /***
     * 附件(照片)
     * @param holder：
     * @param entity：
     */
    private void inAnnexPhoto(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final List<Object> text = entity.getField(MultipleFields.IMAGE_ARRAY);
        final PictureShowView pictureShowView = holder.findView(R.id.ps_annex);
        if (pictureShowView != null) {
            pictureShowView.setData(text);
        }
    }

    /***
     * 历史信息(app更新记录)
     * @param holder：
     * @param entity：
     */
    private void inEditionRecord(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        holder.setText(R.id.text_app_time, entity, MultipleFields.TIME)
                .setText(R.id.text_app_content, entity, MultipleFields.TEXT)
                .setText(R.id.text_app_code, entity, MultipleFields.NAME)
                .setText(R.id.text_app_size, entity, MultipleFields.VALUE);
        holder.setGone(R.id.id_btn_apk_download, holder.position() != 0);
        holder.setOnClickListener(R.id.id_btn_apk_download, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickIntObjListener != null) {
                    mClickIntObjListener.onClick(holder.position(), "");
                }
            }
        });
    }

    /***
     * 筛选(app筛选)
     * @param holder：
     * @param entity：
     */
    private void inScreen(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        final String text = entity.getField(MultipleFields.TEXT);
        final CheckBox checkBox = holder.getView(R.id.cb_place);
        if (checkBox != null) {
            checkBox.setText(text);
            // cb1操作
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // 将其他的cb设置为未选中,将自己设置为选中
                    checkBox.setChecked(false);
                    if (mClickIntCheckListener != null) {
                        mClickIntCheckListener.onClick(holder.position(), checkBox);
                    }
                }
            });
        }
    }

    /***
     * 列表点击
     * @param holder：
     * @param entity：
     */
    private void inChargeType(@NonNull final MultipleViewHolder holder, @NonNull final MultipleItemEntity entity) {
        holder.setGlideIcon(R.id.img_image, entity)
                .setText(R.id.id_text_title, entity, MultipleFields.TITLE)
                .setText(R.id.id_text_text, entity, MultipleFields.TEXT);

    }

    /************************************** Set Listener *****************************************/

    @Override
    public void setOnBannerListener(OnBannerListener listener) {
        this.mBannerListener = listener;
    }

    @Override
    public void setOnTabSelectListener(OnTabSelectListener listener) {
        this.mTabSelectListener = listener;
    }

    @Override
    public void setOnClickIntListener(OnClickIntListener listener) {
        this.mClickIntListener = listener;
    }

    @Override
    public void setOnClickIntBoolListener(OnClickIntBoolListener listener) {
        this.mClickIntBoolListener = listener;
    }

    @Override
    public void setOnClickIntObjListener(OnClickIntObjListener listener) {
        this.mClickIntObjListener = listener;
    }

    @Override
    public void setOnClickIntStrListener(OnClickIntStrListener listener) {
        this.mClickIntStrListener = listener;
    }

    @Override
    public void setOnClickObjectListener(OnClickObjectListener listener) {
        this.mClickObjectListener = listener;
    }

    @Override
    public void setOnClickStringListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
    }

    @Override
    public void setOnClickStrObjListener(OnClickStrObjListener listener) {
        this.mClickStrObjListener = listener;
    }

    @Override
    public void setOnClickTableListener(OnClickTableListener listener) {
        this.mClickTableListener = listener;
    }

    @Override
    public void setOnClickIntCheckListener(OnClickIntCheckListener listener) {
        this.mClickIntCheckListener = listener;
    }

    @Override
    public void setOnClickDecisionListener(OnClickDecisionListener listener) {
        this.mClickDecisionListener = listener;
    }

    @Override
    public int getSpanSize(@NonNull GridLayoutManager gridLayoutManager, int viewType, int position) {
        try {
            return getData().get(position).getField(MultipleFields.SPAN_SIZE);
        } catch (Exception e) {
            return 0;
        }
    }

    @NonNull
    @Override
    public BaseLoadMoreModule addLoadMoreModule(@NonNull BaseQuickAdapter<?, ?> baseQuickAdapter) {
        return new BaseLoadMoreModule(baseQuickAdapter);
    }

}