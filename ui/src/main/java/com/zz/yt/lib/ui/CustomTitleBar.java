package com.zz.yt.lib.ui;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.SizeUtils;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;

/**
 * title
 *
 * @author qf
 * @version 2020.04.17
 */
public class CustomTitleBar extends BaseLinearLayout {
    private final TextView titleBarLeftBtn;
    private final TextView titleBarRightBtn;
    private final TextView titleBarTitle;

    @Override
    protected int setLayout() {
        return R.layout.hai_custom_title_bar;
    }

    public CustomTitleBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        titleBarLeftBtn = findViewById(R.id.title_bar_left);
        titleBarRightBtn = findViewById(R.id.title_bar_right);
        titleBarTitle = findViewById(R.id.title_bar_title);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomTitleBar);
        if (attributes != null) {

            ////////先处理左边按钮
            //获取是否要显示左边按钮
            boolean leftButtonVisible = attributes.getBoolean(R.styleable.CustomTitleBar_left_button_visible, true);
            if (leftButtonVisible) {
                titleBarLeftBtn.setVisibility(View.VISIBLE);
            } else {
                titleBarLeftBtn.setVisibility(View.INVISIBLE);
            }
            //设置左边按钮的文字
            String leftButtonText = attributes.getString(R.styleable.CustomTitleBar_left_button_text);
            if (!TextUtils.isEmpty(leftButtonText)) {
                setTextLeft(leftButtonText);
            }
            //设置左边按钮文字颜色
            int leftTextColor = attributes.getColor(R.styleable.CustomTitleBar_left_button_text_color, Color.BLACK);
            setTextLeftColor(leftTextColor);
            //设置左边按钮文字大小
            float leftTextSize = attributes.getDimension(R.styleable.CustomTitleBar_left_button_text_size, sp2px(14f));
            setTextLeftSizePx(leftTextSize);

            //设置左边图片icon
            Drawable leftButtonDrawable = attributes.getDrawable(R.styleable.CustomTitleBar_android_drawableStart);
            if (leftButtonDrawable != null) {
                //设置到哪个控件的位置（）
                setLeftImage(leftButtonDrawable);
            } else {
                setLeftImage(R.drawable.arrow_left_black);
            }

            ////////处理标题
            //先获取标题是否要显示图片icon
            Drawable titleTextDrawable = attributes.getDrawable(R.styleable.CustomTitleBar_title_text_drawable);
            if (titleTextDrawable != null) {
                titleBarTitle.setBackground(titleTextDrawable);
            } else {
                //如果不是图片标题 则获取文字标题
                String titleText = attributes.getString(R.styleable.CustomTitleBar_title_text);
                if (!TextUtils.isEmpty(titleText)) {
                    setText(titleText);
                }
                //获取标题显示颜色
                int titleTextColor = attributes.getColor(R.styleable.CustomTitleBar_title_text_color, Color.BLACK);
                setTextColor(titleTextColor);
                //获取标题显示大小
                float titleTextSize = attributes.getDimension(R.styleable.CustomTitleBar_title_text_size, sp2px(14f));
                setTextSizePx(titleTextSize);
            }

            ////////处理右边按钮
            //获取是否要显示右边按钮
            boolean rightButtonVisible = attributes.getBoolean(R.styleable.CustomTitleBar_right_button_visible, true);
            if (rightButtonVisible) {
                titleBarRightBtn.setVisibility(View.VISIBLE);
            } else {
                titleBarRightBtn.setVisibility(View.INVISIBLE);
            }
            //设置右边按钮的文字
            String rightButtonText = attributes.getString(R.styleable.CustomTitleBar_right_button_text);
            if (!TextUtils.isEmpty(rightButtonText)) {
                setTextRight(rightButtonText);
            }
            //设置右边按钮文字颜色
            int rightButtonTextColor = attributes.getColor(R.styleable.CustomTitleBar_right_button_text_color, Color.BLACK);
            setTextRightColor(rightButtonTextColor);
            //设置右边按钮文字大小
            float rightTextSize = attributes.getDimension(R.styleable.CustomTitleBar_right_button_text_size, sp2px(14f));
            setTextRightSizePx(rightTextSize);
            //设置右边图片
            Drawable rightButtonDrawable = attributes.getDrawable(R.styleable.CustomTitleBar_android_drawableEnd);
            if (rightButtonDrawable != null) {
                //设置到哪个控件的位置（）
                setRightImage(rightButtonDrawable);
            }
            attributes.recycle();
        }

    }

    /***
     * @param listener：点击事件
     */
    @Deprecated
    public void setTitleClickListener(OnClickListener listener) {
        if (listener != null) {
            setTitleClickLeftListener(listener);
            setTitleClickRightListener(listener);
        }
    }

    /**
     * 点击事件
     *
     * @param listener：点击事件
     */
    public void setTitleClickLeftListener(OnClickListener listener) {
        if (titleBarLeftBtn != null && listener != null) {
            titleBarLeftBtn.setOnClickListener(listener);
        }
    }

    /**
     * 点击事件
     *
     * @param listener：点击事件
     */
    public void setTitleClickRightListener(OnClickListener listener) {
        if (titleBarRightBtn != null && listener != null) {
            titleBarRightBtn.setOnClickListener(listener);
        }
    }

    /**
     * 左边按钮
     *
     * @return Button
     */
    public TextView getTitleBarLeftBtn() {
        return titleBarLeftBtn;
    }

    /**
     * 右边按钮
     *
     * @return Button
     */
    public TextView getTitleBarRightBtn() {
        return titleBarRightBtn;
    }

    /**
     * 标题内容
     *
     * @return TextView
     */
    public TextView getTitleBarTitle() {
        return titleBarTitle;
    }

    /***
     * 设置标题内容
     * @param text：标题内容
     */
    public void setText(CharSequence text) {
        if (titleBarTitle != null) {
            titleBarTitle.setText(text);
        }
    }

    /**
     * @param color：标题内容的颜色
     */
    public void setTextColor(@ColorInt int color) {
        if (titleBarTitle != null) {
            titleBarTitle.setTextColor(color);
        }
    }

    /**
     * @param size：标题内容的大小
     */
    public void setTextSize(float size) {
        setTextSizePx(SizeUtils.sp2px(size));
    }

    /**
     * @param size：标题内容的大小
     */
    protected void setTextSizePx(float size) {
        if (titleBarTitle != null) {
            titleBarTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
    }


    /**
     * @param text:设置左边按钮文字
     */
    public void setTextLeft(CharSequence text) {
        if (titleBarLeftBtn != null) {
            titleBarLeftBtn.setText(text);
        }
    }

    /**
     * @param color:设置左边按钮文字颜色
     */
    public void setTextLeftColor(@ColorInt int color) {
        if (titleBarLeftBtn != null) {
            titleBarLeftBtn.setTextColor(color);
        }
    }

    /**
     * @param size:设置左边按钮文字大小
     */
    public void setTextLeftSize(float size) {
        setTextLeftSizePx(SizeUtils.sp2px(size));
    }

    /**
     * @param size:设置左边按钮文字大小
     */
    protected void setTextLeftSizePx(float size) {
        if (titleBarLeftBtn != null) {
            titleBarLeftBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
    }

    /**
     * @param image:左边按钮图片
     */
    public void setLeftImage(int image) {
        super.setLeftImage(titleBarLeftBtn, image);
    }

    /**
     * @param image:左边按钮图片
     */
    public void setLeftImage(Drawable image) {
        super.setLeftImage(titleBarLeftBtn, image);
    }

    /**
     * @param text:设置右边按钮文字
     */
    public void setTextRight(CharSequence text) {
        if (titleBarRightBtn != null) {
            titleBarRightBtn.setText(text);
        }
    }

    /**
     * @param color:设置右边按钮文字的颜色
     */
    public void setTextRightColor(@ColorInt int color) {
        if (titleBarRightBtn != null) {
            titleBarRightBtn.setTextColor(color);
        }
    }

    /**
     * @param size:设置右边按钮文字大小
     */
    public void setTextRightSize(float size) {
        setTextRightSizePx(SizeUtils.sp2px(size));
    }

    /**
     * @param size:设置右边按钮文字大小
     */
    protected void setTextRightSizePx(float size) {
        if (titleBarRightBtn != null) {
            titleBarRightBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
    }

    /**
     * @param image:右边按钮图片
     */
    public void setRightImage(int image) {
        super.setRightImage(titleBarRightBtn, image);
    }

    /**
     * @param image:右边按钮图片
     */
    public void setRightImage(Drawable image) {
        super.setRightImage(titleBarRightBtn, image);
    }

}