package com.zz.yt.lib.ui.mvp.presenter;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ObjectUtils;
import com.google.gson.reflect.TypeToken;
import com.whf.android.jar.base.mvp.BasePresenter;
import com.whf.android.jar.base.mvp.model.LatteModel;
import com.whf.android.jar.net.callback.ISuccess;
import com.zz.yt.lib.ui.entity.DateEntry;
import com.zz.yt.lib.ui.mvp.contract.ExampleContract;
import com.zz.yt.lib.ui.mvp.model.ExampleModel;

import java.util.WeakHashMap;

/**
 * 示例的操作层
 *
 * @author lzl
 * @version 1.0
 */
public class ExamplePresenter<T> extends BasePresenter<ExampleModel, ExampleContract.View<T>>
        implements ExampleContract.Presenter {


    @Override
    protected void onViewDestroy() {
        if (model != null) {
            model.stopRequest();
        }
    }

    @Override
    public void onGet(String url, WeakHashMap<String, Object> params) {
        if (getView() != null) {
            getView().showLoading();
        }
        if (model != null) {
            model.inGet(url, params, new ISuccess() {
                @Override
                public void onSuccess(String data) {
                    if (getView() != null) {
                        getView().hideLoading();
                    }
                    if (ObjectUtils.isEmpty(data)) {
                        return;
                    }
                    DateEntry<T> enter = GsonUtils.fromJson(data, new TypeToken<DateEntry<T>>() {
                    }.getType());
                    if (getView() != null) {
                        //数据返回 可以是string也可以是JSON对象
                        getView().onSuccess(enter);
                    }
                }
            });
        }
    }

    @Override
    public void onGetMenu(String url, int pageNumber, int pageSize, WeakHashMap<String, Object> params) {
        if (getView() != null) {
            getView().showLoading();
        }
        if (params != null) {
            params.put("current", pageNumber);
            params.put("size", pageSize);
        }
        if (model != null) {
            model.inGet(url, params, new ISuccess() {
                @Override
                public void onSuccess(String data) {
                    if (getView() != null) {
                        getView().hideLoading();
                    }
                    if (ObjectUtils.isEmpty(data)) {
                        return;
                    }
                    DateEntry<T> enter = GsonUtils.fromJson(data, new TypeToken<DateEntry<T>>() {
                    }.getType());
                    if (getView() != null) {
                        //数据返回 可以是string也可以是JSON对象
                        getView().onSuccess(enter);
                    }
                }
            });
        }
    }
}
