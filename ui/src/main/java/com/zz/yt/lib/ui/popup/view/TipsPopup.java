package com.zz.yt.lib.ui.popup.view;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.ui.R;


/**
 * 警告类提示
 *
 * @author qf
 * @version 1.0.24
 **/
@SuppressLint("ViewConstructor")
public class TipsPopup extends LattePopupCenter {

    private CharSequence mTitle = null;
    private CharSequence mTips = null;
    private CharSequence button = null;
    private CharSequence cancel = null;


    @NonNull
    public static TipsPopup create() {
        return new TipsPopup();
    }

    private TipsPopup() {
        super(Latte.getActivity());
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_tips;
    }

    /**
     * 设置数据
     *
     * @param title：
     */
    public TipsPopup setTitle(CharSequence title) {
        this.mTitle = title;
        return this;
    }

    /**
     * 设置数据
     *
     * @param tips：
     */
    public TipsPopup setDate(CharSequence tips) {
        this.mTips = tips;
        return this;
    }

    /**
     * 设置数据
     *
     * @param button：
     */
    public TipsPopup setText(CharSequence button) {
        this.button = button;
        return this;
    }

    /**
     * 设置数据
     *
     * @param buttonProceed：
     */
    public TipsPopup setConfirm(CharSequence buttonProceed) {
        this.button = buttonProceed;
        return this;
    }

    /**
     * 设置数据
     *
     * @param buttonCancel：
     */
    public TipsPopup setCancel(CharSequence buttonCancel) {
        this.cancel = buttonCancel;
        return this;
    }

    @Override
    protected void initViews() {
        setText(R.id.id_text_title, mTitle);
        setText(R.id.id_text_tips, mTips);
        setText(R.id.confirm, button);
        setText(R.id.cancel, cancel);
        final Button idOkBtn = findViewById(R.id.confirm);
        final Button idCancelBtn = findViewById(R.id.cancel);
        idOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    mClickDecisionListener.onConfirm(null);
                }
                dismiss();
            }
        });
        idCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    mClickDecisionListener.onCancel();
                }
                dismiss();
            }
        });
    }

}

