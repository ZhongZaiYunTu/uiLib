package com.zz.yt.lib.ui;

/***
 * 传参
 * @author qf
 * @version 1.0
 */
public enum BundleKey {

    /***传参*/
    ENTITY,

    /*** 传参*/
    MAP,

    /*** 传参*/
    TAG,

    /*** 传参*/
    INFO,

    /*** 传参*/
    BEAN,

    /*** 状态*/
    STATUS,

    /*** 传参*/
    INDEX,

    /*** 传参*/
    DATA,

    /*** 计数*/
    COUNT,

    /*** 状态*/
    STATE,

    /*** id*/
    ID,

    /*** 传参*/
    TYPE_ID,

    /*** id*/
    PRC_INST_ID,

    /*** 收文Id*/
    RECEIVE_ID,

    /*** 抄送id*/
    MAKE_ID,

    /*** 任务id*/
    TASK_ID,

    /*** 申请id*/
    APPLY_ID,

    /*** 模板表id*/
    FORM_ID,

    /*** 模板id*/
    MODEL_ID,

    /*** 历史任务id*/
    DEFINITION_ID,

    /*** 历史任务id*/
    ACT_TASK_ID,

    /*** 模板id*/
    NODE_ID,

    /*** 扫描*/
    ON_SCAN,

    /*** 任务状态*/
    TASK_STATUS,

    /*** 历史任务id*/
    FORM_EDIT_FIELD,

    /*** 地址*/
    URL,

    /*** 审批*/
    APPROVAL,

    /*** 审批状态*/
    APPLY_STATUS,

    /*** title*/
    TITLE,

    /*** text*/
    TEXT,

    /*** 部门id*/
    DEPARTMENT_ID,

    /*** 部门名称*/
    DEPARTMENT_NAME,

    /*** 汇报类型*/
    DAILY_TYPE,

    /*** 是否显示*/
    IS_GONE,

    /*** 服务*/
    SERVICE,

    /*** 前端地址*/
    FRONT_END,

    /*** 是否全屏*/
    IS_TOP,

    /*** 电子签名标识*/
    SIGNATURE,

    /*** 电话号*/
    PHONE,

    /*** 数量*/
    NUMBER,

    /*** 日期*/
    DATE,

    /*** 时间*/
    TIME,

    /*** 开始时间*/
    TIME_START,

    /*** 结束时间*/
    TIME_END,

    /*** 经纬度 */
    LONGITUDE,

    /*** 纬度 */
    LATITUDE,

    /*** id号 */
    NO_ID,

    /*** 订单号 */
    NO_ORDER,

    /*** 票号 */
    NO_VOTE,

    /*** 任务号 */
    NO_TASK
}