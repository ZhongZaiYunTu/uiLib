package com.zz.yt.lib.ui.item;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;

import com.zz.yt.lib.ui.R;

/**
 * 摄像头列表
 *
 * @author qf
 * @date 2020-04-20
 */
public class CameraLayout extends RelativeLayout {

    public CameraLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_item_camera_list, this, true);
    }

    public ImageView getImageView(@IdRes int id) {
        return findViewById(id);
    }

    public ImageView getImageLeft() {
        return findViewById(R.id.id_image_camera);
    }

    public ImageView getImageRight() {
        return findViewById(R.id.id_image_right);
    }

    public TextView getTextView() {
        return findViewById(R.id.id_text_name);
    }

    /**
     * 设置控件的显示值
     *
     * @param text：
     */
    public void setTextView(CharSequence text) {
        getTextView().setText(text);
    }

}
