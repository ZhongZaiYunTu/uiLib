package com.zz.yt.lib.ui.listener;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickObjectListener {

    /**
     * 点击
     *
     * @param obj：值
     */
    void onClick(Object obj);

}
