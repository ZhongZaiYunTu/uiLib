package com.zz.yt.lib.ui.listener;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickStringsListener {

    /**
     * 点击
     *
     * @param index：值
     */
    void onClick(String... index);

}
