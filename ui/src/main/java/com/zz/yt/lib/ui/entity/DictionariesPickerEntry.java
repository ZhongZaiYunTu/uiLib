package com.zz.yt.lib.ui.entity;

import android.text.TextUtils;

import com.contrarywind.interfaces.IPickerViewData;

/**
 * 字典
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/6/17
 **/
public class DictionariesPickerEntry implements IPickerViewData {

    /**
     * 字典数据id
     */
    private String id;
    /**
     * 字典值
     */
    protected String value;
    /**
     * 字典描述
     */
    protected String label;
    /**
     * 字典描述
     */
    protected String name;
    /**
     * 字典type
     */
    private String type;
    /**
     * 字典type描述
     */
    private String description;
    /**
     * 排序
     */
    private String sort;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 删除标识：0正常，1 删除
     */
    private String delFlag;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String getPickerViewText() {
        return TextUtils.isEmpty(label) ? name : label;
    }

}
