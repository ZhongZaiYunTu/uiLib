package com.zz.yt.lib.ui.delegate.record;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.recycler.adapter.MultipleRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;

import java.util.WeakHashMap;


/***
 * @author qf
 * @version 1.0
 */

public class LatteRefreshHandler extends BaseLatteRefreshHandler<MultipleRecyclerAdapter> {


    protected LatteRefreshHandler(LatteDelegate delegate,
                                  SwipeRefreshLayout swipeRefreshLayout,
                                  RecyclerView recyclerView,
                                  BaseDataConverter converter) {
        super(delegate, swipeRefreshLayout, recyclerView, converter);
    }

    @NonNull
    static LatteRefreshHandler create(LatteDelegate delegate,
                                      SwipeRefreshLayout swipeRefreshLayout,
                                      RecyclerView recyclerView,
                                      BaseDataConverter converter) {
        return new LatteRefreshHandler(delegate, swipeRefreshLayout, recyclerView, converter);
    }

    @Override
    protected void setPaging(String data, MultipleRecyclerAdapter adapter) {
        super.setPaging(data, adapter);
    }

    @Override
    public WeakHashMap<String, Object> params() {
        return super.params();
    }

    @Override
    protected void refresh(String url, WeakHashMap<String, Object> params, final boolean refresh) {
        super.refresh(url, params, refresh);
    }

    @Override
    protected MultipleRecyclerAdapter getNewAdapter() {
        return MultipleRecyclerAdapter.create();
    }


}

