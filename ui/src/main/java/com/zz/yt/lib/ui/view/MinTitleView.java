package com.zz.yt.lib.ui.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.ColorUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.R;

/**
 * General title bar
 *
 * @author qf
 * @since 2.5.0
 */
public class MinTitleView extends LinearLayout {

    private TextView text, textBack, textViewMore;
    private ImageView imageViewMore, imageViewMore2;
    private Context context;
    private AttributeSet attrs;
    private OnClickListener onClickListener;
    private RelativeLayout idLayoutReturn, idLayoutMore;
    private Button more;

    public MinTitleView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public MinTitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        initView();
        onTypedArray();
    }

    public MinTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.attrs = attrs;
        initView();
        onTypedArray();
    }

    /**
     * init
     */
    private void initView() {
        LayoutInflater.from(context).inflate(R.layout.hai_layout_min_title, this, true);
        text = getView(R.id.id_textTitle);
        textBack = getView(R.id.id_textBack);
        more = getView(R.id.id_more);
        imageViewMore = getView(R.id.image_more);
        imageViewMore2 = getView(R.id.iv_more_2);
        textViewMore = getView(R.id.tv_more);

        idLayoutReturn = getView(R.id.id_layout_return);
        idLayoutMore = getView(R.id.id_layout_more);

        findViewById(R.id.id_return).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onClick(v);
                } else {
                    ((Activity) getContext()).finish();
                }

            }
        });

    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        onClickListener = l;
    }

    public void setOnClickMoreListener(OnClickListener l) {
        findViewById(R.id.id_more).setOnClickListener(l);
    }

    /**
     * 通用findViewById,减少重复的类型转换
     */
    @SuppressWarnings("unchecked")
    public final <E extends View> E getView(int id) {
        try {
            return (E) findViewById(id);
        } catch (ClassCastException ex) {
            LatteLogger.e(ex.getMessage());
            throw ex;
        }
    }

    /**
     * init
     */
    private void onTypedArray() {
        @SuppressLint("Recycle")
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MinTitleView);
        String titleBack = a.getString(R.styleable.MinTitleView_textBack);
        String title = a.getString(R.styleable.MinTitleView_android_text);
        int titleColor = a.getColor(R.styleable.MinTitleView_color, 0xFF1E90FF);

        String textMore = a.getString(R.styleable.MinTitleView_textMore);
        int resMore = a.getResourceId(R.styleable.MinTitleView_moreImage, R.drawable.arrow_more_black);
        int is = a.getInteger(R.styleable.MinTitleView_isMoreDisplay, -3);
        if (title != null && !"".equals(title)) {
            setText(title);
        }
        if (titleBack != null && !"".equals(titleBack)) {
            setTextBack(titleBack);
        }
        if (titleColor != 0) {
            text.setBackgroundColor(titleColor);
        }

        if (textMore != null && !"".equals(textMore)) {
            textViewMore.setText(textMore);
        }
        switch (is) {
            case -1:
                imageViewMore.setVisibility(VISIBLE);
                if (resMore != 0) {
                    imageViewMore.setImageResource(resMore);
                }
                break;
            case -2:
                textViewMore.setVisibility(VISIBLE);
                break;
            default:
                imageViewMore.setVisibility(GONE);
                textViewMore.setVisibility(GONE);
                break;
        }
    }

    /**
     * 设置标题
     */
    public void setText(String title) {
        text.setText(title);
    }

    /**
     * @return 返回 textViewMore 的文字
     */
    public String getText() {
        return textViewMore.getText().toString();
    }

    /**
     * 设置更多按钮的文字描述
     *
     * @param title 文字
     * @param color 颜色
     */
    public void setMoreText(String title, int color) {
        textViewMore.setVisibility(VISIBLE);
        textViewMore.setText(title);
        textViewMore.setTextColor(ContextCompat.getColor(context, color));
    }

    /**
     * 设置更多按钮的文字描述
     *
     * @param visibility 文字
     */
    public void setReturnVisibility(boolean visibility) {
        idLayoutReturn.setVisibility(visibility ? VISIBLE : GONE);
    }

    /**
     * 设置更多按钮的文字描述
     *
     * @param visibility 文字
     */
    public void setMoreVisibility(boolean visibility) {
        idLayoutMore.setVisibility(visibility ? VISIBLE : GONE);
    }

    /**
     * 设置返回展示
     */
    public void setTextBack(String titleBack) {
        textBack.setText(titleBack);
    }
    /**
     * 设置返回展示
     */
    public void setTextBack(String titleBack,int color) {
        textBack.setTextColor(ColorUtils.getColor(color));
        textBack.setText(titleBack);
    }

    /**
     * 设置展示
     */
    public void setTextMore(String titleBack) {
        textViewMore.setText(titleBack);
    }

    /**
     * 设置展示
     */
    public void setTextMore(String titleBack,int color) {
        textViewMore.setText(titleBack);
        textViewMore.setTextColor(ColorUtils.getColor(color));
    }
    /**
     * 设置标题颜色
     */
    public void setTextColor(int color) {
        textBack.setTextColor(ColorUtils.getColor(color));
    }

    /**
     * 设置返回颜色
     */
    public void setTextBackColor(int color) {
        text.setTextColor(context.getResources().getColor(color));
    }

    /**
     * 设置背景颜色
     */
    @Override
    public void setBackgroundResource(int resId) {
        if (textBack != null) {
            textBack.setBackgroundResource(resId);
        }
    }

    /**
     * 设置背景颜色
     */

    @Override
    public void setBackgroundColor(int color) {
        if (textBack != null) {
            textBack.setBackgroundColor(color);
        }
    }


    /**
     * 设置背景颜色
     */
    @Override
    public void setBackground(Drawable background) {
        if (textBack != null) {
            textBack.setBackground(background);
        }
    }
}
