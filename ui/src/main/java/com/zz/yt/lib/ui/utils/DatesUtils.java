package com.zz.yt.lib.ui.utils;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.INumberConstant;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickStringListener;

import java.util.Calendar;
import java.util.Date;


/**
 * 时间选择工具
 * <p>因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-1
 *
 * @author qf
 * @version 1.0.2
 **/
public class DatesUtils {

    private final Calendar calendar = Calendar.getInstance();
    private final Calendar startDate = Calendar.getInstance();
    private final Calendar endDate = Calendar.getInstance();
    private String pattern = "yyyy-MM-dd HH:mm:ss";
    private boolean[] type = new boolean[]{true, true, true, true, true, true};

    private boolean limit = false;
    private OnClickStringListener mClickStringListener = null;
    private OnTimeSelectListener mTimeSelectListener = null;

    @NonNull
    public static DatesUtils create() {
        return new DatesUtils();
    }

    private DatesUtils() {
    }

    /**
     * 设置事件
     */
    public DatesUtils setOnClickStringListener(OnClickStringListener listener) {
        mClickStringListener = listener;
        return this;
    }

    /**
     * 设置事件
     */
    public DatesUtils setOnTimeSelectListener(OnTimeSelectListener listener) {
        mTimeSelectListener = listener;
        return this;
    }

    /**
     * 设置为日期选择(最小)
     */
    public DatesUtils startDate(int year, int month, int date) {
        limit = true;
        startDate.set(year, month, date);
        return this;
    }

    /**
     * 设置为日期选择(最小)
     */
    public DatesUtils startDate(String yearMonthDate) {
        limit = true;
        Date date = TimeUtils.string2Date(yearMonthDate);
        startDate.setTime(date);
        return this;
    }

    /**
     * 设置为日期选择（最大）
     */
    public DatesUtils endDate(int year, int month, int date) {
        limit = true;
        endDate.set(year, month, date);
        return this;
    }

    /**
     * 设置为日期选择（最大）
     */
    public DatesUtils endDate(String yearMonthDate) {
        limit = true;
        Date date = TimeUtils.string2Date(yearMonthDate);
        endDate.setTime(date);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public DatesUtils currentDate(String yearMonthDate) {
        Date date = TimeUtils.string2Date(yearMonthDate);
        calendar.setTime(date);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public DatesUtils currentDate(int year, int month, int date,
                                  int hourOfDay, int minute, int second) {
        calendar.set(year, month, date, hourOfDay, minute, second);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public DatesUtils currentDate(int year, int month, int date) {
        calendar.set(year, month, date);
        return this;
    }

    /**
     * 设置为日期选择
     */
    public DatesUtils setDate() {
        return setType(DateKeysUtils.DATE);
    }

    /**
     * 设置为日期选择
     */
    public DatesUtils setTime() {
        return setType(DateKeysUtils.TIME);
    }

    /**
     * 设置为月份选择
     */
    public DatesUtils setMonth() {
        return setType(DateKeysUtils.MONTH);
    }

    /**
     * 设置为选择模式
     */
    public DatesUtils setType(@DateKeysUtils.Type int patternType) {
        type = DateKeysUtils.setType(patternType);
        pattern = DateKeysUtils.setPattern(patternType);
        return this;
    }


    /**
     * 设置为选择模式
     */
    public DatesUtils setTypePattern(boolean[] type, String pattern) {
        if (type != null && type.length == INumberConstant.SIX) {
            this.type = type;
        } else {
            throw new NullPointerException("type is null or type length no six");
        }

        this.pattern = pattern;
        return this;
    }

    /**
     * 显示
     */
    public void onClick() {
        ViewGroup decorView = Latte.getActivity()
                .getWindow()
                .getDecorView()
                .findViewById(android.R.id.content);
        if (limit) {
            if (startDate.after(endDate)) {
                // 最小时间大于最大时间，将2时互换。
                final Date date = endDate.getTime();
                // 储存其中一个时间，把储存的时间重新设置。
                endDate.setTime(startDate.getTime());
                // 把储存的设置设置到另一个时间上。
                startDate.setTime(date);
            }
            if (startDate.after(calendar)) {
                // 选中的时间小于规定的最小时间,最小时间设置为选中的时间。
                calendar.setTime(startDate.getTime());
            }
            if (endDate.before(calendar)) {
                // 选中的时间大于规定的最大时间,最大时间设置为选中的时间。
                calendar.setTime(endDate.getTime());
            }
            new TimePickerBuilder(Latte.getActivity(),
                    new OnTimeSelectListener() {
                        @Override
                        public void onTimeSelect(Date date, View v) {
                            final String text = TimeUtils.date2String(date, pattern);
                            if (mTimeSelectListener != null) {
                                mTimeSelectListener.onTimeSelect(date, v);
                            }
                            if (mClickStringListener != null) {
                                mClickStringListener.onClick(text);
                            }
                        }
                    })
                    .setDate(calendar)// 如果不设置的话，默认是当前时间*/
                    .setRangDate(startDate, endDate)
                    .setDecorView(decorView)
                    .setDividerColor(ColorUtils.getColor(R.color.colorAccent))
                    .setTextColorCenter(ColorUtils.getColor(R.color.colorAccent))
                    .setLineSpacingMultiplier(3f)
                    .setItemVisibleCount(7)
                    .setContentTextSize(16)
                    .setTitleSize(20)
                    .setType(type)
                    .build()
                    .show();
        } else {
            new TimePickerBuilder(Latte.getActivity(),
                    new OnTimeSelectListener() {
                        @Override
                        public void onTimeSelect(Date date, View v) {
                            final String text = TimeUtils.date2String(date, pattern);
                            if (mTimeSelectListener != null) {
                                mTimeSelectListener.onTimeSelect(date, v);
                            }
                            if (mClickStringListener != null) {
                                mClickStringListener.onClick(text);
                            }
                        }
                    })
                    .setDate(calendar)
                    .setDecorView(decorView)
                    .setDividerColor(ColorUtils.getColor(R.color.colorAccent))
                    .setTextColorCenter(ColorUtils.getColor(R.color.colorAccent))
                    .setLineSpacingMultiplier(3f)
                    .setItemVisibleCount(7)
                    .setContentTextSize(16)
                    .setTitleSize(20)
                    .setType(type)
                    .build()
                    .show();
        }
    }

    /**
     * 获得当前时间
     */
    public String getNowString() {
        return TimeUtils.getNowString(TimeUtils.getSafeDateFormat(pattern));
    }
}
