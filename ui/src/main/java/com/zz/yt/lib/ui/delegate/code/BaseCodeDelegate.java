package com.zz.yt.lib.ui.delegate.code;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.aes.AesKeys;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.view.VerificationCodeView;


/**
 * 防机器动态码
 *
 * @author qf
 * @version 1.0
 */
public abstract class BaseCodeDelegate extends LatteTitleDelegate {


    private EditText mEditCode = null;
    private VerificationCodeView mCodeView = null;


    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        final LinearLayout mCodeLayout = rootView.findViewById(R.id.layout_code);
        mEditCode = rootView.findViewById(R.id.id_edit_code);
        mCodeView = rootView.findViewById(R.id.id_code_view);
        if (mCodeLayout != null) {
            initCodeView(mCodeLayout);
        }

        final Button mBtnSubmit = rootView.findViewById(R.id.id_btn_submit);
        if (mBtnSubmit != null) {
            mBtnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSubmit();
                }
            });
        }
    }

    /**
     * 提交按钮
     */
    protected abstract void onSubmit();

    /**
     * @param codeLayout:防机器动态码
     */
    protected void initCodeView(@NonNull LinearLayout codeLayout) {
        codeLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 防机器动态码比对
     */
    protected boolean isCode() {
        if (mEditCode == null) {
            return true;
        }
        String code = mEditCode.getText().toString().trim();
        if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("动态码不能为空");
            return false;
        }
        if (!code.equals(mCodeView.onRealCode())) {
            ToastUtils.showShort("验证码错误");
            mCodeView.onAfresh();
            return false;
        }
        return true;
    }

    /**
     * @param password:需加密的密码
     */
    protected String getPassAes(String password) {
        return getPassAes(password, AesKeys.KEY_AES);
    }

    /**
     * @param password:需加密的密码
     * @param strKey：密钥       (需要前端和后端保持一致)
     */
    protected String getPassAes(String password, String strKey) {
        return getPassAes(password, strKey, AesKeys.AES_CBC_PK_CS5_PADDING);
    }

    /**
     * @param password:需加密的密码
     * @param strKey：密钥       (需要前端和后端保持一致)
     * @param aes：算法
     */
    protected String getPassAes(String password, String strKey, String aes) {
        final byte[] bytesDataAes = ConvertUtils.string2Bytes(password);
        final byte[] bytesKeyAes = ConvertUtils.string2Bytes(strKey);
        return ConvertUtils.bytes2String(EncryptUtils.encryptAES2Base64(bytesDataAes,
                bytesKeyAes, aes, bytesKeyAes));
    }
}
