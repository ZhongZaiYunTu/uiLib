package com.zz.yt.lib.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;

import com.blankj.utilcode.util.SizeUtils;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickBoolListener;


/**
 * 选中状态
 *
 * @author qf
 * @author wang.hai.feng
 * @version 2.5
 */
public class SelectImageView extends RelativeLayout {

    private final ImageView imgSelect;
    private final TextView textName;

    private int selectOnSrc;
    private int selectUnSrc;
    private OnClickBoolListener mClickBoolListener = null;

    public SelectImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_view_img_select, this, true);
        imgSelect = findViewById(R.id.id_image_select);
        textName = findViewById(R.id.tv_store_name);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SelectImageView);

        //background
        final Drawable background = attributes.getDrawable(R.styleable.SelectImageView_android_background);
        if (background != null) {
            setBackground(background);
        }

        //
        selectOnSrc = attributes.getResourceId(R.styleable.SelectImageView_onSrc, R.drawable.tree_select_on);
        selectUnSrc = attributes.getResourceId(R.styleable.SelectImageView_unSrc, R.drawable.tree_select_un);
        //image
        boolean select = attributes.getBoolean(R.styleable.SelectImageView_select, false);
        setChecked(select);

        //textView
        String titleText = attributes.getString(R.styleable.SelectImageView_android_text);
        if (!TextUtils.isEmpty(titleText)) {
            setText(titleText);
        }
        int titleTextColor = attributes.getColor(R.styleable.SelectImageView_android_textColor, -1);
        if (titleTextColor != -1) {
            setTextColor(titleTextColor);
        }
        float titleTextSize = attributes.getDimension(R.styleable.SelectImageView_android_textSize, -1);
        if (titleTextSize != -1) {
            setTextSizePx(titleTextSize);
        }
        attributes.recycle();
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                select();
            }
        });
    }

    public void setSelectOnSrc(int selectOnSrc) {
        this.selectOnSrc = selectOnSrc;
    }

    public void setSelectUnSrc(int selectUnSrc) {
        this.selectUnSrc = selectUnSrc;
    }

    /**
     * 设置点击选中与不选中的效果
     */
    public void select() {
        setChecked(!isChecked());
        if (mClickBoolListener != null) {
            mClickBoolListener.onClick(isChecked());
        }
    }

    /**
     * 获得是否选中状态
     */
    public boolean isChecked() {
        return imgSelect.isSelected();
    }

    /**
     * 设置是否选中
     */
    public void setChecked(boolean selected) {
        if (selected) {
            imgSelect.setImageResource(selectOnSrc);
        } else {
            imgSelect.setImageResource(selectUnSrc);
        }
        imgSelect.setSelected(selected);
    }

    /**
     * @param text：设置汉字
     */
    public void setText(CharSequence text) {
        if (textName != null) {
            textName.setText(text);
        }
    }

    /**
     * @param textColor：设置汉字颜色
     */
    public void setTextColor(@ColorInt int textColor) {
        if (textName != null) {
            textName.setTextColor(textColor);
        }
    }

    /**
     * @param size：设置汉字大小
     */
    public void setTextSize(float size) {
        setTextSizePx(SizeUtils.sp2px(size));
    }

    /**
     * @param textSize：设置汉字大小
     */
    protected void setTextSizePx(float textSize) {
        if (textName != null) {
            textName.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        }
    }

    /**
     * @param maxLines：设置显示的最大行数
     */
    public void setMaxLines(int maxLines) {
        if (textName != null) {
            textName.setMaxLines(maxLines);
        }
    }

    /**
     * @param listener：设置选中回调
     */
    public void setOnClickBoolListener(OnClickBoolListener listener) {
        this.mClickBoolListener = listener;
    }

}
