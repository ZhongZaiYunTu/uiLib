package com.zz.yt.lib.ui.privacy;


/**
 * 设置跳转到用户协议和隐私政策
 *
 * @author hf
 * @version 201.4.22
 */
public interface IPrivacyListener {

    /**
     * 用户协议
     */
    void onClickAgreement();

    /**
     * 隐私政策
     */
    void onClickPrivacy();
}
