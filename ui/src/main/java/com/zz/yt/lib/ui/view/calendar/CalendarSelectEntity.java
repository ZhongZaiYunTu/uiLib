package com.zz.yt.lib.ui.view.calendar;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.TimeUtils;
import com.zz.yt.lib.ui.view.calendar.month.CalendarMonthEntity;
import com.zz.yt.lib.ui.view.calendar.month.CalendarUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarSelectEntity {
    private final String month;
    private final List<CalendarMonthEntity> mList;

    public CalendarSelectEntity(@NonNull Calendar calendar) {
        this.month = TimeUtils.date2String(calendar.getTime(), " yyyy年MM月");
        this.mList = CalendarUtils.setData(calendar);
    }

    public String getMonth() {
        return month;
    }

    public List<CalendarMonthEntity> getList() {
        return mList;
    }
}
