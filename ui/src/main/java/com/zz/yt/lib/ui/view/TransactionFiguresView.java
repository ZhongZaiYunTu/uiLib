package com.zz.yt.lib.ui.view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;

/***
 * 交易数字
 * @author qf
 * @version 1.0
 */
public class TransactionFiguresView extends BaseLinearLayout {

    private final TextView figuresTitle;
    private final TextView figuresBg;
    private final TextView figuresUnit;
    private int textColor, textColorUnit;
    private int textStyle, textStyleUnit;
    private float textSize, textSizeUnit;

    @Override
    protected int setLayout() {
        return R.layout.hai_view_figure_transactions;
    }

    public TransactionFiguresView(Context context, AttributeSet attrs) {
        super(context, attrs);
        figuresTitle = findViewById(R.id.figure_transactions_text);
        figuresBg = findViewById(R.id.figure_transactions_bg);
        figuresUnit = findViewById(R.id.figure_transactions_unit);
        final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.TransactionFiguresView);
        if (attributes != null) {
            //处理下划线 *背景色
            try {
                Drawable background = attributes.getDrawable(R.styleable.TransactionFiguresView_android_background);
                if (background != null) {
                    figuresBg.setBackground(background);
                }
            } catch (Exception e) {
                LatteLogger.e(e.getMessage());
            }
            int backgroundColor = attributes.getColor(R.styleable.TransactionFiguresView_backgroundColor, -1);
            if (backgroundColor != -1) {
                figuresBg.setBackgroundColor(backgroundColor);
            }


            boolean backgroundGone = attributes.getBoolean(R.styleable.TransactionFiguresView_backgroundGone, false);
            figuresBg.setVisibility(backgroundGone ? VISIBLE : GONE);
            //数字

            String text = attributes.getString(R.styleable.TransactionFiguresView_android_text);
            textColor = attributes.getColor(R.styleable.TransactionFiguresView_android_textColor, Color.BLACK);
            textSize = attributes.getDimension(R.styleable.TransactionFiguresView_android_textSize, sp2px(24f));
            textStyle = attributes.getInt(R.styleable.TransactionFiguresView_android_textStyle, -1);
            setText(text);

            //单位
            String textUnit = attributes.getString(R.styleable.TransactionFiguresView_unitText);
            textColorUnit = attributes.getColor(R.styleable.TransactionFiguresView_unitTextColor, 0xA6000000);
            textSizeUnit = attributes.getDimension(R.styleable.TransactionFiguresView_unitTextSize, sp2px(12f));
            textStyleUnit = attributes.getInt(R.styleable.TransactionFiguresView_unitTextStyle, -1);
            setTextUnit(textUnit);


            attributes.recycle();
        }
    }

    public void setText(CharSequence text) {
        if (figuresTitle != null) {
            figuresTitle.setText(text);
            if (textColor != -1) {
                figuresTitle.setTextColor(textColor);
            }
            if (textSize != -1) {
                figuresTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
            if (textStyle != -1) {
                figuresTitle.setTypeface(null, textStyle);
            }
        }
    }

    public void setTextUnit(CharSequence text) {
        if (figuresUnit != null) {
            figuresUnit.setText(text);
            if (textColorUnit != -1) {
                figuresUnit.setTextColor(textColorUnit);
            }
            if (textSizeUnit != -1) {
                figuresUnit.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeUnit);
            }
            if (textStyleUnit != -1) {
                figuresUnit.setTypeface(null, textStyleUnit);
            }
        }
    }

    public TextView getFiguresTitle() {
        return figuresTitle;
    }

    public TextView getFiguresBg() {
        return figuresBg;
    }


    public TextView getFiguresUnit() {
        return figuresUnit;
    }
}
