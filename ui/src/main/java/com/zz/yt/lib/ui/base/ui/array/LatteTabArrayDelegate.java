package com.zz.yt.lib.ui.base.ui.array;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;

import java.util.ArrayList;


/**
 * tab处理
 *
 * @author qf
 * @version 1.0.24
 **/
public abstract class LatteTabArrayDelegate extends LatteArray implements OnTabSelectListener {


    private final ArrayList<CustomTabEntity> mCustomTabEntities = new ArrayList<>();

    /**
     * 获得数据处理
     *
     * @return 数据处理
     */
    protected abstract BaseDataConverter setDataConverter();

    /**
     * 设置tabLayout数据
     *
     * @param customTabEntities:数据
     */
    protected abstract void setCustomTabEntity(@NonNull ArrayList<CustomTabEntity> customTabEntities);


    @Override
    public Object setLayout() {
        return R.layout.a_ui_base_tab_recycler;
    }


    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        initTabLayout();
    }

    private void initTabLayout() {
        setCustomTabEntity(mCustomTabEntities);
        CommonTabLayout mTabLayout = findViewById(R.id.id_tab_layout);
        mTabLayout.setTabData(mCustomTabEntities);
        mTabLayout.setOnTabSelectListener(this);
        setTabLayout(mTabLayout);
    }

    protected void setTabLayout(CommonTabLayout commonTabLayout) {

    }

    @Override
    protected void initRecyclerView(RecyclerView recyclerView) {
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
    }


    @Override
    public void onTabReselect(int position) {

    }


}
