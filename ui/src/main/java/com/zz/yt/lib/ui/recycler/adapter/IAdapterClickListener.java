package com.zz.yt.lib.ui.recycler.adapter;

import com.flyco.tablayout.listener.OnTabSelectListener;
import com.youth.banner.listener.OnBannerListener;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.listener.OnClickIntBoolListener;
import com.zz.yt.lib.ui.listener.OnClickIntCheckListener;
import com.zz.yt.lib.ui.listener.OnClickIntListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickIntStrListener;
import com.zz.yt.lib.ui.listener.OnClickObjectListener;
import com.zz.yt.lib.ui.listener.OnClickStrObjListener;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.listener.OnClickTableListener;

/**
 * 事件
 *
 * @author qf
 * @version 1.0
 */
public interface IAdapterClickListener {

    /***
     * 事件
     * @param listener:
     */
    void setOnBannerListener(OnBannerListener listener);

    /***
     * 事件
     * @param listener：
     */
    void setOnTabSelectListener(OnTabSelectListener listener);

    /***
     * 事件
     * @param listener:
     */
    void setOnClickIntListener(OnClickIntListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickIntBoolListener(OnClickIntBoolListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickIntObjListener(OnClickIntObjListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickIntStrListener(OnClickIntStrListener listener);

    /***
     * 事件
     * @param listener:
     */
    void setOnClickObjectListener(OnClickObjectListener listener);

    /***
     * 事件
     * @param listener:
     */
    void setOnClickStringListener(OnClickStringListener listener);
    /***
     * 事件
     * @param listener:
     */
    void setOnClickStrObjListener(OnClickStrObjListener listener);

    /***
     * 事件
     * @param listener:
     */
    void setOnClickTableListener(OnClickTableListener listener);

    /***
     * 事件
     * @param listener:
     */
    void setOnClickIntCheckListener(OnClickIntCheckListener listener);

    /***
     * 事件
     * @param listener:
     */
    void setOnClickDecisionListener(OnClickDecisionListener listener);
}
