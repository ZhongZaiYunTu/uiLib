package com.zz.yt.lib.ui.base.ui.array;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.view.ScreenListView;


/**
 * 纯列表类处理(item)
 *
 * @author qf
 * @version 1.0.24
 **/
public abstract class LatteItemArrayDelegate extends LatteDelegate {

    protected ScreenListView mScreenListView = null;

    /**
     * 获得数据处理
     *
     * @return 数据处理
     */
    protected abstract BaseDataConverter setDataConverter();

    protected SwipeRefreshLayout mSwipeRefreshLayout = null;

    /***
     * 设置列表
     * @param recyclerView:数据处理
     */
    protected abstract void initRecyclerView(RecyclerView recyclerView);

    /***
     * 设置下拉刷新
     * @param swipeRefreshLayout:数据处理
     * @param recyclerView:数据处理
     */
    protected abstract void initRefreshHandler(SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView);

    @Override
    public Object setLayout() {
        return R.layout.a_ui_content_swipe_recycler;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        mSwipeRefreshLayout = findViewById(R.id.id_swipe_Layout);
        initRefreshLayout();

        final RecyclerView recyclerView = findViewById(R.id.id_recycler_view);
        initRecyclerView(recyclerView);

        //初始化数据处理
        initRefreshHandler(mSwipeRefreshLayout, recyclerView);

        final ScreenListView screenView = rootView.findViewById(R.id.screen_list_view);
        if (screenView != null) {
            initScreenView(screenView);
        }

        final ImageView imageAdded = findViewById(R.id.id_image_portrait);
        initShadowView(imageAdded);
    }

    /**
     * 刷新的背景
     */
    private void initRefreshLayout() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeColors(Color.BLACK, Color.RED);
        }
    }

    /**
     * @param screenView：数据筛选
     */
    protected void initScreenView(ScreenListView screenView) {
        this.mScreenListView = screenView;
    }

    /**
     * @param imageAdded:添加按钮
     */
    protected void initShadowView(ImageView imageAdded) {

    }


}
