package com.zz.yt.lib.mvp.base.list.abolish.click;

import android.view.View;

/**
 * 子类的点击事件
 *
 * @author Gags
 * @version 1.0
 */
public interface OnItemObjectClick {

    /**
     * 子类的点击事件
     *
     * @param view     当前控件
     * @param position 当前条目
     * @param object   任意数据
     */
    void onItemObjectClick(View view, int position, Object object);
}
