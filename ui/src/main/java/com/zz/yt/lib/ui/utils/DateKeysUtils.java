package com.zz.yt.lib.ui.utils;

import androidx.annotation.IntDef;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 时间选择工具
 *
 * @author qf
 * @version 1.0
 * @date 2020/11/2
 **/
public class DateKeysUtils {
    /**
     * 审批
     */

    public static final int ALL = 0;
    /**
     * 设置为年份选择
     */
    public static final int YEAR = 1;
    /**
     * 设置为月份选择
     */
    public static final int MONTH = 3;
    /**
     * 设置为日期选择
     */
    public static final int DATE = 2;

    /**
     * 设置为时间选择
     */
    public static final int TIME = 5;


    @IntDef({ALL, YEAR, MONTH, DATE, TIME})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }


    public static boolean[] setType(@Type int patternType) {
        boolean[] type = new boolean[]{true, true, true, true, true, true};
        switch (patternType) {
            case DateKeysUtils.ALL:
                type = new boolean[]{true, true, true, true, true, true};
                break;
            case DateKeysUtils.YEAR:
                type = new boolean[]{true, false, false, false, false, false};
                break;
            case DateKeysUtils.MONTH:
                type = new boolean[]{true, true, false, false, false, false};
                break;
            case DateKeysUtils.DATE:
                type = new boolean[]{true, true, true, false, false, false};
                break;
            case DateKeysUtils.TIME:
                type = new boolean[]{false, false, false, true, true, false};
                break;
        }
        return type;
    }

    public static String setPattern(@Type int patternType) {
        String pattern = "yyyy-MM-dd HH:mm:ss";
        switch (patternType) {
            case DateKeysUtils.ALL:
                pattern = "yyyy-MM-dd HH:mm:ss";
                break;
            case DateKeysUtils.YEAR:
                pattern = "yyyy";
                break;
            case DateKeysUtils.MONTH:
                pattern = "yyyy-MM";
                break;
            case DateKeysUtils.DATE:
                pattern = "yyyy-MM-dd";
                break;
            case DateKeysUtils.TIME:
                pattern = "HH:mm";
                break;
        }
        return pattern;
    }

}
