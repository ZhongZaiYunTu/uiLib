package com.zz.yt.lib.ui.utils;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ResourceUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.whf.android.jar.net.gson.HttpGsonUtils;
import com.whf.android.jar.util.FormatUtil;
import com.whf.android.jar.util.number.DoubleUtils;

import java.util.Date;
import java.util.List;

public class FestivalUtils {

    private final List<FestivalBean> beanList;
    private static FestivalUtils instance;

    public static FestivalUtils getInstance() {
        if (instance == null) {
            instance = new FestivalUtils();
        }
        return instance;
    }

    public FestivalUtils() {
        final String assets = ResourceUtils.readAssets2String("festival_240423.json");
        beanList = HttpGsonUtils.fromJsonList(assets, FestivalBean.class);
    }

    public List<FestivalBean> getBeanList() {
        return beanList;
    }

    @NonNull
    public String getDay(Date date) {
        if (date == null) {
            return "";
        }
        int mm = DoubleUtils.objToInt(TimeUtils.date2String(date, "MM"));
        int dd = DoubleUtils.objToInt(TimeUtils.date2String(date, "dd"));
        if (!beanList.isEmpty()) {
            for (FestivalBean bean : beanList) {
                if (mm == bean.month && dd == bean.day) {
                    return bean.name;
                }
            }
        }
        if (FormatUtil.isToday(date)) {
            return "今";
        }
        return dd + "";
    }

    public static class FestivalBean {

        int month;
        int day;
        String name;

        public int getMonth() {
            return month;
        }

        public int getDay() {
            return day;
        }

        public String getName() {
            return name;
        }

    }
}
