package com.zz.yt.lib.ui.mvp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.zz.yt.lib.ui.base.mvp.ui.LatteTitleMvpFragment;
import com.zz.yt.lib.ui.callback.RequestCodes;
import com.zz.yt.lib.ui.mvp.contract.ExampleContract;
import com.zz.yt.lib.ui.mvp.model.ExampleModel;
import com.zz.yt.lib.ui.mvp.presenter.ExamplePresenter;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;


/***
 * 示例
 * @author qf
 * @version 1.0
 */
public abstract class ExampleFragment<adapter extends BaseQuickAdapter<Entry, MultipleViewHolder>, Entry extends MultiItemEntity>
        extends LatteTitleMvpFragment<ExampleModel, ExampleContract.View<Entry>, ExamplePresenter<Entry>>
        implements ExampleContract.View<Entry> {


    /**
     * 适配器
     */
    protected List<Entry> data = new ArrayList<>();

    /**
     * 适配器
     */
    protected adapter mAdapter;

    /********************************* 分页数据处理  *****************************************/

    protected final WeakHashMap<String, Object> PARAMS = new WeakHashMap<>();

    /**
     * 当前页数
     */
    protected int page = 1;
    /**
     * 每页查询多少条
     */
    protected int pageSize = 10;


    protected void params() {
        params("pageNumber", "pageSize");
    }

    protected void params(String keyNumber, String keySize) {
        PARAMS.put(keyNumber, page);
        PARAMS.put(keySize, pageSize);
    }

    /********************************* RecyclerView  *****************************************/

    protected void inRecycler(RecyclerView mRecyclerView) {
        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(getLayoutManager());
            mAdapter = getAdapter();
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    /**
     * @return LayoutManager
     */
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(context);
    }

    /**
     * @return 设置你的适配器
     */
    protected abstract adapter getAdapter();

    @Override
    public ExampleModel createModel() {
        return new ExampleModel(this, this);
    }

    @Override
    public ExampleContract.View<Entry> createView() {
        return this;
    }

    @Override
    public ExamplePresenter<Entry> createPresenter() {
        return new ExamplePresenter<>();
    }


    /********************************* 操作成功回调，刷新数据  *****************************************/

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RequestCodes.REFRESH) {
            refreshing();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RequestCodes.REFRESH) {
            refreshing();
        }
    }

    /**
     * 刷新
     */
    protected abstract void refreshing();

}
