package com.zz.yt.lib.ui.base.bottom.item;


import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.dao.UserDao;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.popup.view.TipsMinPopup;

import me.yokeyword.fragmentation.ISupportFragment;

/**
 * @author 傅令杰
 */

public abstract class BaseBottomItemDelegate extends LatteDelegate {
    /**
     * 再点一次退出程序时间设置
     */
    private static final long WAIT_TIME = 2000L;
    private long mTouchTime = 0;

    @Override
    public boolean onBackPressedSupport() {
        if (System.currentTimeMillis() - mTouchTime < WAIT_TIME) {
            _mActivity.finish();
            System.exit(0);
        } else {
            mTouchTime = System.currentTimeMillis();
            ToastUtils.showShort("再次点击退出" + StringUtils.getString(R.string.app_name));
        }
        return true;
    }


    /**
     * @param toFragment:密码修改
     */
    public void pass(ISupportFragment toFragment) {
        pass(true, "123456", toFragment);
    }

    /**
     * @param is:是否开启debug不使用提示。
     * @param key:初始密码
     * @param toFragment:密码修改
     */
    public void pass(boolean is, String key, final ISupportFragment toFragment) {
        if (is && Latte.getDebug()) {
            return;
        }
        //密码为初始密码，提示修改
        final String pass = UserDao.getUserPass();
        if (pass.equals(key)) {
            TipsMinPopup.create()
                    .setTitle("您的密码过于简单，请修改")
                    .setText("修改")
                    .setOnClickListener(new OnClickDecisionListener() {
                        @Override
                        public void onConfirm(Object decision) {
                            if (getParentDelegate() != null) {
                                getParentDelegate().getSupportDelegate().start(toFragment);
                            } else {
                                getSupportDelegate().start(toFragment);
                            }
                        }

                        @Override
                        public void onCancel() {

                        }
                    })
                    .popup();
        }
    }

}
