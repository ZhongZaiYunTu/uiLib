package com.zz.yt.lib.ui.callback;


import android.view.View;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

/**
 * 列表item项回调
 *
 * @author qf
 * @version 1.0
 **/
public interface OnRecyclerAddedCallback extends OnRecyclerItemCallback {

    /**
     * 添加事件
     */
    void addChildClick();

    /**
     * 添加
     */
    Object newData();

    /**
     * 保存填写的数据
     *
     * @param entity：保存到这里面
     * @param view：获取控件
     */
    void setEntity(@NonNull Object entity, View view);

    /**
     * @param i:为true时，弹出填错提示
     * @param view:
     */
    boolean isEmpty(int i, View view);

    /**
     * 为true时，弹出填错提示
     */
    boolean canGetData();

}
