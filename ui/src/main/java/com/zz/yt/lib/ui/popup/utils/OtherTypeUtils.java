package com.zz.yt.lib.ui.popup.utils;

import android.view.View;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.contrarywind.interfaces.IPickerViewData;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickIntStrListener;
import com.zz.yt.lib.ui.popup.utils.picker.PickerPopupUtils;
import com.zz.yt.lib.ui.utils.ObjUtils;

import java.util.ArrayList;
import java.util.List;


/***
 * 其他类型
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 **/
public class OtherTypeUtils implements View.OnClickListener, OnClickIntObjListener {

    private OnClickIntStrListener mClickStringListener = null;

    private final EditLayoutBar mEditLayoutBar;
    private final List<PickerEntry> jsonArray;

    /**
     * 性别
     */
    public final static int SEX = 10;
    /**
     * 决定
     */
    public final static int DECIDE = 11;
    /**
     * 学历
     */
    public final static int EDUCATION = 12;

    @NonNull
    public static OtherTypeUtils create(EditLayoutBar editLayoutBar, @Types int type) {
        return new OtherTypeUtils(editLayoutBar, type);
    }

    private OtherTypeUtils(@NonNull EditLayoutBar editLayoutBar, int type) {
        this.mEditLayoutBar = editLayoutBar;
        editLayoutBar.setOnClickListener(this);
        jsonArray = new ArrayList<>();
        switch (type) {
            case SEX:
                jsonArray.add(new PickerEntry(0, "男"));
                jsonArray.add(new PickerEntry(1, "女"));
                break;
            case DECIDE:
                jsonArray.add(new PickerEntry(0, "是"));
                jsonArray.add(new PickerEntry(1, "否"));
                break;
            case EDUCATION:
                jsonArray.add(new PickerEntry(0, "小学"));
                jsonArray.add(new PickerEntry(1, "初中"));
                jsonArray.add(new PickerEntry(2, "高中"));
                jsonArray.add(new PickerEntry(3, "专科"));
                jsonArray.add(new PickerEntry(4, "本科"));
                //研究生(硕士研究生，博士研究生)
                jsonArray.add(new PickerEntry(5, "研究生"));
                break;
            default:
                jsonArray.add(new PickerEntry(0, "其他"));
                break;
        }
        setText();
    }

    /**
     * 设置事件
     */
    public OtherTypeUtils setOnClickIntStrListener(OnClickIntStrListener listener) {
        mClickStringListener = listener;
        return this;
    }

    @Override
    public void onClick(View v) {
        if (jsonArray == null || jsonArray.size() == 0) {
            ToastUtils.showShort("未查询到数据，请稍后再试。");
            return;
        }
        PickerPopupUtils.showPickerData(Latte.getActivity(),
                "", jsonArray, 0, this);
    }

    private void setText() {
        if (jsonArray == null || jsonArray.size() == 0) {
            ToastUtils.showShort("未查询到数据，请稍后再试。");
            return;
        }
        onClick(0, jsonArray.get(0));
    }

    @Override
    public void onClick(int code, Object index) {
        PickerEntry pickerEntry = ObjUtils.getField(index);
        if (pickerEntry != null) {
            final int tag = pickerEntry.getId();
            final String text = pickerEntry.getPickerViewText();
            mEditLayoutBar.getValueView().setTag(tag);
            mEditLayoutBar.getValueView().setText(text);
            if (mClickStringListener != null) {
                mClickStringListener.onClick(code, text);
            }
        }
    }


    @IntDef({SEX, DECIDE, EDUCATION})
    public @interface Types {

    }

    public static class PickerEntry implements IPickerViewData {


        private final int id;
        private final String name;

        public PickerEntry(int id, String name) {
            this.id = id;
            this.name = name;
        }


        public int getId() {
            return id;
        }


        @Override
        public String getPickerViewText() {
            return name;
        }

    }
}
