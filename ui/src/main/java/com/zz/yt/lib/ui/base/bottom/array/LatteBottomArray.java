package com.zz.yt.lib.ui.base.bottom.array;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;

import java.util.WeakHashMap;


/**
 * 纯列表类处理(基类)
 *
 * @author qf
 * @version 1.0
 **/
abstract class LatteBottomArray extends BaseBottomItemDelegate implements
        SwipeRefreshLayout.OnRefreshListener {

    /***  条件参数 */
    protected final WeakHashMap<String, Object> PARAMETER = new WeakHashMap<>();

    /***
     * 刷新控件
     */
    protected SwipeRefreshLayout mSwipeRefreshLayout = null;

    /***
     * 设置列表
     * @param recyclerView:数据处理
     */
    protected abstract void initRecyclerView(RecyclerView recyclerView);

    /***
     * 设置下拉刷新
     * @param swipeRefreshLayout:数据处理
     * @param recyclerView:数据处理
     */
    protected abstract void initRefreshHandler(SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView);


    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {

        mSwipeRefreshLayout = rootView.findViewById(R.id.id_swipe_Layout);
        initRefreshLayout();

        final RecyclerView recyclerView = rootView.findViewById(R.id.id_recycler_view);
        if (recyclerView != null) {
            initRecyclerView(recyclerView);
            //初始化数据处理
            initRefreshHandler(mSwipeRefreshLayout, recyclerView);
        }

        final ImageView imageAdded = rootView.findViewById(R.id.id_image_portrait);
        if (imageAdded != null) {
            initShadowView(imageAdded);
        }
    }

    private void initRefreshLayout() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeColors(Color.BLACK, Color.RED);
        }
    }

    protected void initShadowView(ImageView imageAdded) {

    }

    @Override
    public void onRefresh() {
        PARAMETER.clear();
        firstPage();
    }

    /**
     * 刷新
     */
    protected void firstPage() {

    }

    /**
     * @param url:刷新
     */
    protected void firstPage(String url) {

    }

}
