package com.zz.yt.lib.ui.view.abst;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.GridSpanSizeLookup;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;
import com.zz.yt.lib.ui.view.base.BaseRecycleLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * 默认列表控件
 *
 * @author qf
 * @version 2.5
 */
public abstract class BaseQuickListener<Entity> extends BaseRecycleLayout implements ILayoutAdapter,
        OnItemClickListener {

    protected RecycleLayoutAdapter<Entity> mRecycleAdapter;

    public BaseQuickListener(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mRecycleAdapter = new RecycleLayoutAdapter<>(layoutResId(), this);
        mRecycleAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(mRecycleAdapter);
    }

    protected abstract int layoutResId();

    @Override
    protected String setTitle() {
        return null;
    }

    @Override
    protected String setTime() {
        return null;
    }

    @Override
    protected String setMore() {
        return null;
    }

    @Override
    protected boolean setIsMore() {
        return false;
    }

    public void setData(List<Entity> data) {
        if (data != null && data.size() > 0) {
            if (mRecycleAdapter != null) {
                mRecycleAdapter.setNewInstance(data);
            }
        } else {
            recyclerView.setBackgroundResource(com.zz.yt.lib.ui.R.mipmap.iv_empty_data);
        }
    }

    public void setDiffNewData(List<Entity> data) {
        if (data != null && data.size() > 0) {
            if (mRecycleAdapter != null) {
                mRecycleAdapter.setDiffNewData(data);
            }
        } else {
            recyclerView.setBackgroundResource(com.zz.yt.lib.ui.R.mipmap.iv_empty_data);
        }
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view,
                            int position) {
        if (mRecycleAdapter != null) {
            Object entity = mRecycleAdapter.getData().get(position);
            if (mClickMoreListener != null) {
                mClickMoreListener.onClickMore(position, entity);
            }
        }
    }

    protected static class RecycleLayoutAdapter<Entity> extends BaseMvpRecyclerAdapter<Entity>
            implements GridSpanSizeLookup {

        private final ILayoutAdapter iLayoutAdapter;

        public RecycleLayoutAdapter(int layoutResId, ILayoutAdapter iLayoutAdapter) {
            super(layoutResId, new ArrayList<Entity>());
            this.iLayoutAdapter = iLayoutAdapter;
            //设置动画
            setAnimationEnable(true);
        }

        @Override
        public void setNewInstance(List<Entity> list) {
            super.setNewInstance(list);
            //宽度监听
            setGridSpanSizeLookup(this);
        }

        @Override
        protected void convert(@NonNull MultipleViewHolder holder, @NonNull Entity entity) {
            super.convert(holder, entity);
            if (iLayoutAdapter != null) {
                iLayoutAdapter.convert(holder, entity);
            }
        }

        @Override
        public int getSpanSize(@NonNull GridLayoutManager gridLayoutManager, int viewType, int position) {
            if (iLayoutAdapter != null) {
                return iLayoutAdapter.getSpanSize(getData().get(position));
            }
            return 1;
        }
    }
}
