package com.zz.yt.lib.ui.view.abst.multi;


import androidx.annotation.ColorInt;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.zz.yt.lib.ui.recycler.data.MenuEntity;

/**
 * 类型布局，适用于类型较少，业务不复杂的场景，便于快速使用。
 *
 * @author qf
 * @version 2.5
 */
public class MenuMultiEntity extends MenuEntity implements MultiItemEntity {

    private int itemType;

    public MenuMultiEntity(int imgError, String textName) {
        super(imgError, textName);
    }

    public MenuMultiEntity(int num, int imgError, String textName) {
        super(num, imgError, textName);
    }

    public MenuMultiEntity(int num, String picture, int imgError, String textName) {
        super(num, picture, imgError, textName);
    }

    @Override
    public MenuMultiEntity setOther(String other) {
        super.setOther(other);
        return this;
    }

    @Override
    public MenuMultiEntity setColorBack(String colorBack) {
        super.setColorBack(colorBack);
        return this;
    }

    @Override
    public MenuMultiEntity setColorBack(@ColorInt int colorBack) {
        super.setColorBack(colorBack);
        return this;
    }

    @Override
    public MenuMultiEntity setSpanSize(int spanSize) {
        super.setSpanSize(spanSize);
        return this;
    }


    @Override
    public MenuMultiEntity setObj(Object obj) {
        super.setObj(obj);
        return this;
    }

    /**
     * @param itemType:多布局的标识
     */
    public MenuMultiEntity setItemType(int itemType) {
        this.itemType = itemType;
        return this;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

}
