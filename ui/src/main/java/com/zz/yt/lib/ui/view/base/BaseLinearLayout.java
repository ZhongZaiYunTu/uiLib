package com.zz.yt.lib.ui.view.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.base.delegate.IPageLoadListener;
import com.whf.android.jar.net.callback.IError;
import com.whf.android.jar.net.callback.IFailure;
import com.whf.android.jar.net.callback.ISuccess;
import com.whf.android.jar.net.utils.RestClientUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.listener.OnClickIntBoolListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickIntStrListener;
import com.zz.yt.lib.ui.listener.OnClickMoreListener;
import com.zz.yt.lib.ui.listener.OnClickStrObjListener;
import com.zz.yt.lib.ui.utils.ObjUtils;

import java.util.WeakHashMap;

/**
 * 默认LinearLayout
 *
 * @author qf
 * @version 1.0
 **/
public abstract class BaseLinearLayout extends AbstractLinearLayout implements IFailure, IError {

    protected IPageLoadListener mIPageLoadListener = null;
    protected OnClickIntBoolListener mOnClickIntBoolListener = null;
    protected OnClickIntObjListener mClickIntObjListener = null;
    protected OnClickIntStrListener mClickIntStrListener = null;
    protected OnClickStrObjListener mClickStrObjListener = null;
    protected OnClickMoreListener mClickMoreListener = null;
    private IFailure mFailure = null;
    private IError mError = null;

    protected abstract int setLayout();

    public BaseLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        try {
            if (setLayout() > 0) {
                LayoutInflater.from(context).inflate(setLayout(), this, true);
            }
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
    }

    @Override
    public void setPageLoadListener(IPageLoadListener listener) {
        this.mIPageLoadListener = listener;
    }

    @Override
    public void setOnClickIntBoolListener(OnClickIntBoolListener listener) {
        this.mOnClickIntBoolListener = listener;
    }

    @Override
    public void setOnClickIntObjListener(OnClickIntObjListener listener) {
        this.mClickIntObjListener = listener;
    }

    @Override
    public void setOnClickIntStrListener(OnClickIntStrListener listener) {
        this.mClickIntStrListener = listener;
    }

    @Override
    public void setOnClickStrObjListener(OnClickStrObjListener listener) {
        this.mClickStrObjListener = listener;
    }

    @Override
    public void setOnClickMoreListener(OnClickMoreListener listener) {
        this.mClickMoreListener = listener;
    }

    @Override
    public void setFailure(IFailure mFailure) {
        this.mFailure = mFailure;
    }

    @Override
    public void setError(IError mError) {
        this.mError = mError;
    }

    @Override
    public void onError(int code, String msg) {
        if (mIPageLoadListener != null) {
            mIPageLoadListener.hideLoading();
        }
        if (mError != null) {
            mError.onError(code, msg);
        } else {
            if (ObjectUtils.isEmpty(msg)) {
                ToastUtils.showShort("服务器异常，请联系管理员");
            } else {
                ToastUtils.showShort(msg);
            }
        }
    }

    @Override
    public void onFailure() {
        if (mIPageLoadListener != null) {
            mIPageLoadListener.hideLoading();
        }
        if (mFailure != null) {
            mFailure.onFailure();
        } else {
            ToastUtils.showShort("连接服务器失败，请稍后再试。");
        }
    }

    protected final void inGet(String url, ISuccess success) {
        RestClientUtils.create(mIPageLoadListener, this)
                .inGet(url, success);
    }

    protected final void inGet(String url, String ids, ISuccess success) {
        RestClientUtils.create(mIPageLoadListener, this)
                .inGet(url, ids, success);
    }

    protected final void inGet(String url, WeakHashMap<String, Object> params, ISuccess success) {
        LatteLogger.json(GsonUtils.toJson(params));
        RestClientUtils.create(mIPageLoadListener, this)
                .inGet(url, params, success);
    }

    protected final void inPost(String url, WeakHashMap<String, Object> params, ISuccess success) {
        RestClientUtils.create(mIPageLoadListener, this)
                .inPost(url, params, success);
    }

    protected final void inPost(String url, String data, ISuccess success) {
        RestClientUtils.create(mIPageLoadListener, this)
                .inPost(url, data, success);
    }

    protected final void inPost(String url, ISuccess success) {
        RestClientUtils.create(mIPageLoadListener, this)
                .inPost(url, success);
    }

    /******************************** 数据湖de请求方法     ****************************************/

    protected void inNetLake(int code, ISuccess success) {
        inNetLake(code, null, success);
    }

    protected void inNetLake(int code, String key, Object obj, ISuccess success) {
        inNetLake(code, ObjUtils.getFieldWeak(key, obj), success);
    }

    protected void inNetLake(int code, WeakHashMap<String, Object> params, ISuccess success) {
        WeakHashMap<String, Object> paramsCode = ObjUtils.getFieldWeak("code", code);
        if (ObjectUtils.isNotEmpty(params)) {
            paramsCode.put("params", GsonUtils.toJson(params));
        }
        inPost(Latte.getField(), params, success);
    }

    /******************************* 数据湖de请求方法（Bi等其他）     ***********************************/

    protected void inNetLakeBi(int code, ISuccess success) {
        inNetLakeBi(Latte.getBiServer(), code, null, success);
    }

    protected void inNetLakeBi(int code, String key, Object obj, ISuccess success) {
        inNetLakeBi(Latte.getBiServer(), code, ObjUtils.getFieldWeak(key, obj), success);
    }

    protected void inNetLakeBi(int code, WeakHashMap<String, Object> params, ISuccess success) {
        inNetLakeBi(Latte.getBiServer(), code, params, success);
    }

    protected void inNetLakeBi(String url, int code, WeakHashMap<String, Object> params, ISuccess success) {
        WeakHashMap<String, Object> paramsCode = ObjUtils.getFieldWeak("code", code);
        if (ObjectUtils.isNotEmpty(params)) {
            paramsCode.put("params", GsonUtils.toJson(params));
        }
        inPost(url, params, success);
    }


}
