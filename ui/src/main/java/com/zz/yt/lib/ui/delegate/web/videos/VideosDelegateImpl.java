package com.zz.yt.lib.ui.delegate.web.videos;

import android.webkit.WebChromeClient;

import androidx.annotation.NonNull;

import com.whf.android.jar.web.WebDelegateImpl;


public class VideosDelegateImpl extends WebDelegateImpl {


    protected VideosDelegateImpl(String url, boolean handleWebUrl) {
        super(url, handleWebUrl);
    }

    @NonNull
    public static VideosDelegateImpl create(String url) {
        return new VideosDelegateImpl(url,true);
    }

    @Override
    public WebChromeClient initWebChromeClient() {
        return new VideosViewClientImpl();
    }
}
