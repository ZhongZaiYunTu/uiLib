package com.zz.yt.lib.ui.listener;


/**
 * 点击事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickPageSizeListener {

    /**
     * 点击
     *
     * @param page:当前页数
     * @param pageSize:每页查询多少条
     */
    void onClick(int page, int pageSize);

}
