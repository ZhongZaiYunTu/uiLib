package com.zz.yt.lib.ui.view.abst;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

/**
 * 默认列表控件
 *
 * @author qf
 * @version 2.5
 */
public interface ILayoutAdapter {

    void convert(@NonNull MultipleViewHolder holder, @NonNull Object obj);

    int getSpanSize(@NonNull Object obj);
}
