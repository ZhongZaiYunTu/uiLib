package com.zz.yt.lib.ui.view.calendar;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.mvp.base.view.BaseMvpRecyclerLayout;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.callback.OnSelectDateCallback;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;
import com.zz.yt.lib.ui.view.calendar.month.CalendarMonthView;

import java.util.Calendar;
import java.util.Date;

/**
 * 日历控件
 *
 * @author qf
 * @version 1.0.1
 */
public class CalendarSelectView extends BaseMvpRecyclerLayout<CalendarSelectEntity> {

    /*** 是否是时间段选择 */
    private boolean isRadio = false;

    /*** true(只能选择今天及以后的时间) */
    private boolean isToday = false;

    /*** 选中的时间 */
    private Date startTime, endTime;

    /*** 选中的时间的提示文字 */
    private String startHint = "起", endHint = "止";
    private boolean dateSelect;


    public CalendarSelectView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int setLayout() {
        return R.layout.a_ui_view_recycler;
    }

    @Override
    protected RecyclerView setRecyclerView() {
        return findViewById(R.id.rl_customer);
    }

    /*** 是否是时间段选择 */
    public void setRadio(boolean radio) {
        isRadio = radio;
    }

    /*** true(只能选择今天及以后的时间) */
    public void setIsToday(boolean over) {
        isToday = over;
    }

    /*** 选中的时间 */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /*** 选中的时间 */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /*** 选中的提示 */
    public void setSelectHint(String startHint, String endHint) {
        this.startHint = startHint;
        this.endHint = endHint;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    @Override
    public int setLayoutItem() {
        return R.layout.hai_view_calendar_monthe;
    }

    @Override
    public void convert(@NonNull MultipleViewHolder holder, @NonNull Object entity, Object other) {
        final CalendarSelectEntity calendar = dataItem.get(holder.position());
        holder.setText(R.id.tv_title, calendar.getMonth());
        final CalendarMonthView monthView = holder.getView(R.id.tv_calendar_month_view);
        if (monthView != null) {
            monthView.setRadio(isRadio);
            monthView.setOver(isToday);
            monthView.setStartTime(startTime);
            monthView.setEndTime(endTime);
            monthView.setData(calendar.getList());
            monthView.setSelectHint(startHint, endHint);
            monthView.setOnSelectDateCallback(new OnSelectDateCallback() {
                @Override
                public void selectDate(Date item, Date endDate) {
                    if (isRadio) {
                        startTime = item;
                        endTime = null;
                    } else {
                        if (dateSelect) {
                            startTime = item;
                            endTime = null;
                        } else {
                            isWithin(item);
                        }
                        dateSelect = !dateSelect;
                    }
                    notifyDataSetChanged();
                }
            });
        }

    }

    /**
     * @param item:是否在选择的时间段内
     */
    protected void isWithin(Date item) {
        if (item == null) {
            return;
        }
        Calendar start = Calendar.getInstance();
        start.setTime(startTime);
        Calendar end = Calendar.getInstance();
        end.setTime(item);
        if (start.after(end)) {
            startTime = item;
            endTime = null;
            dateSelect = true;
        } else {
            endTime = item;
        }
    }

}
