package com.zz.yt.lib.ui.mvp.model;


import com.whf.android.jar.base.delegate.IPageLoadListener;
import com.whf.android.jar.base.mvp.model.LatteModel;
import com.whf.android.jar.net.callback.IError;
/**
 * 示例的网络操作层
 *
 * @author lzl
 * @version 1.0
 */
public class ExampleModel extends LatteModel {

    public ExampleModel(IPageLoadListener listener, IError error) {
        super(listener, error);
    }

}
