package com.zz.yt.lib.ui.delegate.record;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.array.BaseArrayRefreshDelegate;

/***
 * 默认的列表界面
 * @author qf
 * @version 1.0
 */
public abstract class BaseRefreshDelegate extends BaseArrayRefreshDelegate<LatteRefreshHandler> {

    /**
     * 是否是全屏
     */
    private boolean isTop = true;


    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        isTop = bundle.getBoolean(BundleKey.IS_TOP.name(), true);
    }

    @Override
    protected boolean isTopView() {
        return isTop;
    }

    @Override
    protected void initRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(Latte.getActivity()));
    }

    @Override
    protected LatteRefreshHandler setRefreshHandler(RecyclerView recyclerView) {
        return LatteRefreshHandler.create(this, mSwipeRefreshLayout, recyclerView, setDataConverter());
    }

}
