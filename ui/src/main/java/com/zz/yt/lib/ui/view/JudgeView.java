package com.zz.yt.lib.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.R;


public class JudgeView extends RelativeLayout implements RadioGroup.OnCheckedChangeListener {
    private final TextView titleBarTitle;
    private final RadioGroup radioGroup;
    private final RadioButton radioButtonYes;
    private final RadioButton radioButtonNo;

    private boolean jude;

    public JudgeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_view_judge, this, true);
        titleBarTitle = findViewById(R.id.id_judge_text);
        radioGroup = findViewById(R.id.id_judge_group);
        radioButtonYes = findViewById(R.id.id_judge_btn_yes);
        radioButtonNo = findViewById(R.id.id_judge_btn_no);

        radioGroup.setOnCheckedChangeListener(this);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.JudgeView);
        //如果不是图片标题 则获取文字标题
        String titleText = attributes.getString(R.styleable.JudgeView_android_text);
        if (!TextUtils.isEmpty(titleText)) {
            titleBarTitle.setText(titleText);
        }
        //获取标题显示颜色
        int titleTextColor = attributes.getColor(R.styleable.JudgeView_android_textColor, Color.BLACK);
        titleBarTitle.setTextColor(titleTextColor);

        //获取标题显示颜色
        jude = attributes.getBoolean(R.styleable.JudgeView_jude, true);
        if (jude) {
            radioButtonYes.setChecked(true);
        } else {
            radioButtonNo.setChecked(true);
        }

        //获取标题显示颜色
        int titleTextSize = attributes.getInt(R.styleable.JudgeView_android_textSize, -1);
        if (titleTextSize != -1) {
            titleBarTitle.setTextSize(titleTextSize);
        }
        String titleTextYes = attributes.getString(R.styleable.JudgeView_value_text_yes);
        if (!TextUtils.isEmpty(titleTextYes)) {
            radioButtonYes.setText(titleTextYes);
        }
        String titleTextNo = attributes.getString(R.styleable.JudgeView_value_text_no);
        if (!TextUtils.isEmpty(titleTextNo)) {
            radioButtonNo.setText(titleTextNo);
        }

        attributes.recycle();
    }

    public TextView getTitleBarTitle() {
        return titleBarTitle;
    }

    public RadioGroup getRadioGroup() {
        return radioGroup;
    }

    public RadioButton getRadioButtonYes() {
        return radioButtonYes;
    }

    public RadioButton getRadioButtonNo() {
        return radioButtonNo;
    }

    public boolean isJude() {
        return jude;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton bGroup = findViewById(checkedId);
        if (radioButtonYes == bGroup) {
            jude = true;
        } else if (radioButtonNo == bGroup) {
            jude = false;
        }
    }
}
