package com.zz.yt.lib.ui.recycler.divider;


import androidx.annotation.ColorInt;

import com.choices.divider.DividerItemDecoration;

/**
 * 分割线
 *
 * @author qf
 * @version 2020-05-15
 */

public class BaseDecoration extends DividerItemDecoration {

    private BaseDecoration(@ColorInt int color, int size) {
        setDividerLookup(new DividerLookupImpl(color, size));
    }

    public static BaseDecoration create(@ColorInt int color, int size) {
        return new BaseDecoration(color, size);
    }
}
