package com.zz.yt.lib.ui.aes;

/**
 * aes 加密模式
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/4
 **/
public interface AesKeys {


    String KEY_AES = "0123456789abcdef";

    /**
     * CBC
     */
    String AES_CBC_NO_PADDING = "AES/CBC/NoPadding";

    String AES_CBC_PK_CS5_PADDING = "AES/CBC/PKCS5Padding";

    String AES_CBC_PK_CS7_PADDING = "AES/CBC/pkcs7padding";

    String AES_CBC_ISO10126PADDING = "AES/CBC/ISO10126Padding";

    /**
     * CFB
     */
    String AES_CFB_PK_NO_PADDING = "AES/CFB/NoPadding";

    String AES_CFB_PK_CS5_PADDING = "AES/CFB/PKCS5Padding";

    String AES_CFB_PK_CS7_PADDING = "AES/CFB/PKCS7Padding";

    String AES_CFB_PK_ISO10126PADDING = "AES/CFB/ISO10126Padding";

    /**
     * ECB
     */
    String AES_ECB_PK_NO_PADDING = "AES/ECB/NoPadding";

    String AES_ECB_PK_CS5_PADDING = "AES/ECB/PKCS5Padding";

    String AES_ECB_PK_CS7_PADDING = "AES/ECB/PKCS57Padding";

    String AES_ECB_PK_ISO10126PADDING = "AES/ECB/ISO10126Padding";

    /**
     * ofb
     */
    String AES_OFB_PK_NO_PADDING = "AES/OFB/NoPadding";

    String AES_OFB_PK_CS5_PADDING = "AES/OFB/PKCS5Padding";

    String AES_OFB_PK_CS7_PADDING = "AES/OFB/PKCS7Padding";

    String AES_OFB_PK_ISO10126PADDING = "AES/OFB/ISO10126Padding";

    /**
     * PCBC
     */
    String AES_P_CBC_PK_NO_PADDING = "AES/PCBC/NoPadding";

    String AES_P_CBC_PK_CS5_PADDING = "AES/PCBC/PKCS5Padding";

    String AES_P_CBC_PK_CS7_PADDING = "AES/PCBC/PKCS7Padding";

    String AES_P_CBC_PK_ISO10126PADDING = "AES/PCBC/ISO10126Padding";


}
