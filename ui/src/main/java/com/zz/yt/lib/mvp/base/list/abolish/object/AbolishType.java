package com.zz.yt.lib.mvp.base.list.abolish.object;

/**
 * 这里记录了 详情  适配器的类型
 *
 * @author gags
 * @version 1.0
 */
public interface AbolishType {
    /**
     * 单行 文本框(输入 和 显示 文本框)
     */
    int TEXT = 2333001;

    /**
     * 选择
     */
    int TEXT_2 = 2333002;


}
