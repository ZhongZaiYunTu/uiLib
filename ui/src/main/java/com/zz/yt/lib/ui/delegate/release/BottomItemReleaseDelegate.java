package com.zz.yt.lib.ui.delegate.release;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;

/**
 * 突出按钮的空白界面
 *
 * @author qf
 * @version 1.0
 **/
public class BottomItemReleaseDelegate extends BaseBottomItemDelegate {

    @NonNull
    public static BottomItemReleaseDelegate create() {
        Bundle args = new Bundle();
        BottomItemReleaseDelegate fragment = new BottomItemReleaseDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_delegate_ui_null;
    }

    @Override
    protected void onBindView(@Nullable Bundle savedInstanceState,
                              @NonNull View rootView) {

    }
}
