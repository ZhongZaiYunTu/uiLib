package com.zz.yt.lib.ui.base;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * 接口的实现类
 *
 * @author qf
 * @version 1.0
 */
public interface IResponse {

    /**
     * 预览附件
     *
     * @param fileUrl：
     */
    void inFile(@NonNull String fileUrl);

    /***
     * 获得保存处理数据
     */
    boolean isFile();

    /***
     * 当不是必传时
     * 获得保存处理数据
     */
    String getFile();
}
