package com.zz.yt.lib.ui.popup.view.base;

import android.content.Context;

import androidx.annotation.NonNull;

import com.whf.android.jar.popup.LattePopupAttach;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickIntStrListener;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.listener.OnClickStringsListener;

/**
 * 依附于某个View的弹窗
 *
 * @author qf
 * @version 1.0.4
 **/
public abstract class BaseAttachPopup extends LattePopupAttach {

    protected OnClickIntObjListener mClickIntObjListener = null;
    protected OnClickStringListener mClickStringListener = null;
    protected OnClickStringsListener mClickStringsListener = null;

    public BaseAttachPopup(@NonNull Context context) {
        super(context);
    }

    /**
     * @param listener：添加事件
     */
    public BaseAttachPopup setOnClickIntObjListener(OnClickIntObjListener listener) {
        this.mClickIntObjListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseAttachPopup setOnClickListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseAttachPopup setOnClickListener(OnClickStringsListener listener) {
        this.mClickStringsListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseAttachPopup setOnClickListener(OnClickDecisionListener listener) {
        this.mClickDecisionListener = listener;
        return this;
    }


}