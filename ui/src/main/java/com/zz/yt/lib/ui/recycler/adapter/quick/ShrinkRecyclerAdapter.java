package com.zz.yt.lib.ui.recycler.adapter.quick;


import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.whf.android.jar.app.Latte;
import com.youth.banner.listener.OnBannerListener;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.listener.OnClickIntBoolListener;
import com.zz.yt.lib.ui.listener.OnClickIntCheckListener;
import com.zz.yt.lib.ui.listener.OnClickIntListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickIntStrListener;
import com.zz.yt.lib.ui.listener.OnClickObjectListener;
import com.zz.yt.lib.ui.listener.OnClickStrObjListener;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.listener.OnClickTableListener;
import com.zz.yt.lib.ui.recycler.adapter.IAdapterClickListener;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.List;

/**
 * 单类型布局
 *
 * @author oa
 * @version 1.0
 * @date 2020/4/24
 */
public class ShrinkRecyclerAdapter<T> extends BaseQuickAdapter<T, MultipleViewHolder>
        implements IAdapterClickListener {

    protected OnBannerListener mBannerListener = null;
    protected OnTabSelectListener mTabSelectListener = null;
    protected OnClickIntListener mClickIntListener = null;
    protected OnClickIntBoolListener mClickIntBoolListener = null;
    protected OnClickIntObjListener mClickIntObjListener = null;
    protected OnClickIntStrListener mClickIntStrListener = null;
    protected OnClickObjectListener mClickObjectListener = null;
    protected OnClickStringListener mClickStringListener = null;
    protected OnClickStrObjListener mClickStrObjListener = null;
    protected OnClickTableListener mClickTableListener = null;
    protected OnClickIntCheckListener mClickIntCheckListener = null;
    protected OnClickDecisionListener mClickDecisionListener = null;

    protected ShrinkRecyclerAdapter(int layout, List<T> data) {
        super(layout, data);
    }

    @Override
    protected void convert(@NonNull MultipleViewHolder holder, @NonNull T entity) {

    }

    /************************************** Set Listener *****************************************/

    @Override
    public void setOnBannerListener(OnBannerListener listener) {
        this.mBannerListener = listener;
    }

    @Override
    public void setOnTabSelectListener(OnTabSelectListener listener) {
        this.mTabSelectListener = listener;
    }

    @Override
    public void setOnClickIntListener(OnClickIntListener listener) {
        this.mClickIntListener = listener;
    }

    @Override
    public void setOnClickIntBoolListener(OnClickIntBoolListener listener) {
        this.mClickIntBoolListener = listener;
    }

    @Override
    public void setOnClickIntObjListener(OnClickIntObjListener listener) {
        this.mClickIntObjListener = listener;
    }

    @Override
    public void setOnClickIntStrListener(OnClickIntStrListener listener) {
        this.mClickIntStrListener = listener;
    }

    @Override
    public void setOnClickObjectListener(OnClickObjectListener listener) {
        this.mClickObjectListener = listener;
    }

    @Override
    public void setOnClickStringListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
    }

    @Override
    public void setOnClickStrObjListener(OnClickStrObjListener listener) {
        this.mClickStrObjListener = listener;
    }

    @Override
    public void setOnClickTableListener(OnClickTableListener listener) {
        this.mClickTableListener = listener;
    }

    @Override
    public void setOnClickIntCheckListener(OnClickIntCheckListener listener) {
        this.mClickIntCheckListener = listener;
    }

    @Override
    public void setOnClickDecisionListener(OnClickDecisionListener listener) {
        this.mClickDecisionListener = listener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void onRefresh() {
        Latte.getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        }, 222);
    }
}