package com.zz.yt.lib.ui.base.bottom;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;
import com.zz.yt.lib.ui.entity.BottomTabEntry;


import java.util.LinkedHashMap;

/**
 * @author 傅令杰
 */
public final class ItemBuilder {

    private final LinkedHashMap<BottomTabEntry, BaseBottomItemDelegate> ITEMS = new LinkedHashMap<>();

    @NonNull
    static ItemBuilder builder() {
        return new ItemBuilder();
    }

    public final ItemBuilder addItem(BottomTabEntry bean, BaseBottomItemDelegate delegate) {
        ITEMS.put(bean, delegate);
        return this;
    }

    public final ItemBuilder addItems(LinkedHashMap<BottomTabEntry, BaseBottomItemDelegate> items) {
        ITEMS.putAll(items);
        return this;
    }

    public final LinkedHashMap<BottomTabEntry, BaseBottomItemDelegate> build() {
        return ITEMS;
    }
}
