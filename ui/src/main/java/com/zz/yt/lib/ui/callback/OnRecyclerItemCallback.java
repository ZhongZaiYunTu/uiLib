package com.zz.yt.lib.ui.callback;


import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

/**
 * 列表item项添加回调
 *
 * @author qf
 * @version 1.0
 **/
public interface OnRecyclerItemCallback {

    /**
     * 列表的item布局
     */
    int setLayoutItem();

    /**
     * @param holder:布局对象
     * @param entity：item数据
     * @param other：其他属性
     */
    void convert(MultipleViewHolder holder, @NonNull Object entity, Object other);

}
