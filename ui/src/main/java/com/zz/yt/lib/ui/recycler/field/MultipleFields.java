package com.zz.yt.lib.ui.recycler.field;

/**
 * 数据Kay
 *
 * @author qf
 * @version 2.5.15
 */
public enum MultipleFields {

    /*** 布局类型 */
    ITEM_TYPE,

    /*** 布局权重 */
    SPAN_SIZE,

    /*** id */
    ID,

    /*** NO */
    NO,

    /*** code */
    CODE,

    /*** GONE */
    GONE,

    /*** SHOW */
    SHOW,

    /*** 标签 */
    LABEL,

    /*** 提示 */
    HINT,

    /*** 提示 */
    TIPS,


    /*** 角标 */
    BADGE,

    /*** 实体 */
    ENTITY,

    /*** 实体 */
    BEAN,

    /*** 事件 */
    THING,

    /*** 表格数据 */
    TABLE,

    /*** 图标string[] */
    ICON_ARRAY,

    /*** 标题string[] */
    TITLE_ARRAY,

    /*** 标题上是否有空白 */
    IS_TITLE,

    /*** 标题 */
    TITLE,

    /*** 内容 */
    TEXT,

    /*** 内容颜色 */
    TEXT_COLOR,

    /*** 内容string[] */
    TEXT_ARRAY,

    /*** 内容de状态 */
    TEXT_STATUS,

    /*** url */
    URL,

    /*** 附件 */
    ANNEX,

    /*** 内容 */
    TEXT_TEXT,

    /*** 已读 */
    READ,

    /*** 状态 */
    STATUS,

    /*** 类型 */
    TYPE,

    /*** 点击 */
    CLICK,

    /*** 图片地址 */
    IMAGE,

    /*** 图片地址 */
    IMAGE_ARRAY,

    /*** 本地图片 */
    ICON_IMAGE,

    /*** 占位图片 */
    ERROR_IMAGE,

    /*** 滑动 */
    BANNERS,

    /*** 名称 */
    NAME,

    /*** 名称de状态*/
    NAME_STATUS,

    /*** 数量 */
    NUMBER,

    /*** 金额 */
    MONEY,

    /*** 颜色 */
    COLOR,

    /*** 时间 */
    TIME,

    /*** 时间de状态 */
    TIME_STATUS,

    /*** 时间 string[] */
    TIME_ARRAY,

    /*** 时间开始 */
    TIME_START,

    /*** 时间结束 */
    TIME_END,

    /*** 值 */
    VALUE,

    /*** 值de状态 */
    VALUE_STATUS,

    /*** 当前 */
    PROGRESS,

    /*** 步骤 */
    STEP,

    /*** 最大值 */
    MAX,

    /*** 签名 */
    SIGNATURE,

    /*** 其他 */
    TAG
}
