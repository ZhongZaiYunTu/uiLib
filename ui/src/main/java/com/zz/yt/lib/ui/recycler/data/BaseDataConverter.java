package com.zz.yt.lib.ui.recycler.data;

import android.graphics.Color;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.whf.android.jar.constants.HttpConstant;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据解析类
 *
 * @author oa
 * @date 2020-05-15
 */
public abstract class BaseDataConverter {

    protected final ArrayList<MultipleItemEntity> ENTITIES = new ArrayList<>();
    private String mJsonData = null;

    /**
     * 数据解析
     *
     * @return 解析后的数据
     */
    public abstract ArrayList<MultipleItemEntity> convert();

    public BaseDataConverter setData(Object data) {
        return this;
    }

    public BaseDataConverter setJsonData(String json) {
        this.mJsonData = json;
        return this;
    }

    protected String getJsonData() {
        if (mJsonData == null || mJsonData.isEmpty()) {
            throw new NullPointerException("DATA IS NULL!");
        }
        return mJsonData;
    }

    /**
     * 数据解析
     *
     * @param type:
     * @return 解析后的数据
     */
    protected <T> List<T> fromJson(final Type type) {
        try {
            return GsonUtils.fromJson(mJsonData, GsonUtils.getListType(type));
        } catch (Exception e) {
            LatteLogger.e("接口字段类型改了；" + e.getMessage());
            return null;
        }
    }

    /**
     * 数据解析(分页)
     *
     * @param type:
     * @return 解析后的数据
     */
    protected <T> List<T> fromJsonRecords(final Type type) {
        try {
            final String records = JsonUtils.getString(mJsonData, HttpConstant.RECORDS);
            return GsonUtils.fromJson(records, GsonUtils.getListType(type));
        } catch (Exception e) {
            LatteLogger.e("接口字段类型改了；" + e.getMessage());
            return null;
        }
    }

    /**
     * 数据解析(bu分页)
     *
     * @param type:
     * @return 解析后的数据
     */
    protected <T> List<T> fromJsonList(final Type type) {
        try {
            final String lists = JsonUtils.getString(mJsonData, HttpConstant.LIST);
            return GsonUtils.fromJson(lists, GsonUtils.getListType(type));
        } catch (Exception e) {
            LatteLogger.e("接口字段类型改了；" + e.getMessage());
            return null;
        }
    }

    /**
     * 设置每行详情(靠左)
     *
     * @param title:
     */
    protected void setTitle(String title) {
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.TITLE)
                .setField(MultipleFields.SPAN_SIZE, 1)
                .setField(MultipleFields.TEXT, title)
                .build());
    }

    /**
     * 设置每行详情(靠左;建议单行)
     *
     * @param title:
     * @param text:
     */
    protected void setDetail(String title, String text) {
        //默认黑色字体
        setDetail(title, text, false);
    }

    /**
     * 设置每行详情(靠左;建议单行)
     *
     * @param title:
     * @param text:
     */
    protected void setDetail(String title, String text, boolean valueColor) {
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.TEXT_TEXT)
                .setField(MultipleFields.SPAN_SIZE, 1)
                .setField(MultipleFields.TEXT_TEXT, new OrderFormEntity(title, text, valueColor, Color.WHITE))
                .build());
    }

    /**
     * 设置每行详情（靠左）
     *
     * @param title:
     * @param text:
     */
    protected void setMultiple(String title, String text) {
        //默认黑色字体
        setMultiple(title, text, false);
    }

    /**
     * 设置每行详情（靠左）
     *
     * @param title:
     * @param text:
     */
    protected void setMultiple(String title, String text, boolean valueColor) {
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.TEXT_MULTIPLE)
                .setField(MultipleFields.SPAN_SIZE, 1)
                .setField(MultipleFields.TEXT_TEXT, new OrderFormEntity(title, text, valueColor, Color.WHITE))
                .build());
    }

    /**
     * 设置每行详情（左右分布）
     *
     * @param title:
     * @param text:
     */
    protected void setDetails(String title, String text) {
        //默认黑色字体
        setDetails(title, text, false);
    }

    /**
     * 设置每行详情（左右分布）
     *
     * @param title:
     * @param text:
     * @param valueColor:
     */
    protected void setDetails(String title, String text, boolean valueColor) {
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.TEXT_TEXTS)
                .setField(MultipleFields.SPAN_SIZE, 1)
                .setField(MultipleFields.TEXT_TEXT, new OrderFormEntity(title, text, valueColor, Color.WHITE))
                .build());
    }

    /**
     * @param text:设置附件
     */
    protected void setAnnex(List<AnnexBean> text) {
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.ANNEX)
                .setField(MultipleFields.SPAN_SIZE, 1)
                .setField(MultipleFields.ANNEX, text)
                .build());
    }

    /**
     * @param text:设置图片（String or int）
     */
    protected void setAnnexPhoto(List<Object> text) {
        ENTITIES.add(MultipleItemEntity.builder()
                .setItemType(ItemType.ANNEX_PHOTO)
                .setField(MultipleFields.SPAN_SIZE, 1)
                .setField(MultipleFields.IMAGE_ARRAY, text)
                .build());
    }

    /**
     * @param children:设置功能
     */
    protected void setMenuEntity(List<MenuEntity> children) {
        final int size = children == null ? 0 : children.size();
        for (int i = 0; i < size; i++) {
            final MenuEntity menuBean = children.get(i);
            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.IMAGE_NUM_NAME)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.ENTITY, menuBean)
                    .setField(MultipleFields.NUMBER, menuBean.getNum())
                    .setField(MultipleFields.IMAGE, menuBean.getPicture())
                    .setField(MultipleFields.ERROR_IMAGE, menuBean.getImgError())
                    .setField(MultipleFields.TEXT, menuBean.getTextName())
                    .setField(MultipleFields.CLICK, false)
                    .build());
        }
    }

    @NonNull
    protected StatusEntity setStatus(@NonNull String status, String statusCh) {
        switch (status) {
            case "0":
                return new StatusEntity(statusCh,
                        ColorUtils.string2Int("#2E82FF"),
                        R.drawable.status_frame_blue);
            case "1":
                return new StatusEntity(statusCh,
                        ColorUtils.string2Int("#FF5252"),
                        R.drawable.status_frame_red);
            case "2":
                return new StatusEntity(statusCh,
                        ColorUtils.string2Int("#4CAF50"),
                        R.drawable.status_frame_orange);
            default:
                return new StatusEntity(statusCh,
                        ColorUtils.string2Int("#989898"),
                        R.drawable.status_frame_gray);
        }
    }

    public void clearData() {
        ENTITIES.clear();
    }

}
