package com.zz.yt.lib.ui.view.calendar.month;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.zz.yt.lib.mvp.base.view.BaseMvpRecyclerLayout;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.callback.OnSelectDateCallback;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.Calendar;
import java.util.Date;

/**
 * 日历每月（对没天的设置）
 *
 * @author qf
 * @version 1.0.1
 */
public final class CalendarMonthView extends BaseMvpRecyclerLayout<CalendarMonthEntity> {

    /*** 是否是时间段选择 */
    private boolean isRadio = false;

    /*** 是否只能选择今天及以后的时间 */
    private boolean isOver = false;
    /*** 选中的时间 */
    private Date startTime, endTime;
    /*** 选中的时间的提示文字 */
    private String startHint = "起", endHint = "止";

    private OnSelectDateCallback mSelectDateCallback = null;

    public CalendarMonthView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        recyclerView.setPadding(15, 15, 15, 15);
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_view_recycler_date;
    }

    @Override
    protected RecyclerView setRecyclerView() {
        return findViewById(R.id.rl_customer);
    }

    @NonNull
    protected RecyclerView.LayoutManager layoutManager(Context context) {
        return new GridLayoutManager(context, 7);
    }

    public final void init(Calendar calendar) {
        setData(CalendarUtils.setData(calendar));
    }

    /**
     * @param radio:true是单选；false是时间段选择
     */
    public final void setRadio(boolean radio) {
        isRadio = radio;
    }

    /**
     * @param over:是否只能选择今天及以后的时间
     */
    public final void setOver(boolean over) {
        isOver = over;
    }

    /*** 选中的时间 */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /*** 选中的时间 */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /*** 选中的提示 */
    public void setSelectHint(String startHint, String endHint) {
        this.startHint = startHint;
        this.endHint = endHint;
    }

    /**
     * @param callback:设置点击回调
     */
    public final void setOnSelectDateCallback(OnSelectDateCallback callback) {
        this.mSelectDateCallback = callback;
    }

    @Override
    public int setLayoutItem() {
        return R.layout.hai_view_calendar_day;
    }

    @Override
    public void convert(@NonNull MultipleViewHolder holder, @NonNull Object entity, Object other) {
        //设置默认的左右边条颜色
        holder.setBackgroundColor(R.id.tv_day, Color.TRANSPARENT);
        //获得数据
        final CalendarMonthEntity item = dataItem.get(holder.position());
        holder.setText(R.id.tv_day, item.getDay());
        holder.setOnClickListener(R.id.tv_day, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final boolean isExpire = isExpire(item.getDate());
                if (isOver && isExpire) {
                    LogUtils.e("啊哈,后悔药暂不出售哟。。。");
                } else {
                    if (mSelectDateCallback != null) {
                        mSelectDateCallback.selectDate(item.getDate(), null);
                    }
                }
            }
        });
        if (isOver) {
            //是否是过去
            final boolean isExpire = isExpire(item.getDate());
            //不可以选择过去
            holder.setTextColor(R.id.tv_day, ColorUtils.getColor(isExpire ?
                    R.color.textColor_25 : R.color.textColor_85));
        }

        if (isRadio) {
            if (isToDay(item.getDate())) {
                holder.setBackgroundResource(R.id.tv_day, R.drawable.ring_accent_1);
            } else {
                holder.setBackgroundColor(R.id.tv_day, Color.TRANSPARENT);
            }
        } else {
            if (isToDay(startTime, item.getDate()) && isToDay(endTime, item.getDate())) {
                holder.setText(R.id.tv_select, startHint + "~" + endHint);
                holder.setShow(R.id.tv_select, true);
                holder.setTextColor(R.id.tv_day, Color.WHITE);
                holder.setBackgroundColor(R.id.ll_type, ColorUtils.getColor(R.color.colorAccent));
            } else if (isToDay(startTime, item.getDate())) {
                holder.setText(R.id.tv_select, startHint);
                holder.setShow(R.id.tv_select, true);
                holder.setTextColor(R.id.tv_day, Color.WHITE);
                holder.setBackgroundColor(R.id.ll_type, ColorUtils.getColor(R.color.colorAccent));
            } else if (isToDay(endTime, item.getDate())) {
                holder.setText(R.id.tv_select, endHint);
                holder.setShow(R.id.tv_select, true);
                holder.setTextColor(R.id.tv_day, Color.WHITE);
                holder.setBackgroundColor(R.id.ll_type, ColorUtils.getColor(R.color.colorAccent));
            } else if (isWithin(item.getDate())) {
                holder.setBackgroundColor(R.id.ll_type, ColorUtils.getColor(R.color.colorAccent_25));
            } else {
                holder.setBackgroundColor(R.id.ll_type, Color.WHITE);
            }
        }
    }

    /**
     * @param item:过去=true
     */
    protected boolean isExpire(Date item) {
        if (item == null) {
            return false;
        }
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(item);
        return calendar.before(cal);
    }

    /**
     * @param item:是否是同一天
     */
    protected boolean isToDay(Date item) {
        return isToDay(startTime, item);
    }


    /**
     * @param item:是否是同一天
     */
    protected boolean isToDay(Date star, Date item) {
        if (star == null || item == null) {
            return false;
        }
        String str = TimeUtils.date2String(star, "yyyyMMdd");
        String str2 = TimeUtils.date2String(item, "yyyyMMdd");
        return str.equals(str2);
    }

    /**
     * @param item:是否在选择的时间段内
     */
    protected boolean isWithin(Date item) {
        if (item == null || endTime == null) {
            return false;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(item);

        Calendar start = Calendar.getInstance();
        start.setTime(startTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        return calendar.after(start) && calendar.before(end);
    }

}
