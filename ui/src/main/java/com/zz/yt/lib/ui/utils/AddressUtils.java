package com.zz.yt.lib.ui.utils;

import android.view.View;
import android.view.ViewGroup;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickStringListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 省市区选择
 *
 * @author qf
 * @version 1.0
 **/
public final class AddressUtils {

    //创建 SingleObject 的一个对象
    private final static AddressUtils instance = new AddressUtils();

    private final List<String> options1Items = new ArrayList<>();
    private final List<List<String>> options2Items = new ArrayList<>();
    private final List<List<List<String>>> options3Items = new ArrayList<>();

    private OnClickStringListener mClickStringListener = null;

    //获取唯一可用的对象
    public static AddressUtils getInstance() {
        return instance;
    }

    //让构造函数为 private，这样该类就不会被实例化
    private AddressUtils() {
        final String assets = ResourceUtils.readAssets2String("province_230619.json");
        final List<AddressBean> beanList = GsonUtils.fromJson(assets, GsonUtils.getListType(AddressBean.class));
        final int size = beanList == null ? 0 : beanList.size();
        //遍历省份
        for (int i = 0; i < size; i++) {
            final String name = beanList.get(i).getName();
            final List<AddressBean.CityBean> city = beanList.get(i).getCity();
            final int citySize = city == null ? 0 : city.size();
            //该省的城市列表（第二级）
            ArrayList<String> cityList = new ArrayList<>();
            //该省的所有地区列表（第三极）
            List<List<String>> province = new ArrayList<>();
            //遍历该省份的所有城市
            for (int j = 0; j < citySize; j++) {
                String cityName = city.get(j).getName();
                //添加城市
                cityList.add(cityName);
                //该城市的所有地区列表
                ArrayList<String> cityAreaList = new ArrayList<>(city.get(j).getArea());
                //添加该省所有地区数据
                province.add(cityAreaList);
            }

            /* 添加城市数据 */
            options1Items.add(name);
            /*  添加城市数据 */
            options2Items.add(cityList);
            /*  添加地区数据 */
            options3Items.add(province);
            LatteLogger.i("初始化省市区选择");
        }
    }

    public final AddressUtils onClickListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
        return instance;
    }

    public final void popup() {
        KeyboardUtils.hideSoftInput(Latte.getActivity());
        if (options1Items.size() == 0) {
            LatteLogger.e("assets文件夹没找到province.json文件");
            return;
        }
        final ViewGroup decorView = Latte.getActivity()
                .getWindow()
                .getDecorView()
                .findViewById(android.R.id.content);
        OptionsPickerView<String> pvOptions = new OptionsPickerBuilder(Latte.getActivity(),
                new OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int options2, int options3, View v) {
                        //返回的分别是三个级别的选中位置
                        String opt1tx = options1Items.size() > 0 ?
                                options1Items.get(options1) + "-" : "";

                        String opt2tx = options2Items.size() > 0
                                && options2Items.get(options1).size() > 0 ?
                                options2Items.get(options1).get(options2) + "-" : "";

                        String opt3tx = options2Items.size() > 0
                                && options3Items.get(options1).size() > 0
                                && options3Items.get(options1).get(options2).size() > 0 ?
                                options3Items.get(options1).get(options2).get(options3) : "";

                        String tx = opt1tx + opt2tx + opt3tx;
                        LatteLogger.i("省市区：" + tx);
                        if (mClickStringListener != null) {
                            mClickStringListener.onClick(tx);
                        }
                    }
                })
                .setTitleText("城市选择")
                .setTitleSize(20)
                .setDividerColor(ColorUtils.getColor(R.color.colorAccent))
                .setTextColorCenter(ColorUtils.getColor(R.color.colorAccent))
                .setLineSpacingMultiplier(3f)
                .setItemVisibleCount(7)
                .setDecorView(decorView)
                .setContentTextSize(16)
                .build();

        //三级选择器
        pvOptions.setPicker(options1Items, options2Items, options3Items);
        pvOptions.show();
    }


    static class AddressBean {

        private String name;
        private List<CityBean> city;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private List<CityBean> getCity() {
            return city;
        }

        public void setCity(List<CityBean> city) {
            this.city = city;
        }

        public static class CityBean {

            private String name;
            private List<String> area;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            private List<String> getArea() {
                return area;
            }

            public void setArea(List<String> area) {
                this.area = area;
            }
        }
    }

}
