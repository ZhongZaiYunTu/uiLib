package com.zz.yt.lib.ui.view.mothOrYearSelect;

import android.view.View;

import com.bigkoo.pickerview.adapter.ArrayWheelAdapter;
import com.bigkoo.pickerview.adapter.NumericWheelAdapter;
import com.bigkoo.pickerview.listener.ISelectTimeCallback;
import com.bigkoo.pickerview.utils.ChinaDate;
import com.bigkoo.pickerview.utils.LunarCalendar;
import com.contrarywind.listener.OnItemSelectedListener;
import com.contrarywind.view.WheelView;
import com.zz.yt.lib.ui.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MothOrYearTime {

    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
    private View view;
    private WheelView wv_year;
    private WheelView wv_month;

    private WheelView wv_year_2;
    private WheelView wv_month_2;

    private int gravity;

    private boolean[] type;
    private static final int DEFAULT_START_YEAR = 1900;
    private static final int DEFAULT_END_YEAR = 2100;
    private static final int DEFAULT_START_MONTH = 1;
    private static final int DEFAULT_END_MONTH = 12;
    private static final int DEFAULT_START_DAY = 1;
    private static final int DEFAULT_END_DAY = 31;

    private int startYear = DEFAULT_START_YEAR;
    private int endYear = DEFAULT_END_YEAR;
    private int startMonth = DEFAULT_START_MONTH;
    private int endMonth = DEFAULT_END_MONTH;
    private int startDay = DEFAULT_START_DAY;
    private int endDay = DEFAULT_END_DAY; //表示31天的
    private int currentYear;
    private int currentYear2;

    private int textSize;

    private boolean isLunarCalendar = false;

    public MothOrYearTime(View view, boolean[] type, int gravity, int textSize) {
        super();
        this.view = view;
        this.type = type;
        this.gravity = gravity;
        this.textSize = textSize;
    }

    public void setLunarMode(boolean isLunarCalendar) {
        this.isLunarCalendar = isLunarCalendar;
    }

    public boolean isLunarMode() {
        return isLunarCalendar;
    }

    public void setPicker(int year) {
        this.setPicker(year, 1);
    }

    public void setPicker(int year, final int month) {
        if (isLunarCalendar) {
            int[] lunar = LunarCalendar.solarToLunar(year, month + 1, 1);
            setLunar(lunar[0], lunar[1] - 1, false);
        } else {
            setSolar(year, month);
        }
    }

    /**
     * 设置农历
     *
     * @param year
     * @param month
     */
    private void setLunar(int year, final int month, boolean isLeap) {
        // 年
        wv_year = (WheelView) view.findViewById(R.id.year);
        wv_year.setAdapter(new ArrayWheelAdapter(ChinaDate.getYears(startYear, endYear)));// 设置"年"的显示数据
        wv_year.setLabel("");// 添加文字
        wv_year.setCurrentItem(year - startYear);// 初始化时显示的数据
        wv_year.setGravity(gravity);

        // 月
        wv_month = (WheelView) view.findViewById(R.id.month);
        wv_month.setAdapter(new ArrayWheelAdapter(ChinaDate.getMonths(year)));
        wv_month.setLabel("");

        // 年2
        wv_year_2 = (WheelView) view.findViewById(R.id.year_2);
        wv_year_2.setAdapter(new ArrayWheelAdapter(ChinaDate.getYears(startYear, endYear)));// 设置"年"的显示数据
        wv_year_2.setLabel("");// 添加文字
        wv_year_2.setCurrentItem(year - startYear);// 初始化时显示的数据
        wv_year_2.setGravity(gravity);

        // 月2
        wv_month_2 = (WheelView) view.findViewById(R.id.month_2);
        wv_month_2.setAdapter(new ArrayWheelAdapter(ChinaDate.getMonths(year)));
        wv_month_2.setLabel("");

        int leapMonth = ChinaDate.leapMonth(year);
        if (leapMonth != 0 && (month > leapMonth - 1 || isLeap)) { //选中月是闰月或大于闰月
            wv_month.setCurrentItem(month + 1);
            wv_month_2.setCurrentItem(month + 1);
        } else {
            wv_month.setCurrentItem(month);
            wv_month_2.setCurrentItem(month);
        }

        wv_month.setGravity(gravity);
        wv_month_2.setGravity(gravity);

        // 添加"年"监听
        wv_year.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                int year_num = index + startYear;
                // 判断是不是闰年,来确定月的选择
                wv_month.setAdapter(new ArrayWheelAdapter(ChinaDate.getMonths(year_num)));
                if (ChinaDate.leapMonth(year_num) != 0 && wv_month.getCurrentItem() > ChinaDate.leapMonth(year_num) - 1) {
                    wv_month.setCurrentItem(wv_month.getCurrentItem() + 1);
                } else {
                    wv_month.setCurrentItem(wv_month.getCurrentItem());
                }

                int year_num_2 = wv_year_2.getCurrentItem() + startYear;
                if (year_num > year_num_2 || (year_num == year_num_2 && wv_month.getCurrentItem() > wv_month_2.getCurrentItem())) {
                    wv_year_2.setCurrentItem(index);
                    wv_month_2.setAdapter(new ArrayWheelAdapter(ChinaDate.getMonths(year_num)));
                    wv_month_2.setCurrentItem(wv_month.getCurrentItem());
                }

            }
        });

        // 添加"月"监听
        wv_month.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                if (wv_year.getCurrentItem() == wv_year_2.getCurrentItem() && wv_month.getCurrentItem() > wv_month_2.getCurrentItem()) {
                    wv_month_2.setCurrentItem(wv_month.getCurrentItem());
                }
            }
        });

        // 添加"年2"监听
        wv_year_2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                int year_num_2 = index + startYear;
                // 判断是不是闰年,来确定月的选择
                wv_month_2.setAdapter(new ArrayWheelAdapter(ChinaDate.getMonths(year_num_2)));
                if (ChinaDate.leapMonth(year_num_2) != 0 && wv_month_2.getCurrentItem() > ChinaDate.leapMonth(year_num_2) - 1) {
                    wv_month_2.setCurrentItem(wv_month_2.getCurrentItem() + 1);
                } else {
                    wv_month_2.setCurrentItem(wv_month_2.getCurrentItem());
                }

                int year_num = wv_year.getCurrentItem() + startYear;
                if (year_num > year_num_2 || (year_num == year_num_2 && wv_month.getCurrentItem() > wv_month_2.getCurrentItem())) {
                    wv_year.setCurrentItem(index);
                    wv_month.setAdapter(new ArrayWheelAdapter(ChinaDate.getMonths(year_num_2)));
                    wv_month.setCurrentItem(wv_month_2.getCurrentItem());
                }
            }
        });

        // 添加"月2"监听
        wv_month_2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                if (wv_year.getCurrentItem() == wv_year_2.getCurrentItem() && wv_month.getCurrentItem() > wv_month_2.getCurrentItem()) {
                    wv_month.setCurrentItem(wv_month_2.getCurrentItem());
                }
            }
        });

        if (type.length != 6) {
            throw new RuntimeException("type[] length is not 6");
        }
        wv_year.setVisibility(type[0] ? View.VISIBLE : View.GONE);
        wv_month.setVisibility(type[1] ? View.VISIBLE : View.GONE);

        wv_year_2.setVisibility(type[0] ? View.VISIBLE : View.GONE);
        wv_month_2.setVisibility(type[1] ? View.VISIBLE : View.GONE);

        setContentTextSize();
    }

    /**
     * 设置公历
     *
     * @param year
     * @param month
     */
    private void setSolar(int year, final int month) {
        // 添加大小月月份并将其转换为list,方便之后的判断
        String[] months_big = {"1", "3", "5", "7", "8", "10", "12"};
        String[] months_little = {"4", "6", "9", "11"};

        final List<String> list_big = Arrays.asList(months_big);
        final List<String> list_little = Arrays.asList(months_little);

        currentYear = year;
        currentYear2 = year;
        // 年
        wv_year = (WheelView) view.findViewById(R.id.year);
        wv_year.setAdapter(new NumericWheelAdapter(startYear, endYear));// 设置"年"的显示数据

        wv_year.setCurrentItem(year - startYear);// 初始化时显示的数据
        wv_year.setGravity(gravity);

        // 月
        wv_month = (WheelView) view.findViewById(R.id.month);
        if (startYear == endYear) {//开始年等于终止年
            wv_month.setAdapter(new NumericWheelAdapter(startMonth, endMonth));
            wv_month.setCurrentItem(month + 1 - startMonth);
        } else if (year == startYear) {
            //起始日期的月份控制
            wv_month.setAdapter(new NumericWheelAdapter(startMonth, 12));
            wv_month.setCurrentItem(month + 1 - startMonth);
        } else if (year == endYear) {
            //终止日期的月份控制
            wv_month.setAdapter(new NumericWheelAdapter(1, endMonth));
            wv_month.setCurrentItem(month);
        } else {
            wv_month.setAdapter(new NumericWheelAdapter(1, 12));
            wv_month.setCurrentItem(month);
        }
        wv_month.setGravity(gravity);

        // 添加"年"监听
        wv_year.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                int year_num = index + startYear;
                currentYear = year_num;
                int currentMonthItem = wv_month.getCurrentItem();//记录上一次的item位置
                // 判断大小月及是否闰年,用来确定"日"的数据
                if (startYear == endYear) {
                    //重新设置月份
                    wv_month.setAdapter(new NumericWheelAdapter(startMonth, endMonth));

                    if (currentMonthItem > wv_month.getAdapter().getItemsCount() - 1) {
                        currentMonthItem = wv_month.getAdapter().getItemsCount() - 1;
                        wv_month.setCurrentItem(currentMonthItem);
                    }

                } else if (year_num == startYear) {//等于开始的年
                    //重新设置月份
                    wv_month.setAdapter(new NumericWheelAdapter(startMonth, 12));

                    if (currentMonthItem > wv_month.getAdapter().getItemsCount() - 1) {
                        currentMonthItem = wv_month.getAdapter().getItemsCount() - 1;
                        wv_month.setCurrentItem(currentMonthItem);
                    }
                } else if (year_num == endYear) {
                    //重新设置月份
                    wv_month.setAdapter(new NumericWheelAdapter(1, endMonth));
                    if (currentMonthItem > wv_month.getAdapter().getItemsCount() - 1) {
                        currentMonthItem = wv_month.getAdapter().getItemsCount() - 1;
                        wv_month.setCurrentItem(currentMonthItem);
                    }

                } else {
                    //重新设置月份
                    wv_month.setAdapter(new NumericWheelAdapter(1, 12));
                }

                int year_num_2 = wv_year_2.getCurrentItem() + startYear;
                if (year_num > year_num_2 || (year_num == year_num_2 && wv_month.getCurrentItem() > wv_month_2.getCurrentItem())) {
                    wv_year_2.setCurrentItem(index);
                    wv_month_2.setCurrentItem(wv_month.getCurrentItem());
                }
            }
        });

        // 添加"月"监听
        wv_month.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                if (wv_year.getCurrentItem() == wv_year_2.getCurrentItem() && wv_month.getCurrentItem() > wv_month_2.getCurrentItem()) {
                    wv_month_2.setCurrentItem(wv_month.getCurrentItem());
                }
            }
        });


        // 年2
        wv_year_2 = (WheelView) view.findViewById(R.id.year_2);
        wv_year_2.setAdapter(new NumericWheelAdapter(startYear, endYear));// 设置"年"的显示数据

        wv_year_2.setCurrentItem(year - startYear);// 初始化时显示的数据
        wv_year_2.setGravity(gravity);

        // 月2
        wv_month_2 = (WheelView) view.findViewById(R.id.month_2);
        if (startYear == endYear) {//开始年等于终止年
            wv_month_2.setAdapter(new NumericWheelAdapter(startMonth, endMonth));
            wv_month_2.setCurrentItem(month + 1 - startMonth);
        } else if (year == startYear) {
            //起始日期的月份控制
            wv_month_2.setAdapter(new NumericWheelAdapter(startMonth, 12));
            wv_month_2.setCurrentItem(month + 1 - startMonth);
        } else if (year == endYear) {
            //终止日期的月份控制
            wv_month_2.setAdapter(new NumericWheelAdapter(1, endMonth));
            wv_month_2.setCurrentItem(month);
        } else {
            wv_month_2.setAdapter(new NumericWheelAdapter(1, 12));
            wv_month_2.setCurrentItem(month);
        }
        wv_month_2.setGravity(gravity);

        // 添加"年2"监听
        wv_year_2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                int year_num_2 = index + startYear;
                currentYear2 = year_num_2;
                int currentMonthItem = wv_month_2.getCurrentItem();//记录上一次的item位置
                // 判断大小月及是否闰年,用来确定"日"的数据
                if (startYear == endYear) {
                    //重新设置月份
                    wv_month_2.setAdapter(new NumericWheelAdapter(startMonth, endMonth));

                    if (currentMonthItem > wv_month_2.getAdapter().getItemsCount() - 1) {
                        currentMonthItem = wv_month_2.getAdapter().getItemsCount() - 1;
                        wv_month_2.setCurrentItem(currentMonthItem);
                    }

                } else if (year_num_2 == startYear) {//等于开始的年
                    //重新设置月份
                    wv_month_2.setAdapter(new NumericWheelAdapter(startMonth, 12));

                    if (currentMonthItem > wv_month_2.getAdapter().getItemsCount() - 1) {
                        currentMonthItem = wv_month_2.getAdapter().getItemsCount() - 1;
                        wv_month_2.setCurrentItem(currentMonthItem);
                    }
                } else if (year_num_2 == endYear) {
                    //重新设置月份
                    wv_month_2.setAdapter(new NumericWheelAdapter(1, endMonth));
                    if (currentMonthItem > wv_month_2.getAdapter().getItemsCount() - 1) {
                        currentMonthItem = wv_month_2.getAdapter().getItemsCount() - 1;
                        wv_month_2.setCurrentItem(currentMonthItem);
                    }

                } else {
                    //重新设置月份
                    wv_month_2.setAdapter(new NumericWheelAdapter(1, 12));
                }

                int year_num = wv_year.getCurrentItem() + startYear;
                if (year_num > year_num_2 || (year_num == year_num_2 && wv_month.getCurrentItem() > wv_month_2.getCurrentItem())) {
                    wv_year.setCurrentItem(index);
                    wv_month.setCurrentItem(wv_month_2.getCurrentItem());
                }
            }
        });

        // 添加"月2"监听
        wv_month_2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                if (wv_year.getCurrentItem() == wv_year_2.getCurrentItem() && wv_month.getCurrentItem() > wv_month_2.getCurrentItem()) {
                    wv_month.setCurrentItem(wv_month_2.getCurrentItem());
                }
            }
        });


        if (type.length != 6) {
            throw new IllegalArgumentException("type[] length is not 6");
        }
        wv_year.setVisibility(type[0] ? View.VISIBLE : View.GONE);
        wv_month.setVisibility(type[1] ? View.VISIBLE : View.GONE);

        wv_year_2.setVisibility(type[0] ? View.VISIBLE : View.GONE);
        wv_month_2.setVisibility(type[1] ? View.VISIBLE : View.GONE);

        setContentTextSize();
    }

    private void setContentTextSize() {
        wv_month.setTextSize(textSize);
        wv_year.setTextSize(textSize);

        wv_month_2.setTextSize(textSize);
        wv_year_2.setTextSize(textSize);
    }

    public void setLabels(String label_year, String label_month) {
        if (isLunarCalendar) {
            return;
        }

        if (label_year != null) {
            wv_year.setLabel(label_year);
            wv_year_2.setLabel(label_year);
        } else {
            wv_year.setLabel(view.getContext().getString(R.string.pickerview_year));
            wv_year_2.setLabel(view.getContext().getString(R.string.pickerview_year));
        }
        if (label_month != null) {
            wv_month.setLabel(label_month);
            wv_month_2.setLabel(label_month);
        } else {
            wv_month.setLabel(view.getContext().getString(R.string.pickerview_month));
            wv_month_2.setLabel(view.getContext().getString(R.string.pickerview_month));
        }
    }

    public void setTextXOffset(int x_offset_year, int x_offset_month) {
        wv_year.setTextXOffset(x_offset_year);
        wv_month.setTextXOffset(x_offset_month);

        wv_year_2.setTextXOffset(x_offset_year);
        wv_month_2.setTextXOffset(x_offset_month);
    }

    /**
     * 设置是否循环滚动
     *
     * @param cyclic boolean
     */
    public void setCyclic(boolean cyclic) {
        wv_year.setCyclic(cyclic);
        wv_month.setCyclic(cyclic);

        wv_year_2.setCyclic(cyclic);
        wv_month_2.setCyclic(cyclic);
    }

    public String getTime() {
        if (isLunarCalendar) {
            //如果是农历 返回对应的公历时间
            return getLunarTime();
        }
        StringBuilder sb = new StringBuilder();
        if (currentYear == startYear) {
           /* int i = wv_month.getCurrentItem() + startMonth;
            System.out.println("i:" + i);*/
            sb.append((wv_year.getCurrentItem() + startYear)).append("-")
                    .append((wv_month.getCurrentItem() + startMonth));

        } else {
            sb.append((wv_year.getCurrentItem() + startYear)).append("-")
                    .append((wv_month.getCurrentItem() + 1));
        }

        return sb.toString();
    }


    /**
     * 农历返回对应的公历时间
     *
     * @return String
     */
    private String getLunarTime() {
        StringBuilder sb = new StringBuilder();
        int year = wv_year.getCurrentItem() + startYear;
        int month;
        boolean isLeapMonth = false;
        if (ChinaDate.leapMonth(year) == 0) {
            month = wv_month.getCurrentItem() + 1;
        } else {
            if ((wv_month.getCurrentItem() + 1) - ChinaDate.leapMonth(year) <= 0) {
                month = wv_month.getCurrentItem() + 1;
            } else if ((wv_month.getCurrentItem() + 1) - ChinaDate.leapMonth(year) == 1) {
                month = wv_month.getCurrentItem();
                isLeapMonth = true;
            } else {
                month = wv_month.getCurrentItem();
            }
        }

        int[] solar = LunarCalendar.lunarToSolar(year, month, 1, isLeapMonth);

        sb.append(solar[0]).append("-")
                .append(solar[1]).append("-")
                .append(solar[2]);
        return sb.toString();
    }

    public String getTime2() {
        if (isLunarCalendar) {
            //如果是农历 返回对应的公历时间
            return getLunarTime2();
        }
        StringBuilder sb = new StringBuilder();
        if (currentYear2 == startYear) {
           /* int i = wv_month.getCurrentItem() + startMonth;
            System.out.println("i:" + i);*/
            sb.append((wv_year_2.getCurrentItem() + startYear)).append("-")
                    .append((wv_month_2.getCurrentItem() + startMonth));

        } else {
            sb.append((wv_year_2.getCurrentItem() + startYear)).append("-")
                    .append((wv_month_2.getCurrentItem() + 1));
        }

        return sb.toString();
    }


    /**
     * 农历返回对应的公历时间
     *
     * @return String
     */
    private String getLunarTime2() {
        StringBuilder sb = new StringBuilder();
        int year = wv_year_2.getCurrentItem() + startYear;
        int month;
        boolean isLeapMonth = false;
        if (ChinaDate.leapMonth(year) == 0) {
            month = wv_month_2.getCurrentItem() + 1;
        } else {
            if ((wv_month_2.getCurrentItem() + 1) - ChinaDate.leapMonth(year) <= 0) {
                month = wv_month_2.getCurrentItem() + 1;
            } else if ((wv_month_2.getCurrentItem() + 1) - ChinaDate.leapMonth(year) == 1) {
                month = wv_month_2.getCurrentItem();
                isLeapMonth = true;
            } else {
                month = wv_month_2.getCurrentItem();
            }
        }

        int[] solar = LunarCalendar.lunarToSolar(year, month, 1, isLeapMonth);

        sb.append(solar[0]).append("-")
                .append(solar[1]).append("-")
                .append(solar[2]);
        return sb.toString();
    }

    public View getView() {
        return view;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }


    public void setRangDate(Calendar startDate, Calendar endDate) {

        if (startDate == null && endDate != null) {
            int year = endDate.get(Calendar.YEAR);
            int month = endDate.get(Calendar.MONTH) + 1;
            if (year > startYear) {
                this.endYear = year;
                this.endMonth = month;
            } else if (year == startYear) {
                if (month > startMonth) {
                    this.endYear = year;
                    this.endMonth = month;
                } else if (month == startMonth) {
                    this.endYear = year;
                    this.endMonth = month;
                }
            }

        } else if (startDate != null && endDate == null) {
            int year = startDate.get(Calendar.YEAR);
            int month = startDate.get(Calendar.MONTH) + 1;
            if (year < endYear) {
                this.startMonth = month;
                this.startYear = year;
            } else if (year == endYear) {
                if (month < endMonth) {
                    this.startMonth = month;
                    this.startYear = year;
                } else if (month == endMonth) {
                    this.startMonth = month;
                    this.startYear = year;
                }
            }

        } else if (startDate != null) {
            this.startYear = startDate.get(Calendar.YEAR);
            this.endYear = endDate.get(Calendar.YEAR);
            this.startMonth = startDate.get(Calendar.MONTH) + 1;
            this.endMonth = endDate.get(Calendar.MONTH) + 1;
        }

    }

    /**
     * 设置间距倍数,但是只能在1.0-4.0f之间
     *
     * @param lineSpacingMultiplier lineSpacingMultiplier
     */
    public void setLineSpacingMultiplier(float lineSpacingMultiplier) {
        wv_month.setLineSpacingMultiplier(lineSpacingMultiplier);
        wv_year.setLineSpacingMultiplier(lineSpacingMultiplier);

        wv_month_2.setLineSpacingMultiplier(lineSpacingMultiplier);
        wv_year_2.setLineSpacingMultiplier(lineSpacingMultiplier);
    }

    /**
     * 设置分割线的颜色
     *
     * @param dividerColor dividerColor
     */
    public void setDividerColor(int dividerColor) {
        wv_month.setDividerColor(dividerColor);
        wv_year.setDividerColor(dividerColor);

        wv_month_2.setDividerColor(dividerColor);
        wv_year_2.setDividerColor(dividerColor);
    }

    /**
     * 设置分割线的类型
     *
     * @param dividerType dividerType
     */
    public void setDividerType(WheelView.DividerType dividerType) {
        wv_month.setDividerType(dividerType);
        wv_year.setDividerType(dividerType);

        wv_month_2.setDividerType(dividerType);
        wv_year_2.setDividerType(dividerType);
    }

    /**
     * 设置分割线之间的文字的颜色
     *
     * @param textColorCenter textColorCenter
     */
    public void setTextColorCenter(int textColorCenter) {
        wv_month.setTextColorCenter(textColorCenter);
        wv_year.setTextColorCenter(textColorCenter);

        wv_month_2.setTextColorCenter(textColorCenter);
        wv_year_2.setTextColorCenter(textColorCenter);
    }

    /**
     * 设置分割线以外文字的颜色
     *
     * @param textColorOut textColorOut
     */
    public void setTextColorOut(int textColorOut) {
        wv_month.setTextColorOut(textColorOut);
        wv_year.setTextColorOut(textColorOut);

        wv_month_2.setTextColorOut(textColorOut);
        wv_year_2.setTextColorOut(textColorOut);
    }

    /**
     * @param isCenterLabel 是否只显示中间选中项的
     */
    public void isCenterLabel(boolean isCenterLabel) {
        wv_month.isCenterLabel(isCenterLabel);
        wv_year.isCenterLabel(isCenterLabel);

        wv_month_2.isCenterLabel(isCenterLabel);
        wv_year_2.isCenterLabel(isCenterLabel);
    }

    public void setItemsVisible(int itemsVisibleCount) {
        wv_month.setItemsVisibleCount(itemsVisibleCount);
        wv_year.setItemsVisibleCount(itemsVisibleCount);

        wv_month_2.setItemsVisibleCount(itemsVisibleCount);
        wv_year_2.setItemsVisibleCount(itemsVisibleCount);
    }

    public void setAlphaGradient(boolean isAlphaGradient) {
        wv_month.setAlphaGradient(isAlphaGradient);
        wv_year.setAlphaGradient(isAlphaGradient);

        wv_month_2.setAlphaGradient(isAlphaGradient);
        wv_year_2.setAlphaGradient(isAlphaGradient);
    }
}

