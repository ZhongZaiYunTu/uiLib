package com.zz.yt.lib.ui.listener;

/**
 * 下标点击事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickIntsListener {

    /**
     * 点击
     *
     * @param index：下标
     */
    void onClick(int... index);

}
