package com.zz.yt.lib.ui.base.bottom.tab;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;
import com.zz.yt.lib.ui.base.builder.HomeTabBuilder;
import com.zz.yt.lib.ui.recycler.data.TabEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import me.yokeyword.fragmentation.ISupportFragment;

/**
 * 首页主页
 *
 * @author 傅令杰
 */

public abstract class BaseBottomTabDelegate extends LatteDelegate implements OnTabSelectListener {

    private final ArrayList<CustomTabEntity> TAB_BEANS = new ArrayList<>();
    private final ArrayList<BaseBottomItemDelegate> ITEM_DELEGATES = new ArrayList<>();
    private final LinkedHashMap<CustomTabEntity, BaseBottomItemDelegate> ITEMS = new LinkedHashMap<>();
    private int mCurrentDelegate = 0;
    private int mIndexDelegate = 0;
    private int mClickedColor = Color.RED;

    protected CommonTabLayout mCommonTabBar = null;

    @Override
    protected boolean setMourning() {
        return true;
    }

    /**
     * 初始化界面
     *
     * @param builder:
     * @return 界面
     */
    public abstract LinkedHashMap<TabEntity, BaseBottomItemDelegate> setItems(HomeTabBuilder builder);

    public ArrayList<BaseBottomItemDelegate> getItemDelegates() {
        return ITEM_DELEGATES;
    }

    @Override
    public Object setLayout() {
        return R.layout.a_ui_delegate_bottom;
    }

    /**
     * 第一次默认显示的 tab
     *
     * @return 0
     */
    public abstract int setIndexDelegate();

    /**
     * 选项的颜色
     *
     * @return ss
     */
    @ColorInt
    public abstract int setClickedColor();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIndexDelegate = setIndexDelegate();
        if (setClickedColor() != 0) {
            mClickedColor = setClickedColor();
        }

        final HomeTabBuilder builder = HomeTabBuilder.builder();
        final LinkedHashMap<TabEntity, BaseBottomItemDelegate> items = setItems(builder);
        ITEMS.putAll(items);
        for (Map.Entry<CustomTabEntity, BaseBottomItemDelegate> item : ITEMS.entrySet()) {
            final CustomTabEntity key = item.getKey();
            final BaseBottomItemDelegate value = item.getValue();
            TAB_BEANS.add(key);
            ITEM_DELEGATES.add(value);
        }
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        final LinearLayoutCompat bottomBar = findViewById(R.id.bottom_bar);
        inBottomBar(bottomBar);
        LayoutInflater.from(getContext()).inflate(R.layout.bottom_item_common_tab_layout, bottomBar);
        mCommonTabBar = bottomBar.findViewById(R.id.common_tab_bar);
        if (mCommonTabBar != null) {
            mCommonTabBar.setTabData(TAB_BEANS);
            mCommonTabBar.setOnTabSelectListener(this);
            mCommonTabBar.setCurrentTab(mIndexDelegate);
            mCommonTabBar.setTextSelectColor(mClickedColor);
            initCommonTabBar(mCommonTabBar);
        }
        final int size = ITEMS.size();
        final ISupportFragment[] delegateArray = ITEM_DELEGATES.toArray(new ISupportFragment[size]);
        getSupportDelegate().loadMultipleRootFragment(R.id.bottom_bar_delegate_container, mIndexDelegate, delegateArray);
        mCurrentDelegate = mIndexDelegate;
        ///是否显示右上角的  debug 标签
        setShow(R.id.text_debug_show, Latte.getDebug() && debugShowCheckedModeBanner());
    }

    /**
     * 是否显示右上角的  debug 标签
     */
    protected boolean debugShowCheckedModeBanner() {
        return false;
    }

    public void initCommonTabBar(CommonTabLayout commonTabBar) {

    }

    protected void inBottomBar(LinearLayoutCompat bottomBar) {
        if (bottomBar != null) {
            bottomBar.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    public void onTabSelect(int position) {
        if (onSelect(position)) {
            return;
        }
        mCommonTabBar.setCurrentTab(position);
        getSupportDelegate().showHideFragment(ITEM_DELEGATES.get(position), ITEM_DELEGATES.get(mCurrentDelegate));
        //注意先后顺序
        mCurrentDelegate = position;
    }

    public int getCurrentDelegate() {
        return mCurrentDelegate;
    }

    @Override
    public void onTabReselect(int position) {

    }

    /**
     * 是否有凸出按钮
     *
     * @param position：
     * @return true
     */
    protected boolean onSelect(int position) {
        return false;
    }
}
