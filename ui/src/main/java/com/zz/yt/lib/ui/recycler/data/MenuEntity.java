package com.zz.yt.lib.ui.recycler.data;

import android.graphics.Color;

import androidx.annotation.ColorInt;

/**
 * 功能
 *
 * @author qf
 * @version 1.0
 **/
public class MenuEntity {

    private final int num;
    private final String picture;
    private final int imgError;
    private final String textName;
    private String other = "";
    private int colorBack = Color.TRANSPARENT;
    private int spanSize = 1;
    private Object obj;

    public MenuEntity(int imgError, String textName) {
        this.num = 0;
        this.picture = "";
        this.imgError = imgError;
        this.textName = textName;
    }

    public MenuEntity(int num, int imgError, String textName) {
        this.num = num;
        this.picture = "";
        this.imgError = imgError;
        this.textName = textName;
    }

    public MenuEntity(int num, String picture, int imgError, String textName) {
        this.num = num;
        this.picture = picture;
        this.imgError = imgError;
        this.textName = textName;
    }

    public MenuEntity setOther(String other) {
        this.other = other;
        return this;
    }

    public MenuEntity setColorBack(String colorBack) {
        this.colorBack = Color.parseColor(colorBack);
        return this;
    }

    public MenuEntity setColorBack(@ColorInt int colorBack) {
        this.colorBack = colorBack;
        return this;
    }

    public MenuEntity setSpanSize(int spanSize) {
        if (spanSize < 1) {
            this.spanSize = 1;
            return this;
        }
        this.spanSize = spanSize;
        return this;
    }

    public MenuEntity setObj(Object obj) {
        this.obj = obj;
        return this;
    }

    public int getNum() {
        return num;
    }

    public String getPicture() {
        return picture;
    }

    public int getImgError() {
        return imgError;
    }

    public String getTextName() {
        return textName;
    }

    public String getOther() {
        return other;
    }

    public int getColorBack() {
        return colorBack;
    }

    public int getSpanSize() {
        return spanSize;
    }

    public Object getObj() {
        return obj;
    }

}
