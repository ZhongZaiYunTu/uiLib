package com.zz.yt.lib.ui.listener;

import android.widget.CheckBox;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickIntCheckListener {

    /**
     * 点击
     *
     * @param index：下标
     * @param check：View
     */
    void onClick(int index, CheckBox check);

}
