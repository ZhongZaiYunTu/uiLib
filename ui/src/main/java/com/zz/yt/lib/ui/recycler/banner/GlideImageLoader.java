package com.zz.yt.lib.ui.recycler.banner;

import android.content.Context;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.youth.banner.loader.ImageLoader;

/**
 * urlImage的广告lan
 *
 * @author 傅令杰
 * @version 1.0
 * @date 2020/4/24
 */
public class GlideImageLoader extends ImageLoader {

    private static final RequestOptions OPTIONS = new RequestOptions()
            .centerCrop()
            .diskCacheStrategy(DiskCacheStrategy.NONE);

    @Override
    public void displayImage(Context context, Object path, @NonNull ImageView imageView) {
        Glide.with(imageView.getContext())
                .load(path)
                .apply(OPTIONS)
                .into(imageView);
    }

    @Override
    public ImageView createImageView(Context context) {
        return new AppCompatImageView(context);
    }
}