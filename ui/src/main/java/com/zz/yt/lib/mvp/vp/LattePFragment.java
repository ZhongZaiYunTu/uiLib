package com.zz.yt.lib.mvp.vp;


import com.zz.yt.lib.ui.mvp.contract.ExampleContract;

public abstract class LattePFragment<T> extends LatteVpFragment<ExampleContract.View<T>, LattePresenter<T>>
        implements ExampleContract.View<T> {


    @Override
    public ExampleContract.View<T> createView() {
        return this;
    }

    @Override
    public LattePresenter<T> createPresenter() {
        return new LattePresenter<T>();
    }


}
