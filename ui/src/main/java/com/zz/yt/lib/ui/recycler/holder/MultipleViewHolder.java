package com.zz.yt.lib.ui.recycler.holder;

import android.text.InputFilter;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ColorUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.joanzapata.iconify.widget.IconTextView;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.ButtonListLayout;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.data.OrderFormEntity;
import com.zz.yt.lib.ui.recycler.data.StatusEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;
import com.zz.yt.lib.ui.utils.SpannableStringUtils;


/**
 * Created by 傅令杰
 *
 * @author 傅令杰
 */
public class MultipleViewHolder extends BaseViewHolder {


    /**
     * 设置图片加载策略
     */
    private static final RequestOptions RECYCLER_OPTIONS =
            new RequestOptions()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate();

    private MultipleViewHolder(View view) {
        super(view);
    }

    @NonNull
    public static MultipleViewHolder create(View view) {
        return new MultipleViewHolder(view);
    }


    @NonNull
    public MultipleViewHolder setText(int viewId, @NonNull MultipleItemEntity entity, Object key) {
        final String text = entity.getField(key);
        setText(viewId, text);
        return this;
    }

    @NonNull
    public MultipleViewHolder setTextIcon(int viewId, @NonNull MultipleItemEntity entity, Object keyIcon, Object keyColor) {
        final String text = entity.getField(keyIcon);
        final int color = entity.getField(keyColor);
        IconTextView textView = getView(viewId);
        textView.setText(text);
        textView.setTextColor(color);
        return this;
    }

    @NonNull
    public MultipleViewHolder setTextColor(int viewId, @NonNull MultipleItemEntity entity, Object key) {
        try {
            final int color = entity.getField(key);
            setTextColor(viewId, color);
        } catch (Exception e) {
            setTextColor(viewId, ColorUtils.getColor(R.color.colorAccent));
        }
        return this;
    }

    @NonNull
    public MultipleViewHolder setGone(int viewId, @NonNull MultipleItemEntity entity, Object key) {
        try {
            boolean isGone = entity.getField(key);
            setGone(viewId, isGone);
        } catch (Exception e) {
            setGone(viewId, true);
        }
        return this;
    }

    @NonNull
    public MultipleViewHolder setShow(int viewId, @NonNull MultipleItemEntity entity, Object key) {
        try {
            boolean isGone = entity.getField(key);
            setShow(viewId, isGone);
        } catch (Exception e) {
            setShow(viewId, true);
        }
        return this;
    }

    @NonNull
    private MultipleViewHolder setButtonList(@IdRes int viewId, @NonNull MultipleItemEntity entity, Object key) {
        try {
            final OrderFormEntity title = entity.getField(key);
            final ButtonListLayout mBll = getView(viewId);
            mBll.getTextLeftBtn().setText(title.getText());
            mBll.getTextRightBtn().setText(title.getValue());
        } catch (Exception e) {
            setShow(viewId, false);
        }
        return this;
    }

    public MultipleViewHolder setOrderForm(@IdRes int viewId, @NonNull MultipleItemEntity entity, Object key) {
        final OrderFormEntity orderEntity = entity.getField(key);
        return setOrderForm(viewId, orderEntity);
    }

    public MultipleViewHolder setOrderFormGone(@IdRes int viewId, @NonNull MultipleItemEntity entity, Object key) {
        try {
            final boolean isGone = entity.getField(key);
            setGone(viewId, isGone);
        } catch (Exception e) {
            setGone(viewId, false);
        }
        return this;
    }

    public MultipleViewHolder setOrderFormShow(@IdRes int viewId, @NonNull MultipleItemEntity entity, Object key) {
        try {
            final boolean isGone = entity.getField(key);
            setShow(viewId, isGone);
        } catch (Exception e) {
            setShow(viewId, false);
        }
        return this;
    }

    @NonNull
    public MultipleViewHolder setTag(int viewId, Object tag) {
        getView(viewId).setTag(tag);
        return this;
    }

    @NonNull
    public MultipleViewHolder setText(int viewId, CharSequence key) {
        super.setText(viewId, key);
        return this;
    }

    @NonNull
    public MultipleViewHolder setGone(int viewId, boolean key) {
        super.setGone(viewId, key);
        return this;
    }

    @NonNull
    public MultipleViewHolder setShow(int viewId, boolean key) {
        super.setGone(viewId, !key);
        return this;
    }


    @NonNull
    @Override
    public MultipleViewHolder setTextColorRes(int viewId, int colorRes) {
        super.setTextColorRes(viewId, colorRes);
        return this;
    }

    @NonNull
    @Override
    public MultipleViewHolder setTextColor(@IdRes int viewId, @ColorInt int color) {
        super.setTextColor(viewId, color);
        return this;
    }

    public MultipleViewHolder setOrderForm(@IdRes int viewId, @NonNull OrderFormEntity orderEntity) {
        setText(viewId, SpannableStringUtils.getBuilder(orderEntity.getText())
                .append(orderEntity.getValue() == null ? "" : orderEntity.getValue())
                .setClickSpan(orderEntity.isValueColor() ? accentSpan : textSpan)
                .create());
        return this;
    }

    public MultipleViewHolder setOrderForm(@IdRes int viewId, @NonNull OrderFormEntity orderEntity, @ColorInt int color) {
        setText(viewId, SpannableStringUtils.getBuilder(orderEntity.getText())
                .append(orderEntity.getValue() == null ? "" : orderEntity.getValue())
                .setClickSpan(new OtherClickableSpan(color))
                .create());
        return this;
    }

    @Deprecated
    public MultipleViewHolder inGlideIcon(@IdRes int viewId, @NonNull MultipleItemEntity entity) {
        final int imageIcon = entity.getField(MultipleFields.ICON_IMAGE);
        setImageResource(viewId, imageIcon);
        return this;
    }

    public MultipleViewHolder setGlideIcon(@IdRes int viewId, @NonNull MultipleItemEntity entity) {
        final int imageIcon = entity.getField(MultipleFields.ICON_IMAGE);
        setImageResource(viewId, imageIcon);
        return this;
    }

    @Deprecated
    public MultipleViewHolder inGlideUrl(@IdRes int viewId, @NonNull MultipleItemEntity entity) {
        final String imageUrl = entity.getField(MultipleFields.IMAGE);
        @DrawableRes int resourceId;
        try {
            resourceId = entity.getField(MultipleFields.ERROR_IMAGE);
        } catch (Exception e) {
            resourceId = R.mipmap.iv_default_head_image;
        }
        return setGlideUrl(viewId, imageUrl, resourceId);
    }

    public MultipleViewHolder setGlideUrl(@IdRes int viewId, @NonNull MultipleItemEntity entity) {
        final String imageUrl = entity.getField(MultipleFields.IMAGE);
        @DrawableRes int resourceId;
        try {
            resourceId = entity.getField(MultipleFields.ERROR_IMAGE);
        } catch (Exception e) {
            resourceId = R.mipmap.iv_default_head_image;
        }
        return setGlideUrl(viewId, imageUrl, resourceId);
    }

    public MultipleViewHolder setStatus(@IdRes int viewId, @NonNull MultipleItemEntity entity) {
        return setStatus(viewId, entity, MultipleFields.STATUS);
    }

    public MultipleViewHolder setStatus(@IdRes int viewId, @NonNull MultipleItemEntity entity, Object key) {
        try {
            final StatusEntity status = entity.getField(key);
            setShow(viewId, true)
                    .setText(viewId, status.getText())
                    .setTextColor(viewId, status.getTextColor())
                    .setBackgroundResource(viewId, status.getBackdropColorRes());
        } catch (Exception e) {
            setShow(viewId, false);
        }
        return this;
    }

    public MultipleViewHolder setGlideCircleUrl(@IdRes int viewId, @NonNull MultipleItemEntity entity) {
        final String imageUrl = entity.getField(MultipleFields.IMAGE);
        @DrawableRes int resourceId;
        try {
            resourceId = entity.getField(MultipleFields.ERROR_IMAGE);
        } catch (Exception e) {
            resourceId = R.mipmap.iv_default_head_image;
        }
        return setGlideCircleUrl(viewId, imageUrl, resourceId);
    }

    public MultipleViewHolder setGlideCircleUrl(ImageView imageView, @NonNull MultipleItemEntity entity) {
        final String imageUrl = entity.getField(MultipleFields.IMAGE);
        @DrawableRes int resourceId;
        try {
            resourceId = entity.getField(MultipleFields.ERROR_IMAGE);
        } catch (Exception e) {
            resourceId = R.mipmap.iv_default_head_image;
        }
        return setGlideCircleUrl(imageView, imageUrl, resourceId);
    }

    @Deprecated
    public MultipleViewHolder inGlideUrl(@IdRes int viewId, @NonNull MultipleItemEntity entity, @DrawableRes int resourceId) {
        final String imageUrl = entity.getField(MultipleFields.IMAGE);
        return setGlideUrl(viewId, imageUrl, resourceId);
    }

    public MultipleViewHolder setGlideUrl(@IdRes int viewId, @NonNull MultipleItemEntity entity, @DrawableRes int resourceId) {
        final String imageUrl = entity.getField(MultipleFields.IMAGE);
        return setGlideUrl(viewId, imageUrl, resourceId);
    }

    @Deprecated
    public MultipleViewHolder inGlideUrl(@IdRes int viewId, String imageUrl) {
        return setGlideUrl(viewId, imageUrl, R.mipmap.iv_default_head_image);
    }

    public MultipleViewHolder setGlideUrl(@IdRes int viewId, String imageUrl) {
        return setGlideUrl(viewId, imageUrl, R.mipmap.iv_default_head_image);
    }

    @Deprecated
    public MultipleViewHolder inGlideUrl(@IdRes int viewId, String imageUrl, @DrawableRes int resourceId) {
        return setGlideUrl(viewId, imageUrl, resourceId);
    }


    public MultipleViewHolder setGlideUrl(@IdRes int viewId, String imageUrl, @DrawableRes int resourceId) {
        final ImageView imageView = getView(viewId);
        Glide.with(Latte.getActivity())
                .load(imageUrl)
                .error(resourceId)
                .apply(RECYCLER_OPTIONS)
                .into(imageView);
        return this;
    }

    public MultipleViewHolder setGlideCircleUrl(@IdRes int viewId, String imageUrl, @DrawableRes int resourceId) {
        final ImageView imageView = getView(viewId);
        return setGlideCircleUrl(imageView, imageUrl, resourceId);
    }

    public MultipleViewHolder setGlideCircleUrl(ImageView imageView, String imageUrl, @DrawableRes int resourceId) {
        Glide.with(Latte.getActivity())
                .load(imageUrl)
                .error(resourceId)
                .apply(RECYCLER_OPTIONS)
                .circleCrop()
                .into(imageView);
        return this;
    }

    private final ClickableSpan textSpan = OtherClickableSpan.create(R.color.textColor_85);

    private final ClickableSpan accentSpan =  OtherClickableSpan.create(R.color.colorAccent);


    /********************************** TextView 相关函数 ****************************************/

    public MultipleViewHolder setInputType(int viewId, int type) {
        EditText view = getView(viewId);
        view.setInputType(type);
        view.setSingleLine(false);
        return this;
    }

    /***
     * 设置最大输入长度
     * @param viewId:
     * @param value:
     */
    public MultipleViewHolder setFilters(int viewId, int value) {
        TextView view = getView(viewId);
        view.setFilters(new InputFilter[]{new InputFilter.LengthFilter(value)});
        return this;
    }

    /***
     * 设置提示
     * @param viewId:
     * @param hint:
     */
    public MultipleViewHolder setHint(int viewId, String hint) {
        TextView editText = getView(viewId);
        editText.setHint(hint);
        return this;
    }

    /***
     * 针对按钮的点击事件
     * @param viewId：
     * @param is：
     */
    public MultipleViewHolder setOnClick(int viewId, boolean is) {
        //去掉点击时编辑框下面横线:
        getView(viewId).setEnabled(is);
        //不可编辑
        getView(viewId).setFocusable(is);
        return this;
    }

    /***
     * 针对输入控件的是否编辑
     * @param viewId：
     * @param is：
     */
    public MultipleViewHolder setEdit(int viewId, boolean is) {
        //去掉点击时编辑框下面横线:
        getView(viewId).setEnabled(is);
        //不可编辑
        getView(viewId).setFocusable(is);
        //不可编辑
        getView(viewId).setFocusableInTouchMode(is);
        return this;
    }

    //****************************** 手机号、身份证号等星号显示  *********************************/

    /**
     * @param viewId:控件
     * @param text:设置‘手机号’显示为隐藏
     */
    protected MultipleViewHolder setTextPhone(@IdRes int viewId, String text) {
        if (TextUtils.isEmpty(text)) {
            return this;
        }
        setText(viewId, text.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));
        return this;
    }

    /**
     * @param viewId:控件
     * @param text:设置‘身份证号’显示为隐藏
     */
    protected MultipleViewHolder setTextIdCard(@IdRes int viewId, String text) {
        if (TextUtils.isEmpty(text)) {
            return this;
        }
        setText(viewId, text.replaceAll("(\\d{4})\\d{10}(\\w{4})", "$1****$2"));
        return this;
    }

    /**
     * @param viewId:控件
     * @param text:设置‘邮件’显示为隐藏
     */
    protected MultipleViewHolder setTextEmail(@IdRes int viewId, String text) {
        if (TextUtils.isEmpty(text)) {
            return this;
        }
        setText(viewId, text.replaceAll("(\\w?)(\\w+)(\\w)(@\\w+\\.[a-z]+(\\.[a-z]+)?)", "$1****$3$4"));
        return this;
    }

    /********************************** RecyclerView 相关函数 ****************************************/

    public void setRecyclerAdapter(int viewId,
                                   RecyclerView.Adapter<?> value,
                                   RecyclerView.LayoutManager layoutManager) {
        RecyclerView view = getView(viewId);
        view.setLayoutManager(layoutManager);
        view.setAdapter(value);
    }

    /**
     * 获得item
     */
    public int position() {
        return getLayoutPosition();
    }

    /**
     * @param color:设置背景颜色
     */
    public void setBackgroundColor(int color) {
        itemView.setBackgroundColor(color);
    }

    public MultipleViewHolder setOnClickListener(int viewId, View.OnClickListener listener) {
        getView(viewId).setOnClickListener(listener);
        return this;
    }

}
