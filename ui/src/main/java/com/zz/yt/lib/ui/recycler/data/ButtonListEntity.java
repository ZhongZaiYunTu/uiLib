package com.zz.yt.lib.ui.recycler.data;

/**
 * 信息栏item
 *
 * @author qf
 * @version 1.0
 **/
public final class ButtonListEntity {

    /**
     * key
     */
    private final String TEXT;

    /**
     * value
     */
    private final String VALUE;

    /**
     * value显示修改标识
     */
    private final boolean IS_IMAGE;

    public ButtonListEntity(String text) {
        this.TEXT = text;
        this.VALUE = "";
        this.IS_IMAGE = true;
    }

    public ButtonListEntity(String text, String value) {
        this.TEXT = text;
        this.VALUE = value;
        this.IS_IMAGE = true;
    }

    public ButtonListEntity(String text, String value, boolean isImage) {
        this.TEXT = text;
        this.VALUE = value;
        this.IS_IMAGE = isImage;
    }

    public String getText() {
        return TEXT;
    }

    public String getValue() {
        return VALUE;
    }

    public boolean isImage() {
        return IS_IMAGE;
    }
}
