package com.zz.yt.lib.mvp.base.list.adapter;


import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.mvp.base.list.abolish.click.OnItemObjectClick;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.recycler.adapter.quick.ShrinkRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gags
 * @version 1.0
 */
public abstract class BaseMvpRecyclerAdapter<Entry>
        extends ShrinkRecyclerAdapter<Entry> {

    protected Object object = null;
    protected OnItemObjectClick mItemObjectClick = null;
    protected OnClickIntObjListener mClickIntObjListener = null;

    public BaseMvpRecyclerAdapter(int layoutResId, List<Entry> data) {
        super(layoutResId, data);
    }

    /**
     * 获得 List<Object> 类型数据
     */
    @NonNull
    public List<Object> getObjData() {
        return new ArrayList<Object>(getData());
    }

    /**
     * @param object：全局的其他属性
     */
    public void setObject(Object object) {
        this.object = object;
    }

    /**
     * @param click:设置按钮点击事件
     */
    public void setOnItemObjectClick(OnItemObjectClick click) {
        this.mItemObjectClick = click;
    }

    /**
     * @param listener:设置按钮点击事件
     */
    public void setOnClickIntObjListener(OnClickIntObjListener listener) {
        this.mClickIntObjListener = listener;
    }

    /**
     * @param other：其他后面请求数据
     */
    public void setOther(Object other) {

    }

    //********************************* 判断及赋值方法 *******************************************///

    /***
     * 获得控件输入内容
     * @param view：
     * @param viewId：
     */
    protected String getTagView(@NonNull View view, int viewId) {
        TextView depText = view.findViewById(viewId);
        if (depText != null) {
            return depText.getTag() + "";
        }
        return "";
    }

    /***
     * 获得控件输入内容
     * @param view：
     * @param viewId：
     */
    protected String getTextView(@NonNull View view, int viewId) {
        TextView depText = view.findViewById(viewId);
        if (depText != null) {
            return depText.getText().toString().trim();
        }
        return "";
    }

    /***
     * 判断数据是否为空
     * @param view：
     * @param viewId：
     * @param text：
     */
    protected boolean isEmpty(@NonNull View view, int viewId, final CharSequence text) {
        TextView depText = view.findViewById(viewId);
        if (depText != null && depText.getText().toString().isEmpty()) {
            ToastUtils.showShort(text);
            return true;
        }
        return false;
    }


}
