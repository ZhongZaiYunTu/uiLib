package com.zz.yt.lib.ui.view.base;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ObjectUtils;
import com.zz.yt.lib.mvp.base.view.BaseMvpRecyclerLayout;
import com.zz.yt.lib.ui.R;


/**
 * 默认列表控件(带adapter)
 *
 * @author qf
 * @version 2.5
 */
public abstract class BaseRecycleView<T> extends BaseMvpRecyclerLayout<T> {

    @Override
    protected int setLayout() {
        return R.layout.hai_view_title_time_recycler;
    }

    public BaseRecycleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setIsIcon(setIsIcon());

        setTextTitle(setTitle());

        setTextTime(setTime());

        setIsMore(setIsMore());

        setMore(setMore());

    }

    @Override
    protected RecyclerView setRecyclerView() {
        return findViewById(R.id.view_chart_recycler_view);
    }


    /*** 是否显示标题栏左边的色块 */
    protected boolean setIsIcon() {
        return true;
    }

    /**
     * @param is:是否显示标题栏左边的色块
     */
    public void setIsIcon(boolean is) {
        setShow(R.id.view_chart_icon, is);
    }

    /*** 标题 */
    protected abstract String setTitle();

    /**
     * @param title:标题
     */
    public void setTextTitle(String title) {
        if (ObjectUtils.isNotEmpty(title)) {
            setText(R.id.view_chart_text_title, title);
        }
    }

    /**
     * textStyle效果展示
     */
    public void setTitleBold() {
        setTextStyle(R.id.view_chart_text_title, Typeface.BOLD);
    }

    /**
     * textStyle效果展示
     */
    public void setTitleItalic() {
        setTextStyle(R.id.view_chart_text_title, Typeface.ITALIC);
    }

    /*** 标题后的时间 */
    protected String setTime() {
        return null;
    }

    /**
     * @param time:标题后的时间
     */
    public void setTextTime(String time) {
        if (ObjectUtils.isNotEmpty(time)) {
            setText(R.id.view_chart_text_time, time);
        }
    }

    /*** 是否显示标题栏右边的更多 */
    protected boolean setIsMore() {
        return true;
    }

    /*** 是否显示标题栏右边的更多 */
    public void setIsMore(boolean isMore) {
        setShow(R.id.view_chart_text_more, isMore);
        setOnClickListener(R.id.view_chart_text_more, new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickMoreListener != null) {
                    mClickMoreListener.onClickMore(-1, null);
                }
            }
        });
    }

    /*** 标题栏右边的内容设置，默认是更多 */
    protected String setMore() {
        return null;
    }

    /*** 标题栏右边的按钮内容设置 */
    public void setMore(String more) {
        if (ObjectUtils.isNotEmpty(more)) {
            setText(R.id.view_chart_text_more, more);
        }
    }
}
