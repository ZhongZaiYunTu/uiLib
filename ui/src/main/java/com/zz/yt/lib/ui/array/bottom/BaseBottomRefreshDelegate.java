package com.zz.yt.lib.ui.array.bottom;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zz.yt.lib.ui.base.bottom.array.LatteBottomArrayDelegate;
import com.zz.yt.lib.ui.refresh.BaseRefreshHandler;

/***
 * 纯列表类处理
 * @author qf
 * @version 2.0
 **/
public abstract class BaseBottomRefreshDelegate<refresh extends BaseRefreshHandler>
        extends LatteBottomArrayDelegate {


    protected refresh mRefreshHandler = null;

    /**
     * 设置加载
     *
     * @param refreshHandler:数据处理
     */
    protected abstract void setRefreshHandler(refresh refreshHandler);

    @Override
    protected void initRefreshHandler(SwipeRefreshLayout swipeRefreshLayout,
                                      RecyclerView recyclerView) {
        mRefreshHandler = setRefreshHandler(recyclerView);
    }

    /***
     * 分页数据和刷新处理
     * @param recyclerView:数据处理
     */
    protected abstract refresh setRefreshHandler(RecyclerView recyclerView);

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        setRefreshHandler(mRefreshHandler);
        firstPage();
    }

    @Override
    protected void firstPage(String url) {
        if (mRefreshHandler != null) {
            mRefreshHandler.firstPage(url, PARAMETER);
        }
    }

    @Override
    public void responseSubmit() {
        if (mRefreshHandler != null) {
            mRefreshHandler.onRefresh();
        }
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (mRefreshHandler != null && resultCode == Activity.RESULT_OK) {
            mRefreshHandler.onRefresh();
        }
    }


}
