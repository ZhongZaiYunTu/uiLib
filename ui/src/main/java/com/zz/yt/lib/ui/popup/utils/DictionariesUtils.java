package com.zz.yt.lib.ui.popup.utils;

import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.net.callback.ISuccess;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.entity.DictionariesPickerEntry;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.popup.utils.picker.PickerPopupUtils;
import com.zz.yt.lib.ui.utils.ObjUtils;

import java.util.List;

/**
 * 字典查询（EditLayoutBar）
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/6/17
 **/
public class DictionariesUtils implements View.OnClickListener {

    private final EditLayoutBar mEditLayoutBar;
    private List<DictionariesEntry> jsonArray;
    private OnClickIntObjListener mClickIntObjListener = null;


    @NonNull
    public static DictionariesUtils create(EditLayoutBar editLayoutBar, String type) {
        return new DictionariesUtils(editLayoutBar, type);
    }

    private DictionariesUtils(@NonNull EditLayoutBar editLayoutBar, String type) {
        this.mEditLayoutBar = editLayoutBar;
        editLayoutBar.setOnClickListener(this);
        //type:字典参数
        final String TYPE = StringUtils.getString(R.string.api_dictionaries_type, "type");
        RestClient.builder()
                .url(StringUtils.getString(R.string.api_dictionaries))
                .params(TYPE, type)
                .analysis(true)
                .success(new ISuccess() {
                    @Override
                    public void onSuccess(String data) {
                        if (!ObjectUtils.isEmpty(data)) {
                            jsonArray = GsonUtils.fromJson(data, GsonUtils.getListType(DictionariesEntry.class));
                        }
                    }
                })
                .build()
                .get();
    }

    public DictionariesUtils setOnClickIntObjListener(OnClickIntObjListener listener) {
        mClickIntObjListener = listener;
        return this;
    }

    @Override
    public void onClick(View v) {
        if (jsonArray == null || jsonArray.size() == 0) {
            ToastUtils.showShort("未查询到数据，请稍后再试。");
            return;
        }
        PickerPopupUtils.showPickerData(Latte.getActivity(),
                "", jsonArray, 0, new OnClickIntObjListener() {
                    @Override
                    public void onClick(int index, Object obj) {
                        if (mClickIntObjListener != null) {
                            mClickIntObjListener.onClick(index, obj);
                        }
                        DictionariesEntry entry = ObjUtils.getField(obj);
//                    DictionariesEntry entry = jsonArray.get(index);
                        if (entry != null) {

                            mEditLayoutBar.setTag(entry.getValue());
                            mEditLayoutBar.setText(entry.getPickerViewText());
                        }
                    }
                });
    }

    static class DictionariesEntry extends DictionariesPickerEntry {

        @Override
        public String getPickerViewText() {
            return TextUtils.isEmpty(label) ? name : label;
        }
    }

}
