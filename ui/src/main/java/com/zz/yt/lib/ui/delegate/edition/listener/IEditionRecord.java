package com.zz.yt.lib.ui.delegate.edition.listener;

public interface IEditionRecord {
    void onEditionRecord();
}
