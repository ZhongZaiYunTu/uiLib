package com.zz.yt.lib.ui.recycler.data;

import androidx.annotation.Nullable;

import com.flyco.tablayout.listener.CustomTabEntity;

/**
 * tab 内容
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/5/26
 **/
public class TabEntity implements CustomTabEntity {

    /**
     * tab 内容
     */
    private final String TEXT;

    /**
     * 选中的图标
     */
    private final int ICON;

    /**
     * 未选中的图标
     */
    private final int unIcon;

    public TabEntity(String text) {
        this.TEXT = text;
        this.ICON = 0;
        this.unIcon = 0;
    }

    public TabEntity(String text, int icon) {
        this.TEXT = text;
        this.ICON = icon;
        this.unIcon = 0;
    }

    public TabEntity(String text, int icon, int unIcon) {
        this.TEXT = text;
        this.ICON = icon;
        this.unIcon = unIcon;
    }

    @Override
    public String getTabTitle() {
        return TEXT;
    }

    @Override
    public int getTabSelectedIcon() {
        return ICON;
    }

    @Override
    public int getTabUnselectedIcon() {
        return unIcon;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj);
    }
}
