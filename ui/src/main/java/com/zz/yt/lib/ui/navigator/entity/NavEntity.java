package com.zz.yt.lib.ui.navigator.entity;

/**
 * 目录类导航器
 *
 * @author qf
 * @version 1.0
 * @date 2020/7/13
 */
public class NavEntity {

    /**
     * 第index级
     */
    private final int INDEX;

    /**
     * 导航标题
     */
    private final String NAV_TITLE;

    /**
     * 导航id
     */
    private final Object DATA_ID;

    /**
     * 当前目录缓存的数据
     */
    private final Object CACHE_DATA;

    public NavEntity(int index, String navTitle, Object dataId) {
        this.INDEX = index;
        this.NAV_TITLE = navTitle;
        this.DATA_ID = dataId;
        this.CACHE_DATA = null;
    }

    public NavEntity(int index, String navTitle, Object dataId, Object cacheData) {
        this.INDEX = index;
        this.NAV_TITLE = navTitle;
        this.DATA_ID = dataId;
        this.CACHE_DATA = cacheData;
    }

    public int getIndex() {
        return INDEX;
    }

    public String getNavTitle() {
        return NAV_TITLE;
    }

    public Object getDataId() {
        return DATA_ID;
    }

    public Object getCacheData() {
        return CACHE_DATA;
    }
}
