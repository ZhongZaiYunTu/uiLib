package com.zz.yt.lib.ui.utils;



import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ColorUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import me.yokeyword.fragmentation.ISupportFragment;

/***
 * 切换的delegate
 *
 * @author qf
 * @version 2022/4/24
 * @since 2.5
 */
public class LatteCustomTabUtils implements OnTabSelectListener {

    private final CommonTabLayout mCommonTabBar;

    /**
     * 补充控件
     */
    private final int containerId;

    /**
     * 第一次默认显示的 tab
     */
    private int mIndexDelegate = 0;

    /**
     * 上一次显示的tab
     */
    private int mCurrentDelegate = 0;

    /**
     * CommonTabLayout 选中的颜色
     */
    private int mClickedColor = ColorUtils.getColor(R.color.colorAccent);

    /**
     * CommonTabLayout 内容
     */
    private final ArrayList<CustomTabEntity> TAB_BEANS = new ArrayList<>();

    /**
     * 填充的布局
     */
    private final ArrayList<LatteDelegate> ITEM_DELEGATES = new ArrayList<>();

    private LatteDelegate delegate;

    public LatteCustomTabUtils(CommonTabLayout mCommonTabBar, int containerId) {
        this.mCommonTabBar = mCommonTabBar;
        this.containerId = containerId;
    }

    public void setContent(LatteDelegate delegate) {
        this.delegate = delegate;
    }

    public void setSelectColor(int mClickedColor) {
        this.mClickedColor = mClickedColor;
    }

    public void setIndexDelegate(int mIndexDelegate) {
        this.mIndexDelegate = mIndexDelegate;
    }

    public void setEntity(@NonNull LinkedHashMap<CustomTabEntity, BaseBottomItemDelegate> ITEMS) {
        for (Map.Entry<CustomTabEntity, BaseBottomItemDelegate> item : ITEMS.entrySet()) {
            final CustomTabEntity key = item.getKey();
            final BaseBottomItemDelegate value = item.getValue();
            TAB_BEANS.add(key);
            ITEM_DELEGATES.add(value);
        }
    }

    public void init() {
        if (mCommonTabBar != null) {
            mCommonTabBar.setTabData(TAB_BEANS);
            mCommonTabBar.setOnTabSelectListener(this);
            mCommonTabBar.setCurrentTab(mIndexDelegate);
            mCommonTabBar.setTextSelectColor(mClickedColor);
        }
        //
        final int size = ITEM_DELEGATES.size();
        final ISupportFragment[] delegateArray = ITEM_DELEGATES.toArray(new ISupportFragment[size]);
        delegate.getSupportDelegate().loadMultipleRootFragment(containerId, mIndexDelegate, delegateArray);
    }

    @Override
    public void onTabSelect(int position) {
        mCommonTabBar.setCurrentTab(position);
        //
        delegate.getSupportDelegate().showHideFragment(ITEM_DELEGATES.get(position), ITEM_DELEGATES.get(mCurrentDelegate));
        //注意先后顺序
        mCurrentDelegate = position;
    }

    @Override
    public void onTabReselect(int position) {

    }
}
