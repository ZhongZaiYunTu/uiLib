package com.zz.yt.lib.ui.popup.utils;

import android.view.View;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.utils.AddressUtils;


/**
 * 省市区选择
 *
 * @author qf
 * @version 1.0
 **/
public class AddressPopupUtils implements View.OnClickListener {

    private final EditLayoutBar mEditLayoutBar;

    @NonNull
    public static AddressPopupUtils create(EditLayoutBar editLayoutBar) {
        return new AddressPopupUtils(editLayoutBar);
    }

    private AddressPopupUtils(@NonNull EditLayoutBar editLayoutBar) {
        this.mEditLayoutBar = editLayoutBar;
        if (editLayoutBar != null) {
            editLayoutBar.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        AddressUtils.getInstance().onClickListener(new OnClickStringListener() {
            @Override
            public void onClick(String tx) {
                if (mEditLayoutBar != null) {
                    mEditLayoutBar.getValueView().setTag(tx);
                    mEditLayoutBar.getValueView().setText(tx);
                }
            }
        }).popup();
    }

}
