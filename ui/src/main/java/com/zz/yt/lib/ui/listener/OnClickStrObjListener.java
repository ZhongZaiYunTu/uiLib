package com.zz.yt.lib.ui.listener;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0
 **/
public interface OnClickStrObjListener {

    /**
     * 点击
     *
     * @param str：值a
     * @param obj：值b
     */
    void onClick(String str, Object obj);

}
