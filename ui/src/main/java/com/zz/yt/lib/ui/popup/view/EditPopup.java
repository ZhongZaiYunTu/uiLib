package com.zz.yt.lib.ui.popup.view;

import android.annotation.SuppressLint;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.ui.R;

/**
 * 修改名称匡
 *
 * @author qf
 * @version 1.0.30
 **/
@SuppressLint("ViewConstructor")
public class EditPopup extends LattePopupCenter {

    private String mTitle = null;
    private String mText = null;
    private int mInputType = InputType.TYPE_NULL;

    private EditText textEdit = null;

    @NonNull
    public static EditPopup create() {
        return new EditPopup();
    }

    private EditPopup() {
        super(Latte.getActivity());
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_edit;
    }

    @Override
    protected void initViews() {
        setText(R.id.tv_title, mTitle);
        setText(R.id.id_et_text, mText);
        setTextHint(R.id.id_et_text, "请填写" + mTitle);
        textEdit = getEditText(R.id.id_et_text);
        if (mInputType != InputType.TYPE_NULL) {
            textEdit.setInputType(mInputType);
        }
        final Button idOkBtn = getButton(R.id.confirm);
        idOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    final String confirm = textEdit.getText().toString().trim();
                    if (TextUtils.isEmpty(confirm)) {
                        ToastUtils.showShort("请填写" + mTitle);
                        return;
                    }
                    mClickDecisionListener.onConfirm(confirm);
                }
                dismiss();
            }
        });
        final Button idCancelBtn = getButton(R.id.cancel);
        idCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    mClickDecisionListener.onCancel();
                }
                dismiss();
            }
        });

    }

    /**
     * 设置标题数据
     *
     * @param title：
     */
    public EditPopup setTitle(String title) {
        this.mTitle = title;
        return this;
    }

    /**
     * 设置默认值
     *
     * @param text：
     */
    public EditPopup setText(String text) {
        this.mText = text;
        return this;
    }

    /**
     * 设置输入类型
     *
     * @param inputType：
     */
    public EditPopup setText(int inputType) {
        this.mInputType = inputType;
        return this;
    }


}
