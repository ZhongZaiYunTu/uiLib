package com.zz.yt.lib.ui.recycler.holder;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ColorUtils;


/**
 * 动态设置颜色
 *
 * @author qf
 * @version 1.0.3
 */
public final class OtherClickableSpan extends ClickableSpan {

    private final int color;

    @NonNull
    public static OtherClickableSpan create(@ColorRes int id) {
        return new OtherClickableSpan(ColorUtils.getColor(id));
    }

    public OtherClickableSpan(@ColorInt int color) {
        this.color = color;
    }

    @Override
    public void onClick(@NonNull View widget) {

    }

    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
        ds.setColor(color);
        ds.setUnderlineText(false);
    }
}
