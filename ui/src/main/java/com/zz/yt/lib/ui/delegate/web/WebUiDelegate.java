package com.zz.yt.lib.ui.delegate.web;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whf.android.jar.web.WebDelegate;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.delegate.web.download.WebDelegateUiImpl;
import com.zz.yt.lib.ui.listener.OnClickStringListener;

import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

/**
 * web
 *
 * @author qf
 * @version 1.0
 */
public class WebUiDelegate extends LatteTitleDelegate implements OnClickStringListener {

    private String title = null;
    protected String url = null;

    protected WebUiDelegate() {
    }

    @NonNull
    public static WebUiDelegate create(String title, String url) {
        final Bundle args = new Bundle();
        args.putString(BundleKey.TITLE.name(), title);
        args.putString(BundleKey.URL.name(), url);
        final WebUiDelegate delegate = new WebUiDelegate();
        delegate.setArguments(args);
        return delegate;
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        title = bundle.getString(BundleKey.TITLE.name(), "视频预览");
        url = bundle.getString(BundleKey.URL.name(), "index.html");
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        final Bundle args = getArguments();
        if (args != null) {
            titleBar.setText(title);
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_web;
    }

    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        final Bundle args = getArguments();
        if (args != null) {
            final WebDelegate delegate = loadRootDelegate();
            delegate.setTopDelegate(this);
            getSupportDelegate().loadRootFragment(R.id.id_layout_frame, delegate);
        }
    }

    /**
     * @return 界面
     */
    protected WebDelegate loadRootDelegate() {
        WebDelegateUiImpl delegate = WebDelegateUiImpl.create(url);
        delegate.setOnClickStringListener(this);
        delegate.setPageLoadListener(this);
        return delegate;
    }

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        return new DefaultHorizontalAnimator();
    }

    @Override
    public void onClick(String fileUrl) {
        inFile(fileUrl);
    }
}
