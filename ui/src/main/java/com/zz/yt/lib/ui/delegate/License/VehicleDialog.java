package com.zz.yt.lib.ui.delegate.License;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.parkingwang.keyboard.OnInputChangedListener;
import com.parkingwang.keyboard.PopupKeyboard;
import com.parkingwang.keyboard.view.InputView;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickStringListener;

/**
 * @author 黄浩杭 (msdx.android@qq.com)
 * @since 2018-12-14
 */
public class VehicleDialog extends DialogFragment {

    private OnClickStringListener mClickStringListener = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vehicle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final InputView mInputView = view.findViewById(R.id.input_view);

        // 创建弹出键盘
        final PopupKeyboard mPopupKeyboard = new PopupKeyboard(getContext());
        // 弹出键盘内部包含一个KeyboardView，在此绑定输入两者关联。
        mPopupKeyboard.attach(mInputView, getDialog());

        // 隐藏确定按钮
        mPopupKeyboard.getKeyboardEngine().setHideOKKey(false);

        // KeyboardInputController提供一个默认实现的新能源车牌锁定按钮
        mPopupKeyboard.getController()
                .setDebugEnabled(true);
        mPopupKeyboard.getController().addOnInputChangedListener(new OnInputChangedListener() {
            @Override
            public void onChanged(String number, boolean isCompleted) {
                if (isCompleted) {
                    mPopupKeyboard.dismiss(getDialog().getWindow());
                }
            }

            @Override
            public void onCompleted(String number, boolean isAutoCompleted) {
                mPopupKeyboard.dismiss(getDialog().getWindow());
            }
        });
        view.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = mInputView.getNumber();
                LatteLogger.i(number);
                if (number.length() < 7) {
                    return;
                }
                if (mClickStringListener != null) {
                    mClickStringListener.onClick(number);
                }
                dismissAllowingStateLoss();
            }
        });
    }

    public void setOnClickStringListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
    }

    public void show(FragmentManager manager) {
        super.show(manager, getTag());
    }
}
