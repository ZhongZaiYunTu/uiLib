package com.zz.yt.lib.ui.delegate.edition;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.ButtonListLayout;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.delegate.edition.about.EditionAboutDelegate;
import com.zz.yt.lib.ui.delegate.edition.record.EditionRecordDelegate;
import com.zz.yt.lib.ui.privacy.PrivacyDao;


/**
 * 版本信息
 *
 * @author qf
 * @version 1.0.9
 **/
public class EditionDelegate extends LatteTitleDelegate {

    /**
     * 是否是全屏
     */
    private boolean isTop = true;

    @NonNull
    public static EditionDelegate create() {
        Bundle args = new Bundle();
        EditionDelegate fragment = new EditionDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    public static EditionDelegate create(boolean isTop) {
        final Bundle args = new Bundle();
        args.putBoolean(BundleKey.IS_TOP.name(), isTop);
        final EditionDelegate delegate = new EditionDelegate();
        delegate.setArguments(args);
        return delegate;
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        isTop = bundle.getBoolean(BundleKey.IS_TOP.name(), true);
    }

    @Override
    protected boolean isTopView() {
        return isTop;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("版本信息");
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_delegate_ui_edition;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        ImageView imgIcon = findViewById(R.id.img_icon);
        if (imgIcon != null) {
            imgIcon.setImageResource(Latte.getAppIcon());
        }

        ButtonListLayout bllCode = findViewById(R.id.bll_code);
        String v = Latte.getDebug() ? "debug v" : "v";
        bllCode.getTextRightBtn().setText(v + Latte.getVersionName());

        ButtonListLayout bllAbout = findViewById(R.id.bll_about);
        if (bllAbout != null) {
            bllAbout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //start(create(R.string.about, R.string.about_url));
                    start(EditionAboutDelegate.create());
                }
            });
        }

        ButtonListLayout bllRecord = findViewById(R.id.bll_record);
        if (bllRecord != null) {
            bllRecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    start(EditionRecordDelegate.create());
                }
            });
        }

        TextView textAgreement = findViewById(R.id.text_agreement);
        if (textAgreement != null) {
            textAgreement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //用户协议
                    if (mIPrivacyListener != null) {
                        mIPrivacyListener.onClickAgreement();
                    } else {
                        create(R.string.agreement, PrivacyDao.agreement());
                    }
                }
            });
        }
        TextView textPrivacy = findViewById(R.id.text_privacy);
        if (textPrivacy != null) {
            textPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //隐私政策
                    if (mIPrivacyListener != null) {
                        mIPrivacyListener.onClickPrivacy();
                    } else {
                        create(R.string.privacy, PrivacyDao.privacy());
                    }
                }
            });
        }
    }

    /**
     * @param title:跳转到网页界面的标题
     * @param url:网页地址
     */
    protected void create(int title, String url) {
        startWebUi(getResources().getString(title), url);
    }

}
