package com.zz.yt.lib.ui.listener;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0
 **/
public interface OnClickIntStrListener {

    /**
     * 点击
     *
     * @param index：下标
     * @param str：值
     */
    void onClick(int index, String str);

}
