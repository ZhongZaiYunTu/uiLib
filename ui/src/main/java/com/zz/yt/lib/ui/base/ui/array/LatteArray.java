package com.zz.yt.lib.ui.base.ui.array;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.view.ScreenListView;

import java.util.WeakHashMap;


/**
 * 纯列表类处理(基类)
 *
 * @author qf
 * @version 1.0.2
 **/
abstract class LatteArray extends LatteTitleDelegate implements SwipeRefreshLayout.OnRefreshListener {


    /***  新进界面是否默认加载数据 */
    protected boolean netStatus = true;

    protected ScreenListView mScreenListView = null;

    /***  条件参数 */
    protected final WeakHashMap<String, Object> PARAMETER = new WeakHashMap<>();

    /***
     * 刷新控件
     */
    protected SwipeRefreshLayout mSwipeRefreshLayout = null;

    /***
     * 设置列表
     * @param recyclerView:数据处理
     */
    protected abstract void initRecyclerView(RecyclerView recyclerView);

    /***
     * 设置下拉刷新
     * @param swipeRefreshLayout:数据处理
     * @param recyclerView:数据处理
     */
    protected abstract void initRefreshHandler(SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView);


    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);

        mSwipeRefreshLayout = rootView.findViewById(R.id.id_swipe_Layout);
        initRefreshLayout();

        final RecyclerView recyclerView = rootView.findViewById(R.id.id_recycler_view);
        if (recyclerView != null) {
            initRecyclerView(recyclerView);
            //初始化数据处理
            initRefreshHandler(mSwipeRefreshLayout, recyclerView);
        }

        final ScreenListView screenView = rootView.findViewById(R.id.screen_list_view);
        if (screenView != null) {
            initScreenView(screenView);
        }

        final ImageView imageAdded = rootView.findViewById(R.id.id_image_portrait);
        if (imageAdded != null) {
            initShadowView(imageAdded);
        }
    }

    /**
     * 刷新的背景
     */
    private void initRefreshLayout() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeColors(Color.BLACK, Color.RED);
        }
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        //懒加载，防止事件加不上去
        if (isNet()) {
            firstPage();
        }
    }

    public void setNetStatus(boolean netStatus) {
        this.netStatus = netStatus;
    }

    protected boolean isNet() {
        return netStatus;
    }

    /**
     * @param screenView：数据筛选（设置显示、事件）
     */
    protected void initScreenView(ScreenListView screenView) {
        this.mScreenListView = screenView;
        settScreenView(mScreenListView);
    }

    /**
     * @param screenView：数据筛选（设置adapter）
     */
    protected void settScreenView(ScreenListView screenView) {

    }

    /**
     * @param imageAdded:添加按钮
     */
    protected void initShadowView(ImageView imageAdded) {

    }

    @Override
    public void onRefresh() {
        PARAMETER.clear();
        if (mScreenListView != null) {
            settScreenView(mScreenListView);
        }
        firstPage();
    }

    /**
     * 刷新
     */
    protected void firstPage() {
        if (isNet()) {
            setNetStatus(false);
        }
    }

    /**
     * 刷新加载的网址
     */
    protected String urlPage() {
        return "";
    }
}
