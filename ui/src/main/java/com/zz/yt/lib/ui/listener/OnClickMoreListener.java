package com.zz.yt.lib.ui.listener;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0
 **/
public interface OnClickMoreListener {
    /***
     * 点击
     *
     * @param index：下标
     * @param more：值
     */
    void onClickMore(int index, Object more);
}
