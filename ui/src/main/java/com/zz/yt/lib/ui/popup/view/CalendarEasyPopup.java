package com.zz.yt.lib.ui.popup.view;

import android.annotation.SuppressLint;
import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupBottom;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.callback.OnSelectDateStrCallback;
import com.zz.yt.lib.ui.callback.OnSelectDateCallback;
import com.zz.yt.lib.ui.view.calendar.CalendarSelectEntity;
import com.zz.yt.lib.ui.view.calendar.CalendarSelectView;

import java.util.Calendar;
import java.util.Date;


/**
 * 时间选择(日历de 时间段)
 *
 * @author qf
 * @version 2020/4/24
 **/
@SuppressLint("ViewConstructor")
public class CalendarEasyPopup extends LattePopupBottom {

    /*** 是否多选*/
    private final boolean isRadio;
    /*** true(只能选择今天及以后的时间) */
    private final boolean toDay;

    private OnSelectDateCallback mSelectDateCallback = null;
    private OnSelectDateStrCallback mConfirmSelectDateCallback = null;

    private Date startDayTime = new Date(), endDayTime;
    private CalendarSelectView calendarSelectView = null;

    @NonNull
    public static CalendarEasyPopup create(boolean isRadio, boolean toDay) {
        return new CalendarEasyPopup(isRadio, toDay);
    }

    private CalendarEasyPopup(boolean isRadio, boolean toDay) {
        super(Latte.getActivity());
        this.isRadio = isRadio;
        this.toDay = toDay;
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_calendar;
    }

    /***
     * 设置回调
     * @param callback：
     */
    public CalendarEasyPopup setOnClickListener(OnSelectDateCallback callback) {
        this.mSelectDateCallback = callback;
        return this;
    }

    /***
     * 设置回调*
     * @param callback：
     */
    public CalendarEasyPopup setOnClickListener(OnSelectDateStrCallback callback) {
        this.mConfirmSelectDateCallback = callback;
        return this;
    }

    /***
     * 设置默认选中的时间段
     * @param startDayTime：开始时间
     * @param endDayTime：结束
     */
    public CalendarEasyPopup setStartEndTime(Date startDayTime, Date endDayTime) {
        this.startDayTime = startDayTime;
        this.endDayTime = endDayTime;
        return this;
    }

    @Override
    protected void initViews() {
        calendarSelectView = findViewById(R.id.calendar_view);
        if (calendarSelectView != null) {
            calendarSelectView.setRadio(isRadio);
            calendarSelectView.setIsToday(toDay);
            calendarSelectView.setStartTime(startDayTime);
            calendarSelectView.setEndTime(endDayTime);
            if (toDay) {
                for (int i = 0; i < 12; i++) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MONTH, i);
                    if (calendar.get(Calendar.HOUR_OF_DAY) < 6) {
                        calendar.add(Calendar.DATE, -1);
                    }
                    calendarSelectView.setData(new CalendarSelectEntity(calendar));
                }
            } else {
                for (int i = -12; i < 12; i++) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MONTH, i);
                    calendarSelectView.setData(new CalendarSelectEntity(calendar));
                }
                Latte.getHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //定位点当前时间
                        calendarSelectView.scrollToPosition(12);
                    }
                }, 200);
            }

        }
        setOnClickListener(R.id.tv_close, new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.hai_popup_cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.hai_popup_confirm).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRadio && ObjectUtils.isEmpty(getTime(calendarSelectView.getEndTime()))) {
                    ToastUtils.showLong("请选择结束日期！");
                    return;
                }
                if (mSelectDateCallback != null && calendarSelectView != null) {
                    mSelectDateCallback.selectDate(calendarSelectView.getStartTime(), calendarSelectView.getEndTime());
                }
                if (mConfirmSelectDateCallback != null && calendarSelectView != null) {
                    mConfirmSelectDateCallback.selectDate(getTime(calendarSelectView.getStartTime()),
                            getTime(calendarSelectView.getEndTime()));
                }
                dismiss();
            }
        });
    }


    @NonNull
    private String getTime(Date timeEntity) {
        if (timeEntity != null) {
            return TimeUtils.date2String(timeEntity, "yyyy-MM-dd");
        }
        return "";
    }
}
