package com.zz.yt.lib.ui.view.base;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ObjectUtils;
import com.zz.yt.lib.ui.R;


/**
 * 默认列表控件
 *
 * @author qf
 * @version 2.5
 */
public abstract class BaseRecycleLayout extends BaseLinearLayout {

    protected final RecyclerView recyclerView;

    @Override
    protected int setLayout() {
        return R.layout.hai_view_title_time_recycler;
    }

    public BaseRecycleLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        recyclerView = findViewById(R.id.view_chart_recycler_view);
        recyclerView.setLayoutManager(setLayoutManager(context));

        setIsIcon(setIsIcon());

        setTextTitle(setTitle());

        setTextTime(setTime());

        setIsMore(setIsMore());

        setMore(setMore());

    }

    /**
     * @param context:设置布局管理器
     */
    protected RecyclerView.LayoutManager setLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    /*** 是否显示标题栏左边的色块 */
    protected boolean setIsIcon() {
        return true;
    }

    /**
     * @param is:是否显示标题栏左边的色块
     */
    public void setIsIcon(boolean is) {
        setShow(R.id.view_chart_icon, is);
    }

    /*** 标题 */
    protected abstract String setTitle();

    /**
     * @param title:标题
     */
    public void setTextTitle(String title) {
        if (ObjectUtils.isNotEmpty(title)) {
            setText(R.id.view_chart_text_title, title);
        }
    }

    /**
     * textStyle效果展示
     */
    public void setTitleBold() {
        setTextStyle(R.id.view_chart_text_title, Typeface.BOLD);
    }

    /**
     * textStyle效果展示
     */
    public void setTitleItalic() {
        setTextStyle(R.id.view_chart_text_title, Typeface.ITALIC);
    }

    /*** 标题后的时间 */
    protected String setTime() {
        return null;
    }

    /**
     * @param time:标题后的时间
     */
    public void setTextTime(String time) {
        if (ObjectUtils.isNotEmpty(time)) {
            setText(R.id.view_chart_text_time, time);
        }
    }

    /*** 是否显示标题栏右边的更多 */
    protected boolean setIsMore() {
        return true;
    }

    /*** 是否显示标题栏右边的更多 */
    public void setIsMore(boolean isMore) {
        setShow(R.id.view_chart_text_more, isMore);
        setOnClickListener(R.id.view_chart_text_more, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickMoreListener != null) {
                    mClickMoreListener.onClickMore(-1, null);
                }
            }
        });
    }

    /*** 标题栏右边的内容设置，默认是更多 */
    protected String setMore() {
        return null;
    }

    /*** 标题栏右边的按钮内容设置 */
    public void setMore(String more) {
        if (ObjectUtils.isNotEmpty(more)) {
            setText(R.id.view_chart_text_more, more);
        }
    }
}
