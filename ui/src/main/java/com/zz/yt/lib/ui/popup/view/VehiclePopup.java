package com.zz.yt.lib.ui.popup.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;

import androidx.annotation.NonNull;

import com.parkingwang.keyboard.KeyboardInputController;
import com.parkingwang.keyboard.view.InputView;
import com.parkingwang.keyboard.view.KeyboardView;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.popup.view.base.BaseBottomPopup;


/**
 * 车牌输入弹框
 *
 * @author qf
 * @version 1.0.3
 */
@SuppressLint("ViewConstructor")
public class VehiclePopup extends BaseBottomPopup {

    private final OnClickStringListener mClickStringListener;

    private InputView mInputView;

    public static void create(Activity context, OnClickStringListener listener) {
        new VehiclePopup(context, listener).popup();
    }

    private VehiclePopup(@NonNull Activity context, OnClickStringListener listener) {
        super(context);
        this.mClickStringListener = listener;
    }

    @Override
    protected int setLayout() {
        return R.layout.dialog_vehicle;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initViews() {
        setOnClickListener(R.id.ok, new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickStringListener != null) {
                    mClickStringListener.onClick(mInputView.getNumber());
                }
                dismiss();
            }
        });

        /// 创建键盘
        mInputView = findViewById(R.id.input_view);

        // 创建弹出键盘
        KeyboardView mKeyboardView = findViewById(R.id.vehicle_view);

        // 隐藏确定按钮
        mKeyboardView.getKeyboardEngine().setHideOKKey(true);

        // 弹出键盘内部包含一个KeyboardView，在此绑定输入两者关联。
        KeyboardInputController mController = KeyboardInputController
                .with(mKeyboardView, mInputView);

        mController.useDefaultMessageHandler();

        // KeyboardInputController提供一个默认实现的新能源车牌锁定按钮
        mController.setDebugEnabled(true);

    }

}
