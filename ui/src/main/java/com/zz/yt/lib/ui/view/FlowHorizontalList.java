package com.zz.yt.lib.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * 物流进度（或者步骤）
 *
 * @author qf
 * @version 1.0
 * @date 2020/9/8
 **/
public class FlowHorizontalList extends View {

    public FlowHorizontalList(Context context) {
        super(context);
    }

    public FlowHorizontalList(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FlowHorizontalList(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
