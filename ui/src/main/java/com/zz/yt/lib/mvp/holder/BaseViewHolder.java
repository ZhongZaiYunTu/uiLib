package com.zz.yt.lib.mvp.holder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.whf.android.jar.change.ChangeListView;

/**
 * 通用的万能 holder
 *
 * @author Administrator
 */
public class BaseViewHolder extends RecyclerView.ViewHolder  {
    private Context context;
    private final SparseArray<View> mSparseArray;

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
        mSparseArray = new SparseArray<>();
    }

    public BaseViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        this.context = context;
        mSparseArray = new SparseArray<>();
    }

    @SuppressWarnings("unchecked")
    public final <T extends View> T getView(@IdRes int viewId) {
        View view = mSparseArray.get(viewId);
        if (view == null) {
            // 使用 RecyclerView.ViewHolder 保存的 itemView
            view = itemView.findViewById(viewId);
            mSparseArray.put(viewId, view);
        }
        return (T) view;
    }


    /********************************** 常用函数 ****************************************/

    public  BaseViewHolder setVisibility(int viewId, int visibility) {
        View view = getView(viewId);
        view.setVisibility(visibility);
        return this;
    }
    public  BaseViewHolder setVisibility(int viewId, boolean visibility) {
        View view = getView(viewId);
        view.setVisibility(visibility?View.VISIBLE:View.GONE);
        return this;
    }

    /**
     * 用来控制 控件是否有焦点
     *  是否可输入
     * @param viewId
     * @param isEdit
     */
    public BaseViewHolder setIsEdit(int viewId, boolean isEdit) {
        View editText = getView(viewId);
        //去掉点击时编辑框下面横线:
        editText.setEnabled(isEdit);
        //不可编辑
        editText.setFocusable(isEdit);
        //不可编辑
        editText.setFocusableInTouchMode(isEdit);
        return this;
    }
    /**
     * 用来控制 控件是否有焦点
     *  是否可点击
     * @param viewId:
     * @param isEdit:
     */
    public BaseViewHolder setIsOnClick(int viewId, boolean isEdit) {
        View view = getView(viewId);
        //去掉点击时编辑框下面横线:
        view.setEnabled(isEdit);
        //不可编辑
        view.setFocusable(isEdit);
        //不可编辑
//        view.setFocusableInTouchMode(isEdit);
        return this;
    }

    public BaseViewHolder setPadding(int viewId, int left, int top, int right, int bottom) {
        View view = getView(viewId);
        view.setPadding(left, top, right, bottom);
        return this;
    }

    public BaseViewHolder setSelected(int viewId, boolean selected) {
        View view = getView(viewId);
        view.setSelected(selected);
        return this;
    }

    public BaseViewHolder setBackgroundColor(int viewId, int color) {
        View view = getView(viewId);
        view.setBackgroundColor(context.getResources().getColor(color));
        return this;
    }

    public BaseViewHolder setBackgroundRes(int viewId, @DrawableRes int resId) {
        View view = getView(viewId);
        view.setBackgroundResource(resId);
        return this;
    }

    public BaseViewHolder setBackgroundDrawable(int viewId, Drawable drawable) {
        View view = getView(viewId);
        view.setBackground(drawable);
        return this;
    }

    public BaseViewHolder setBackgroundBitmap(int viewId, Bitmap bitmap) {
        View view = getView(viewId);
        view.setBackground(new BitmapDrawable(context.getResources(), bitmap));
        return this;
    }

    public BaseViewHolder setEnabled(int viewId, boolean enabled) {
        View view = getView(viewId);
        view.setEnabled(enabled);
        return this;
    }

    public boolean isEnabled(int viewId) {
        return getView(viewId).isEnabled();
    }


    /********************************** 常用监听 ****************************************/

    public BaseViewHolder setOnClickListener(int viewId, View.OnClickListener listener) {
        View view = getView(viewId);
        view.setOnClickListener(listener);
        return this;
    }

    public BaseViewHolder setOnTouchListener(int viewId, View.OnTouchListener listener) {
        View view = getView(viewId);
        view.setOnTouchListener(listener);
        return this;
    }

    public BaseViewHolder setOnLongClickListener(int viewId, View.OnLongClickListener listener) {
        View view = getView(viewId);
        view.setOnLongClickListener(listener);
        return this;
    }

    /**
     * 设置 RecyclerView item 点击监听
     *
     * @param listener
     */
    public BaseViewHolder setOnItemClickListener(View.OnClickListener listener) {
        itemView.setClickable(true);
        itemView.setOnClickListener(listener);
        return this;
    }

    /**
     * 设置 RecyclerView item 长按监听
     *
     * @param listener
     */
    public BaseViewHolder onItenLongClickListener(View.OnLongClickListener listener) {
        itemView.setLongClickable(true);
        itemView.setOnLongClickListener(listener);
        return this;
    }

    /********************************** ListView 相关函数 ****************************************/
    public BaseViewHolder setAdapter(int viewId, BaseAdapter value) {
        ChangeListView view = getView(viewId);
        view.setAdapter(value);
        return this;
    }

    /********************************** RecyclerView 相关函数 ****************************************/

    public void setRecyclerAdapter(int viewId,
                                   RecyclerView.Adapter value,
                                   RecyclerView.LayoutManager layoutManager) {
        RecyclerView view = getView(viewId);
        view.setLayoutManager(layoutManager);
        view.setAdapter(value);
    }

    /********************************** TextView 相关函数 ****************************************/

    public BaseViewHolder setText(int viewId, CharSequence value) {
        TextView view = getView(viewId);
        view.setText(value);
        return this;
    }
    public BaseViewHolder setTag(int viewId, CharSequence value) {
        TextView view = getView(viewId);
        view.setTag(value);
        return this;
    }

    /**
     * 设置最大输入长度
     * @param viewId
     * @param value
     */
    public BaseViewHolder setFilters(int viewId, int value) {
        TextView view = getView(viewId);
        view.setFilters(new InputFilter[]{new InputFilter.LengthFilter(value)});
        return this;
    }

    /**
     * 设置 控件 Margin 距左 上 右 下
     * @param viewId
     * @param left 左
     * @param top 上
     * @param right 右
     * @param bottom 下
     */
    public BaseViewHolder setMargins(int viewId,int left, int top, int right, int bottom) {
        View view = getView(viewId);
        //定义一个LayoutParams
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        //4个参数按顺序分别是左上右下
        layoutParams.setMargins(left,top,right,bottom);
        //mView是控件
        view.setLayoutParams(layoutParams);

        return this;
    }

    public BaseViewHolder setInputType(int viewId, int type) {
        EditText view = getView(viewId);
        view.setInputType(type);
        view.setSingleLine(false);
        return this;
    }

    public BaseViewHolder setTextSize(int viewId, int testSize) {
        TextView view = getView(viewId);
        view.setTextSize(testSize);
        return this;
    }

    public BaseViewHolder setTextColor(int viewId, @ColorInt int textColor) {
        TextView view = getView(viewId);
        view.setTextColor(textColor);
        return this;
    }

    public BaseViewHolder setTextColorRes(int viewId, @ColorRes int color) {
        TextView view = getView(viewId);
        view.setTextColor(ContextCompat.getColor(context, color));
        return this;
    }

    public BaseViewHolder setTextDrawable(int viewId, Drawable left, Drawable top, Drawable right, Drawable bottom) {
        TextView textView = getView(viewId);
        textView.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        return this;
    }

    public BaseViewHolder addTextChangedListener(int viewId, TextWatcher watcher) {
        EditText editText = getView(viewId);
        editText.addTextChangedListener(watcher);
        return this;
    }
    public BaseViewHolder setHint(int viewId, String hint) {
        TextView editText = getView(viewId);
        editText.setHint(hint);
        return this;
    }
    public BaseViewHolder setGravity(int viewId, int gravity) {
        TextView editText = getView(viewId);
        editText.setGravity(gravity);
        return this;
    }

    /********************************** CheckBox 相关函数 ****************************************/

    public BaseViewHolder setOnCheckedChangeListener(int viewId, final CheckBox.OnCheckedChangeListener listener) {
        CheckBox checkBox = getView(viewId);
        checkBox.setOnCheckedChangeListener(listener);
        return this;
    }

    public BaseViewHolder setChecked(int viewId, boolean checked) {
        Checkable view = getView(viewId);
        view.setChecked(checked);
        return this;
    }

    public boolean isChecked(int viewId) {
        CheckBox checkBox = getView(viewId);
        return checkBox.isChecked();
    }


    /********************************** ProgressBar 相关函数 ****************************************/

    public BaseViewHolder setMax(int viewId, int max) {
        ProgressBar view = getView(viewId);
        view.setMax(max);
        return this;
    }

    public BaseViewHolder setPressed(int viewId, boolean pressed) {
        View view = getView(viewId);
        view.setPressed(pressed);
        return this;
    }


    /********************************** ImageView 相关函数 *****************************************/

    public BaseViewHolder setImageResource(int viewId, int imageResId) {
        ImageView view = getView(viewId);
        view.setImageResource(imageResId);
        return this;
    }

    public BaseViewHolder setImageDrawable(int viewId, Drawable drawable) {
        ImageView view = getView(viewId);
        view.setImageDrawable(drawable);
        return this;
    }

    public BaseViewHolder setImageBitmap(int viewId, Bitmap bitmap) {
        ImageView view = getView(viewId);
        view.setImageBitmap(bitmap);
        return this;
    }



}
