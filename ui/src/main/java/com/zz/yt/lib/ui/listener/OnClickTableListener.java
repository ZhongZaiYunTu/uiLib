package com.zz.yt.lib.ui.listener;


import com.zz.yt.lib.ui.table.entity.TableEntity;

import java.util.List;

/**
 * Table每一个表格点击事件
 *
 * @author qf
 * @version 1.0
 **/
public interface OnClickTableListener {
    /**
     * 点击
     *
     * @param index：下标
     * @param column：每行的列数
     * @param text：内容
     */
    void onClickTable(int index, int column, List<TableEntity> text);

}

