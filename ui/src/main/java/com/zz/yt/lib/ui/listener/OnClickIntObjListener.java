package com.zz.yt.lib.ui.listener;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickIntObjListener {

    /**
     * 点击
     *
     * @param index：下标
     * @param obj：值
     */
    void onClick(int index, Object obj);

}
