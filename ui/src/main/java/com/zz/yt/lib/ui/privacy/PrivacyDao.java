package com.zz.yt.lib.ui.privacy;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.blankj.utilcode.util.ObjectUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.ui.R;


/**
 * 设置跳转到用户协议和隐私政策
 *
 * @author hf
 * @version 201.4.22
 */
public final class PrivacyDao {
    enum SigIn {
        AGREEMENT_URL,//用户协议
        PRIVACY_URL,//隐私政策
    }

    /**
     * @param string:用户协议
     */
    public static void setAgreement(String string) {
        LattePreference.addCustomAppProfile(SigIn.AGREEMENT_URL.name(),
                ObjectUtils.isNotEmpty(string) ? string :
                        getString(R.string.agreement_url));
    }

    public static String agreement() {
        return LattePreference.getCustomAppProfile(SigIn.AGREEMENT_URL.name(),
                getString(R.string.agreement_url));
    }

    /**
     * @param string:隐私政策
     */
    public static void setPrivacy(String string) {
        LattePreference.addCustomAppProfile(SigIn.PRIVACY_URL.name(),
                ObjectUtils.isNotEmpty(string) ? string :
                        getString(R.string.privacy_url));
    }

    public static String privacy() {
        return LattePreference.getCustomAppProfile(SigIn.PRIVACY_URL.name(),
                getString(R.string.privacy_url));
    }

    @NonNull
    public static String getString(@StringRes int id) {
        return Latte.getActivity().getResources().getString(id);
    }
}
