package com.zz.yt.lib.ui.recycler.data;

import androidx.annotation.ColorInt;

import com.blankj.utilcode.util.ColorUtils;
import com.zz.yt.lib.ui.R;

/**
 * 订单内容item
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/19
 **/
public final class OrderFormEntity {

    /**
     * key
     */
    private final String TEXT;

    /**
     * key 显示的颜色
     */
    @ColorInt
    private final int TEXT_COLOR;

    /**
     * value
     */
    private final String VALUE;
    /**
     * value显示的颜色
     */
    private final boolean VALUE_COLOR;

    /**
     * 背景颜色
     */
    @ColorInt
    private final int BACKDROP_COLOR;
    /**
     * 空格
     */
    private final static String SPACE = "  ";

    public OrderFormEntity(String text, String value) {
        this.TEXT = text + SPACE;
        this.TEXT_COLOR = ColorUtils.getColor(R.color.textColor_45);
        this.VALUE = value;
        this.VALUE_COLOR = false;
        this.BACKDROP_COLOR = ColorUtils.getColor(R.color.transparent);
    }

    public OrderFormEntity(String text, String value, boolean valueColor) {
        this(text + SPACE, ColorUtils.getColor(R.color.textColor_45), value, valueColor,
                ColorUtils.getColor(R.color.transparent));
    }

    public OrderFormEntity(String text, String value, @ColorInt int backdropColor) {
        this(text + SPACE, ColorUtils.getColor(R.color.textColor_45), value, false, backdropColor);
    }

    public OrderFormEntity(String text, @ColorInt int textColor, String value, @ColorInt int backdropColor) {
        this(text + SPACE, textColor, value, false, backdropColor);
    }

    public OrderFormEntity(String text, String value, boolean valueColor, @ColorInt int backdropColor) {
        this(text + SPACE, ColorUtils.getColor(R.color.textColor_45), value, valueColor, backdropColor);
    }

    public OrderFormEntity(String value, boolean valueColor, @ColorInt int backdropColor) {
        this(SPACE, ColorUtils.getColor(R.color.textColor_45), value, valueColor, backdropColor);
    }

    public OrderFormEntity(String text, @ColorInt int textColor, String value, boolean valueColor, @ColorInt int backdropColor) {
        this.TEXT = text + SPACE;
        this.TEXT_COLOR = textColor;
        this.VALUE = value;
        this.VALUE_COLOR = valueColor;
        this.BACKDROP_COLOR = backdropColor;
    }

    public String getText() {
        return TEXT;
    }

    public int getTextColor() {
        return TEXT_COLOR;
    }

    public String getValue() {
        return VALUE;
    }

    public boolean isValueColor() {
        return VALUE_COLOR;
    }

    public int getBackdropColor() {
        return BACKDROP_COLOR;
    }

}
