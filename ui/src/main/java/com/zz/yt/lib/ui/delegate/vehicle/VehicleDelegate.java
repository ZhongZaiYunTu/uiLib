package com.zz.yt.lib.ui.delegate.vehicle;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.delegate.code.BaseCodeDelegate;
import com.zz.yt.lib.ui.item.EditLayoutBar;


/**
 * 车辆信息录入
 *
 * @author qf
 * @version 1.0.6.8
 */
public class VehicleDelegate extends BaseCodeDelegate {

    private EditLayoutBar mEditOwner = null;
    private EditLayoutBar mEditIdCard = null;
    private EditLayoutBar mEditNoFile = null;
    private EditLayoutBar mEditPhone = null;
    private EditLayoutBar mEditVehicleType = null;
    private EditLayoutBar mEditNoFrame = null;

    @NonNull
    public static VehicleDelegate create() {
        Bundle args = new Bundle();
        VehicleDelegate fragment = new VehicleDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("车辆信息录入");
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_delegate_ui_vehicle;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        mEditOwner = rootView.findViewById(R.id.id_edit_owner);
        mEditIdCard = rootView.findViewById(R.id.id_edit_id_card);
        mEditNoFile = rootView.findViewById(R.id.id_edit_no_file);
        mEditPhone = rootView.findViewById(R.id.id_edit_phone);
        mEditVehicleType = rootView.findViewById(R.id.id_edit_vehicle_type);
        mEditNoFrame = rootView.findViewById(R.id.id_edit_no_frame);

        initView();
    }

    private void initView() {


    }

    @Override
    protected void onSubmit() {
        String sOwner = mEditOwner.toString();
        String sIdCard = mEditIdCard.toString();
        String sNoFile = mEditNoFile.toString();
        String sPhone = mEditPhone.toString();
        String sVehicleType = mEditVehicleType.toString();
        String sNoFrame = mEditNoFrame.toString();
        if (isVerification(sOwner, sIdCard, sNoFile, sPhone, sNoFrame)) {
            submit(sOwner, sIdCard, sNoFile, sPhone, sVehicleType, sNoFrame);
        }
    }

    protected boolean isVerification(String sOwner, String sIdCard, String sNoFile,
                                     String sPhone, String sNoFrame) {
        if (mEditOwner.isString()) {
            return false;
        }
        if (!RegexUtils.isZh(sOwner) || sOwner.length() < 2 || sOwner.length() > 4) {
            ToastUtils.showShort(mEditOwner.getHint());
            return false;
        }
        if (mEditIdCard.isString()) {
            return false;
        }
        if (!RegexUtils.isIDCard15(sIdCard) && !RegexUtils.isIDCard18(sIdCard)) {
            ToastUtils.showShort(mEditOwner.getHint());
            return false;
        }
        if (mEditNoFile.isString()) {
            return false;
        }
        if (sNoFile.length() != 12) {
            ToastUtils.showShort("请填写正确的" + mEditNoFile.getTitle());
            return false;
        }
        if (mEditPhone.isString()) {
            return false;
        }
        if (!RegexUtils.isMobileSimple(sPhone)) {
            ToastUtils.showShort(mEditPhone.getHint());
            return false;
        }
        if (mEditVehicleType.isString()) {
            return false;
        }
        if (mEditNoFrame.isString()) {
            return false;
        }
        if (sNoFrame.length() != 4) {
            ToastUtils.showShort("请填写正确的车架号");
            return false;
        }
        return isCode();
    }


    protected void submit(String sOwner, String sIdCard, String sNoFile,
                          String sPhone, String sVehicleType, String sNoFrame) {

    }
}
