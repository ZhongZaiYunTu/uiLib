package com.zz.yt.lib.ui.delegate.record;


import android.annotation.SuppressLint;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.recycler.adapter.MultipleRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.refresh.BaseRefreshHandler;
import com.zz.yt.lib.ui.refresh.PagingBean;

import java.util.List;


/***
 * @author qf
 * @version 1.0
 */

public abstract class BaseLatteRefreshHandler<Adapter extends MultipleRecyclerAdapter>
        extends BaseRefreshHandler {

    protected Adapter mAdapter = null;

    protected BaseLatteRefreshHandler(LatteDelegate delegate,
                                      SwipeRefreshLayout swipeRefreshLayout,
                                      RecyclerView recyclerView,
                                      BaseDataConverter converter) {
        super(delegate, swipeRefreshLayout, recyclerView, converter);
    }

    @SuppressLint("NotifyDataSetChanged")
    protected void onSuccess(String data, boolean refresh) {
        if (refresh) {
            mAdapter = getNewAdapter();
            mAdapter.setList(CONVERTER.setJsonData(data).convert());
            //// 滑动最后一个Item的时候回调onLoadMoreRequested方法
            mAdapter.getLoadMoreModule().setOnLoadMoreListener(this);
            RECYCLERVIEW.setAdapter(mAdapter);
        } else {
            mAdapter.addData(CONVERTER.setJsonData(data).convert());
        }
        setListener(mAdapter);
        setPaging(data, mAdapter);
        CONVERTER.clearData();
    }

    @Override
    public Adapter getAdapter() {
        return mAdapter;
    }

    @Override
    public List<MultipleItemEntity> getData() {
        return mAdapter.getData();
    }

    protected abstract Adapter getNewAdapter();


}

