package com.zz.yt.lib.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

/**
 * 列表开关按钮
 *
 * @author qf
 * @version 1.0
 **/
@SuppressLint("UseSwitchCompatOrMaterialCode")
public class SwitchListLayout extends RelativeLayout {

    private final ImageView mImageLeft;
    private final Switch mSwitchRight;
    private final TextView mTextLeft, mTextRight;


    public SwitchListLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_list_switch_layout, this, true);
        mImageLeft = findViewById(R.id.id_hai_image_left);
        mTextLeft = findViewById(R.id.id_hai_text_left);
        mSwitchRight = findViewById(R.id.id_hai_switch_right);
        mTextRight = findViewById(R.id.id_hai_text_right);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SwitchListLayout);
        if (attributes != null) {
            //开关按钮是否可以点击
            boolean clickable = attributes.getBoolean(R.styleable.SwitchListLayout_android_clickable, true);
            setClickable(clickable);

            //获取是否要显示左边按钮
            boolean leftButtonVisible = attributes.getBoolean(R.styleable.SwitchListLayout_left_list_switch_visible, false);
            if (leftButtonVisible) {
                mImageLeft.setVisibility(View.VISIBLE);
            } else {
                mImageLeft.setVisibility(View.GONE);
            }
            //设置左边按钮的文字
            String leftButtonText = attributes.getString(R.styleable.SwitchListLayout_left_list_switch_text);
            if (!TextUtils.isEmpty(leftButtonText)) {
                mTextLeft.setText(leftButtonText);
                //设置左边按钮文字颜色
                int leftButtonTextColor = attributes.getColor(R.styleable.SwitchListLayout_left_list_switch_text_color, Color.BLACK);
                mTextLeft.setTextColor(leftButtonTextColor);
                //设置左边按钮文字大小
                int leftButtonTextSize = attributes.getInt(R.styleable.SwitchListLayout_left_list_switch_text_size, 14);
                mTextLeft.setTextSize(leftButtonTextSize);
            }
            //设置左边图片icon
            int leftButtonDrawable = attributes.getResourceId(R.styleable.SwitchListLayout_left_list_switch_drawable, R.mipmap.iv_default_head_image);
            if (leftButtonDrawable != -1) {
                //设置到哪个控件的位置（）
                mImageLeft.setImageResource(leftButtonDrawable);
            }


            //处理右边按钮
            //获取是否要显示右边按钮
            boolean rightButtonVisible = attributes.getBoolean(R.styleable.SwitchListLayout_right_list_switch_visible, true);
            if (rightButtonVisible) {
                mSwitchRight.setVisibility(View.VISIBLE);
            } else {
                mSwitchRight.setVisibility(View.INVISIBLE);
            }
            //设置右边按钮的文字
            String rightButtonText = attributes.getString(R.styleable.SwitchListLayout_right_list_switch_text);
            if (!TextUtils.isEmpty(rightButtonText)) {
                mTextRight.setText(rightButtonText);
                //设置右边按钮文字颜色
                int rightButtonTextColor = attributes.getColor(R.styleable.SwitchListLayout_right_list_switch_text_color, Color.BLACK);
                mTextRight.setTextColor(rightButtonTextColor);
                //设置右边按钮文字大小
                int rightButtonTextSize = attributes.getInt(R.styleable.SwitchListLayout_right_list_switch_text_size, 14);
                mTextRight.setTextSize(rightButtonTextSize);
            }
            //设置右边图片icon
            boolean rightButtonChecked = attributes.getBoolean(R.styleable.SwitchListLayout_right_list_switch_checked, false);
            mSwitchRight.setChecked(rightButtonChecked);

            attributes.recycle();

        }

    }

    /**
     * @param focusable：开关按钮是否可以点击
     */
    @Override
    public final void setClickable(boolean focusable) {
        if (mSwitchRight != null) {
            mSwitchRight.setClickable(focusable);
            mSwitchRight.setFocusable(focusable);
        }
    }

    /**
     * 点击事件
     *
     * @param listener：点击事件
     */
    public final void setImageLeftClickListener(View.OnClickListener listener) {
        if (mImageLeft != null) {
            mImageLeft.setOnClickListener(listener);
        }
    }

    /**
     * 开关事件
     *
     * @param listener：点击事件
     */
    public final void setSwitchRightCheckedChangeListener(CompoundButton.OnCheckedChangeListener listener) {
        if (mSwitchRight != null) {
            mSwitchRight.setOnCheckedChangeListener(listener);
        }
    }


    /**
     * 左边按钮
     *
     * @return ImageView
     */
    public final ImageView getImageLeftView() {
        return mImageLeft;
    }

    /**
     * 左边按钮
     *
     * @return View
     */
    public final TextView getTextLeftView() {
        return mTextLeft;
    }

    /**
     * 右边开关按钮
     *
     * @return Switch
     */
    public final Switch getSwitchRightView() {
        return mSwitchRight;
    }

    /**
     * 右边按钮
     *
     * @return View
     */
    public final TextView getTextRightView() {
        return mTextRight;
    }

    /**
     * 设置是否开启
     *
     * @param checked:
     */
    public final void setChecked(boolean checked) {
        if (mSwitchRight != null) {
            mSwitchRight.setChecked(checked);
        }
    }

    /**
     * 获得是否开启
     *
     * @return View
     */
    public final boolean isChecked() {
        if (mSwitchRight == null) {
            return false;
        }
        return mSwitchRight.isChecked();
    }


}
