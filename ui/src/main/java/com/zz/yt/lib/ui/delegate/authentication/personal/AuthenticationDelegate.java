package com.zz.yt.lib.ui.delegate.authentication.personal;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.annex.constant.PictureConstant;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.popup.utils.HeadPortraitUtils;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;

import java.util.ArrayList;
import java.util.List;


/**
 * 身份验证
 *
 * @author qf
 * @version 1.0
 **/
public class AuthenticationDelegate extends LatteTitleDelegate {

    private ImageView mImageIdPositive = null;
    private ImageView mImageIdSide = null;
    private Button mBtnSubmit = null;

    private final List<String> mIdPositiveList = new ArrayList<>();
    private final List<String> mIdSideList = new ArrayList<>();
    private String title = null;

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_aut;
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText(TextUtils.isEmpty(title) ? "身份验证" : title);
        }
    }

    @NonNull
    public static AuthenticationDelegate create(String title) {
        Bundle args = new Bundle();
        args.putString(BundleKey.TITLE.name(), title);
        AuthenticationDelegate fragment = new AuthenticationDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        title = bundle.getString(BundleKey.TITLE.name());
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        mImageIdPositive = findViewById(R.id.id_image_id_positive);
        mImageIdSide = findViewById(R.id.id_image_id_side);

        mBtnSubmit = findViewById(R.id.id_btn_submit);
        inListener();
    }

    private void inListener() {
        if (mImageIdPositive != null) {
            HeadPortraitUtils.create(mImageIdPositive,
                    new OnClickPictureCallback() {
                        @Override
                        public void onCallback(List<String> selectList) {
                            if (mIdPositiveList != null) {
                                mImageIdPositive.setOnClickListener(null);
                                mIdPositiveList.addAll(selectList);
                                isBtnSubmit();
                            }
                        }

                        @Override
                        public void remove(int position) {
                            if (mIdPositiveList != null && mIdPositiveList.size() > position) {
                                mIdPositiveList.remove(position);
                            }
                        }
                    })
                    .setAddition(PictureConstant.CAMERA)
                    .setNoCircle();
        }

        if (mImageIdSide != null) {
            HeadPortraitUtils.create(mImageIdSide,
                    new OnClickPictureCallback() {
                        @Override
                        public void onCallback(List<String> selectList) {
                            if (mIdSideList != null) {
                                mImageIdSide.setOnClickListener(null);
                                mIdSideList.addAll(selectList);
                                isBtnSubmit();
                            }
                        }

                        @Override
                        public void remove(int position) {
                            if (mIdSideList != null && mIdSideList.size() > position) {
                                mIdSideList.remove(position);
                            }
                        }
                    })
                    .setAddition(PictureConstant.CAMERA)
                    .setNoCircle();
        }

        if (mBtnSubmit != null) {
            mBtnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itSubmit(mIdPositiveList, mIdSideList);
                }
            });
        }
    }

    private void isBtnSubmit() {
        if (mIdPositiveList != null && mIdSideList != null && mIdPositiveList.size() > 0 && mIdSideList.size() > 0) {
            mBtnSubmit.setEnabled(true);
        } else {
            mBtnSubmit.setEnabled(false);
        }
    }


    protected void itSubmit(List<String> mIdPositiveList, List<String> mIdSideList) {

    }

}
