package com.zz.yt.lib.mvp.base.list;

import java.io.Serializable;
import java.util.Map;

/**
 * @author gags
 * @version 1.0
 */
public class SerializableMap implements Serializable {

    /**
     * 序列化map供Bundle传递map使用
     */
    private Map<String, Object> map;

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;

    }

    public SerializableMap(Map<String, Object> map) {
        this.map = map;
    }

    public SerializableMap() {
    }
}
