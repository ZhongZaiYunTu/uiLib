package com.zz.yt.lib.ui;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;

import com.blankj.utilcode.util.SizeUtils;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;


/**
 * 列表按钮
 *
 * @author qf
 * @version 2020.5.28
 **/
public class ButtonListLayout extends BaseLinearLayout {

    private final ImageView mImageLeft, mImageRight;
    private final TextView mTextLeft, mTextRight;

    @Override
    protected int setLayout() {
        return R.layout.hai_list_button_layout;
    }

    public ButtonListLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mImageLeft = findViewById(R.id.id_hai_image_left);
        mTextLeft = findViewById(R.id.id_hai_text_left);
        mImageRight = findViewById(R.id.id_hai_image_right);
        mTextRight = findViewById(R.id.id_hai_text_right);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ButtonListLayout);
        if (attributes != null) {

            //获取是否要显示左边按钮
            boolean leftButtonVisible = attributes.getBoolean(R.styleable.ButtonListLayout_left_list_btn_visible, false);
            if (leftButtonVisible) {
                mImageLeft.setVisibility(View.VISIBLE);
            } else {
                mImageLeft.setVisibility(View.GONE);
            }
            //设置左边按钮的文字
            String leftButtonText = attributes.getString(R.styleable.ButtonListLayout_left_list_btn_text);
            if (!TextUtils.isEmpty(leftButtonText)) {
                setTextLeft(leftButtonText);
                //设置左边按钮文字颜色
                int leftButtonTextColor = attributes.getColor(R.styleable.ButtonListLayout_left_list_btn_text_color, Color.BLACK);
                setTextLeftColor(leftButtonTextColor);
                //设置左边按钮文字大小
                float leftButtonTextSize = attributes.getDimension(R.styleable.ButtonListLayout_left_list_btn_text_size, sp2px(14f));
                setTextLeftSizePx(leftButtonTextSize);
            }
            //设置左边图片icon
            int leftButtonDrawable = attributes.getResourceId(R.styleable.ButtonListLayout_left_list_btn_drawable, R.mipmap.iv_default_head_image);
            if (leftButtonDrawable != -1) {
                //设置到哪个控件的位置（）
                mImageLeft.setImageResource(leftButtonDrawable);
            }
            boolean leftButtonPadding = attributes.getBoolean(R.styleable.ButtonListLayout_left_list_btn_padding, false);
            if (leftButtonPadding) {
                setThumbnail();
            }

            //处理右边按钮
            //获取是否要显示右边按钮
            boolean rightButtonVisible = attributes.getBoolean(R.styleable.ButtonListLayout_right_list_btn_visible, true);
            if (rightButtonVisible) {
                mImageRight.setVisibility(View.VISIBLE);
            } else {
                mImageRight.setVisibility(View.INVISIBLE);
            }
            //设置右边按钮的文字
            String rightButtonText = attributes.getString(R.styleable.ButtonListLayout_right_list_btn_text);
            if (!TextUtils.isEmpty(rightButtonText)) {
                setTextRight(rightButtonText);
                //设置右边按钮文字颜色
                int rightButtonTextColor = attributes.getColor(R.styleable.ButtonListLayout_right_list_btn_text_color, Color.BLACK);
                setTextRightColor(rightButtonTextColor);
                //设置右边按钮文字大小
                float rightButtonTextSize = attributes.getDimension(R.styleable.ButtonListLayout_right_list_btn_text_size, sp2px(14f));
                setTextRightSizePx(rightButtonTextSize);
            }
            //设置右边图片icon
            int rightButtonDrawable = attributes.getResourceId(R.styleable.ButtonListLayout_right_list_btn_drawable, -1);
            if (rightButtonDrawable != -1) {
                //设置到哪个控件的位置（）
                mImageRight.setImageResource(rightButtonDrawable);
            }
            attributes.recycle();
        }
    }

    public void setTextLeft(CharSequence text) {
        if (mTextLeft != null) {
            mTextLeft.setText(text);
        }
    }

    public void setTextLeftColor(@ColorInt int color) {
        if (mTextLeft != null) {
            mTextLeft.setTextColor(color);
        }
    }

    /**
     * @param size：标题内容的大小
     */
    public void setTextLeftSize(float size) {
        setTextLeftSizePx(SizeUtils.sp2px(size));
    }

    /**
     * @param size：标题左边内容的大小
     */
    protected void setTextLeftSizePx(float size) {
        if (mTextLeft != null) {
            mTextLeft.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
    }

    public void OnClickLeftListener(OnClickListener listener) {
        if (mTextLeft != null) {
            mTextLeft.setOnClickListener(listener);
        }
    }

    public void setTextRight(CharSequence text) {
        if (mTextRight != null) {
            mTextRight.setText(text);
        }
    }

    public void setTextRightColor(@ColorInt int color) {
        if (mTextRight != null) {
            mTextRight.setTextColor(color);
        }
    }

    /**
     * @param size：标题右边内容的大小
     */
    public void setTextRightSize(float size) {
        setTextRightSizePx(SizeUtils.sp2px(size));
    }

    /**
     * @param size：标题右边内容的大小
     */
    protected void setTextRightSizePx(float size) {
        if (mTextRight != null) {
            mTextRight.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
    }

    public void OnClickRightListener(View.OnClickListener listener) {
        if (mTextRight != null) {
            mTextRight.setOnClickListener(listener);
        }
    }


    /***
     * 点击事件
     * @param listener：点击事件
     */
    public void setImageLeftClickListener(View.OnClickListener listener) {
        if (mImageLeft != null) {
            mImageLeft.setOnClickListener(listener);
        }
    }

    /**
     * 小图
     */
    public void setThumbnail() {
        if (mImageLeft != null) {
            mImageLeft.setPadding(35, 15, 35, 15);
        }
    }

    /***
     * 左边按钮
     * @return ImageView
     */
    public ImageView getImageLeftBtn() {
        return mImageLeft;
    }

    /***
     * 左边按钮
     * @return Button
     */
    public TextView getTextLeftBtn() {
        return mTextLeft;
    }

    /***
     * 右边按钮
     * @return Button
     */
    public ImageView getImageRightBtn() {
        return mImageRight;
    }

    /***
     * 右边按钮
     * @return TextView
     */
    public TextView getTextRightBtn() {
        return mTextRight;
    }

}
