package com.zz.yt.lib.ui.delegate.authentication.personal.entity;


import android.text.TextUtils;

import java.io.Serializable;

/**
 * bean
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/15
 */
public final class PersonalEntity implements Serializable {

    /**
     * 姓名
     */
    private final String NAME;

    /**
     * 性别
     */
    private final String SEX;

    /**
     * 国籍
     */
    private final String NATIONALITY;

    /**
     * 证件名称
     */
    private final String ID_TYPE;

    /**
     * 证件号码
     */
    private final String ID;

    /**
     * 有效期
     */
    private final String TERM_VALIDITY;

    /**
     * 证件照片
     */
    private final String ID_PHOTO;

    public PersonalEntity(String name, String sex, String nationality, String idType, String id, String termValidity, boolean idPhoto) {
        this.NAME = TextUtils.isEmpty(name) ? "未知" : name;
        this.SEX = TextUtils.isEmpty(sex) ? "未知" : sex;
        this.NATIONALITY = TextUtils.isEmpty(nationality) ? "中国大陆" : nationality;
        this.ID_TYPE = TextUtils.isEmpty(idType) ? "身份证" : idType;
        this.ID = TextUtils.isEmpty(id) ? "未知" : id;
        this.TERM_VALIDITY = TextUtils.isEmpty(termValidity) ? "未知" : termValidity;
        this.ID_PHOTO = idPhoto ? "已上传" : "未完善";
    }

    public String getName() {
        return NAME;
    }

    public String getSex() {
        return SEX;
    }

    public String getNationality() {
        return NATIONALITY;
    }

    public String getIdType() {
        return ID_TYPE;
    }

    public String getId() {
        return ID;
    }

    public String getTermValidity() {
        return TERM_VALIDITY;
    }

    public String getIdPhoto() {
        return ID_PHOTO;
    }
}
