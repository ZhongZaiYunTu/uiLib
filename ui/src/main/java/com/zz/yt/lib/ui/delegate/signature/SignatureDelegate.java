package com.zz.yt.lib.ui.delegate.signature;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ObjectUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.storage.LattePreference;

import com.zz.yt.lib.annex.popup.utils.HeadUtils;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.callback.RequestCodes;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.popup.view.TipsMinPopup;


/**
 * 电子签名
 *
 * @author qf
 * @version 1.0.8
 */
public class SignatureDelegate extends LatteTitleDelegate {

    /**
     * 电子控件
     */
    private ImageView imgSignature;

    /**
     * 电子签名
     */
    private String signature = "";

    /**
     * 是否是全屏
     */
    private boolean isTop = true;

    @NonNull
    public static SignatureDelegate create() {
        return new SignatureDelegate(null);
    }

    @NonNull
    public static SignatureDelegate create(String signature) {
        Bundle args = new Bundle();
        if (ObjectUtils.isNotEmpty(signature)) {
            args.putString(BundleKey.SIGNATURE.name(), signature);
        }
        SignatureDelegate delegate =  new SignatureDelegate(signature);
        delegate.setArguments(args);
        return delegate;
    }

    /**
     * @param signature:电子签名
     */
    protected SignatureDelegate(String signature) {
        LatteLogger.i("签字地址=" + signature);
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        isTop = bundle.getBoolean(BundleKey.IS_TOP.name(), true);
        signature = bundle.getString(BundleKey.SIGNATURE.name());
        if (TextUtils.isEmpty(signature)) {
            //没有传值，取保存的值
            signature = LattePreference.getCustomAppProfile(BundleKey.SIGNATURE.name());
        } else {
            LattePreference.addCustomAppProfile(BundleKey.SIGNATURE.name(), signature);
        }
    }

    @Override
    protected boolean isTopView() {
        return isTop;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("电子签名");
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_delegate_ui_signature;
    }

    @Override
    public void onBindView(Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        TextView textSignature = rootView.findViewById(R.id.text_signature);
        if (textSignature != null) {
            initSignature(textSignature);
        }

        imgSignature = rootView.findViewById(R.id.img_signature);
        if (imgSignature != null) {
            HeadUtils.image(imgSignature, signature, R.drawable.ic_signature_no);
            imgSignature.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(signature)) {
                        onClickSignature();
                        return;
                    }
                    TipsMinPopup.create()
                            .setTitle("您是否更换电子签名")
                            .setText("更换")
                            .setOnClickListener(new OnClickDecisionListener() {
                                @Override
                                public void onConfirm(Object decision) {
                                    onClickSignature();
                                }

                                @Override
                                public void onCancel() {

                                }
                            })
                            .popup();
                }
            });
        }
    }

    /**
     * @param textSignature：提示
     */
    protected void initSignature(@NonNull TextView textSignature) {
        textSignature.setVisibility(View.VISIBLE);
    }

    /**
     * 跳转到签名
     */
    protected void onClickSignature() {
        startSignatureWithCheck(this);
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (imgSignature != null && requestCode == RequestCodes.SIGNATURE) {
            final String signature = LattePreference.getCustomAppProfile(BundleKey.SIGNATURE.name());
            HeadUtils.image(imgSignature, signature, R.drawable.ic_signature_no);
            upSignature(signature);
        }
    }

    /**
     * @param signature：上传签名
     */
    protected void upSignature(String signature) {

    }
}
