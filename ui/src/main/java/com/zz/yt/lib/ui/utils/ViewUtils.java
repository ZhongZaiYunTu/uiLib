package com.zz.yt.lib.ui.utils;

import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.data.OrderFormEntity;
import com.zz.yt.lib.ui.recycler.holder.OtherClickableSpan;


/**
 * @author qf
 * @version 1.0
 * 2020/11/5
 **/
public class ViewUtils {

    private final View view;

    private ViewUtils(View view) {
        this.view = view;
    }

    @NonNull
    public static ViewUtils create(View view) {
        return new ViewUtils(view);
    }

    @NonNull
    public ViewUtils setText(@NonNull TextView textView, @NonNull CharSequence text) {
        textView.setText(text);
        return this;
    }

    @NonNull
    public ViewUtils setOrderForm(@NonNull TextView textView, @NonNull OrderFormEntity orderEntity) {
        textView.setText(SpannableStringUtils.getBuilder(orderEntity.getText())
                .append(orderEntity.getValue() == null ? "" : orderEntity.getValue())
                .setClickSpan(orderEntity.isValueColor() ? accentSpan : textSpan)
                .create());
        return this;
    }

    public ViewUtils setOrderForm(@NonNull TextView textView, String text, String value) {
        textView.setText(SpannableStringUtils.getBuilder(text)
                .append(value)
                .setClickSpan(textSpan)
                .create());
        return this;
    }

    public ViewUtils setOrderForm(@NonNull TextView textView, String text, String value, boolean valueColor) {
        textView.setText(SpannableStringUtils.getBuilder(text)
                .append(value)
                .setClickSpan(valueColor ? accentSpan : textSpan)
                .create());
        return this;
    }

    public ViewUtils setText(@IdRes int viewId, CharSequence text) {
        TextView textView = view.findViewById(viewId);
        if (textView != null) {
            textView.setText(text);
        }
        return this;
    }

    public ViewUtils setOrderForm(@IdRes int viewId, @NonNull OrderFormEntity orderEntity) {
        TextView textView = view.findViewById(viewId);
        if (textView != null) {
            textView.setText(SpannableStringUtils.getBuilder(orderEntity.getText())
                    .append(orderEntity.getValue() == null ? "" : orderEntity.getValue())
                    .setClickSpan(orderEntity.isValueColor() ? accentSpan : textSpan)
                    .create());
        }
        return this;
    }

    public ViewUtils setOrderForm(@IdRes int viewId, String text, String value) {
        TextView textView = view.findViewById(viewId);
        if (textView != null) {
            textView.setText(SpannableStringUtils.getBuilder(text)
                    .append(value)
                    .setClickSpan(textSpan)
                    .create());
        }
        return this;
    }

    public ViewUtils setOrderForm(@IdRes int viewId, String text, String value, boolean valueColor) {
        TextView textView = view.findViewById(viewId);
        if (textView != null) {
            textView.setText(SpannableStringUtils.getBuilder(text)
                    .append(value)
                    .setClickSpan(valueColor ? accentSpan : textSpan)
                    .create());
        }
        return this;
    }


    public ViewUtils setGone(@NonNull TextView textView, boolean text) {
        textView.setVisibility(text ? View.GONE : View.VISIBLE);
        return this;
    }

    public ViewUtils setGone(@IdRes int viewId, boolean text) {
        view.findViewById(viewId).setVisibility(text ? View.GONE : View.VISIBLE);
        return this;
    }

    protected final ClickableSpan textSpan = OtherClickableSpan.create(R.color.textColor_85);

    protected final ClickableSpan accentSpan =  OtherClickableSpan.create(R.color.colorAccent);
}
