package com.zz.yt.lib.mvp.base.list.abolish.click;


import com.zz.yt.lib.annex.document.DocumentView;

/**
 * 附件控件
 *
 * @author Gags
 * @version 1.0
 */
public interface OnItemAnnexClick {

    void initDocument(DocumentView documentView);
}
