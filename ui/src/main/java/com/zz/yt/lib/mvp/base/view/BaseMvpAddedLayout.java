package com.zz.yt.lib.mvp.base.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.mvp.base.list.added.BaseAddedAdapter;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.callback.OnRecyclerAddedCallback;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;
import com.zz.yt.lib.ui.utils.ObjUtils;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表控件
 *
 * @author qf
 * @version 1.0
 */
public abstract class BaseMvpAddedLayout<T> extends BaseLinearLayout implements
        OnRecyclerAddedCallback {

    private boolean isEmptyView = false;
    private boolean isTipRepeat = false;
    private final InvoiceViewAdapter<T> viewAdapter;
    protected final RecyclerView recyclerView;
    protected List<T> dataItem = null;

    public BaseMvpAddedLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        recyclerView = setRecyclerView();
        recyclerView.setLayoutManager(layoutManager(context));

        viewAdapter = adapter();
        recyclerView.setAdapter(viewAdapter);
    }

    /**
     * @param tipRepeat:设置是否显示重复添加提示。
     */
    public void setTipRepeat(boolean tipRepeat) {
        isTipRepeat = tipRepeat;
    }

    /**
     * @param emptyView:设置是否显示无数据图片
     */
    public void setEmptyView(boolean emptyView) {
        isEmptyView = emptyView;
    }

    /**
     * 初始化列表控件
     */
    protected abstract RecyclerView setRecyclerView();

    /**
     * 初始化适配器
     */
    protected InvoiceViewAdapter<T> adapter() {
        return new InvoiceViewAdapter<>(recyclerView, this);
    }

    /**
     * 设置数据
     */
    public void setData(List<T> option) {
        dataItem = option;
        if (viewAdapter != null) {
            viewAdapter.setList(dataItem);
            if (isEmptyView) {
                viewAdapter.setEmptyView(R.layout.hai_loading_view);
            }
        }
    }

    /**
     * setting up a new instance to data;
     * 设置新的数据实例，替换原有内存引用。
     * 通常情况下，如非必要，请使用setList修改内容
     */
    public void setNewInstance(List<T> option) {
        dataItem = option;
        if (viewAdapter != null) {
            viewAdapter.setNewInstance(dataItem);
            if (isEmptyView) {
                viewAdapter.setEmptyView(R.layout.hai_loading_view);
            }
        }
    }

    /**
     * 网络服务失败清空数据
     */
    public void setError() {
        setNewInstance(new ArrayList<T>());
    }


    /**
     * @param i:删除一行
     */
    public void remove(int i) {
        if (dataItem.size() > i && getData().size() > i) {
            getData().remove(i);
            dataItem.remove(i);
            notifyDataSetChanged();
        }
    }

    /**
     * 添加数据
     */
    public void setData(T option) {
        if (dataItem == null) {
            dataItem = new ArrayList<>();
        }
        if (dataItem.contains(option)) {
            if (isTipRepeat) {
                ToastUtils.showShort("已添加,请勿重复添加");
            }
            return;
        }
        dataItem.add(option);
        if (viewAdapter != null) {
            viewAdapter.addData(option);
        }
    }

    /**
     * 获得 List<Object> 类型数据
     */
    public List<Object> getObjData() {
        return viewAdapter.getObjData();
    }

    /**
     * 获得 值数据
     */
    protected List<T> getData() {
        return viewAdapter.getData();
    }

    /**
     * @param object：设置全局的其他属性
     */
    public void setObject(Object object) {
        if (viewAdapter != null) {
            viewAdapter.setObject(object);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    protected void notifyDataSetChanged() {
        Latte.getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (viewAdapter != null) {
                    viewAdapter.notifyDataSetChanged();
                    if (isEmptyView) {
                        viewAdapter.setEmptyView(R.layout.hai_loading_view);
                    }
                }
            }
        }, 333);
    }

    /**
     * 设置设置添加一行
     */
    public void addData() {
        if (viewAdapter != null) {
            viewAdapter.addData();
        }
    }

    /**
     * 初始化一行
     */
    public void init() {
        if (dataItem == null || dataItem.size() == 0) {
            addData();
        }
    }

    private static class InvoiceViewAdapter<T> extends BaseAddedAdapter<T> {

        private final OnRecyclerAddedCallback callbackRecycler;

        public InvoiceViewAdapter(RecyclerView recycler, @NonNull OnRecyclerAddedCallback callback) {
            super(recycler, callback.setLayoutItem(), new ArrayList<T>());
            this.callbackRecycler = callback;
        }

        @Override
        protected void addChildClick() {
            if (callbackRecycler != null) {
                callbackRecycler.addChildClick();
            }
        }

        @Override
        public void addData() {
            if (callbackRecycler != null && callbackRecycler.newData() != null) {
                final T t = ObjUtils.getField(callbackRecycler.newData());
                addData(t);
            }
        }

        @Override
        protected void convert(@NonNull MultipleViewHolder holder, @NonNull T entity) {
            if (callbackRecycler != null) {
                callbackRecycler.convert(holder, entity, object);
            }
        }

        @Override
        protected void setEntity(@NonNull T entity, View view) {
            if (callbackRecycler != null) {
                callbackRecycler.setEntity(entity, view);
            }
        }

        /**
         * @param i:为true时，弹出填错提示
         * @param view:
         */
        @Override
        protected boolean isEmpty(int i, View view) {
            if (callbackRecycler != null) {
                callbackRecycler.isEmpty(i, view);
            }
            return false;
        }
    }

}
