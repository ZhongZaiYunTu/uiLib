package com.zz.yt.lib.ui.item;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.utils.DateKeysUtils;
import com.zz.yt.lib.ui.utils.DatesUtils;


/**
 * 列表输入布局
 *
 * @author qf
 * @date 2020/4/24
 **/
public class EditLayoutBar extends RelativeLayout implements View.OnFocusChangeListener, View.OnClickListener {

    private final TextView idTextLeft;
    private final TextView idTextTitle;
    private final EditText idEditValue;
    private final ImageView idImageOption;
    /**
     * 是否是时间选项
     */
    private final int selectType;
    /**
     * 輸入时标题的颜色
     */
    private final int valueTextTitleColor;

    private String pattern = "yyyy-MM-dd HH:mm:ss";
    private boolean[] type = new boolean[]{true, true, true, true, true, true};

    /**
     * 密码显示or隐藏图片
     */
    private int offResource, onResource;


    @SuppressLint("WrongConstant")
    public EditLayoutBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_edit_layout, this, true);

        idTextLeft = findViewById(R.id.id_text_left);
        idTextTitle = findViewById(R.id.id_text_title);
        idEditValue = findViewById(R.id.id_edit_value);
        idImageOption = findViewById(R.id.id_image_option);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.EditLayoutBar);

        //先处理左边按钮
        //获取是否要显示左边按钮
        boolean leftButtonVisible = attributes.getBoolean(R.styleable.EditLayoutBar_left_visible, true);
        if (leftButtonVisible) {
            idTextLeft.setVisibility(View.VISIBLE);
        } else {
            idTextLeft.setVisibility(View.INVISIBLE);
        }

        //处理标题
        //设置按钮的文字
        String valueText = attributes.getString(R.styleable.EditLayoutBar_value_text);
        if (!TextUtils.isEmpty(valueText)) {
            idEditValue.setText(valueText);
            //设置文字颜色
            int valueTextColor = attributes.getColor(R.styleable.EditLayoutBar_value_text_color, getResources().getColor(R.color.textColor_85));
            idEditValue.setTextColor(valueTextColor);
        }

        //是否需要下划线
        boolean isBackgroundLine = attributes.getBoolean(R.styleable.EditLayoutBar_is_background_line, false);
        if (isBackgroundLine) {
            idEditValue.setBackgroundResource(R.drawable.line_et_bg);
        } else {
            idEditValue.setBackgroundColor(Color.TRANSPARENT);
        }
        //设置按钮的标题文字
        String valueTextTitle = attributes.getString(R.styleable.EditLayoutBar_value_text_title);
        if (!TextUtils.isEmpty(valueTextTitle)) {
            idTextTitle.setText(valueTextTitle);
        }
        //设置按钮标题文字颜色
        valueTextTitleColor = attributes.getColor(R.styleable.EditLayoutBar_value_text_title_color, getResources().getColor(R.color.colorAccent));
        //设置按钮的标题文字
        String valueTextHint = attributes.getString(R.styleable.EditLayoutBar_value_text_hint);
        if (!TextUtils.isEmpty(valueTextHint)) {
            SpannableString ss = new SpannableString(valueTextHint);
            AbsoluteSizeSpan ass = new AbsoluteSizeSpan(14, true);
            ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            idEditValue.setHint(new SpannedString(ss));
            //设置按钮标题文字颜色
            int valueTextHintColor = attributes.getColor(R.styleable.EditLayoutBar_value_text_hint_color, getResources().getColor(R.color.textColor_45));
            idEditValue.setHintTextColor(valueTextHintColor);
        }
        //
        boolean mode = attributes.getBoolean(R.styleable.EditLayoutBar_text_option, true);
        idEditValue.setFocusable(mode);
        idEditValue.setFocusableInTouchMode(mode);
        idEditValue.setLongClickable(mode);
        int inputType = attributes.getInteger(R.styleable.EditLayoutBar_android_inputType, InputType.TYPE_CLASS_TEXT);
        idEditValue.setInputType(inputType);
        idEditValue.setOnFocusChangeListener(this);
        idTextTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, (mode ? 16f : 14f));

        //先处理右边按钮
        //获取是否要显示右边按钮
        boolean rightButtonVisible = attributes.getBoolean(R.styleable.EditLayoutBar_right_visible, false);
        if (rightButtonVisible) {
            idImageOption.setVisibility(View.VISIBLE);
        } else {
            idImageOption.setVisibility(View.INVISIBLE);
        }
        //设置右边图片icon 这里是二选一 要么只能是文字 要么只能是图片
        int rightButtonDrawable = attributes.getResourceId(R.styleable.EditLayoutBar_right_drawable, -1);
        if (rightButtonDrawable != -1) {
            //设置到哪个控件的位置（）
            idImageOption.setImageResource(rightButtonDrawable);
        }

        //是否是时间选项
        selectType = attributes.getInteger(R.styleable.EditLayoutBar_value_select_type, -1);
        if (selectType != -1) {
            setType(selectType);
            idImageOption.setOnClickListener(this);
        }
        attributes.recycle();

        //改变默认的单行模式
        idEditValue.setSingleLine(false);
        //水平滚动设置为False
        idEditValue.setHorizontallyScrolling(false);
    }

    public void setText(CharSequence text) {
        idEditValue.setText(text);
        onFocusChange(null, false);
    }

    /***
     * 是否可以编辑
     * @param isEdit：
     */
    public void setEdit(boolean isEdit) {
        idEditValue.setFocusable(isEdit);
        idEditValue.setFocusableInTouchMode(isEdit);
        idEditValue.setLongClickable(isEdit);
    }

    /***
     * 是否可以点击
     * @param isEdit：
     */
    public void setEditClick(boolean isEdit) {
        if (!isEdit) {
            idImageOption.setOnClickListener(null);
        }
    }

    /**
     * 设置为选择模式
     */
    private void setType(@DateKeysUtils.Type int patternType) {
        type = DateKeysUtils.setType(patternType);
        pattern = DateKeysUtils.setPattern(patternType);
    }


    @Override
    public void onFocusChange(View view, boolean b) {
        if (b) {
            //获得焦点
            idTextTitle.setTextColor(valueTextTitleColor);
            idTextTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
        } else {
            //失去焦答点
            if (idEditValue.getText().toString().trim().isEmpty()) {
                idTextTitle.setTextColor(getResources().getColor(R.color.textColor_85));
                idTextTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
            } else {
                idTextTitle.setTextColor(getResources().getColor(R.color.textColor_65));
                idTextTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
            }
        }
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        if (idImageOption != null && selectType == -1) {
            idImageOption.setOnClickListener(l);
        }
    }

    @Override
    public void setTag(Object tag) {
        idEditValue.setTag(tag);
    }

    @Override
    public Object getTag() {
        return idEditValue.getTag();
    }

    /**
     * 提示内容
     */
    public CharSequence getHint() {
        return idEditValue.getHint();
    }

    /**
     * 标题内容
     */
    public CharSequence getTitle() {
        return idTextTitle.getText();
    }

    @NonNull
    @Override
    public String toString() {
        return idEditValue.getText().toString().trim();
    }

    /**
     * 左边按钮
     *
     * @return Button
     */
    public TextView getLeftView() {
        return idTextLeft;
    }

    /**
     * 左边按钮（title）
     *
     * @return Button
     */
    public TextView getTitleView() {
        return idTextTitle;
    }

    /**
     * 按钮内容
     *
     * @return Button
     */
    public EditText getValueView() {
        return idEditValue;
    }

    /**
     * 右边按钮
     *
     * @return TextView
     */
    public ImageView getRightView() {
        return idImageOption;
    }

    @Override
    public void onClick(View v) {
        DatesUtils.create()
                .setTypePattern(type, pattern)
                .setOnClickStringListener(new OnClickStringListener() {
                    @Override
                    public void onClick(String index) {
                        idEditValue.setText(index);
                    }
                })
                .onClick();
    }

    public boolean isObject() {
        if (idTextLeft.getVisibility() != View.VISIBLE) {
            //非必填
            return false;
        }
        if (getVisibility() != View.VISIBLE) {
            //控件被隐藏
            return false;
        }
        final boolean is = getTag() == null;
        if (is) {
            ToastUtils.showShort(getHint());
        }
        return is;
    }

    public boolean isString() {
        if (idTextLeft.getVisibility() != View.VISIBLE) {
            //非必填
            return false;
        }
        if (getVisibility() != View.VISIBLE) {
            //控件被隐藏
            return false;
        }
        final boolean is = TextUtils.isEmpty(toString());
        if (is) {
            ToastUtils.showShort(getHint().toString());
        }
        return is;
    }

    public boolean isEmpty() {
        if (idTextLeft.getVisibility() != View.VISIBLE) {
            //非必填
            return false;
        }
        if (getVisibility() != View.VISIBLE) {
            //控件被隐藏
            return false;
        }
        final boolean is = TextUtils.isEmpty(toString());
        if (is) {
            ToastUtils.showShort(getHint());
        }
        return is;
    }

    /**
     * 密码显示隐藏
     *
     * @param off:密码隐藏图片
     * @param on:密码显示图片
     */
    public final void setPassword(final int off, final int on) {
        this.offResource = off;
        this.onResource = on;
        getRightView().setImageResource(offResource);
        getValueView().setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        getRightView().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelected()) {
                    setSelected(false);
                    getRightView().setImageResource(offResource);
                    getValueView().setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    setSelected(true);
                    getRightView().setImageResource(onResource);
                    getValueView().setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
                getValueView().setSelection(getValueView().getText().length());
            }
        });
    }

}
