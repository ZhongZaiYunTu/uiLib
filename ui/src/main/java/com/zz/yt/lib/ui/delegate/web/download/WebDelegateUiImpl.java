package com.zz.yt.lib.ui.delegate.web.download;

import android.webkit.DownloadListener;

import androidx.annotation.NonNull;

import com.whf.android.jar.web.WebDelegateImpl;
import com.zz.yt.lib.ui.listener.OnClickStringListener;


/**
 * 具体实现的webView布局
 * 布局加 FragmentContainerView
 * 然后 final WebDelegateImpl delegate = WebDelegateImpl.create(url);
 * delegate.setPageLoadListener(this);
 * loadRootFragment(id,delegate);
 *
 * @author hf
 * @version 1.0.5
 */
public class WebDelegateUiImpl extends WebDelegateImpl {

    private OnClickStringListener mClickStringListener = null;

    protected WebDelegateUiImpl(String url, boolean handleWebUrl) {
        super(url, handleWebUrl);
    }

    @NonNull
    public static WebDelegateUiImpl create(String url) {
        return new WebDelegateUiImpl(url,true);
    }


    public void setOnClickStringListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
    }

    @Override
    public DownloadListener initDownloadListener() {
        return new WebDownLoadUiView(new OnClickStringListener() {
            @Override
            public void onClick(String url) {
                if (mClickStringListener != null) {
                    mClickStringListener.onClick(url);
                }
            }
        });
    }
}
