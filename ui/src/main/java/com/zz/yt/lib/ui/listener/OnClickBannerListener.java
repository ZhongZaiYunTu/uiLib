package com.zz.yt.lib.ui.listener;

/**
 * Banner点击事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickBannerListener {

    /**
     * @param position：点击下标
     */
    void OnBannerClick(int position);
}
