package com.zz.yt.lib.ui.table.adapter;

import android.view.View;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.adapter.MultipleRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.List;

/**
 * Table item
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/6/3
 **/
public class TableAdapter extends MultipleRecyclerAdapter {

    private TableAdapter(List<MultipleItemEntity> data) {
        super(data);
        addItemType(ItemType.TABLE_ITEM, R.layout.hai_item_multiple_table);
    }

    @NonNull
    public static TableAdapter create(@NonNull List<MultipleItemEntity> data) {
        return new TableAdapter(data);
    }

    @NonNull
    public static TableAdapter create(@NonNull BaseDataConverter converter) {
        return new TableAdapter(converter.convert());
    }

    @NonNull
    @Override
    protected MultipleViewHolder createBaseViewHolder(@NonNull View view) {
        return MultipleViewHolder.create(view);
    }


    @Override
    protected void convert(@NonNull MultipleViewHolder holder, @NonNull MultipleItemEntity entity) {
        final String text = entity.getField(MultipleFields.TEXT);
        holder.setText(R.id.text_single, text);
        final int textColor = entity.getField(MultipleFields.TEXT_COLOR);
        holder.setTextColor(R.id.text_single, textColor);
        final int color = entity.getField(MultipleFields.COLOR);
        holder.itemView.setBackgroundColor(color);
        final int position = holder.position();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickIntListener != null) {
                    mClickIntListener.onClick(position);
                }
            }
        });
    }


}
