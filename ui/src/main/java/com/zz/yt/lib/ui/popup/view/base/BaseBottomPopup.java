package com.zz.yt.lib.ui.popup.view.base;


import android.content.Context;

import androidx.annotation.NonNull;

import com.whf.android.jar.popup.LattePopupBottom;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.listener.OnClickStringsListener;


/**
 * 弹框基层
 *
 * @author qf
 * @version 1.0.4
 **/
public abstract class BaseBottomPopup extends LattePopupBottom {

    protected OnClickIntObjListener mClickIntObjListener = null;
    protected OnClickStringListener mClickStringListener = null;
    protected OnClickStringsListener mClickStringsListener = null;

    public BaseBottomPopup(@NonNull Context context) {
        super(context);
    }

    /**
     * @param listener：添加事件
     */
    public BaseBottomPopup setOnClickIntObjListener(OnClickIntObjListener listener) {
        this.mClickIntObjListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseBottomPopup setOnClickListener(OnClickStringListener listener) {
        this.mClickStringListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseBottomPopup setOnClickListener(OnClickStringsListener listener) {
        this.mClickStringsListener = listener;
        return this;
    }

    /**
     * @param listener：添加事件
     */
    public BaseBottomPopup setOnClickListener(OnClickDecisionListener listener) {
        this.mClickDecisionListener = listener;
        return this;
    }


}
