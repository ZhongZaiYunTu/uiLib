package com.zz.yt.lib.ui.callback;


import java.util.Date;

/**
 * 时间选择回调
 *
 * @author qf
 * @version 1.0
 **/
public interface OnSelectDateCallback {
    /**
     * 时间段
     *
     * @param startDate：
     * @param endDate：(单时间时，为空)
     */
    void selectDate(Date startDate, Date endDate);
}

