package com.zz.yt.lib.ui.delegate.License;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.parkingwang.keyboard.OnInputChangedListener;
import com.parkingwang.keyboard.PopupKeyboard;
import com.parkingwang.keyboard.view.InputView;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleActivity;


public class LicenseActivity extends LatteTitleActivity {

    private PopupKeyboard mPopupKeyboard;


    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("输入车牌");
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_delegate_input_number;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState) {

        final InputView mInputView = findViewById(R.id.input_view);
        final Button lockTypeButton = findViewById(R.id.btn_next);
        lockTypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = mInputView.getNumber();
                LatteLogger.i(number);
                if (number.length() < 7) {
                    return;
                }
                Intent intent = getIntent();
                intent.putExtra(BundleKey.NUMBER.name(), number);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        // 创建弹出键盘
        mPopupKeyboard = new PopupKeyboard(this);
        // 弹出键盘内部包含一个KeyboardView，在此绑定输入两者关联。
        mPopupKeyboard.attach(mInputView, this);
        mPopupKeyboard.getController().setSwitchVerify(true);
        // 隐藏确定按钮
        mPopupKeyboard.getKeyboardEngine().setHideOKKey(false);
        // KeyboardInputController提供一个默认实现的新能源车牌锁定按钮
        mPopupKeyboard.getController()
                .setDebugEnabled(true);
        mPopupKeyboard.getController().addOnInputChangedListener(new OnInputChangedListener() {
            @Override
            public void onChanged(String number, boolean isCompleted) {
                if (isCompleted) {
                    mPopupKeyboard.dismiss(LicenseActivity.this);
                }
            }

            @Override
            public void onCompleted(String number, boolean isAutoCompleted) {
                mPopupKeyboard.dismiss(LicenseActivity.this);
            }
        });

    }

}
