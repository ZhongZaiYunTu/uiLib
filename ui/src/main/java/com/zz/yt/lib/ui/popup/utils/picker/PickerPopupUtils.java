package com.zz.yt.lib.ui.popup.utils.picker;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.blankj.utilcode.util.ColorUtils;
import com.contrarywind.interfaces.IPickerViewData;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;

import java.util.Collections;
import java.util.List;


/**
 * 选择器
 *
 * @author qf
 * @version 1.0
 */
public final class PickerPopupUtils {


    /**
     * 选择器
     *
     * @param context  : 上下文
     * @param title    : 标题
     * @param Items    : 需要单选的 集合
     * @param options  : 触发选择控件
     * @param listener : 触发选择控件
     */
    public static void showPickerStr(Context context,
                                     String title,
                                     final List<String> Items,
                                     int options,
                                     final OnClickIntObjListener listener) {
        showPickerStr(context, null, title, Items, options, listener);
    }

    /**
     * 选择器
     *
     * @param context  : 上下文
     * @param title    : 标题
     * @param Items    : 需要单选的 集合
     * @param options  : 触发选择控件
     * @param listener : 触发选择控件
     */
    public static void showPickerStr(Context context,
                                     View group,
                                     String title,
                                     final List<String> Items,
                                     int options,
                                     final OnClickIntObjListener listener) {
        final ViewGroup decorView = Latte.getActivity()
                .getWindow()
                .getDecorView()
                .findViewById(android.R.id.content);
        OptionsPickerView<String> pvOptions = new OptionsPickerBuilder(context,
                new OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int options2, int options3, View v) {

                        if (Items == null || Items.size() == 0) {
                            return;
                        }
                        if (listener != null) {
                            listener.onClick(options1, Items.get(options1));
                        }

                    }
                })
                .setTitleSize(20)
                .setTitleText(title)
                .setDividerColor(ColorUtils.getColor(R.color.colorAccent))
                .setTextColorCenter(ColorUtils.getColor(R.color.colorAccent))
                .setLineSpacingMultiplier(3f)
                .setSelectOptions(options)
                .setDecorView(decorView)
                .setItemVisibleCount(7)
                .setContentTextSize(16)
                .build();
        if (pvOptions != null) {
            pvOptions.setPicker(Items);
            if (group != null) {
                pvOptions.show(group);
            } else {
                pvOptions.show();
            }
        }
    }

    /**
     * 选择器
     *
     * @param context  : 上下文
     * @param title    : 标题
     * @param Items    : 需要单选的 集合
     * @param options  : 触发选择控件
     * @param listener : 触发选择控件
     */
    public static void showPickerData(Context context,
                                      String title,
                                      final List<? extends IPickerViewData> Items,
                                      int options,
                                      final OnClickIntObjListener listener) {
        showPickerData(context, null, title, Items, options, listener);
    }

    /**
     * 选择器
     *
     * @param context  : 上下文
     * @param title    : 标题
     * @param Items    : 需要单选的 集合
     * @param options  : 触发选择控件
     * @param listener : 触发选择控件
     */
    public static void showPickerData(Context context,
                                      View group,
                                      String title,
                                      final List<? extends IPickerViewData> Items,
                                      int options,
                                      final OnClickIntObjListener listener) {
        final ViewGroup decorView = Latte.getActivity()
                .getWindow()
                .getDecorView()
                .findViewById(android.R.id.content);
        OptionsPickerView<IPickerViewData> pvOptions = new OptionsPickerBuilder(context,
                new OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int options2, int options3, View v) {
                        if (Items == null || Items.size() == 0) {
                            return;
                        }
                        if (listener != null) {
                            listener.onClick(options1, Items.get(options1));
                        }
                    }
                })
                .setTitleSize(20)
                .setTitleText(title)
                .setDividerColor(ColorUtils.getColor(R.color.colorAccent))
                .setTextColorCenter(ColorUtils.getColor(R.color.colorAccent))
                .setLineSpacingMultiplier(3f)
                .setSelectOptions(options)
                .setDecorView(decorView)
                .setItemVisibleCount(7)
                .setContentTextSize(16)
                .build();
        if (pvOptions != null) {
            pvOptions.setPicker(Collections.unmodifiableList(Items));
            if (group != null) {
                pvOptions.show(group);
            } else {
                pvOptions.show();
            }
        }
    }


}
