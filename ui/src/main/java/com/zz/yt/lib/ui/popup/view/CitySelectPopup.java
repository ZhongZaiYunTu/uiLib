package com.zz.yt.lib.ui.popup.view;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.JsonUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.blankj.utilcode.util.StringUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.gson.HttpGsonUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.mvp.holder.BaseViewHolder;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.popup.view.base.BaseBottomPopup;

import java.util.ArrayList;
import java.util.List;

import me.yokeyword.indexablerv.IndexableAdapter;
import me.yokeyword.indexablerv.IndexableEntity;
import me.yokeyword.indexablerv.IndexableLayout;

/**
 * 城市选择弹框
 *
 * @author qf
 * @version 1.0.30
 **/
@SuppressLint("ViewConstructor")
public final class CitySelectPopup extends BaseBottomPopup {

    private List<CityBean> cityList;
    private IndexAdapter indexAdapter;

    @NonNull
    public static CitySelectPopup create() {
        return new CitySelectPopup();
    }

    public CitySelectPopup() {
        super(Latte.getActivity());
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_city_select;
    }

    @Override
    protected void initViews() {
        setOnClickListener(R.id.tv_close, new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        final String assets = ResourceUtils.readAssets2String("city_230619.json");
        final String provinces = JsonUtils.getString(assets, "provinces");
        cityList = HttpGsonUtils.fromJsonList(provinces, CityBean.class);
        indexAdapter = new IndexAdapter();

        IndexableLayout indexableLayout = findViewById(R.id.indexAble);
        if (indexableLayout != null) {
            indexableLayout.setLayoutManager(new GridLayoutManager(Latte.getActivity(), 3));
            indexableLayout.setAdapter(indexAdapter);

            // 设置字母提示框为仿os居中
            indexableLayout.setOverlayStyle_Center();
            // 设置排序方式。
            indexableLayout.setCompareMode(IndexableLayout.MODE_FAST);
        }

        // 设置数据
        indexAdapter.setDatas(cityList);

        SearchView searchView = findViewById(R.id.search_view);
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (indexAdapter == null) {
                        return false;
                    }
                    if (StringUtils.isEmpty(newText)) {
                        indexAdapter.setDatas(cityList);
                        return false;
                    } else {
                        indexAdapter.setDatas(filterCity(newText));
                    }
                    indexAdapter.notifyDataSetChanged();
                    return true;
                }
            });
        }
    }

    @NonNull
    private List<CityBean> filterCity(String newText) {
        List<CityBean> data = new ArrayList<>();
        if (cityList == null) {
            return data;
        }
        for (CityBean item : cityList) {
            if (item.getFieldIndexBy().contains(newText)) {
                data.add(item);
            }
        }
        return data;
    }

    private class IndexAdapter extends IndexableAdapter<CityBean> {

        private final LayoutInflater mInflater;

        public IndexAdapter() {
            mInflater = LayoutInflater.from(Latte.getActivity());
        }

        @NonNull
        public RecyclerView.ViewHolder onCreateTitleViewHolder(ViewGroup parent) {
            // 创建 TitleItem 布局
            View view = mInflater.inflate(R.layout.hai_popup_city_select_title, parent, false);
            return new BaseViewHolder(view);
        }

        @NonNull
        public RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent) {
            // 创建 内容Item 布局
            View view = mInflater.inflate(R.layout.hai_popup_city_select_item, parent, false);
            return new BaseViewHolder(view);
        }

        @Override
        public void onBindTitleViewHolder(@NonNull RecyclerView.ViewHolder holder, String indexTitle) {
            // 填充 TitleItem 布局
            TextView textView = holder.itemView.findViewById(R.id.tv_title_city);
            if (textView != null) {
                textView.setText(indexTitle);
            }
        }

        @Override
        public void onBindContentViewHolder(@NonNull RecyclerView.ViewHolder holder, final CityBean entity) {
            TextView textView = holder.itemView.findViewById(R.id.tv_item_city);
            if (textView != null) {
                textView.setText(entity.getFieldIndexBy());
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LatteLogger.i("选中的：" + getTextStr((TextView) view));
                        if (mClickStringListener != null) {
                            mClickStringListener.onClick(getTextStr((TextView) view));
                        }
                        dismiss();
                    }
                });
            }
        }
    }

    private static class CityBean implements IndexableEntity {

        String cityName;
        String pinyin;

        @Nullable
        public String getFieldIndexBy() {
            return cityName;
        }

        @Override
        public void setFieldIndexBy(String indexField) {
            this.cityName = indexField;
        }

        @Override
        public void setFieldPinyinIndexBy(String pinyin) {
            // 保存排序field的拼音,在执行比如搜索等功能时有用 （若不需要，空实现该方法即可）
            this.pinyin = pinyin;
        }

    }

}
