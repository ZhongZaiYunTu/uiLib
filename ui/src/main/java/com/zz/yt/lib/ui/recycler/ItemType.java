package com.zz.yt.lib.ui.recycler;

/**
 * item的类型
 *
 * @author qf
 * @version 1.0
 */
public interface ItemType {

    //region 普通类

    /**
     * 当行文字
     */
    int TEXT = 10100;

    /**
     * 标签
     */
    int LABEL = 10101;

    /**
     * 当行文字对
     */
    int TEXT_TEXT = 10102;

    /**
     * 当行文字de可点击
     */
    int TEXT_MORE = 10103;

    /**
     * 当行文字对
     */
    int TEXT_TEXTS = 10104;

    /**
     * 当行文字对(左右靠边分布)
     */
    int TEXT_MULTIPLE = 10105;

    /**
     * 当行文字
     */
    int TITLE = 10106;

    /**
     * 模块分
     */
    int BRANCH = 10107;

    /**
     * 单个图片
     */
    int ICON = 10108;

    /**
     * 单个图片
     */
    int IMAGE = 10109;

    /**
     * 多个图文(上面图片 ，下面文字<广告>)
     */
    int BANNER = 10111;

    /**
     * 信息栏
     */
    int INFORMATION_BAR = 10112;

    /**
     * 图文
     * 右边文字 ，左边图片
     */
    int ICON_TEXT = 10113;
    /**
     * 图文
     * 右边文字 ，左边图片
     */
    int IMAGE_TEXT = 10114;

    /**
     * 图文
     * 上面文字 ，下面图片
     */
    int TEXT_IMAGE = 10115;


    /**
     * 类九宫格（字体图标）
     */
    int ICON_FONT_NAME = 10121;

    /**
     * 类九宫格
     */
    int ICON_NAME = 10123;

    /**
     * 类九宫格
     */
    int ICON_MAX_NAME = 10124;
    /**
     * 类九宫格（带角标）
     */
    int ICON_NUM_NAME = 10125;

    /**
     * 类九宫格(图片是圆的)
     */
    int IMAGE_CIRCLE_NAME = 10131;

    /**
     * 类九宫格
     */
    int IMAGE_NAME = 10133;

    /**
     * 类九宫格
     */
    int IMAGE_MAX_NAME = 10134;

    /**
     * 类九宫格（带角标）
     */
    int IMAGE_NUM_NAME = 10135;


    //endregion

    //region 信息类

    /**
     * 类消息(本地图片*单行)
     */
    int ICON_TEXT_TEXT = 10201;

    /**
     * 类消息(网络等图片*单行)
     */
    int IMAGE_TEXT_TEXT = 10202;

    /**
     * 类消息(网络等图片*单行)
     */
    int IMAGE_CIRCLE_TEXT_TEXT = 10203;

    /**
     * 类消息(本地图片)
     */
    int ICON_INFORMATION = 10204;

    /**
     * 类消息(网络等图片)
     */
    int IMAGE_INFORMATION = 10205;

    /**
     * 类消息(网络等图片，图片是圆的)
     */
    int IMAGE_CIRCLE_INFORMATION = 10206;

    /**
     * 类消息(本地图片+计数)
     */
    int ICON_INFORMATION_COUNT = 10207;

    /**
     * 类消息(网络等图片+计数)
     */
    int IMAGE_INFORMATION_COUNT = 10208;

    /**
     * 系统消息
     */
    int INFORMATION_MSG = 10209;

    //endregion

    //region记录类

    /**
     * 记录de内容2行
     */
    int ICON_RECORD_NAME2 = 10301;

    /**
     * 记录de内容2行
     */
    int IMAGE_RECORD_NAME2 = 10302;

    /**
     * 记录de内容3行
     */
    int ICON_RECORD_NAME3 = 10303;

    /**
     * 记录de内容3行
     */
    int IMAGE_RECORD_NAME3 = 10304;

    /**
     * 记录de内容4行
     */
    int ICON_RECORD_NAME4 = 10305;

    /**
     * 记录de内容4行
     */
    int IMAGE_RECORD_NAME4 = 10306;

    // endregion

    // region 任务

    /**
     * 任务
     */
    int TASK_NAME2 = 10402;

    /**
     * 任务
     */
    int TASK_NAME3 = 10403;

    /**
     * 任务
     */
    int TASK_NAME4 = 10404;

    /**
     * 任务
     */
    int TASK_NAME5 = 10405;

    /**
     * 任务
     */
    int TASK_NAME6 = 10406;

    // endregion

    // region 任务

    /**
     * 任务
     */
    int TASK_CHECK2 = 10502;
    /**
     * 任务
     */
    int TASK_CHECK3 = 10503;
    /**
     * 任务
     */
    int TASK_CHECK4 = 10504;

    // endregion

    //region其他类

    /**
     * 分类
     */
    int CLASSIFICATION = 10600;

    /**
     * 排序筛选类功能
     */
    int FUNCTION = 10601;

    /**
     * 表格
     */
    int TABLE = 10602;

    /**
     * 表格每格
     */
    int TABLE_ITEM = 10603;

    /**
     * 附件
     */
    int ANNEX = 10604;

    /**
     * 附件(相片)
     */
    int ANNEX_PHOTO = 10605;

    /**
     * 历史信息(app更新记录)
     */
    int EDITION_RECORD = 10606;

    /**
     * 筛选
     */
    int SCREEN = 10607;

    /**
     * 列表点击
     */
    int CHARGE_TYPE = 10608;

    //endregion

}
