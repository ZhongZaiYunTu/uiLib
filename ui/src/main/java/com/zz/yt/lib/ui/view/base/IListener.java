package com.zz.yt.lib.ui.view.base;

import com.whf.android.jar.base.delegate.IPageLoadListener;
import com.whf.android.jar.net.callback.IError;
import com.whf.android.jar.net.callback.IFailure;
import com.zz.yt.lib.ui.listener.OnClickIntBoolListener;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.listener.OnClickIntStrListener;
import com.zz.yt.lib.ui.listener.OnClickMoreListener;
import com.zz.yt.lib.ui.listener.OnClickStrObjListener;

interface IListener {

    /***
     * 点击事件
     * @param listener：
     */
    void setPageLoadListener(IPageLoadListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickIntBoolListener(OnClickIntBoolListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickIntObjListener(OnClickIntObjListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickIntStrListener(OnClickIntStrListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickStrObjListener(OnClickStrObjListener listener);

    /***
     * 点击事件
     * @param listener：
     */
    void setOnClickMoreListener(OnClickMoreListener listener);


    /***
     * 设置网络访问问题回调
     * @param mFailure：
     */
    void setFailure(IFailure mFailure);

    /***
     * 设置接口错误回调
     * @param mError：
     */
    void setError(IError mError);


}
