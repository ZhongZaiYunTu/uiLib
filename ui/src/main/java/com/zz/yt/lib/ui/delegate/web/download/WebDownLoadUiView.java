package com.zz.yt.lib.ui.delegate.web.download;

import android.webkit.DownloadListener;

import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.listener.OnClickStringListener;

/**
 * 调用其它浏览器下载文件：
 * <p> 如果你手机内存在多个浏览器的话，会打开一个对话框供你选择其中一个浏览器进行下载~
 *
 * @author qf
 * @version 1.0.3
 */
public class WebDownLoadUiView implements DownloadListener {

    private final OnClickStringListener mClickStringListener;

    public WebDownLoadUiView(OnClickStringListener listener) {
        this.mClickStringListener = listener;
    }

    @Override
    public void onDownloadStart(String url, String userAgent, String contentDisposition,
                                String mimetype, long contentLength) {
        LatteLogger.i("url=" + url
                + "; userAgent=" + userAgent
                + "; mimetype=" + mimetype
                + "; contentLength=" + contentLength);
        if(mClickStringListener != null){
            mClickStringListener.onClick(url);
        }
    }


}
