package com.zz.yt.lib.ui.entity;

import java.util.List;

/**
 * 基础bean
 *
 * @author lzl
 * @version 2.5.1
 */
public class DateEntry<T> {


    private int total;
    private int pages;
    private int size;
    private List<T> records;

    public DateEntry() {
    }

    public DateEntry(List<T> records) {
        this.records = records;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }
}
