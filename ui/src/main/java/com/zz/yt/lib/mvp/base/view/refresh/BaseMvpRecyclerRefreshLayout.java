package com.zz.yt.lib.mvp.base.view.refresh;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import com.whf.android.jar.constants.Status;
import com.zz.yt.lib.mvp.base.view.BaseMvpRecyclerLayout;
import com.zz.yt.lib.ui.listener.OnClickPageSizeListener;

import cn.bingoogolapple.refreshlayout.BGANormalRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BGARefreshLayout;

/**
 * 可刷新de列表控件。
 *
 * @author qf
 * @version 1.0
 */
public abstract class BaseMvpRecyclerRefreshLayout<T> extends BaseMvpRecyclerLayout<T> implements
        BGARefreshLayout.BGARefreshLayoutDelegate {

    /*** 当前页数 */
    protected int page = 1;
    /*** 每页查询多少条 */
    protected int pageSize = 10;
    /*** 一共多少条 */
    protected int total = pageSize;
    // 刷新状态
    protected Status mStatus = Status.REFRESH;
    // 是否显示刷新、加载更多动画
    protected boolean animation = false;
    protected OnClickPageSizeListener mOnClickPageSizeListener = null;

    private final BGARefreshLayout mRefresh;

    public BaseMvpRecyclerRefreshLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mRefresh = setBGARefreshLayout();
        if (mRefresh != null) {
            mRefresh.setDelegate(this);
            mRefresh.setRefreshViewHolder(new BGANormalRefreshViewHolder(context, true));
        }
    }

    /**
     * @param animation:是否显示刷新、加载更多动画
     */
    public void setAnimation(boolean animation) {
        this.animation = animation;
    }

    /**
     * @param listener:接口回调
     */
    public final void setOnClickIntsListener(OnClickPageSizeListener listener) {
        setOnClickIntsListener(listener, false);
    }

    /**
     * @param listener:接口回调
     * @param init:是否初始化就调用回调
     */
    public final void setOnClickIntsListener(OnClickPageSizeListener listener, boolean init) {
        this.mOnClickPageSizeListener = listener;
        if (init) {
            refreshing();
        }
    }

    /**
     * 初始化列表控件
     */
    protected abstract BGARefreshLayout setBGARefreshLayout();

    @Override
    public void onBGARefreshLayoutBeginRefreshing(BGARefreshLayout refreshLayout) {
        refreshing();
    }

    /**
     * 刷新
     */
    public void refreshing() {
        // 在这里加载最新数据
        mStatus = Status.REFRESH;
        page = 1;
        if (mOnClickPageSizeListener != null) {
            mOnClickPageSizeListener.onClick(page, pageSize);
            if (mRefresh != null && animation) {
                mRefresh.beginRefreshing();
            }
        }
    }

    @Override
    public boolean onBGARefreshLayoutBeginLoadingMore(BGARefreshLayout refreshLayout) {
        //是否可以加载更多
        final boolean isMore = total > page * pageSize;
        if (isMore) {
            mStatus = Status.MORE;
            page++;
            if (mOnClickPageSizeListener != null) {
                mOnClickPageSizeListener.onClick(page, pageSize);
                if (mRefresh != null && animation) {
                    mRefresh.beginLoadingMore();
                }
            }
            return true;
        }
        return false;
    }

    /***
     * 关闭 加载 进度条
     * @param success:网络请求是否成功
     */
    public final void close(boolean success) {
        if (mRefresh == null) {
            return;
        }
        if (mStatus == Status.REFRESH) {
            mRefresh.endRefreshing();
        } else {
            page -= success ? 0 : 1;
            mRefresh.endLoadingMore();
        }
    }

    @Override
    public void onError(int code, String msg) {
        super.onError(code, msg);
        page = 1;
        if (mRefresh != null) {
            //关闭 加载 进度条
            mRefresh.endRefreshing();
            mRefresh.endLoadingMore();
        }
    }

    /**
     * @param total:总条数
     */
    public final void setTotal(int total) {
        this.total = total;
    }

    /**
     * @param pageSize:每页查询多少条
     */
    public final void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
