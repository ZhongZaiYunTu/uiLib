package com.zz.yt.lib.ui.listener;

import java.util.Map;

/**
 * 点击事件
 *
 * @author qf
 * @version 1.0
 **/
public interface OnClickMapListener {

    /***
     * 点击
     * @param objectMap：值
     */
    void onClick(Map<String, Object> objectMap);

}
