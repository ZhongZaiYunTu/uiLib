package com.zz.yt.lib.ui.view.paint.util;

/***
 * 画笔常量
 * @author qf
 * @version 1.0
 * @date 2020/12/7
 **/
public interface PaintKeys {

    /**
     * 默认画笔颜色
     */
    String[] PEN_COLORS = new String[]{"#101010", "#027de9", "#0cba02", "#f9d403", "#ec041f"};

    /**
     * 默认画笔大小
     */
    int[] PEN_SIZES = new int[]{2, 4, 8, 16, 32};
}
