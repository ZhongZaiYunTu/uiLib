package com.zz.yt.lib.mvp.base.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.callback.OnRecyclerItemCallback;
import com.zz.yt.lib.ui.recycler.divider.BaseDecoration;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 列表控件
 *
 * @author qf
 * @version 1.0
 */
public abstract class BaseMvpRecyclerLayout<T> extends BaseLinearLayout implements
        OnRecyclerItemCallback, OnItemClickListener {

    private boolean isEmptyView = false;
    private boolean isTipRepeat = false;
    private Object layoutResId = null;
    protected final BaseMvpRecyclerAdapter<T> viewAdapter;
    protected final RecyclerView recyclerView;
    protected List<T> dataItem = new ArrayList<>();


    public BaseMvpRecyclerLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        recyclerView = setRecyclerView();
        viewAdapter = adapter();
        if (recyclerView != null) {
            recyclerView.setLayoutManager(layoutManager(context));

            if (viewAdapter != null) {
                viewAdapter.setOnItemClickListener(this);
                recyclerView.setAdapter(viewAdapter);
            }
        }
    }

    /**
     * @param tipRepeat:设置是否显示重复添加提示。
     */
    public void setTipRepeat(boolean tipRepeat) {
        isTipRepeat = tipRepeat;
    }

    /**
     * @param emptyView:设置是否显示无数据图片
     */
    public void setEmptyView(boolean emptyView) {
        isEmptyView = emptyView;
    }

    /**
     * @param layoutResId:设置无数据等错误信息情况的图片
     */
    public void setEmptyLayout(Object layoutResId) {
        this.layoutResId = layoutResId;
    }

    protected void setEmptyView() {
        if (viewAdapter != null && isEmptyView) {
            if (layoutResId instanceof Integer) {
                viewAdapter.setEmptyView((int) layoutResId);
            } else if (layoutResId instanceof View) {
                viewAdapter.setEmptyView((View) layoutResId);
            } else {
                viewAdapter.setEmptyView(R.layout.hai_loading_view);
            }
        }
    }

    /**
     * 初始化列表控件
     */
    protected abstract RecyclerView setRecyclerView();

    /**
     * 初始化适配器
     */
    protected BaseMvpRecyclerAdapter<T> adapter() {
        return new InvoiceViewAdapter<>(this);
    }

    /**
     * 设置数据
     */
    public void setData(List<T> option) {
        dataItem = option;
        if (viewAdapter != null) {
            viewAdapter.setList(dataItem);
            setEmptyView();
        }
    }

    public final void scrollToPosition(int position) {
        if (recyclerView != null) {
            recyclerView.scrollToPosition(position);
        }
    }

    /**
     * @param size:设置间隔
     */
    public final void setItemDecoration(int size) {
        if (recyclerView != null) {
            recyclerView.addItemDecoration(BaseDecoration.create(Color.TRANSPARENT, size));
        }
    }

    /**
     * setting up a new instance to data;
     * 设置新的数据实例，替换原有内存引用。
     * 通常情况下，如非必要，请使用setList修改内容
     */
    public void setNewInstance(List<T> option) {
        dataItem = option;
        if (viewAdapter != null) {
            viewAdapter.setNewInstance(dataItem);
            setEmptyView();
        }
    }

    /**
     * 网络服务失败清空数据
     */
    public void setError() {
        setNewInstance(new ArrayList<T>());
    }


    /**
     * @param i:删除一行
     */
    public void remove(int i) {
        if (dataItem.size() > i && getData().size() > i) {
            getData().remove(i);
            dataItem.remove(i);
            notifyDataSetChanged();
        }
    }

    /**
     * @param option:删除一行
     */
    public void remove(T option) {
        if (getData().contains(option)) {
            getData().remove(option);
            dataItem.remove(option);
            notifyDataSetChanged();
        }
    }

    /**
     * 添加数据
     */
    public void setData(T option) {
        if (dataItem == null) {
            dataItem = new ArrayList<>();
        }
        if (dataItem.contains(option)) {
            if (isTipRepeat) {
                ToastUtils.showShort("已添加,请勿重复添加");
            }
            return;
        }
        dataItem.add(option);
        if (viewAdapter != null) {
            viewAdapter.addData(option);
        }
    }

    /**
     * 添加数据
     */
    public void addData(List<T> option) {
        if (dataItem == null) {
            dataItem = new ArrayList<>();
        }
        dataItem.addAll(option);
        if (viewAdapter != null) {
            viewAdapter.addData(option);
        }
    }

    /**
     * 刷新数据
     */
    public void setData(int position, T option) {
        if (dataItem != null && dataItem.size() > position) {
            dataItem.set(position, option);
        }
    }

    public List<T> getDataItem() {
        return dataItem;
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view,
                            int position) {

    }

    /**
     * 获得 List<Object> 类型数据
     */
    public List<Object> getObjData() {
        if (viewAdapter != null) {
            return viewAdapter.getObjData();
        }
        return new ArrayList<>();
    }

    /**
     * 获得 值数据
     */
    public List<T> getData() {
        if (viewAdapter != null) {
            return viewAdapter.getData();
        }
        return new ArrayList<>();
    }

    /**
     * @param object:设置全局的其他属性
     */
    public void setObject(Object object) {
        if (viewAdapter != null) {
            viewAdapter.setObject(object);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    protected void notifyDataSetChanged() {
        Latte.getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (viewAdapter != null) {
                    viewAdapter.notifyDataSetChanged();
                    setEmptyView();
                }
            }
        }, 222);
    }

    /**
     * @param comparator:list集合的排序
     */
    public void sort(Comparator<T> comparator) {
        if (dataItem == null) {
            dataItem = new ArrayList<>();
        }
        Collections.sort(dataItem, comparator);
        notifyDataSetChanged();
    }

    /**
     * 适配器
     */
    public BaseMvpRecyclerAdapter<T> getAdapter() {
        if (viewAdapter != null) {
            return viewAdapter;
        }
        return adapter();
    }

    private static class InvoiceViewAdapter<T> extends BaseMvpRecyclerAdapter<T> {

        private final OnRecyclerItemCallback callbackRecycler;

        public InvoiceViewAdapter(@NonNull OnRecyclerItemCallback callback) {
            super(callback.setLayoutItem(), new ArrayList<T>());
            this.callbackRecycler = callback;
        }

        @Override
        protected void convert(@NonNull MultipleViewHolder holder, @NonNull T entity) {
            if (callbackRecycler != null) {
                callbackRecycler.convert(holder, entity, object);
            }
        }

    }

}
