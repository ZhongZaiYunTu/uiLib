package com.zz.yt.lib.ui.delegate.authentication.personal;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.ButtonListLayout;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.delegate.authentication.personal.entity.PersonalEntity;




/**
 * 个人信息
 *
 * @author qf
 * @version 1.0
 * @date 2020/8/13
 **/
public class PersonalDetailsDelegate extends LatteTitleDelegate {


    private ButtonListLayout mBllName = null;
    private ButtonListLayout mBllSex = null;
    private ButtonListLayout mBllNationality = null;
    private ButtonListLayout mBllIdType = null;
    private ButtonListLayout mBllId = null;
    private ButtonListLayout mBllTermValidity = null;
    private ButtonListLayout mBllIdPhoto = null;

    private String title = null;
    private PersonalEntity entity = null;

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_aut_personal;
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText(TextUtils.isEmpty(title) ? "个人信息" : title);
        }
    }

    @NonNull
    public static PersonalDetailsDelegate create(String title, PersonalEntity entity) {
        Bundle args = new Bundle();
        args.putString(BundleKey.TITLE.name(), title);
        args.putSerializable(BundleKey.ENTITY.name(), entity);
        PersonalDetailsDelegate fragment = new PersonalDetailsDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        title = bundle.getString(BundleKey.TITLE.name());
        entity = (PersonalEntity) bundle.getSerializable(BundleKey.ENTITY.name());
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        initView();
        initView();
    }

    private void initView() {
        mBllName = findViewById(R.id.id_bll_name);
        mBllSex = findViewById(R.id.id_bll_sex);
        mBllNationality = findViewById(R.id.id_bll_nationality);

        mBllIdType = findViewById(R.id.id_bll_id_type);
        mBllId = findViewById(R.id.id_bll_id);
        mBllTermValidity = findViewById(R.id.id_bll_term_validity);

        mBllIdPhoto = findViewById(R.id.id_bll_id_photo);
        inListener();
    }

    private void inListener() {
        if (entity == null) {
            return;
        }
        setBll(mBllName, entity.getName());
        setBll(mBllSex, entity.getSex());
        setBll(mBllNationality, entity.getNationality());
        setBll(mBllIdType, entity.getIdType());
        setBll(mBllId, entity.getId());
        setBll(mBllTermValidity, entity.getTermValidity());
        setBll(mBllIdPhoto, entity.getIdPhoto());
        if (mBllIdPhoto != null) {
            mBllIdPhoto.getTextRightBtn().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itIdPhoto(entity.getIdPhoto());
                }
            });
        }
    }

    private void setBll(ButtonListLayout bllId, String name) {
        if (bllId != null && name != null) {
            bllId.getTextRightBtn().setText(name);
        }
    }


    protected void itIdPhoto(String idPhoto) {
        getSupportDelegate().start(new AuthenticationDelegate());
    }
}
