package com.zz.yt.lib.ui.item;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.zz.yt.lib.ui.R;

/**
 * 意见列表
 *
 * @author qf
 * @version 1.0
 * @date 2020/4/24
 **/
public class OpinionShadowLayout extends RelativeLayout {


    public OpinionShadowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.shadow_opinion_list, this, true);
    }

}
