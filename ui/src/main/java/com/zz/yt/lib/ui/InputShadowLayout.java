package com.zz.yt.lib.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;

import com.lijiankun24.shadowlayout.ShadowLayout;


/**
 * 录入按钮
 *
 * @author qf
 * @date 2020/4/24
 **/
public class InputShadowLayout extends RelativeLayout {


    public InputShadowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.shadow_input, this, true);
    }

    public TextView getValue() {
        return findViewById(R.id.id_text_value);
    }

    public ShadowLayout getShadowView() {
        return findViewById(R.id.sv_one);
    }

    /**
     * 设置控件的显示值
     *
     * @param text：
     */
    public void setValueView(CharSequence text) {
        getValue().setText(text);
    }


    public void setSize() {
        getShadowView();
    }


    /**
     * 背景颜色
     *
     * @param color：颜色
     */
    @Override
    public void setBackgroundColor(@ColorInt int color) {
        getShadowView().setBackgroundColor(color);
    }

    /**
     * 设置控件的显示图片
     *
     * @param text：
     */
    @Override
    public void setBackgroundResource(@DrawableRes int text) {
        getValue().setBackgroundResource(text);
    }

}
