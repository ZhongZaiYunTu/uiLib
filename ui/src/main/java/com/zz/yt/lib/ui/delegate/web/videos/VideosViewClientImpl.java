package com.zz.yt.lib.ui.delegate.web.videos;

import android.app.Activity;
import android.net.Uri;
import android.os.Build;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.UtilsTransActivity;
import com.whf.android.jar.util.log.LatteLogger;

import java.util.List;

import static com.blankj.utilcode.util.ThreadUtils.runOnUiThread;

public class VideosViewClientImpl extends WebChromeClient {

    @Override
    public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        return super.onJsAlert(view, url, message, result);
    }

    //当document 的title变化时，会通知应用程序
    @Override
    public void onReceivedTitle(WebView view, String title) {
        super.onReceivedTitle(view, title);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onPermissionRequest(final PermissionRequest request) {
        // 参考 https://github.com/googlearchive/chromium-webview-samples/blob/master/webrtc-example/app/src/main/java/com/google/chrome/android/webrtcsample/MainActivity.java#L144
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                request.grant(request.getResources());
            }
        });
    }

    /**
     * @param webView:
     * @param filePathCallback:
     * @param fileChooserParams:是文件选择的参数
     */
    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback,
                                     @NonNull FileChooserParams fileChooserParams) {

//        this.filePathCallback = filePathCallback;
//        //姜前端H5接收的格式类型转为字符串,且后面不带分号
//        String[] acceptTypes = fileChooserParams.getAcceptTypes();
//        String acceptType = "*/*";
//        StringBuilder sb = new StringBuilder();
//        if (acceptTypes.length > 0) {
//            for (String type : acceptTypes) {
//                sb.append(type).append(';');
//            }
//        }
//        if (sb.length() > 0) {
//            String typeStr = sb.toString();
//            acceptType = typeStr.substring(0, typeStr.length() - 1);
//        }

        //根据判断，触发相关的操作,如文件选择,拍照等...详见3步讲解
        //这里，也可以实现弹出个对话框供用户选择，记得在弹出对话框之后调用下回调onReceiveValue方法，否则会出现下次无法弹出对话框的Bug
        PermissionUtils
                .permission(PermissionConstants.CAMERA)
                .rationale(new PermissionUtils.OnRationaleListener() {
                    @Override
                    public void rationale(@NonNull UtilsTransActivity activity, @NonNull ShouldRequest shouldRequest) {
                        shouldRequest.again(true);
                    }
                })
                .callback(new PermissionUtils.FullCallback() {
                    @Override
                    public void onGranted(@NonNull List<String> permissionsGranted) {
                        LatteLogger.d("权限获取成功", permissionsGranted);
                    }

                    @Override
                    public void onDenied(@NonNull List<String> permissionsDeniedForever, @NonNull List<String> permissionsDenied) {
                        LatteLogger.d("权限永久拒绝", permissionsDeniedForever);
                        LatteLogger.d("权限拒绝", permissionsDenied);
                    }
                })
                .theme(new PermissionUtils.ThemeCallback() {
                    @Override
                    public void onActivityCreate(@NonNull Activity activity) {
                        ScreenUtils.setFullScreen(activity);
                    }
                })
                .request();
        return true;
    }
}
