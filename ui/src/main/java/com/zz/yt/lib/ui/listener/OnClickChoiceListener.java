package com.zz.yt.lib.ui.listener;

/**
 * 选择事件
 *
 * @author qf
 * @version 1.0.23
 **/
public interface OnClickChoiceListener {

    /**
     * 点击
     *
     * @param tag：选中的标识
     * @param text：选中的值
     */
    void onClick(Object tag, String text);

}
