package com.zz.yt.lib.ui.view.calendar.month;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class CalendarUtils {

    /**
     * @param calendar:获得这个月的信息
     */
    @NonNull
    public static List<CalendarMonthEntity> setData(Calendar calendar) {
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }
        calendar.set(Calendar.DATE, 0);
        List<CalendarMonthEntity> mList = new ArrayList<>();
        //获取第一天是星期几然后计算出需要填充的空白数据
        final int dayWeek = getMonthOneDayWeek(calendar);
        for (int i = 0; i < dayWeek; i++) {
            //填充空白的
            mList.add(new CalendarMonthEntity(null));
        }
        //填充数据
        final int maxData = getMonthMaxData(calendar);
        for (int i = 0; i < maxData; i++) {
            Date date = calendar.getTime();
            mList.add(new CalendarMonthEntity(date));
            calendar.add(Calendar.DATE, 1);
        }
        return mList;
    }

    /**
     * @param calendar:获得这个月的信息
     */
    @NonNull
    public static List<Date> setDataTime(Calendar calendar) {
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }
        calendar.set(Calendar.DATE, 0);
        List<Date> mList = new ArrayList<>();
        //获取第一天是星期几然后计算出需要填充的空白数据
        final int dayWeek = getMonthOneDayWeek(calendar);
        for (int i = 0; i < dayWeek; i++) {
            //填充空白的
            mList.add(null);
        }
        //填充数据
        final int maxData = getMonthMaxData(calendar);
        for (int i = 0; i < maxData; i++) {
            Date date = calendar.getTime();
            mList.add(date);
            calendar.add(Calendar.DATE, 1);
        }
        return mList;
    }

    //获取每个月第一天为星期几
    public static int getMonthOneDayWeek(@NonNull Calendar calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }

    //获取当月有几天
    public static int getMonthMaxData(@NonNull Calendar calendar) {
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

}
