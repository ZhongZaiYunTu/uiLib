package com.zz.yt.lib.ui.delegate.cache;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.popup.view.TipsPopup;


/**
 * 清空缓存数据
 *
 * @author qf
 * @version 1.0
 **/
@SuppressLint("NonConstantResourceId")
public final class CacheSettingDelegate extends LatteTitleDelegate {

    /**
     * 可安全清理空间
     */
    private TextView mTvCache = null;

    @NonNull
    public static CacheSettingDelegate create() {
        final Bundle args = new Bundle();
        final CacheSettingDelegate fragment = new CacheSettingDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_setting_cache;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("清空缓存数据");
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        mTvCache = rootView.findViewById(R.id.id_tv_cache);
        try {
            if (mTvCache != null) {
                mTvCache.setText(DataCleanManager.getTotalCacheSize(Latte.getActivity()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        rootView.findViewById(R.id.id_btn_cache).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCache();
            }
        });
    }

    /**
     * 清空缓存
     */
    void onCache() {
        TipsPopup.create()
                .setTitle("清除缓存")
                .setDate("清除缓存会导致下载的内容删除，是否确定？")
                .setOnClickListener(new OnClickDecisionListener() {
                    @Override
                    public void onConfirm(Object decision) {
                        DataCleanManager.clearAllCache(Latte.getActivity());
                        try {
                            if (mTvCache != null) {
                                mTvCache.setText(DataCleanManager.getTotalCacheSize(Latte.getActivity()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                })
                .popup();
    }
}
