package com.zz.yt.lib.ui.delegate.edition.record;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.delegate.edition.listener.IEditionRecord;
import com.zz.yt.lib.ui.delegate.record.BaseRefreshDelegate;
import com.zz.yt.lib.ui.delegate.record.LatteRefreshHandler;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;


/**
 * 版本记录
 *
 * @author NonNull
 * @version 1.0
 **/
public class EditionRecordDelegate extends BaseRefreshDelegate implements
        OnClickIntObjListener {

    private IEditionRecord recordListener;

    @NonNull
    public static EditionRecordDelegate create() {
        final Bundle args = new Bundle();
        final EditionRecordDelegate delegate = new EditionRecordDelegate();
        delegate.setArguments(args);
        return delegate;
    }

    @NonNull
    public static EditionRecordDelegate create(boolean isTop) {
        final Bundle args = new Bundle();
        args.putBoolean(BundleKey.IS_TOP.name(), isTop);
        final EditionRecordDelegate delegate = new EditionRecordDelegate();
        delegate.setArguments(args);
        return delegate;
    }

    @Override
    public void onAttach(@NonNull Context activity) {
        super.onAttach(activity);
        //这里是类型转换，而不是判断
        if (activity instanceof IEditionRecord) {
            recordListener = (IEditionRecord) activity;
        }
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("版本记录信息");
    }

    @Override
    protected BaseDataConverter setDataConverter() {
        return new EditionRecordConverter();
    }

    @Override
    protected void setRefreshHandler(@NonNull LatteRefreshHandler refreshHandler) {
        refreshHandler.setOnClickIntObjListener(this);
    }

    @Override
    protected String urlPage() {
        return Latte.getCheckHistoryServer();
    }

    @Override
    public void onClick(int index, Object obj) {
        LatteLogger.i("点击下载最新的apk");
        setInstallPermission();
    }

    @Override
    protected void installApk() {
        super.installApk();
        if (recordListener != null) {
            recordListener.onEditionRecord();
        }
    }
}
