package com.zz.yt.lib.ui.base.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.whf.android.jar.constants.Status;
import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.callback.RequestCodes;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import cn.bingoogolapple.refreshlayout.BGANormalRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BGARefreshLayout;

/**
 * 列表界面基类
 *
 * @param <adapter> 你的适配器
 * @param <T>       你的javabean
 * @author gags
 * @version 1.0
 */
public abstract class LatteListFragment<adapter extends BaseMvpRecyclerAdapter<T>, T>
        extends LatteTitleDelegate implements BGARefreshLayout.BGARefreshLayoutDelegate,
        OnItemClickListener {

    /**
     * 适配器
     */
    protected adapter mAdapter;

    @Override
    public Object setLayout() {
        return R.layout.mvp_public_recyclerview;
    }


    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);

        mRefresh = findViewById(R.id.rl_module_name_refresh);
        inRefresh();

        RecyclerView mRecyclerView = findViewById(R.id.rl_customer);
        inRecycler(mRecyclerView);

        firstPage();
    }

    /************************************* 设置默认数据 ****************************************/

    protected List<String> data() {
        return data(10);
    }

    protected List<String> data(int size) {
        List<String> data = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            data.add("");
        }
        return data;
    }


    /********************************* 分页数据处理  *****************************************/

    protected final WeakHashMap<String, Object> PARAMS = new WeakHashMap<>();

    /*** 当前页数 */
    protected int page = 1;

    /*** 每页查询多少条 */
    protected int pageSize = 10;

    /*** 一共多少条 */
    protected int total = 1;

    protected void params() {
        params("pageNumber", "pageSize");
    }

    protected void params(String keyNumber, String keySize) {
        PARAMS.put(keyNumber, page);
        PARAMS.put(keySize, pageSize);
    }


    /********************************* RecyclerView  *****************************************/

    protected void inRecycler(RecyclerView mRecyclerView) {
        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(getLayoutManager());
            mAdapter = getAdapter();
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.setOnItemClickListener(this);

        }
    }

    protected void inRefresh() {
        if (mRefresh != null) {
            mRefresh.setDelegate(this);
            mRefresh.setRefreshViewHolder(new BGANormalRefreshViewHolder(context, true));
        }
    }

    /**
     * @return LayoutManager
     */
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(context);
    }

    /**
     * @return 设置你的适配器
     */
    protected abstract adapter getAdapter();


    /********************************* 结束刷新  *****************************************/

    // 刷新状态
    protected Status mStatus = Status.REFRESH;

    @Override
    public void onError(int code, String msg) {
        super.onError(code, msg);
        close(false);
    }

    /***
     * 关闭 加载 进度条
     */
    protected void close() {
        close(true);
    }

    /***
     * 关闭 加载 进度条
     * @param success:网络请求是否成功
     */
    protected void close(boolean success) {
        if (mRefresh == null) {
            return;
        }
        if (mStatus == Status.REFRESH) {
            mRefresh.endRefreshing();
        } else {
            page -= success ? 0 : 1;
            mRefresh.endLoadingMore();
        }
    }

    /********************************* 开始刷新  *****************************************/

    // 列表数据查询
    public abstract void firstPage();

    protected BGARefreshLayout mRefresh;

    public void onBGARefreshLayoutBeginRefreshing(BGARefreshLayout refreshLayout) {
        refreshing();
    }

    /**
     * 刷新
     */
    private void refreshing() {
        // 在这里加载最新数据
        mStatus = Status.REFRESH;
        page = 1;
        firstPage();

    }

    /***
     * 列表数据查询
     * @param url：
     */
    public void firstPage(String url) {
        params();
        firstPage(url, PARAMS);
    }

    /***
     * 列表数据查询：
     * @param url：
     * @param params：
     */
    public abstract void firstPage(String url, WeakHashMap<String, Object> params);

    /********************************* 开始加载更多  *****************************************/

    @Override
    public boolean onBGARefreshLayoutBeginLoadingMore(BGARefreshLayout refreshLayout) {
        //是否可以加载更多
        final boolean isMore = total > page * pageSize;
        if (isMore) {
            mStatus = Status.MORE;
            page++;
            firstPage();
            return true;
        }
        return false;
    }

    /********************************* 操作成功回调，刷新数据  *****************************************/

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RequestCodes.REFRESH) {
            refreshing();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RequestCodes.REFRESH) {
            refreshing();
        }
    }


}
