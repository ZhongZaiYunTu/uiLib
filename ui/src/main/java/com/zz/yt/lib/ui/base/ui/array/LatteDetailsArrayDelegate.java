package com.zz.yt.lib.ui.base.ui.array;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.annex.opinion.OpinionFlowView;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;


/**
 * 纯列表类处理
 *
 * @author qf
 * @version 1.0
 * @date 2020/4/24
 **/
public abstract class LatteDetailsArrayDelegate extends LatteArray {

    /**
     * 获得数据处理
     *
     * @return 数据处理
     */
    protected abstract BaseDataConverter setDataConverter();

    /**
     * 获得数据处理
     *
     * @return 数据处理
     */
    protected abstract BaseDataConverter setData2Converter();

    @Override
    protected void initRecyclerView(RecyclerView recyclerView) {
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
    }

    /***
     * 设置列表
     * @param recyclerView:数据处理
     */
    protected void initRecycler2View(RecyclerView recyclerView) {
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.a_ui_base_detals_array_recycler;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        //背景处理
        final ScrollView scrollView = findViewById(R.id.id_scroll_view);
        if (scrollView != null) {
            initScrollView(scrollView);
        }
        //第二个
        final RecyclerView recyclerView = rootView.findViewById(R.id.id_recycler_name);
        if (recyclerView != null) {
            initRecycler2View(recyclerView);
        }
        //意见流转区
        final OpinionFlowView opinionFlow = findViewById(R.id.id_opinion_flow_view);
        if (opinionFlow != null) {
            initOpinionFlowView(opinionFlow);
        }
    }

    /***
     * 背景处理
     * @param scrollView：
     */
    protected void initScrollView(@NonNull ScrollView scrollView) {
        scrollView.setBackgroundColor(Color.TRANSPARENT);
    }

    /***
     * 意见流转区
     * @param opinionFlow：
     */
    protected void initOpinionFlowView(@NonNull OpinionFlowView opinionFlow) {
        opinionFlow.setVisibility(View.GONE);
    }


}
