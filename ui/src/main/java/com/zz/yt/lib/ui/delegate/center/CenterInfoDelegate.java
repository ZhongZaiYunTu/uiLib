package com.zz.yt.lib.ui.delegate.center;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.GsonUtils;
import com.whf.android.jar.constants.UserConstant;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.ui.ButtonListLayout;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.delegate.record.BaseRefreshDelegate;
import com.zz.yt.lib.ui.delegate.record.LatteRefreshHandler;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.zz.yt.lib.ui.popup.view.EditPopup;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;

import java.util.LinkedHashMap;

/**
 * 个人信息
 *
 * @author qf
 * @version 1.0.2
 **/
public class CenterInfoDelegate extends BaseRefreshDelegate {

    @NonNull
    public static CenterInfoDelegate create() {

        Bundle args = new Bundle();

        CenterInfoDelegate fragment = new CenterInfoDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String urlPage() {
        return null;
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText("个人信息");
        }
    }

    @Override
    protected BaseDataConverter setDataConverter() {
        return null;
    }

    /**
     * @param id:输入类控件
     * @param text:控件de默认值
     * @param key：取显示值的key
     */
    protected void setEdit(int id, String text, final String key) {
        final ButtonListLayout mButton = mRootView.findViewById(id);
        if (mButton != null) {
            mButton.getTextRightBtn().setText(text);
            //修改
            mButton.getTextRightBtn().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String title = "请输入" + mButton.getTextLeftBtn().getText();
                    EditPopup.create()
                            .setTitle(title)
                            .setOnClickListener(new OnClickDecisionListener() {
                                @Override
                                public void onConfirm(Object text) {
                                    upDate(key, text);
                                }

                                @Override
                                public void onCancel() {

                                }
                            })
                            .popup();
                }
            });
        }
    }

    @Override
    protected void setRefreshHandler(LatteRefreshHandler refreshHandler) {

    }

    protected String getUrlUp() {
        return "";
    }

    /**
     * @param key：修改的key
     * @param value：修改的值
     */
    protected void upDate(String key, Object value) {
        final LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("userId", LattePreference.getCustomAppProfile(UserConstant.USER_ID));
        params.put(key, value);
        inPost(getUrlUp(), GsonUtils.toJson(params), this);
    }
}
