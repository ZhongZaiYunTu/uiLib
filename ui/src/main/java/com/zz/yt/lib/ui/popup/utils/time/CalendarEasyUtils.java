package com.zz.yt.lib.ui.popup.utils.time;

import android.annotation.SuppressLint;
import android.view.View;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.callback.OnSelectDateStrCallback;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.popup.view.CalendarEasyPopup;


/**
 * 时间选择(日历de 时间段)
 *
 * @author qf
 * @version 2020/4/24
 **/
public class CalendarEasyUtils implements View.OnClickListener {

    private final boolean isRadio;
    private final boolean toDay;
    private final EditLayoutBar mEditLayoutBar;

    private OnSelectDateStrCallback mConfirmSelectDateCallback = null;

    @NonNull
    public static CalendarEasyUtils create(boolean isRadio, boolean toDay, EditLayoutBar editLayoutBar) {
        return new CalendarEasyUtils(isRadio, toDay, editLayoutBar);
    }

    private CalendarEasyUtils(boolean isRadio, boolean toDay, @NonNull EditLayoutBar editLayoutBar) {
        this.isRadio = isRadio;
        this.toDay = toDay;
        this.mEditLayoutBar = editLayoutBar;
        editLayoutBar.setOnClickListener(this);
    }

    /**
     * 设置回调
     *
     * @param listener：
     */
    public CalendarEasyUtils setOnClickListener(OnSelectDateStrCallback listener) {
        this.mConfirmSelectDateCallback = listener;
        return this;
    }

    @Override
    public void onClick(View v) {
        CalendarEasyPopup
                .create(isRadio, toDay)
                .setOnClickListener(new OnSelectDateStrCallback() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void selectDate(String startTime, String endTime) {
                        if (mEditLayoutBar != null) {
                            mEditLayoutBar.setText(startTime + " 至 " + endTime);
                        }
                        if (mConfirmSelectDateCallback != null) {
                            mConfirmSelectDateCallback.selectDate(startTime, endTime);
                        }
                    }
                })
                .popup();

    }
}
