package com.zz.yt.lib.ui.recycler.banner;

import androidx.annotation.NonNull;

import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;

/**
 * urlImage的广告lan
 *
 * @author 傅令杰
 * @version 1.0
 * @date 2020/4/24
 */
public class BannerCreator {

    public static void createDefault(@NonNull Banner banner, ArrayList<String> titles, ArrayList<String> images,
                                     OnBannerListener listener) {

        banner.setOnBannerListener(listener);
        //设置banner样式
        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR_TITLE);
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        banner.setImages(images);
        //设置图片Title集合
        banner.setBannerTitles(titles);
        //设置banner动画效果
        banner.setBannerAnimation(Transformer.Default);
        //设置自动轮播，默认为true
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(3000);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        banner.start();
    }
}
