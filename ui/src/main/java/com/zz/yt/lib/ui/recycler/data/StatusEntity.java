package com.zz.yt.lib.ui.recycler.data;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;

import com.blankj.utilcode.util.ColorUtils;
import com.zz.yt.lib.ui.R;

/**
 * 订单内容item
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/19
 **/
public final class StatusEntity {


    /**
     * key
     */
    private final String TEXT;

    /**
     * 字体颜色
     */
    @ColorInt
    private final int TEXT_COLOR;

    /**
     * 背景
     */
    @DrawableRes
    private final int BACKDROP_RES;


    public StatusEntity(String text) {
        this.TEXT = text;
        this.TEXT_COLOR = ColorUtils.getColor(R.color.textColor_60);
        this.BACKDROP_RES = R.drawable.frame_ash;
    }

    public StatusEntity(String text, boolean is) {
        this.TEXT = text;
        if (is) {
            this.TEXT_COLOR = ColorUtils.getColor(R.color.colorAccent);
            this.BACKDROP_RES = R.drawable.frame_accent;
        } else {
            this.TEXT_COLOR = ColorUtils.getColor(R.color.textColor_60);
            this.BACKDROP_RES = R.drawable.frame_ash;
        }
    }

    public StatusEntity(String text, @ColorInt int backdropColor, @DrawableRes int backgroundRes) {
        this.TEXT = text;
        this.TEXT_COLOR = backdropColor;
        this.BACKDROP_RES = backgroundRes;
    }

    public String getText() {
        return TEXT;
    }

    public int getTextColor() {
        return TEXT_COLOR;
    }

    public int getBackdropColorRes() {
        return BACKDROP_RES;
    }
}
