package com.zz.yt.lib.ui.item;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.IdRes;

import com.zz.yt.lib.ui.R;


/**
 * 议案查看
 *
 * @author qf
 * @date 2020-04-17
 */
public class SeeIssueShadowLayout extends RelativeLayout {


    public SeeIssueShadowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.shadow_issue_see, this, true);
    }

    public TextView getTitle() {
        return getTextView(R.id.id_text_title);
    }

    /**
     * 设置标题的显示值
     *
     * @param text：
     */
    public void setTitle(CharSequence text) {
        setTextView(R.id.id_text_title, text);
    }

    public TextView getKey1() {
        return getTextView(R.id.id_text_key_1);
    }

    /**
     * 设置第一行key的显示值
     *
     * @param text：
     */
    public void setKey1(CharSequence text) {
        setTextView(R.id.id_text_key_1, text);
    }

    public TextView getKey2() {
        return getTextView(R.id.id_text_key_2);
    }

    /**
     * 设置第二行key的显示值
     *
     * @param text：
     */
    public void setKey2(CharSequence text) {
        setTextView(R.id.id_text_key_2, text);
    }

    public TextView getKey3() {
        return getTextView(R.id.id_text_key_3);
    }

    /**
     * 设置第三行key的显示值
     *
     * @param text：
     */
    public void setKey3(CharSequence text) {
        setTextView(R.id.id_text_key_3, text);
    }

    public TextView getValue1() {
        return getTextView(R.id.id_text_value_1);
    }

    /**
     * 设置第一行value的显示值
     *
     * @param text：
     */
    public void setValue1(CharSequence text) {
        setTextView(R.id.id_text_value_1, text);
    }

    public TextView getValue2() {
        return getTextView(R.id.id_text_value_2);
    }

    /**
     * 设置第二行value的显示值
     *
     * @param text：
     */
    public void setValue2(CharSequence text) {
        setTextView(R.id.id_text_value_2, text);
    }


    public TextView getValue3() {
        return getTextView(R.id.id_text_value_3);
    }

    /**
     * 设置第三行value的显示值
     *
     * @param text：
     */
    public void setValue3(CharSequence text) {
        setTextView(R.id.id_text_value_3, text);
    }

    /**
     * 设置分割线颜色
     *
     * @param color：
     */
    public void setDividingColor(@ColorInt int color) {
        findViewById(R.id.id_view).setBackgroundColor(color);
    }

    /**
     * 获得详情控件
     *
     * @return textView
     */
    public TextView getDetails() {
        return getTextView(R.id.id_text_details);
    }

    /**
     * 设置详情控件的值
     *
     * @param text：
     */
    public void setDetails(CharSequence text) {
        setTextView(R.id.id_text_details, text);
    }

    public TextView getTextView(@IdRes int id) {
        return findViewById(id);
    }


    /**
     * 设置控件的显示值
     *
     * @param text：
     */
    public void setTextView(@IdRes int id, CharSequence text) {
        getTextView(id).setText(text);
    }

}
