package com.zz.yt.lib.ui.navigator.adapter;

import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.navigator.entity.NavEntity;

import java.util.ArrayList;

/**
 * 目录类导航器
 *
 * @author qf
 * @version 1.0
 * @date 2020/7/13
 */
public class NavigatorAdapter extends RecyclerView.Adapter<NavigatorAdapter.NavViewHolder> {

    private ArrayList<NavEntity> mNavEntities;
    private Drawable navDivider;
    private int navTextSize;
    private int navTextColor;

    private OnNavItemClickListener navItemClickListener;

    public NavigatorAdapter(ArrayList<NavEntity> navEntities, Drawable navDivider, int navTextSize, int navTextColor) {
        super();
        this.mNavEntities = navEntities;
        this.navDivider = navDivider;
        this.navTextSize = navTextSize;
        this.navTextColor = navTextColor;
    }

    public void setData(ArrayList<NavEntity> navEntities) {
        this.mNavEntities = navEntities;
        notifyDataSetChanged();
    }

    public void setNavItemClickListener(OnNavItemClickListener navItemClickListener) {
        this.navItemClickListener = navItemClickListener;
    }

    @NonNull
    @Override
    public NavViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int id) {
        View rootView = View.inflate(viewGroup.getContext(), R.layout.hai_item_multiple_navigator, null);
        return new NavViewHolder(rootView, navDivider, navTextSize, navTextColor);
    }

    @Override
    public void onBindViewHolder(@NonNull NavViewHolder viewHolder, int position) {
        final NavEntity navEntity = mNavEntities.get(position);
        if (navEntity != null) {
            if (position == mNavEntities.size() - 1) {
                viewHolder.navLineIv.setVisibility(View.GONE);
            } else {
                viewHolder.navLineIv.setVisibility(View.VISIBLE);
            }
            viewHolder.navTitle.setText(navEntity.getNavTitle());
            viewHolder.navTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navItemClickListener.onNavItemClick(navEntity);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mNavEntities.size();
    }

    static class NavViewHolder extends RecyclerView.ViewHolder {

        TextView navTitle;
        ImageView navLineIv;

        NavViewHolder(View itemView, Drawable navDivider, int navTextSize, int navTextColor) {
            super(itemView);
            navTitle = itemView.findViewById(R.id.navTitleTv);
            navTitle.setTextColor(navTextColor);
            navTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, navTextSize);
            navLineIv = itemView.findViewById(R.id.navLineIv);
            if (navDivider != null) {
                navLineIv.setImageDrawable(navDivider);
            }
        }
    }

    public interface OnNavItemClickListener {

        void onNavItemClick(NavEntity navEntity);
    }
}
