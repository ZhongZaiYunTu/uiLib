package com.zz.yt.lib.ui.navigator;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.navigator.adapter.NavigatorAdapter;
import com.zz.yt.lib.ui.navigator.entity.NavEntity;


import java.util.ArrayList;

/**
 * 目录类导航器
 *
 * @author qf
 * @version 1.0
 * @date 2020/7/13
 **/
public class NavigatorBarView extends LinearLayout implements NavigatorAdapter.OnNavItemClickListener {

    private final int navBackgroundColor;
    private final int navHeight;
    private final Drawable navDivider;
    private final int navTextColor;
    private final int navTextSize;
    /**
     * 设置根元素是否始终显示，如果为false，则只有根元素的时候，会隐藏导航栏
     */
    private final boolean alwaysShowRoot;

    private View mBarView;
    /**
     * 导航信息
     */
    private ArrayList<NavEntity> mNavEntities = new ArrayList<>();
    private RecyclerView mNavRv;
    private NavigatorAdapter mNavAdapter;

    /**
     * 当前目录
     */
    private NavEntity mCurrEntity;

    private OnNavItemClickCallback clickCallback;

    public NavigatorBarView(Context context) {
        this(context, null);
    }

    public NavigatorBarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigatorBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NavigatorBarView, defStyleAttr, 0);
        navBackgroundColor = typedArray.getColor(R.styleable.NavigatorBarView_navBar_background, Color.GRAY);
        navHeight = typedArray.getDimensionPixelSize(R.styleable.NavigatorBarView_navBar_height, dip2px(context, 40));
        navDivider = typedArray.getDrawable(R.styleable.NavigatorBarView_navBar_divider);
        navTextColor = typedArray.getColor(R.styleable.NavigatorBarView_navBar_text_color, Color.BLACK);
        navTextSize = typedArray.getDimensionPixelSize(R.styleable.NavigatorBarView_navBar_text_size, dip2px(context, 14));
        alwaysShowRoot = typedArray.getBoolean(R.styleable.NavigatorBarView_navBar_always_show_root, true);
        typedArray.recycle();
        init(context);
    }

    private void init(Context context) {
        mBarView = LayoutInflater.from(context).inflate(R.layout.hai_view_recycler, this);
        mBarView.setVisibility(GONE);

        mNavRv = findViewById(R.id.id_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mNavRv.setLayoutManager(layoutManager);
        mNavRv.setOverScrollMode(View.OVER_SCROLL_NEVER);

        mNavAdapter = new NavigatorAdapter(mNavEntities, navDivider, navTextSize, navTextColor);
        mNavRv.setAdapter(mNavAdapter);
        mNavAdapter.setNavItemClickListener(this);

        // 设置控件背景色
        mBarView.setBackgroundColor(navBackgroundColor);
        // 设置控件高度
        mBarView.setMinimumHeight(navHeight);
        // 设置控件是否显示
        if (mBarView.getVisibility() == GONE && alwaysShowRoot) {
            mBarView.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onNavItemClick(@NonNull NavEntity navEntity) {
        if (mCurrEntity.getIndex() == navEntity.getIndex()) {
            return;
        }
        mNavEntities = new ArrayList<>(mNavEntities.subList(0, navEntity.getIndex() + 1));
        mNavAdapter.setData(mNavEntities);
        mNavRv.smoothScrollToPosition(navEntity.getIndex());
        // 如果alwaysShowRoot为false
        if (!alwaysShowRoot && navEntity.getIndex() == 0) {
            mBarView.setVisibility(GONE);
        }
        // 点击事件
        clickCallback.onNavClick(navEntity);
    }

    /**
     * 清空数据
     */
    public void clear() {
        mNavEntities.clear();
        mNavAdapter.setData(mNavEntities);
    }

    /***
     * 导航信息
     * @return ArrayList
     */
    public ArrayList<NavEntity> getData() {
        return mNavEntities;
    }

    /***
     * 导航路径
     * @return ArrayList
     */
    public ArrayList<String> getStringData() {
        ArrayList<String> arrayList = new ArrayList<>();
        for (NavEntity navEntity : mNavEntities) {
            arrayList.add(navEntity.getNavTitle());
        }
        return arrayList;
    }

    /**
     * 增加导航元素，不需要缓存数据
     *
     * @param dataId   导航元素id，用于后续点击事件主键的获取
     * @param navTitle 导航元素名称
     */
    public void addNavEntityNoCache(Object dataId, String navTitle) {
        NavEntity navEntity = new NavEntity(mNavEntities.size(), navTitle, dataId);
        addNavEntity(navEntity);
    }

    /**
     * 增加导航元素，需要缓存数据
     *
     * @param dataId    导航元素id，用于后续点击事件主键的获取
     * @param navTitle  导航元素名称
     * @param cacheData 缓存数据
     */
    public void addNavEntityWithCache(Object dataId, String navTitle, Object cacheData) {
        NavEntity navEntity = new NavEntity(mNavEntities.size(), navTitle, dataId, cacheData);
        addNavEntity(navEntity);
    }

    /**
     * 增加导航元素
     *
     * @param navEntity 导航元素
     */
    private void addNavEntity(NavEntity navEntity) {
        if (mBarView.getVisibility() == GONE) {
            mBarView.setVisibility(VISIBLE);
        }
        mNavEntities.add(navEntity);
        mNavAdapter.setData(mNavEntities);
        mCurrEntity = navEntity;
        // TODO 如果滑动到0，则后续数据都不显示，原因未知
        if (navEntity.getIndex() > 0) {
            mNavRv.smoothScrollToPosition(navEntity.getIndex());
        }
    }

    public int dip2px(@NonNull Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 设置导航item的点击回调事件
     *
     * @param itemClickCallback 导航item的点击回调事件
     */
    public void setNavItemClickCallback(OnNavItemClickCallback itemClickCallback) {
        this.clickCallback = itemClickCallback;
    }

    public interface OnNavItemClickCallback {

        /**
         * 导航栏点击事件
         *
         * @param navEntity 当前点击的NavEntity
         */
        void onNavClick(NavEntity navEntity);
    }
}
