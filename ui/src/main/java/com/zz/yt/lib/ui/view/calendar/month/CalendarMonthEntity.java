package com.zz.yt.lib.ui.view.calendar.month;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.utils.FestivalUtils;

import java.util.Date;
import java.util.Objects;

public final class CalendarMonthEntity {

    private final Date date;

    public CalendarMonthEntity(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    @NonNull
    public String getDay() {
        return FestivalUtils.getInstance().getDay(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalendarMonthEntity that = (CalendarMonthEntity) o;
        return date.equals(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }
}
