package com.zz.yt.lib.ui.array;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.ObjectUtils;
import com.zz.yt.lib.ui.base.ui.array.LatteArrayDelegate;
import com.zz.yt.lib.ui.refresh.BaseRefreshHandler;
import com.zz.yt.lib.ui.view.ScreenListView;


/***
 * 纯列表类处理
 * @author qf
 * @version 2.0
 **/
public abstract class BaseArrayRefreshDelegate<refresh extends BaseRefreshHandler>
        extends LatteArrayDelegate {


    protected ScreenListView mScreenListView = null;
    protected refresh mRefreshHandler = null;

    /***
     * 设置加载
     * @param refreshHandler:数据处理
     */
    protected abstract void setRefreshHandler(refresh refreshHandler);

    @Override
    protected void initRefreshHandler(SwipeRefreshLayout swipeRefreshLayout,
                                      RecyclerView recyclerView) {
        mRefreshHandler = setRefreshHandler(recyclerView);
    }

    /***
     * 分页数据和刷新处理
     * @param recyclerView:数据处理
     */
    protected abstract refresh setRefreshHandler(RecyclerView recyclerView);

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        //懒加载，防止事件加不上去
        setRefreshHandler(mRefreshHandler);
    }

    @Override
    protected void initScreenView(ScreenListView screenView) {
        super.initScreenView(screenView);
        mScreenListView = screenView;
        setScreenListView();
    }

    protected void setScreenListView() {

    }

    @Override
    protected void firstPage() {
        super.firstPage();
        if (mRefreshHandler != null) {
            mRefreshHandler.firstPage(urlPage(), PARAMETER);
        }
    }

    @Override
    public void responseSubmit() {
        if (mRefreshHandler != null) {
            mRefreshHandler.onRefresh();
        }
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (mRefreshHandler != null && resultCode == Activity.RESULT_OK) {
            mRefreshHandler.onRefresh();
        }
    }


}
