package com.zz.yt.lib.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ColorUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.zz.yt.lib.mvp.base.view.BaseMvpRecyclerLayout;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.Arrays;

/**
 * 自定义流式布局(实现热门标签页)
 *
 * @author hf
 * @version 1.0
 */

public final class FlowLayout extends BaseMvpRecyclerLayout<String> {

    private int background = R.drawable.frame_accent_r4;
    private int textColor = ColorUtils.getColor(R.color.colorAccent);

    @Override
    protected int setLayout() {
        return R.layout.a_ui_view_recycler_change;
    }

    public FlowLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected RecyclerView setRecyclerView() {
        return findViewById(R.id.rl_customer);
    }

    @NonNull
    @Override
    protected RecyclerView.LayoutManager layoutManager(Context context) {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        return layoutManager;
    }

    /**
     * 设置item项的样式
     */
    public final void setUpText() {
        setUpText(R.drawable.frame_gray_r4, ColorUtils.getColor(R.color.textColor_65));
    }

    /***
     * 设置item项的样式
     * @param background:item的背景
     * @param textColor:item的文字颜色。
     */
    public final void setUpText(@DrawableRes int background, @ColorInt int textColor) {
        setBackground(background);
        setTextColor(textColor);
    }


    /**
     * 使用默认数据（热门城市）
     */
    public final void setData(String... strings) {
        setData(Arrays.asList(strings));
    }

    /**
     * @param background:item的背景
     */
    public final void setBackground(@DrawableRes int background) {
        this.background = background;
    }

    /**
     * @param textColor:item的文字颜色。
     */
    public void setTextColor(@ColorInt int textColor) {
        this.textColor = textColor;
    }

    /**
     * 使用默认数据（热门城市）
     */
    public final void setData() {
        setData("成都");
        setData("上海");
        setData("北京");
        setData("西安");
        setData("昆明");

        setData("广州");
        setData("重庆");
        setData("杭州");
        setData("南京");
        setData("武汉");

        setData("深圳");
        setData("郑州");
        setData("长沙");
        setData("青海");
        setData("沈阳");
    }

    @Override
    public int setLayoutItem() {
        return R.layout.hai_view_flow;
    }

    @Override
    public void convert(@NonNull MultipleViewHolder holder, @NonNull Object entity, Object other) {
        holder.setText(R.id.tv_content, dataItem.get(holder.position()));
        holder.setTextColor(R.id.tv_content, textColor);
        holder.setBackgroundResource(R.id.tv_content, background);
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view,
                            int position) {
        super.onItemClick(adapter, view, position);
        if (mClickIntStrListener != null) {
            mClickIntStrListener.onClick(position, dataItem.get(position));
        }
    }

}