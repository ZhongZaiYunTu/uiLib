package com.zz.yt.lib.ui.utils;

import android.app.Activity;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;


import com.zz.yt.lib.ui.R;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Administrator on 2019/8/13.
 */

public class KeyboardUtil {

    private final Activity mActivity;
    private final KeyboardView mKeyboardView;
    private final EditText[] mEdit;
    private int currentEditText = 0;
    /**
     * 省份简称键盘
     */
    private final Keyboard provinceKeyboard;
    /**
     * 数字与大写字母键盘
     */
    private final Keyboard numberKeyboard;

    public KeyboardUtil(Activity activity, KeyboardView keyboardView, EditText[] edit) {
        mActivity = activity;
        mKeyboardView = keyboardView;
        mEdit = edit;
        provinceKeyboard = new Keyboard(activity, R.xml.province_abbreviation);
        numberKeyboard = new Keyboard(activity, R.xml.number_or_letters);
        mKeyboardView.setKeyboard(provinceKeyboard);
        mKeyboardView.setEnabled(true);
        mKeyboardView.setPreviewEnabled(false);
        //判定是否是中文的正则表达式 [\\u4e00-\\u9fa5]判断一个中文 [\\u4e00-\\u9fa5]+多个中文
        // 省份简称与数字键盘切换
        //没有输入内容时软键盘重置为省份简称软键盘
        // 判断第一个字符是否是中文,是，则自动切换到数字软键盘
        KeyboardView.OnKeyboardActionListener listener = new KeyboardView.OnKeyboardActionListener() {
            @Override
            public void swipeUp() {
            }

            @Override
            public void swipeRight() {
            }

            @Override
            public void swipeLeft() {
            }

            @Override
            public void swipeDown() {
            }

            @Override
            public void onText(CharSequence text) {

            }

            @Override
            public void onRelease(int primaryCode) {
            }

            @Override
            public void onPress(int primaryCode) {
            }

            @Override
            public void onKey(int primaryCode, int[] keyCodes) {
                Editable editable = mEdit[currentEditText].getText();
                int start = mEdit[currentEditText].getSelectionStart();
                //判定是否是中文的正则表达式 [\\u4e00-\\u9fa5]判断一个中文 [\\u4e00-\\u9fa5]+多个中文
                String reg = "[\\u4e00-\\u9fa5]";
                if (primaryCode == -1) {// 省份简称与数字键盘切换
                    if (currentEditText == 0) {
                        if (editable != null && editable.length() > 0) {
                            changeKeyboard(true);
                        } else {
                            if (mEdit[currentEditText].getText().toString().matches(reg)) {
                                changeKeyboard(false);
                            }
                        }
                    } else {
                        changeKeyboard(true);
                    }
                } else if (primaryCode == -3) {
                    if (editable != null && editable.length() > 0) {
                        //没有输入内容时软键盘重置为省份简称软键盘
                        if (editable.length() == 1) {
                            changeKeyboard(false);
                        }
                        if (start > 0) {
                            editable.delete(start - 1, start);
                        }
                    }
                } else if (primaryCode == -9) {
                    if (!mEdit[currentEditText].getText().toString().equals("无")) {
                        editable.append("挂");
                    }
                } else if (primaryCode == -77) {
                    if (!mEdit[currentEditText].getText().toString().equals("无")) {
                        editable.append("无");
                    }
                } else if (primaryCode == -66) {
                    Intent intent = new Intent();
                    intent.setAction("keyboardhide");
                    mActivity.sendBroadcast(intent);
                } else {
                    if (!mEdit[currentEditText].getText().toString().equals("无")) {
                        editable.insert(start, Character.toString((char) primaryCode));
                        // 判断第一个字符是否是中文,是，则自动切换到数字软键盘
                        if (mEdit[currentEditText].getText().toString().matches(reg)) {
                            changeKeyboard(true);
                        }
                    }
                }
            }
        };
        mKeyboardView.setOnKeyboardActionListener(listener);
    }

    /**
     * 指定切换软键盘 isNumber false表示要切换为省份简称软键盘 true表示要切换为数字软键盘
     */
    private void changeKeyboard(boolean isNumber) {
        if (isNumber) {
            mKeyboardView.setKeyboard(numberKeyboard);
        } else {
            mKeyboardView.setKeyboard(provinceKeyboard);
        }
    }

    /**
     * 软键盘展示状态
     */
    public boolean isShow() {
        return mKeyboardView.getVisibility() == View.VISIBLE;
    }

    /**
     * 软键盘展示
     */
    public void showKeyboard() {
        int visibility = mKeyboardView.getVisibility();
        if (visibility == View.GONE || visibility == View.INVISIBLE) {
            mKeyboardView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 点击那个edit
     */
    public void setTouchint(int editnum) {
        this.currentEditText = editnum;
        for (int i = 0; i < mEdit.length; i++) {
            EditText editText = mEdit[i];
            if (i == editnum) {
                editText.setBackgroundResource(R.drawable.ic_vehicle_bg_accent);
            } else {
                editText.setBackgroundResource(R.drawable.ic_vehicle_bg_normal);
            }
        }
    }

    /**
     * 软键盘隐藏
     */
    public void hideKeyboard() {
        int visibility = mKeyboardView.getVisibility();
        if (visibility == View.VISIBLE) {
            changeKeyboard(false);
            mKeyboardView.setVisibility(View.GONE);
        }
    }

    /**
     * 禁掉系统软键盘
     */
    public void hideSoftInputMethod() {
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        String methodName = null;
        if (currentVersion >= 16) {
            // 4.2
            methodName = "setShowSoftInputOnFocus";
        } else if (currentVersion >= 14) {
            // 4.0
            methodName = "setSoftInputShownOnFocus";
        }
        if (methodName == null) {
            mEdit[currentEditText].setInputType(InputType.TYPE_NULL);
        } else {
            Class<EditText> cls = EditText.class;
            Method setShowSoftInputOnFocus;
            try {
                setShowSoftInputOnFocus = cls.getMethod(methodName, boolean.class);
                setShowSoftInputOnFocus.setAccessible(true);
                setShowSoftInputOnFocus.invoke(mEdit[currentEditText], false);
            } catch (NoSuchMethodException e) {
                mEdit[currentEditText].setInputType(InputType.TYPE_NULL);
                e.printStackTrace();
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
}