package com.zz.yt.lib.ui.item;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.IdRes;

import com.zz.yt.lib.ui.R;

/**
 * @author qf
 * @date 2020-04-20
 */
public class AddressShadowLayout extends RelativeLayout {


    public AddressShadowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.shadow_address_list, this, true);
    }


    public TextView getTitle() {
        return getTextView(R.id.id_text_title);
    }

    /**
     * 设置标题的显示值
     *
     * @param text：
     */
    public void setTitle(CharSequence text) {
        setTextView(R.id.id_text_title, text);
    }

    public ImageView getImagePeople() {
        return getImageView(R.id.id_image_people);
    }

    public ImageView getImagePhone() {
        return getImageView(R.id.id_image_phone);
    }

    public TextView getPeople() {
        return getTextView(R.id.id_text_people);
    }

    /**
     * 设置人名
     *
     * @param text：
     */
    public void setPeople(CharSequence text) {
        setTextView(R.id.id_text_people, text);
    }


    public TextView getPhone() {
        return getTextView(R.id.id_text_phone);
    }

    /**
     * 设置电话号码
     *
     * @param text：
     */
    public void setPhone(CharSequence text) {
        setTextView(R.id.id_text_phone, text);
    }

    /**
     * 设置分割线颜色
     *
     * @param color：
     */
    public void setDividingColor(@ColorInt int color) {
        findViewById(R.id.id_view_dividing).setBackgroundColor(color);
    }

    public TextView getTextView(@IdRes int id) {
        return findViewById(id);
    }

    public ImageView getImageView(@IdRes int id) {
        return findViewById(id);
    }

    /**
     * 设置控件的显示值
     *
     * @param text：
     */
    public void setTextView(@IdRes int id, CharSequence text) {
        getTextView(id).setText(text);
    }

}
