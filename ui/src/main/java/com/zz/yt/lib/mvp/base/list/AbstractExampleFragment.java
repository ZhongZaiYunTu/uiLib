package com.zz.yt.lib.mvp.base.list;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.whf.android.jar.constants.Status;
import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.base.mvp.ui.LatteTitleMvpFragment;
import com.zz.yt.lib.ui.callback.RequestCodes;
import com.zz.yt.lib.ui.mvp.contract.ExampleContract;
import com.zz.yt.lib.ui.mvp.model.ExampleModel;
import com.zz.yt.lib.ui.mvp.presenter.ExamplePresenter;

import java.util.WeakHashMap;

import cn.bingoogolapple.refreshlayout.BGANormalRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BGARefreshLayout;


/***
 *
 * @param <adapter> 你的适配器
 * @param <Entry>       你的javabean
 * @author gags
 * @version 1.0
 */
public abstract class AbstractExampleFragment<adapter extends BaseMvpRecyclerAdapter<Entry>, Entry extends MultiItemEntity>
        extends LatteTitleMvpFragment<ExampleModel, ExampleContract.View<Entry>, ExamplePresenter<Entry>>
        implements ExampleContract.View<Entry>, BGARefreshLayout.BGARefreshLayoutDelegate, OnItemClickListener {


    /**
     * 适配器
     */
    protected adapter mAdapter;

    /********************************* 分页数据处理  *****************************************/

    protected final WeakHashMap<String, Object> PARAMS = new WeakHashMap<>();

    /**
     * 当前页数
     */
    protected int page = 1;
    /**
     * 每页查询多少条
     */
    protected int pageSize = 10;

    protected void params() {
        params("pageNumber", "pageSize");
    }

    protected void params(String keyNumber, String keySize) {
        PARAMS.put(keyNumber, page);
        PARAMS.put(keySize, pageSize);
    }

    /********************************* RecyclerView  *****************************************/

    protected void inRecycler(RecyclerView mRecyclerView) {
        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(getLayoutManager());
            mAdapter = getAdapter();
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.setOnItemClickListener(this);

        }
    }

    protected void inRefresh() {
        if (mRefresh != null) {
            mRefresh.setDelegate(this);
            mRefresh.setRefreshViewHolder(new BGANormalRefreshViewHolder(getContext(),
                    true));
        }
    }

    /**
     * @return LayoutManager
     */
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(context);
    }

    /**
     * @return 设置你的适配器
     */
    protected abstract adapter getAdapter();

    @Override
    public ExampleModel createModel() {
        return new ExampleModel(this,this);
    }

    @Override
    public ExampleContract.View<Entry> createView() {
        return this;
    }

    @Override
    public ExamplePresenter<Entry> createPresenter() {
        return new ExamplePresenter<>();
    }


    /********************************* 开始刷新  *****************************************/

    protected BGARefreshLayout mRefresh;
    /**
     * 刷新状态
     */
    protected Status mStatus = Status.REFRESH;

    /**
     * 用来判断 是否为下拉刷新
     */
    protected boolean isRefresh;

    public void onBGARefreshLayoutBeginRefreshing(BGARefreshLayout refreshLayout) {
        refreshing();
    }

    /**
     * 关闭 刷新进度条
     */
    protected void close(int number) {
        if (mRefresh == null) {
            return;
        }
        if (mStatus == Status.REFRESH) {
            mRefresh.endRefreshing();
        } else {
            page -= number;
            mRefresh.endLoadingMore();
        }
    }

    /***
     * 列表数据查询
     */
    public abstract void firstPage();

    /**
     * 刷新
     */
    private void refreshing() {
        // 在这里加载最新数据
        mStatus = Status.REFRESH;
        page = 1;
        isRefresh = true;
        firstPage();

    }

    /***
     * 列表数据查询
     * @param url：
     */
    public void firstPage(String url) {
        firstPage(url, PARAMS);
    }

    /***
     * 列表数据查询：
     * @param url：
     * @param params：
     */
    public void firstPage(String url, WeakHashMap<String, Object> params) {
        params();
        if (presenter != null) {
            presenter.onGet(url, params);
        }
    }

    /********************************* 操作成功回调，刷新数据  *****************************************/

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RequestCodes.REFRESH) {
            refreshing();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RequestCodes.REFRESH) {
            refreshing();
        }
    }

}
