package com.zz.yt.lib.ui.utils;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.JsonUtils;
import com.whf.android.jar.constants.HttpConstant;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Object 强转为其他类型
 *
 * @author qf
 * @version 1.0
 * @date 2020/10/29
 **/
public final class ObjUtils {

    private final static String ID = "id";

    @NonNull
    @SuppressWarnings("unchecked")
    public static <T> T getField(Object key) {
        return (T) key;
    }

    @NonNull
    public static LinkedHashMap<String, Object> getFieldLinked(Object v) {
        return getFieldLinked(ID, v);
    }

    @NonNull
    public static LinkedHashMap<String, Object> getFieldLinked(String key, Object v) {
        final LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(key, v);
        return linkedHashMap;
    }

    @NonNull
    public static WeakHashMap<String, Object> getFieldWeak(Object v) {
        return getFieldWeak(ID, v);
    }


    @NonNull
    public static WeakHashMap<String, Object> getFieldWeak(String key, Object v) {
        final WeakHashMap<String, Object> linkedHashMap = new WeakHashMap<>();
        linkedHashMap.put(key, v);
        return linkedHashMap;
    }

    @NonNull
    public static String getFieldId(List<String> array) {
        final StringBuilder id = new StringBuilder();
        if (array != null) {
            final int size = array.size();
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    id.append(",");
                }
                id.append(array.get(i));
            }
        }
        return id.toString();
    }

    @NonNull
    public static String getFieldArrayId(ArrayList<String> array) {
        final StringBuilder id = new StringBuilder();
        if (array != null) {
            final int size = array.size();
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    id.append(",");
                }
                id.append(array.get(i));
            }
        }
        return id.toString();
    }

    @NonNull
    private List<String> getArray(List<Map<String, Object>> beanList, String string) {
        List<String> stringList = new ArrayList<>();
        if (beanList != null && beanList.size() > 0) {
            for (Map<String, Object> map : beanList) {
                stringList.add(String.valueOf(map.get(string)));
            }
        }
        return stringList;
    }

    public static int getCode(String response) {
        return JsonUtils.getInt(response, HttpConstant.CODE);
    }

    public static String getData(String response) {
        return JsonUtils.getString(response, HttpConstant.DATA);
    }

    public static String getRecords(String response) {
        return JsonUtils.getString(response, HttpConstant.RECORDS);
    }

    public static boolean isEmpty(List list) {
        return list != null && list.size() > 0;
    }

    public static int size(List list) {
        return isEmpty(list) ? list.size() : 0;
    }
}
