package com.zz.yt.lib.ui.entity;

/**
 * 版本记录
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 **/
public class EditionEntry {

    /**
     * apkSize : 18.49 MB
     * appDesc : 高效管理平台-中再生
     * appFile : 0e856d5b13b14a0ab47ee260df5ca85a
     * appFileName : zzsoa7.apk
     * appId : c5a0abb347176cf7f889a836ac480886
     * appName : 中再生
     * compelUpdate : 1
     * compelUpdateCh : 开启
     * content : <p>【新增】新增收藏流程功能和保存发起数据功能。</p>
     <p>【优化】优化更新流程审批。</p>
     <p>【优化】修复已知崩溃问题。</p>
     * createBy : admin
     * createTime : 2021-04-01 19:37:47
     * deleted : 0
     * icon : 4b1bf40ca5eb4e24b9e27e10aeabca04
     * id : 7c0c6bdd3db18098ca08b8977eb4f6c7
     * osType : 0
     * osTypeCh : 安卓
     * updateBy : admin
     * updateTime : 2021-04-01 19:37:59
     * version : 1
     * versionCode : 7
     * versionName : v1.0.7
     */

    private String apkSize;
    private String appDesc;
    private String appFile;
    private String appFileName;
    private String appId;
    private String appName;
    private String compelUpdate;
    private String compelUpdateCh;
    private String content;
    private String createBy;
    private String createTime;
    private String icon;
    private String id;
    private String osType;
    private String osTypeCh;
    private String updateBy;
    private String updateTime;
    private String versionCode;
    private String versionName;

    public String getApkSize() {
        return apkSize;
    }

    public void setApkSize(String apkSize) {
        this.apkSize = apkSize;
    }

    public String getAppDesc() {
        return appDesc;
    }

    public void setAppDesc(String appDesc) {
        this.appDesc = appDesc;
    }

    public String getAppFile() {
        return appFile;
    }

    public void setAppFile(String appFile) {
        this.appFile = appFile;
    }

    public String getAppFileName() {
        return appFileName;
    }

    public void setAppFileName(String appFileName) {
        this.appFileName = appFileName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getCompelUpdate() {
        return compelUpdate;
    }

    public void setCompelUpdate(String compelUpdate) {
        this.compelUpdate = compelUpdate;
    }

    public String getCompelUpdateCh() {
        return compelUpdateCh;
    }

    public void setCompelUpdateCh(String compelUpdateCh) {
        this.compelUpdateCh = compelUpdateCh;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getOsTypeCh() {
        return osTypeCh;
    }

    public void setOsTypeCh(String osTypeCh) {
        this.osTypeCh = osTypeCh;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}
