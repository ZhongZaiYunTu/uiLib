package com.zz.yt.lib.ui.table.entity;

import android.graphics.Color;

import androidx.annotation.ColorInt;

/**
 * bean
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/15
 */
public class TableEntity {

    /**
     * 显示
     */
    private final String NAME;

    /**
     * 权重
     */
    private final int WEIGHT;

    /**
     * 背景颜色
     */
    private final int COLOR;
    /**
     * 背景颜色
     */
    private final int TEXT_COLOR;

    /**
     * 其他
     */
    private Object other;

    public TableEntity(String name, int weight, @ColorInt int color) {
        this.NAME = name;
        this.WEIGHT = weight;
        this.COLOR = color;
        this.TEXT_COLOR = Color.parseColor("#D9000000");
    }

    public TableEntity(String name, int weight, @ColorInt int textColor, @ColorInt int color) {
        this.NAME = name;
        this.WEIGHT = weight;
        this.COLOR = color;
        this.TEXT_COLOR = textColor;
    }

    public int getWeight() {
        return WEIGHT;
    }

    public String getName() {
        return NAME;
    }

    @ColorInt
    public int getTextColor() {
        return TEXT_COLOR;
    }

    @ColorInt
    public int getColor() {
        return COLOR;
    }

    public Object getOther() {
        return other;
    }

    public TableEntity setOther(Object other) {
        this.other = other;
        return this;
    }
}
