package com.zz.yt.lib.mvp.base.list.abolish.object;


import java.io.Serializable;
import java.util.WeakHashMap;

/**
 * 显示详情的标题和内容，用于详情的每行信息
 *
 * @author Gags
 * @version 1.0
 */

public class InfoItemBean implements Serializable {

    /*** 控件的tag*/
    private String field;

    /*** 标题*/
    private String title;

    /*** 单个内容*/
    private String content;

    /*** 用来存text的标记，比如url*/
    private String tag;

    /**
     * 用来区别 适配不同布局的类型
     */
    private int type;

    /**
     * 是否   可编辑
     */
    private boolean isEdit;
    /**
     * 输入文本框输入长度
     */
    private int length;

    /**
     * 是否   为必填项
     */
    private boolean isRequired;

    /**
     * 输入文本框的类型
     */
    private int inputType;
    /**
     * 文本框的颜色
     */
    private int color;

    /**
     * 如果是单项选择框  这里就是 选项
     */
    private Object option;
    /**
     * 其他
     */
    private Object object;
    /**
     * 其他
     */
    private int background;

    /*** 选择的值*/
    private WeakHashMap<String, Object> fieldObject;

    public InfoItemBean(String title, int type, int background) {
        this.title = title;
        this.background = background;
        this.type = type;
    }

    public InfoItemBean(String title, String content, int type) {
        this.title = title;
        this.content = content;
        this.type = type;
    }

    public InfoItemBean(String title, String content, int type, Object option) {
        this.title = title;
        this.content = content;
        this.type = type;
        this.option = option;
    }

    public InfoItemBean(String title, boolean isEdit, int type,
                        Object option, boolean isRequired) {
        this.title = title;
        this.isEdit = isEdit;
        this.type = type;
        this.option = option;
        this.isRequired = isRequired;
    }

    public InfoItemBean(String title, String content, int type,
                        boolean isEdit, boolean isRequired) {
        this.title = title;
        this.content = content;
        this.type = type;
        this.isEdit = isEdit;
        this.isRequired = isRequired;
    }

    public InfoItemBean(String title, String content, int type,
                        boolean isEdit, boolean isRequired, int length) {
        this.title = title;
        this.content = content;
        this.length = length;
        this.type = type;
        this.isEdit = isEdit;
        this.isRequired = isRequired;
    }

    public InfoItemBean(String title, String content, int type,
                        int inputType, boolean isEdit,
                        boolean isRequired) {
        this.title = title;
        this.content = content;
        this.inputType = inputType;
        this.type = type;
        this.isEdit = isEdit;
        this.isRequired = isRequired;
    }

    public InfoItemBean(String field, String title, String content, int type,
                        boolean isEdit, boolean isRequired) {
        this.field = field;
        this.title = title;
        this.content = content;
        this.type = type;
        this.isEdit = isEdit;
        this.isRequired = isRequired;
    }

    public InfoItemBean(String field, String title, String content, int type, int inputType,
                        boolean isEdit, boolean isRequired) {
        this.field = field;
        this.title = title;
        this.content = content;
        this.inputType = inputType;
        this.type = type;
        this.isEdit = isEdit;
        this.isRequired = isRequired;
    }

    /**
     * 自定义 表单
     *
     * @param field      返回服务器的字段
     * @param title      显示的标题
     * @param content    显示的内容  也是最后获取的内容
     * @param type       类型 0（输入文本框） 1（单项选择）
     *                   2（时间选择） 3（城市选择） 4 （添加文件）
     *                   6 (选择控件  把点击事件放到外边 需要对点击事件自己处理)
     *                   5（文件名称显示  预览文件 删除文件）
     * @param isEdit     是不是必填项 true false  必须 不是
     * @param isRequired 是不是可输入 true false  必须 不是
     * @param option     为条件选择器时 放置的list选项（城市选择器不用）
     * @param inputType  输入文本框类型
     */
    public InfoItemBean(String field, String title, String content, int type,
                        boolean isEdit, boolean isRequired, Object option, int inputType) {
        this.field = field;
        this.title = title;
        this.content = content;
        this.type = type;
        this.isEdit = isEdit;
        this.isRequired = isRequired;
        this.option = option;
        this.inputType = inputType;
    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }


    public int getInputType() {
        return inputType;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    public Object getOption() {
        return option;
    }

    public void setOption(Object option) {
        this.option = option;
    }

    public InfoItemBean() {
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }


    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public WeakHashMap<String, Object> getFieldObject() {
        return fieldObject;
    }

    public void setFieldObject(WeakHashMap<String, Object> fieldObject) {
        this.fieldObject = fieldObject;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }
}
