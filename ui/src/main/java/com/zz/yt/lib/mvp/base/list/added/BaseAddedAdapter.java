package com.zz.yt.lib.mvp.base.list.added;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.Collection;
import java.util.List;

/**
 * 工作计划类的内容适配器
 *
 * @author gags
 * @version 1.0
 **/
public abstract class BaseAddedAdapter<T> extends BaseMvpRecyclerAdapter<T> {

    private final RecyclerView recycler;

    protected BaseAddedAdapter(RecyclerView recycler, int layoutResId, List<T> data) {
        super(layoutResId, data);
        this.recycler = recycler;
        addChildClick();
    }

    /**
     * 删除、添加等事件
     */
    protected abstract void addChildClick();

    /**
     * 添加方法
     */
    public abstract void addData();

    @Override
    public void addData(@NonNull Collection<? extends T> newData) {
        onViewValue();
        super.addData(newData);
    }

    @Override
    public void removeAt(int position) {
        onViewValue();
        super.removeAt(position);
    }

    /**
     * 获得数据
     */
    public void onViewValue() {
        for (int i = 0; i < getData().size(); i++) {
            View view = recycler.getChildAt(i);
            if (view != null) {
                setEntity(getData().get(i), view);
            }
        }
    }

    protected abstract void setEntity(T entity, View view);

    /**
     * 数据是否有空
     */
    public boolean canGetData() {
        onViewValue();
        for (int i = 0; i < getData().size(); i++) {
            View view = recycler.getChildAt(i);
            if (view != null) {
                if (isEmpty(i, view)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected abstract boolean isEmpty(int i, View view);

    protected boolean isGone;

    public void setDeleteGone(boolean isGone) {
        this.isGone = isGone;
    }
}
