package com.zz.yt.lib.ui.callback;

/***
 * 请求码存储
 * @author qf
 * @date 2020-03-23
 */
public interface RequestCodes {

    /**
     * 选择拍照
     */
    int TAKE_PHOTO = 4;

    /**
     * 选择相册
     */
    int PICK_PHOTO = 5;

    /**
     * 选中
     */
    int SELECTED = 6;

    /**
     * 二维码
     */
    int SCAN = 7;

    /**
     * 文件
     */
    int FILE_PICKER = 8;

    /**
     * 电子签名标识
     */
    int SIGNATURE = 9;

    /**
     * 刷新
     */
    int REFRESH = 10;

    /**
     * 添加
     */
    int ADDED = 11;

    /**
     * 详情
     */
    int SEE = 12;

    /**
     * 定位
     */
    int LOCATION = 13;
}
