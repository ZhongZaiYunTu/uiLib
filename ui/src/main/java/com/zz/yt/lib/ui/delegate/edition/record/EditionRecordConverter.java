package com.zz.yt.lib.ui.delegate.edition.record;

import android.text.Html;

import com.zz.yt.lib.ui.entity.EditionEntry;
import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据处理
 *
 * @author qf
 * @version 1.0
 **/
class EditionRecordConverter extends BaseDataConverter {

    @Override
    public ArrayList<MultipleItemEntity> convert() {
        final List<EditionEntry> entryList = fromJsonRecords(EditionEntry.class);
        final int size = entryList == null ? 0 : entryList.size();
        for (int i = 0; i < size; i++) {
            EditionEntry editionEntry = entryList.get(i);
            final String content = Html.fromHtml(editionEntry.getContent()) + "";
            ENTITIES.add(MultipleItemEntity.builder()
                    .setItemType(ItemType.EDITION_RECORD)
                    .setField(MultipleFields.SPAN_SIZE, 1)
                    .setField(MultipleFields.TIME, editionEntry.getCreateTime())
                    .setField(MultipleFields.TEXT, content)
                    .setField(MultipleFields.NAME, editionEntry.getVersionName())
                    .setField(MultipleFields.VALUE, editionEntry.getApkSize())
                    .build());
        }
        return ENTITIES;
    }
}
