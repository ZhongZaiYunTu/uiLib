package com.zz.yt.lib.mvp.base.list.abolish;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.mvp.base.list.abolish.click.OnItemAnnexClick;
import com.zz.yt.lib.mvp.base.list.abolish.click.OnItemObjectClick;
import com.zz.yt.lib.mvp.base.list.abolish.object.InfoItemBean;
import com.zz.yt.lib.mvp.holder.BaseViewHolder;

import java.util.List;

/**
 * 可以适配任意类型的  RecyclerView 适配器基类
 *
 * @author Gags
 * @version 1.0
 */
public abstract class BaseObjectRecyclerAdapter<T> extends RecyclerView.Adapter<BaseViewHolder>
        implements View.OnClickListener {
    protected List<T> mData;
    protected LayoutInflater mInflater;
    protected Context mContext;
    protected OnItemAnnexClick mAnnexItemRemove;
    protected OnItemObjectClick onItemObjectClick;
    public OnItemClickListener onItemClickListener;

    public BaseObjectRecyclerAdapter(Context mContext,
                                     List<T> mData) {
        this.mData = mData;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder holder = onCreateDefaultViewHolder(parent, viewType);
        holder.itemView.setTag(holder);
        holder.itemView.setOnClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        onBindViewHolder(holder, mData.get(position), position);
    }

    /**
     * 获得
     *
     * @param viewGroup:
     * @param viewType:
     * @return Q
     */
    protected abstract BaseViewHolder onCreateDefaultViewHolder(ViewGroup viewGroup,
                                                                int viewType);

    /**
     * 设置数据
     *
     * @param holder:控件
     * @param item:数据实体类
     * @param position:下标
     */
    protected abstract void onBindViewHolder(BaseViewHolder holder,
                                             T item,
                                             int position);


    @Override
    public void onClick(@NonNull View v) {
        RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) v.getTag();
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(holder.getAdapterPosition(), holder.getItemId());
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }


    /**
     * 列表点击事件
     */
    public interface OnItemClickListener {
        /**
         * item的 点击事件
         *
         * @param position:
         * @param itemId:
         */
        void onItemClick(int position, long itemId);

    }

    /**
     * @param items:刷新数据
     */
    @SuppressLint("NotifyDataSetChanged")
    public void setDate(List<T> items) {
        if (items != null && items.size() > 0) {
            mData = items;
        } else {
            // 当刷新的数据没有的时候，清空原来的数据
            mData.clear();
        }
        notifyDataSetChanged();
    }

    /**
     * @param items:刷新指定的数据
     */
    public void addDate(T items, int position) {
        if (items != null) {
            mData.set(position, items);
            notifyItemRemoved(position);
        }

    }

    /**
     * @param items:添加指定的数据
     */
    public void add(int index, T items) {
        if (items != null) {
            mData.add(index, items);
            notifyItemRemoved(index);
        }
    }

    /**
     * @param items:添加指定的数据
     */
    public void addAll(List<T> items) {
        if (items != null && items.size() > 0) {
            mData.addAll(items);
            notifyItemRangeInserted(mData.size(), items.size());
        }
    }

    /**
     * @param index:删除指定的数据
     */
    public void remove(int index) {
        if (mData != null && mData.size() > index) {
            mData.remove(index);
            notifyItemRemoved(index);
        }

    }

    public List<T> getDate() {
        return mData;
    }

    public abstract boolean inspect();

    ///********************************************************************************/

    /**
     * 根据标题获取内容  使用此方法前请先调用 设置数据setViewDate方法
     *
     * @param title 你生成 表单所用的标题
     */
    public String getContents(String title) {

        if (mData != null && mData.size() > 0) {
            if (mData.get(0) != null && mData.get(0) instanceof InfoItemBean) {
                List<InfoItemBean> beans = (List<InfoItemBean>) mData;
                for (int i = 0; i < beans.size(); i++) {
                    if (beans.get(i).getTitle().equals(title)) {
                        return beans.get(i).getContent();
                    }
                }
            }
        }
        return "";
    }

    public InfoItemBean getInfoItemBean(String title) {

        if (mData != null && mData.size() > 0) {
            if (mData.get(0) != null && mData.get(0) instanceof InfoItemBean) {
                List<InfoItemBean> beans = (List<InfoItemBean>) mData;
                for (int i = 0; i < beans.size(); i++) {
                    if (beans.get(i).getTitle().equals(title)) {
                        return beans.get(i);
                    }
                }
            }
        }
        return null;
    }

    public int getInfoItemBeanPosition(String title) {

        if (mData != null && mData.size() > 0) {
            if (mData.get(0) != null && mData.get(0) instanceof InfoItemBean) {
                List<InfoItemBean> beans = (List<InfoItemBean>) mData;
                for (int i = 0; i < beans.size(); i++) {
                    if (beans.get(i).getTitle().equals(title)) {
                        return i;
                    }
                }
            }
        }
        return 999;
    }

    ///********************************************************************************/

    public void setOnItemObjectClick(OnItemObjectClick click) {
        this.onItemObjectClick = click;
    }


    public void setOnItemAnnexClick(OnItemAnnexClick click) {
        this.mAnnexItemRemove = click;
    }
}
