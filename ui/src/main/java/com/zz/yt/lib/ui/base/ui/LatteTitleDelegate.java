package com.zz.yt.lib.ui.base.ui;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;


/***
 * 界面
 * @author qf
 * @version 2.3.23
 */
public abstract class LatteTitleDelegate extends LatteDelegate {

    private CustomTitleBar mTitleBar = null;

    /***
     * 设置标题
     * @param titleBar:数据处理
     */
    protected abstract void setCustomTitleBar(CustomTitleBar titleBar);

    /***
     * 获得标题
     * @return 标题
     */
    protected String getTitle() {
        if (mTitleBar == null) {
            return "";
        }
        return mTitleBar.getTitleBarTitle().getText().toString();
    }

    /***
     * 设置标题
     * @param title:标题
     */
    protected void setTitle(String title) {
        if (mTitleBar != null) {
            mTitleBar.getTitleBarTitle().setText(title);
        }
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        //1
        View viewTop = rootView.findViewById(R.id.id_view_top);
        if (viewTop != null && isTopView()) {
            addMarginTopEqualStatusBarHeight(viewTop);
        }
        //2
        mTitleBar = rootView.findViewById(R.id.id_title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitleClickLeftListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goodPop();
                }
            });
            mTitleBar.getTitleBarLeftBtn().setTextSize(18);
            mTitleBar.getTitleBarLeftBtn().setCompoundDrawablePadding(16);
            mTitleBar.getTitleBarTitle().setTextSize(18);
            setCustomTitleBar(mTitleBar);
        }
        //3
        final SearchView mSearchView = rootView.findViewById(R.id.search_view);
        if (mSearchView != null) {
            setSearchView(mSearchView);
        }
        ///是否显示右上角的  debug 标签
        setShow(R.id.text_debug_show, Latte.getDebug() && debugShowCheckedModeBanner());
    }

    /**
     * 是否显示右上角的  debug 标签
     */
    protected boolean debugShowCheckedModeBanner() {
        return false;
    }

    /***
     * 搜索控件
     * @param search：
     */
    protected void setSearchView(@NonNull SearchView search) {
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return inSearch(newText);
            }
        });
    }

    /***
     * 进行搜索
     * @param query：
     */
    protected boolean inSearch(String query) {
        LatteLogger.i(query);
        return false;
    }

}
