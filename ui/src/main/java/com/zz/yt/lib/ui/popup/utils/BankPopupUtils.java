package com.zz.yt.lib.ui.popup.utils;

import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.contrarywind.interfaces.IPickerViewData;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.listener.OnClickIntObjListener;
import com.zz.yt.lib.ui.popup.utils.picker.PickerPopupUtils;
import com.zz.yt.lib.ui.utils.ObjUtils;


import java.util.List;

/**
 * 开户行选择
 *
 * @author qf
 * @version 1.0
 * @date 2020/7/24
 **/
public class BankPopupUtils implements View.OnClickListener {

    private final EditLayoutBar mEditLayoutBar;
    private final List<Bank> beanList;


    @NonNull
    public static BankPopupUtils create(EditLayoutBar editLayoutBar) {
        return new BankPopupUtils(editLayoutBar);
    }

    private BankPopupUtils( @NonNull EditLayoutBar editLayoutBar) {
        this.mEditLayoutBar = editLayoutBar;
        editLayoutBar.setOnClickListener(this);
        final String assets = ResourceUtils.readAssets2String("bank_230619.json");
        beanList = GsonUtils.fromJson(assets, GsonUtils.getListType(Bank.class));
    }

    @Override
    public void onClick(View v) {
        if (beanList == null || beanList.size() == 0) {
            LatteLogger.e("assets文件夹没找到bank.json文件");
            return;
        }
        PickerPopupUtils.showPickerData(Latte.getActivity(),
                "开户行选择", beanList, 0, new OnClickIntObjListener() {
                    @Override
                    public void onClick(int index, Object obj) {
                        Bank entry = ObjUtils.getField(obj);
                        if (entry != null) {
                            mEditLayoutBar.setTag(entry.getValue());
                            mEditLayoutBar.setText(entry.getPickerViewText());
                        }
                    }
                });
    }

    static class Bank implements IPickerViewData {

        private String value;
        private String text;

        @NonNull
        private String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @NonNull
        private String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public String getPickerViewText() {
            return text;
        }
    }
}
