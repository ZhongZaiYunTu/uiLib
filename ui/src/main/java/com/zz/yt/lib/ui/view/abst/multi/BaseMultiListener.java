package com.zz.yt.lib.ui.view.abst.multi;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.GridSpanSizeLookup;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;
import com.zz.yt.lib.ui.view.base.BaseRecycleLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类型布局，适用于类型较少，业务不复杂的场景，便于快速使用。
 *
 * @author qf
 * @version 2.5
 */
public abstract class BaseMultiListener extends BaseRecycleLayout implements ILayoutMultiAdapter,
        OnItemClickListener {

    protected RecycleLayoutAdapter mRecycleAdapter;

    public BaseMultiListener(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mRecycleAdapter = new RecycleLayoutAdapter(this);
        mRecycleAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(mRecycleAdapter);
    }

    @Override
    protected String setTitle() {
        return null;
    }

    @Override
    protected String setTime() {
        return null;
    }

    @Override
    protected String setMore() {
        return null;
    }

    @Override
    protected boolean setIsMore() {
        return false;
    }

    public void setData(List<MenuMultiEntity> data) {
        if (data != null && data.size() > 0) {
            if (mRecycleAdapter != null) {
                mRecycleAdapter.setNewInstance(data);
            }
        } else {
            recyclerView.setBackgroundResource(com.zz.yt.lib.ui.R.mipmap.iv_empty_data);
        }
    }

    public void setDiffNewData(List<MenuMultiEntity> data) {
        if (data != null && data.size() > 0) {
            if (mRecycleAdapter != null) {
                mRecycleAdapter.setDiffNewData(data);
            }
        } else {
            recyclerView.setBackgroundResource(com.zz.yt.lib.ui.R.mipmap.iv_empty_data);
        }
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view,
                            int position) {
        if (mRecycleAdapter != null) {
            Object entity = mRecycleAdapter.getData().get(position);
            if (mClickMoreListener != null) {
                mClickMoreListener.onClickMore(position, entity);
            }
        }
    }

    protected static class RecycleLayoutAdapter extends BaseMultiItemQuickAdapter<MenuMultiEntity,
            MultipleViewHolder> implements GridSpanSizeLookup {

        private final ILayoutMultiAdapter iLayoutAdapter;

        public RecycleLayoutAdapter(ILayoutMultiAdapter iLayout) {
            super(new ArrayList<MenuMultiEntity>());
            this.iLayoutAdapter = iLayout;
            Map<Integer, Integer> maps = iLayoutAdapter.initAdapter();
            if (maps != null && maps.size() > 0) {
                for (Map.Entry<Integer, Integer> e : maps.entrySet()) {
                    addItemType(e.getKey(), e.getValue());
                }
            }
            //设置动画
            setAnimationEnable(true);
        }

        @Override
        public void setNewInstance(List<MenuMultiEntity> list) {
            super.setNewInstance(list);
            //宽度监听
            setGridSpanSizeLookup(this);
        }


        @Override
        protected void convert(@NonNull MultipleViewHolder holder, @NonNull MenuMultiEntity entity) {
            if (iLayoutAdapter != null) {
                iLayoutAdapter.convert(holder, entity);
            }
        }

        @Override
        public int getSpanSize(@NonNull GridLayoutManager gridLayoutManager, int viewType, int position) {
            return getData().get(position).getSpanSize();
        }
    }
}
