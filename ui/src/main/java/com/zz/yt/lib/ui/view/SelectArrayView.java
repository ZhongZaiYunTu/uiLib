package com.zz.yt.lib.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ColorUtils;
import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.entity.SelectEntry;
import com.zz.yt.lib.ui.listener.OnClickBoolListener;
import com.zz.yt.lib.ui.listener.OnClickObjectListener;
import com.zz.yt.lib.ui.recycler.divider.BaseDecoration;
import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.ArrayList;
import java.util.List;


/**
 * 选中状态
 *
 * @author qf
 * @author wang.hai.feng
 * @version 2.5
 */
public class SelectArrayView extends RecyclerView {

    private final SelectAdapter selectAdapter;
    private final List<SelectEntry> data = new ArrayList<>();

    /*** 是否单选*/
    private boolean multiple;

    /*** 是否可以编辑*/
    private boolean enabled = true;

    private int onColor, unColor;
    private int selectOnSrc = -1, selectUnSrc = -1;

    private OnClickObjectListener mClickObjectListener = null;

    public SelectArrayView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SelectArrayView);

        //background
        final Drawable background = attributes.getDrawable(R.styleable.SelectArrayView_android_background);
        if (background != null) {
            setBackground(background);
        }

        //
        final LinearLayoutManager layout = new LinearLayoutManager(context);
        final int orientation = attributes.getInt(R.styleable.SelectArrayView_android_orientation,
                LinearLayoutManager.HORIZONTAL);
        layout.setOrientation(orientation);
        setLayoutManager(layout);
        //设置间隔
        addItemDecoration(BaseDecoration.create(Color.TRANSPARENT, 16));
        //单选
        multiple = attributes.getBoolean(R.styleable.SelectArrayView_multiple, false);
        //
        selectAdapter = new SelectAdapter(data);
        setAdapter(selectAdapter);

        //
        onColor = attributes.getColor(R.styleable.SelectArrayView_onColor, ColorUtils.getColor(R.color.colorAccent));
        unColor = attributes.getColor(R.styleable.SelectArrayView_unColor, Color.BLACK);

        attributes.recycle();

    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<? extends SelectEntry> data) {
        this.data.clear();
        this.data.addAll(data);
        this.selectAdapter.notifyDataSetChanged();
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public SelectAdapter getAdapter() {
        return selectAdapter;
    }

    public List<? extends SelectEntry> getData() {
        return data;
    }

    public void setOnColor(int onColor) {
        this.onColor = onColor;
    }

    public void setUnColor(int unColor) {
        this.unColor = unColor;
    }

    public void setSelectOnSrc(int selectOnSrc) {
        this.selectOnSrc = selectOnSrc;
    }

    public void setSelectUnSrc(int selectUnSrc) {
        this.selectUnSrc = selectUnSrc;
    }

    public void setOnClickObjectListener(OnClickObjectListener listener) {
        this.mClickObjectListener = listener;
    }

    /***
     *
     */
    private class SelectAdapter extends BaseMvpRecyclerAdapter<SelectEntry> {


        public SelectAdapter(List<SelectEntry> data) {
            super(R.layout.hai_view_item_select, data);
        }

        @Override
        protected void convert(@NonNull MultipleViewHolder holder, @NonNull final SelectEntry s) {
            final SelectImageView selectImageView = holder.getView(R.id.siv_select);
            selectImageView.setTextColor(s.isSelect() ? onColor : unColor);
            selectImageView.setChecked(s.isSelect());
            selectImageView.setText(s.getSelectName());
            selectImageView.setEnabled(enabled);
            if (selectOnSrc != -1) {
                selectImageView.setSelectOnSrc(selectOnSrc);
            }
            if (selectUnSrc != -1) {
                selectImageView.setSelectUnSrc(selectUnSrc);
            }
            selectImageView.setOnClickBoolListener(new OnClickBoolListener() {
                @Override
                public void onClick(boolean index) {
                    setSelect();
                    s.setSelect(index);
                    selectImageView.setTextColor(s.isSelect() ? onColor : unColor);
                    if (mClickObjectListener != null) {
                        mClickObjectListener.onClick(getData());
                    }
                }
            });
        }

        private void setSelect() {
            if (multiple) {
                final int size = getData().size();
                for (int i = 0; i < size; i++) {
                    SelectEntry SelectEntry = getData().get(i);
                    SelectEntry.setSelect(false);
                    setData(i, SelectEntry);
                }
            }
        }
    }

}
