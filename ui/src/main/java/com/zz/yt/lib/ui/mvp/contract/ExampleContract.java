package com.zz.yt.lib.ui.mvp.contract;

import com.whf.android.jar.base.mvp.IView;

import com.zz.yt.lib.ui.entity.DateEntry;

import java.util.WeakHashMap;


/**
 * 示例
 *
 * @author 罗周亮
 * @version 1.0
 */
public interface ExampleContract {

    /**
     * 需实现类
     */
    interface View<T> extends IView {

        /**
         * @param result:分页列表查询
         */
        void onSuccess(DateEntry<T> result);
    }

    /**
     * 界面逻辑类
     */
    interface Presenter {

        /**
         * 示例（分页列表查询）
         *
         * @param url:获取数据
         * @param params:接口参数
         */
        void onGet(String url, WeakHashMap<String, Object> params);

        /**
         * 示例（分页列表查询）
         *
         * @param url:获取数据
         * @param pageNumber:第几页
         * @param pageSize:每页多少条
         * @param params:接口参数
         */
        void onGetMenu(String url, int pageNumber, int pageSize, WeakHashMap<String, Object> params);

    }
}
