package com.zz.yt.lib.ui.table;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.change.ChangeRecyclerView;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.listener.OnClickIntListener;
import com.zz.yt.lib.ui.listener.OnClickTableListener;
import com.zz.yt.lib.ui.recycler.ItemType;
import com.zz.yt.lib.ui.recycler.data.MultipleItemEntity;
import com.zz.yt.lib.ui.recycler.divider.BaseDecoration;
import com.zz.yt.lib.ui.recycler.field.MultipleFields;
import com.zz.yt.lib.ui.table.adapter.TableAdapter;
import com.zz.yt.lib.ui.table.entity.TableEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义表格控件
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/15
 **/
public class TableView extends LinearLayout implements OnClickIntListener {

    private final ChangeRecyclerView RECYCLER_VIEW;
    private final LinearLayout LAYOUT_INCREASE;
    private final ImageView IMAGE_INCREASE;
    private final TextView TEXT_SINGLE;

    private TableAdapter mAdapter = null;
    private int entitiesTotal = 1;
    private List<TableEntity> entities = null;
    private OnClickTableListener mClickTableListener = null;

    public TableView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.hai_table_content_layout, this, true);
        RECYCLER_VIEW = findViewById(R.id.recycler_view);
        LAYOUT_INCREASE = findViewById(R.id.layout_increase);
        IMAGE_INCREASE = findViewById(R.id.image_increase);
        TEXT_SINGLE = findViewById(R.id.text_single);
        setOrientation(LinearLayout.VERTICAL);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.TableView);
        final int weightTotal = attributes.getInteger(R.styleable.TableView_weightTotal, 5);
        RECYCLER_VIEW.setLayoutManager(new GridLayoutManager(Latte.getActivity(), weightTotal));
        final int decorationColor = attributes.getColor(R.styleable.TableView_decorationColor,
                Color.parseColor("#E6E6E6"));
        final int decorationSize = attributes.getInteger(R.styleable.TableView_decorationSize, 2);
        RECYCLER_VIEW.addItemDecoration(BaseDecoration.create(decorationColor, decorationSize));

        final boolean isIncrease = attributes.getBoolean(R.styleable.TableView_isIncrease, true);
        if (isIncrease) {
            LAYOUT_INCREASE.setVisibility(View.VISIBLE);
        } else {
            LAYOUT_INCREASE.setVisibility(View.GONE);
        }
        attributes.recycle();
    }

    /**
     * 设置表格每行的权重和
     *
     * @param weightTotal：权重和
     */
    public void setWeightTotal(int weightTotal) {
        RECYCLER_VIEW.setLayoutManager(new GridLayoutManager(Latte.getActivity(), weightTotal));
    }

    public ChangeRecyclerView getRecyclerView() {
        return RECYCLER_VIEW;
    }

    /**
     * 设置表格下面的添加事件
     *
     * @return LinearLayout
     */
    public LinearLayout getLayoutIncrease() {
        return LAYOUT_INCREASE;
    }

    public ImageView getImageIncrease() {
        return IMAGE_INCREASE;
    }

    public TextView getTextSingle() {
        return TEXT_SINGLE;
    }

    /**
     * 设置titles
     *
     * @param entities：
     */
    public void setTableAdapter(List<TableEntity> entities) {
        if (entities == null) {
            throw new NullPointerException("TableEntity is null!");
        }
        this.entities = entities;
        this.entitiesTotal = entities.size();
        final List<MultipleItemEntity> multipleItem = setTable(entities);
        if (mAdapter == null) {
            mAdapter = TableAdapter.create(multipleItem);
            mAdapter.setOnClickIntListener(this);
        } else {
            mAdapter.setList(multipleItem);
        }
        RECYCLER_VIEW.setAdapter(mAdapter);
    }

    /**
     * 设置内容
     *
     * @param entities：titles
     * @param entities2：texts
     */
    public void setTableAdapter(List<TableEntity> entities, List<TableEntity> entities2) {
        if (entities == null) {
            throw new NullPointerException("TableEntity is null!");
        }
        this.entities = entities;
        this.entitiesTotal = entities.size();
        final List<MultipleItemEntity> multipleItem = setTable(entities);
        if (mAdapter == null) {
            mAdapter = TableAdapter.create(multipleItem);
            mAdapter.setOnClickIntListener(this);
        } else {
            mAdapter.setList(multipleItem);
        }
        RECYCLER_VIEW.setAdapter(mAdapter);
        addData(entities2);
    }


    /**
     * 设置表格内容的点击事件
     *
     * @param listener：
     */
    public void setOnClickTableListener(OnClickTableListener listener) {
        this.mClickTableListener = listener;
    }

    public List<MultipleItemEntity> getData() {
        if (mAdapter != null) {
            return mAdapter.getData();
        }
        return null;
    }

    public TableAdapter getAdapter() {
        return mAdapter;
    }


    /**
     * 设置内容
     *
     * @param entities：texts
     */
    public void addData(@NonNull List<TableEntity> entities) {
        this.entities.addAll(entities);
        if (mAdapter != null) {
            mAdapter.addData(setTable(entities));
        }
    }

    @NonNull
    private List<MultipleItemEntity> setTable(@NonNull List<TableEntity> entities) {
        final List<MultipleItemEntity> multipleItem = new ArrayList<>();
        final int size = entities.size();
        for (int i = 0; i < size; i++) {
            final int weight = entities.get(i).getWeight();
            final String name = entities.get(i).getName();
            final int color = entities.get(i).getColor();
            final int colorText = entities.get(i).getTextColor();
            final MultipleItemEntity entity = MultipleItemEntity.builder()
                    .setItemType(ItemType.TABLE_ITEM)
                    .setField(MultipleFields.SPAN_SIZE, weight)
                    .setField(MultipleFields.TEXT, name)
                    .setField(MultipleFields.COLOR, color)
                    .setField(MultipleFields.TEXT_COLOR, colorText)
                    .build();
            multipleItem.add(entity);
        }
        return multipleItem;
    }

    public void remove(int position) {
        if (mAdapter != null) {
            mAdapter.removeAt(position);
        }
    }

    @Override
    public void setTag(final Object tag) {
        RECYCLER_VIEW.setTag(tag);
    }

    @Override
    public Object getTag() {
        return RECYCLER_VIEW.getTag();
    }

    @Override
    public void onClick(int index) {
        if (mClickTableListener != null) {
            final int id = index / entitiesTotal - 1;
            if (id > -1) {
                mClickTableListener.onClickTable(index, entitiesTotal, entities);
            }
        }
    }

    public void setEntities(int weightTotal,
                            List<TableEntity> entitiesTitle,
                            List<TableEntity> entitiesText) {
        setWeightTotal(weightTotal);
        setTableAdapter(entitiesTitle);
        addData(entitiesText);
    }

}
