package com.zz.yt.lib.mvp.base.list;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.whf.android.jar.constants.Status;
import com.whf.android.jar.util.number.DoubleUtils;
import com.zz.yt.lib.mvp.base.list.adapter.BaseMvpRecyclerAdapter;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.entity.DateEntry;
import com.zz.yt.lib.ui.utils.ObjUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.bingoogolapple.refreshlayout.BGARefreshLayout;

/**
 * 这里是写的列表界面基类
 *
 * @param <adapter> 你的适配器
 * @param <T>       你的javabean
 * @author gags
 * @version 1.0
 */
public abstract class BaseListMenuFragment<adapter extends BaseMvpRecyclerAdapter<T>, T extends MultiItemEntity>
        extends AbstractExampleFragment<adapter, T> {

    /**
     * 适配器
     */
    protected List<T> mData = new ArrayList<>();

    protected RecyclerView mRecyclerView;
    /**
     * 最大页数
     */
    protected int maxNumber;

    /**
     * 传递过来 或者查询的数据
     */
    protected T mDate;

    /**
     * 用来传递任意参数的
     */
    protected SerializableMap map;


    @Override
    public Object setLayout() {
        return R.layout.mvp_public_recyclerview;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        initView();
    }

    protected void initView() {
        firstPage();

        mRefresh = findViewById(R.id.rl_module_name_refresh);
        inRefresh();

        mRecyclerView = findViewById(R.id.rl_customer);
        inRecycler(mRecyclerView);

    }

    /**
     * 进行搜索
     *
     * @param query：
     */
    protected boolean inSearch(String query) {
        mStatus = Status.REFRESH;
        isRefresh = false;
        PARAMS.putAll(getMap());
        firstPage();
        return true;
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        mDate = ObjUtils.getField(bundle.getSerializable(BundleKey.ENTITY.name()));
        map = (SerializableMap) bundle.getSerializable(BundleKey.MAP.name());
    }

    /**
     * 查询条件
     */
    protected HashMap<String, Object> getMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        if (map != null && map.getMap() != null) {
            hashMap = (HashMap<String, Object>) map.getMap();
        }
        return hashMap;
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
        setOnItemClickListener(position);
    }

    /**
     * 列表点击事件
     *
     * @param position 当前的条目
     */
    protected abstract void setOnItemClickListener(int position);

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onSuccess(@NonNull DateEntry<T> result) {
        mRecyclerView.setBackgroundResource(getBackColor());
        // 加载完毕后在 UI 线程结束加载更多
        close(0);
        maxNumber = result.getTotal();
        if (result.getRecords() != null && result.getRecords().size() > 0) {
            if (mStatus == Status.REFRESH) {
                mData = result.getRecords();
            } else {
                mData.addAll(result.getRecords());
            }
            if (mAdapter != null) {
                if (isRefresh) {
                    mData = result.getRecords();
                } else {
                    mData.addAll(result.getRecords());
                }
                mAdapter.notifyDataSetChanged();
            }
        } else {
            mRecyclerView.setBackgroundResource(getBackColor());
        }
    }


    /**
     * 返回  RecyclerView 的背景颜色 或者其他图片资源
     */
    protected int getBackColor() {
        return R.color.back_gray;
    }


    @Override
    public boolean onBGARefreshLayoutBeginLoadingMore(BGARefreshLayout refreshLayout) {
        if (presenter != null) {
            mStatus = Status.MORE;
            page++;
            if (isF(page, maxNumber)) {
                firstPage();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * 设置url
     */
    protected abstract String getUrl();

    @Override
    public void firstPage() {
        super.firstPage(getUrl());
    }

    /**
     * 查询是否可以继续加载数据
     *
     * @param page      当前页数
     * @param maxNumber 最多数据
     */
    public boolean isF(int page, int maxNumber) {
        if (page <= Math.ceil(DoubleUtils.div(maxNumber, 10, 1))) {
            return true;
        } else {
            ToastUtils.showShort("已经没有更多数据了!");
            return false;
        }
    }

}
