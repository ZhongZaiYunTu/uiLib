package com.zz.yt.lib.ui.popup.view;

import android.annotation.SuppressLint;
import android.view.View;

import androidx.annotation.NonNull;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.ui.R;

/**
 * 小提示
 *
 * @author qf
 * @version 1.0.24
 **/
@SuppressLint("ViewConstructor")
public class TipsMinPopup extends LattePopupCenter {

    private CharSequence mTitle = null;
    private CharSequence button = null;

    @NonNull
    public static TipsMinPopup create() {
        return new TipsMinPopup();
    }

    private TipsMinPopup() {
        super(Latte.getActivity());
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_tips_min;
    }

    /**
     * 设置数据
     *
     * @param title：
     */
    public TipsMinPopup setTitle(CharSequence title) {
        this.mTitle = title;
        return this;
    }


    /**
     * 设置数据
     *
     * @param button：
     */
    public TipsMinPopup setText(CharSequence button) {
        this.button = button;
        return this;
    }


    @Override
    protected void initViews() {
        setText(R.id.id_text_title, mTitle);
        setText(R.id.confirm, button);
        setOnClickListener(R.id.confirm, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    mClickDecisionListener.onConfirm(null);
                }
                dismiss();
            }
        });
        setOnClickListener(R.id.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    mClickDecisionListener.onCancel();
                }
                dismiss();
            }
        });
    }

}

