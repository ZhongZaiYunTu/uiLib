package com.zz.yt.lib.ui.entity;


import android.text.TextUtils;

import java.io.Serializable;

/**
 * 字典
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 20.6.17
 **/
public class LoginEntry implements Serializable {

    /**
     * access_token : 7caec37e-4df1-4104-8161-0961d36a2389
     * token_type : bearer
     * refresh_token : d11d95e5-ec7d-442f-a314-8706fa80619e
     * expires_in : 7198
     * scope : server
     * <p>
     * tenant_id : null
     * license : made by crcm
     * user_id : 5031318ddfc34e9091ee5fad9499541e
     * com_id : 4028816666f268010166f2c45632005a
     * dept_id : null
     * username : 张书豪
     */

    private String access_token;
    private String token_type;
    private String refresh_token;
    private int expires_in;
    private String scope;

    private Object tenant_id;
    private String license;
    private String user_id;
    private String com_id;
    private String username;


    private String userId;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Object getTenant_id() {
        return tenant_id;
    }

    public void setTenant_id(Object tenant_id) {
        this.tenant_id = tenant_id;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCom_id() {
        return com_id;
    }

    public void setCom_id(String com_id) {
        this.com_id = com_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        if (TextUtils.isEmpty(userId)) {
            return user_id;
        }
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
