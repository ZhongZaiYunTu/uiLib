package com.zz.yt.lib.ui.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ObjectUtils;
import com.whf.android.jar.dao.TokenDao;
import com.zz.yt.lib.ui.delegate.edition.listener.IEditionRecord;


/***
 * 主界面
 * @author qf
 * @version 1.0
 */
public abstract class BaseMainActivity extends BaseExampleActivity implements IEditionRecord {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ///其他app唤醒我时，给的token信息。
        Intent intent = getIntent();
        if (intent != null) {
            String token = intent.getStringExtra("token");
            if (ObjectUtils.isNotEmpty(token)) {
                awaken(token);
            }
        }
    }

    /**
     * @param token:处理token，例如保存到本地或进行身份验证等
     */
    protected void awaken(String token) {
        //处理token，例如保存到本地或进行身份验证等
        TokenDao.setAuthorization(token);
    }

    @Override
    public void onEditionRecord() {

    }


}
