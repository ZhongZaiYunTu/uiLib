package com.zz.yt.lib.ui.entity;

/**
 * 选中状态
 *
 * @author qf
 * @author wang.hai.feng
 * @version 2.5
 */
public abstract class SelectEntry {

    private boolean select;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public abstract String getSelectName();

}
