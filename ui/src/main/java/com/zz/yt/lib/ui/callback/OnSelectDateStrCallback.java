package com.zz.yt.lib.ui.callback;


/**
 * 时间选择回调
 *
 * @author qf
 * @version 1.0
 **/
public interface OnSelectDateStrCallback {
    /**
     * 时间段
     * @param startTime：
     * @param endTime：
     */
    void selectDate(String startTime, String endTime);
}

