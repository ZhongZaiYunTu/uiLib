package com.zz.yt.lib.ui.popup.view;

import android.annotation.SuppressLint;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.ui.R;


/**
 * 温馨提示
 *
 * @author qf
 * @version 1.0.24
 **/
@SuppressLint("ViewConstructor")
public class TipsWarnPopup extends LattePopupCenter {

    private CharSequence mTitle = null;
    private CharSequence mTips = null;
    private CharSequence button = null;
    private CharSequence cancel = null;


    @NonNull
    public static TipsWarnPopup create() {
        return new TipsWarnPopup();
    }

    private TipsWarnPopup() {
        super(Latte.getActivity());
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_tips_warn;
    }

    /**
     * 设置数据
     *
     * @param title：
     */
    public TipsWarnPopup setTitle(CharSequence title) {
        this.mTitle = title;
        return this;
    }

    /**
     * 设置数据
     *
     * @param tips：
     */
    public TipsWarnPopup setDate(CharSequence tips) {
        this.mTips = tips;
        return this;
    }

    /**
     * 设置数据
     *
     * @param tips：
     */
    public TipsWarnPopup setDate(@ColorInt int color, @NonNull String text, @NonNull String tips, String hint) {
        ForegroundColorSpan span = new ForegroundColorSpan(color);
        SpannableString spanString = new SpannableString(text + tips + hint);
        spanString.setSpan(span, text.length(), text.length() + tips.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

        this.mTips = spanString;
        return this;
    }

    /**
     * 设置数据
     *
     * @param button：
     */
    public TipsWarnPopup setText(CharSequence button) {
        this.button = button;
        return this;
    }

    /**
     * 设置数据
     *
     * @param buttonProceed：
     */
    public TipsWarnPopup setConfirm(CharSequence buttonProceed) {
        this.button = buttonProceed;
        return this;
    }

    /**
     * 设置数据
     *
     * @param buttonCancel：
     */
    public TipsWarnPopup setCancel(CharSequence buttonCancel) {
        this.cancel = buttonCancel;
        return this;
    }

    @Override
    protected void initViews() {
        setText(R.id.id_text_title, mTitle);
        setText(R.id.id_text_tips, mTips);
        setText(R.id.confirm, button);
        setText(R.id.cancel, cancel);

        setOnClickListener(R.id.confirm,new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    mClickDecisionListener.onConfirm(null);
                }
                dismiss();
            }
        });
        setOnClickListener(R.id.cancel,new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickDecisionListener != null) {
                    mClickDecisionListener.onCancel();
                }
                dismiss();
            }
        });
    }

}

