package com.zz.yt.lib.ui.delegate.signature.added;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.view.paint.PaintView;


/**
 * 设置电子签名
 *
 * @author qf
 * @version 1.0
 */
public class SignatureAddedDelegate extends LatteTitleDelegate {

    /**
     * 电子控件
     */
    private PaintView paintSignature;

    /**
     * 电子签名
     */
    private String signature = "";

    /**
     * 是否是全屏
     */
    private boolean isTop = true;

    @NonNull
    public static SignatureAddedDelegate create() {
        return create(true, null);
    }

    @NonNull
    public static SignatureAddedDelegate create(String signature) {
        return create(true, signature);
    }

    @NonNull
    public static SignatureAddedDelegate create(boolean isTop, String signature) {
        final Bundle args = new Bundle();
        args.putBoolean(BundleKey.IS_TOP.name(), isTop);
        args.putString(BundleKey.SIGNATURE.name(), signature);
        SignatureAddedDelegate delegate = new SignatureAddedDelegate(isTop, signature);
        delegate.setArguments(args);
        return delegate;
    }

    /**
     * 设置电子签名
     *
     * @param isTop:是否是全屏
     * @param signature:签字地址
     */
    protected SignatureAddedDelegate(boolean isTop, String signature) {
        LatteLogger.i("是否是全屏=" + isTop + ";签字地址=" + signature);
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        isTop = bundle.getBoolean(BundleKey.IS_TOP.name(), true);
        signature = bundle.getString(BundleKey.SIGNATURE.name());
    }

    @Override
    protected boolean isTopView() {
        return isTop;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("电子签名");
    }

    @Override
    protected Object setLayout() {
        return R.layout.hai_delegate_ui_signature_added;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        paintSignature = rootView.findViewById(R.id.paint_signature);
        if (paintSignature != null) {
            if (TextUtils.isEmpty(signature) || URLUtil.isNetworkUrl(signature)) {
                //没有传值，或传的是网址，取默认保存的值
                signature = getCachePath() + "/signature/signature.png";
            }
            int width = ScreenUtils.getScreenWidth();
            int height = SizeUtils.getMeasuredHeight(paintSignature);
            paintSignature.init(width, height, signature);
            paintSignature.reset();
            setOnClickListener(R.id.reset, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reset();
                }
            });
            setOnClickListener(R.id.redo, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    redo();
                }
            });
            setOnClickListener(R.id.undo, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    undo();
                }
            });
            setOnClickListener(R.id.preserve, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    preserve();
                }
            });

        }
    }

    /**
     * 清除
     */
    private void reset() {
        if (paintSignature != null) {
            paintSignature.reset();
        }
    }

    /**
     * 恢复
     */
    private void redo() {
        if (paintSignature != null) {
            paintSignature.redo();
        }
    }

    /**
     * 撤销
     */
    public void undo() {
        if (paintSignature != null) {
            paintSignature.undo();
        }
    }

    /**
     * 保存
     */
    public void preserve() {
        if (paintSignature != null) {
            boolean save = ImageUtils.save(paintSignature.getBitmap(), signature, Bitmap.CompressFormat.PNG);
            if (save) {
                LattePreference.addCustomAppProfile(BundleKey.SIGNATURE.name(), signature);
                goodPop();
            }
        }
    }

    /**
     * 获取app缓存路径
     */
    public String getCachePath() {
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            //外部存储可用
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            //外部存储不可用
            cachePath = context.getCacheDir().getPath();
        }
        return cachePath;
    }

}
