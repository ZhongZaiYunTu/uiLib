package com.zz.yt.lib.ui.popup.utils.date;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.utils.DateKeysUtils;
import com.zz.yt.lib.ui.utils.DatesUtils;


/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0.30
 **/
public class TimeDateUtils implements View.OnClickListener {

    private final TextView mView;
    private final DatesUtils dataUtils;
    private OnClickStringListener mClickStringListener = null;

    @NonNull
    public static TimeDateUtils create(ViewGroup layout, TextView textView) {
        return new TimeDateUtils(layout, textView);
    }

    protected TimeDateUtils(ViewGroup layout, @NonNull TextView textView) {
        this.dataUtils = DatesUtils.create();
        this.mView = textView;
        if (layout != null) {
            layout.setOnClickListener(this);
        } else {
            textView.setOnClickListener(this);
        }
    }

    /**
     * 设置事件
     */
    public TimeDateUtils setOnClickStringListener(OnClickStringListener listener) {
        mClickStringListener = listener;
        return this;
    }

    /**
     * 设置为日期选择(最小)
     */
    public TimeDateUtils startDate(int year, int month, int date) {
        dataUtils.startDate(year, month, date);
        return this;
    }

    /**
     * 设置为日期选择(最小)
     */
    public TimeDateUtils startDate(String yearMonthDate) {
        dataUtils.startDate(yearMonthDate);
        return this;
    }

    /**
     * 设置为日期选择（最大）
     */
    public TimeDateUtils endDate(int year, int month, int date) {
        dataUtils.endDate(year, month, date);
        return this;
    }

    /**
     * 设置为日期选择(最大)
     */
    public TimeDateUtils endDate(String yearMonthDate) {
        dataUtils.endDate(yearMonthDate);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public TimeDateUtils currentDate(String yearMonthDate) {
        dataUtils.currentDate(yearMonthDate);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public TimeDateUtils currentDate(int year, int month, int date) {
        dataUtils.currentDate(year, month, date);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public TimeDateUtils currentDate(int year, int month, int date,
                                     int hourOfDay, int minute, int second) {
        dataUtils.currentDate(year, month, date, hourOfDay, minute, second);
        return this;
    }

    /**
     * 设置为日期选择
     */
    public TimeDateUtils setDate() {
        return setType(DateKeysUtils.DATE);
    }

    /**
     * 设置为日期选择
     */
    public TimeDateUtils setTime() {
        return setType(DateKeysUtils.TIME);
    }

    /**
     * 设置为月份选择
     */
    public TimeDateUtils setMonth() {
        return setType(DateKeysUtils.MONTH);
    }

    /**
     * 设置为年份选择
     */
    public TimeDateUtils setYear() {
        return setType(DateKeysUtils.YEAR);
    }

    /**
     * 设置为选择模式
     */
    public TimeDateUtils setType(@DateKeysUtils.Type int patternType) {
        dataUtils.setType(patternType);
        return this;
    }

    /**
     * 设置为选择模式
     */
    public TimeDateUtils setTypePattern(boolean[] type, String pattern) {
        dataUtils.setTypePattern(type, pattern);
        return this;
    }


    /**
     * 初始化时间
     */
    public void init() {
        init(dataUtils.getNowString());
    }

    /**
     * 初始化时间
     */
    public void init(String text) {
        mView.setText(text);
        if (mClickStringListener != null) {
            mClickStringListener.onClick(text);
        }
    }

    @Override
    public void onClick(View v) {
        dataUtils.setOnClickStringListener(new OnClickStringListener() {
            @Override
            public void onClick(String text) {
                if (mView != null) {
                    mView.setText(text);
                }
                if (mClickStringListener != null) {
                    mClickStringListener.onClick(text);
                }
            }
        }).onClick();
    }
}
