package com.zz.yt.lib.ui.view.abst.multi;


import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.recycler.holder.MultipleViewHolder;

import java.util.Map;

/**
 * 类型布局，适用于类型较少，业务不复杂的场景，便于快速使用。
 *
 * @author qf
 * @version 2.5
 */
public interface ILayoutMultiAdapter {

    Map<Integer, Integer> initAdapter();

    void convert(@NonNull MultipleViewHolder holder, @NonNull MenuMultiEntity entity);

}
