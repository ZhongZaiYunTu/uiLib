package com.zz.yt.lib.ui.popup.utils.date;

import android.view.View;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.utils.DateKeysUtils;
import com.zz.yt.lib.ui.utils.DatesUtils;


/**
 * 时间
 *
 * @author qf
 * @version 1.0.7.1
 **/
public class DateTimeUtils implements View.OnClickListener {

    private final EditLayoutBar mEditLayoutBar;
    private final DatesUtils dataUtils;
    private OnClickStringListener mClickStringListener = null;


    @NonNull
    public static DateTimeUtils create(EditLayoutBar editLayoutBar) {
        return new DateTimeUtils(editLayoutBar);
    }

    protected DateTimeUtils(@NonNull EditLayoutBar editLayoutBar) {
        this.dataUtils = DatesUtils.create();
        this.mEditLayoutBar = editLayoutBar;
        editLayoutBar.setOnClickListener(this);
    }

    /**
     * 设置事件
     */
    public DateTimeUtils setOnClickStringListener(OnClickStringListener listener) {
        mClickStringListener = listener;
        return this;
    }

    /**
     * 设置为日期选择(最小)
     */
    public DateTimeUtils startDate(int year, int month, int date) {
        dataUtils.startDate(year, month, date);
        return this;
    }

    /**
     * 设置为日期选择(最小)
     */
    public DateTimeUtils startDate(String yearMonthDate) {
        dataUtils.startDate(yearMonthDate);
        return this;
    }

    /**
     * 设置为日期选择（最大）
     */
    public DateTimeUtils endDate(int year, int month, int date) {
        dataUtils.endDate(year, month, date);
        return this;
    }

    /**
     * 设置为日期选择(最大)
     */
    public DateTimeUtils endDate(String yearMonthDate) {
        dataUtils.endDate(yearMonthDate);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public DateTimeUtils currentDate(String yearMonthDate) {
        dataUtils.currentDate(yearMonthDate);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public DateTimeUtils currentDate(int year, int month, int date) {
        dataUtils.currentDate(year, month, date);
        return this;
    }

    /**
     * 设置默认选中时间
     */
    public DateTimeUtils currentDate(int year, int month, int date,
                                     int hourOfDay, int minute, int second) {
        dataUtils.currentDate(year, month, date, hourOfDay, minute, second);
        return this;
    }

    /**
     * 设置为日期选择
     */
    public DateTimeUtils setDate() {
        return setType(DateKeysUtils.DATE);
    }

    /**
     * 设置为日期选择
     */
    public DateTimeUtils setTime() {
        return setType(DateKeysUtils.TIME);
    }

    /**
     * 设置为月份选择
     */
    public DateTimeUtils setMonth() {
        return setType(DateKeysUtils.MONTH);
    }

    /**
     * 设置为年份选择
     */
    public DateTimeUtils setYear() {
        return setType(DateKeysUtils.YEAR);
    }

    /**
     * 设置为选择模式
     */
    public DateTimeUtils setType(@DateKeysUtils.Type int patternType) {
        dataUtils.setType(patternType);
        return this;
    }

    /**
     * 设置为选择模式
     */
    public DateTimeUtils setTypePattern(boolean[] type, String pattern) {
        dataUtils.setTypePattern(type, pattern);
        return this;
    }


    /**
     * 初始化时间
     */
    public void init() {
        init(dataUtils.getNowString());
    }

    /**
     * 初始化时间
     */
    public void init(String text) {
        if (mEditLayoutBar != null) {
            mEditLayoutBar.getValueView().setTag(text);
            mEditLayoutBar.getValueView().setText(text);
            if (mClickStringListener != null) {
                mClickStringListener.onClick(text);
            }
        }
    }

    @Override
    public void onClick(View v) {
        dataUtils.setOnClickStringListener(new OnClickStringListener() {
            @Override
            public void onClick(String text) {
                if (mEditLayoutBar != null) {
                    mEditLayoutBar.getValueView().setTag(text);
                    mEditLayoutBar.getValueView().setText(text);
                }
                if (mClickStringListener != null) {
                    mClickStringListener.onClick(text);
                }
            }
        }).onClick();
    }
}
