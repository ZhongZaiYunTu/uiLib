package com.zz.yt.lib.ui.delegate.authentication.operators;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.item.EditLayoutBar;



/**
 * 运营商认证
 *
 * @author qf
 * @version 1.0
 * @date 2020/8/13
 **/
public class OperatorsDelegate extends LatteTitleDelegate {


    private String phone = "";

    private EditLayoutBar mEditPhone = null;
    private EditLayoutBar mEditName = null;
    private EditLayoutBar idEditIdNumber = null;
    private Button mBtnSubmit = null;


    @NonNull
    public static OperatorsDelegate create(String phone) {
        final Bundle args = new Bundle();
        args.putString(BundleKey.PHONE.name(), phone);
        final OperatorsDelegate fragment = new OperatorsDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_aut_operators;
    }

    @Override
    protected void setCustomTitleBar(CustomTitleBar titleBar) {
        if (titleBar != null) {
            titleBar.setText("运营商认证");
        }
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        phone = bundle.getString(BundleKey.PHONE.name());
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        initView();
    }

    private void initView() {
        mEditPhone = findViewById(R.id.id_edit_phone);
        mEditName = findViewById(R.id.id_edit_name);
        idEditIdNumber = findViewById(R.id.id_edit_id_number);
        mBtnSubmit = findViewById(R.id.id_btn_submit);
        inListener();
    }

    private void inListener() {
        mEditPhone.setText(phone);
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String phone = mEditPhone.toString();
                if (mEditPhone.isString()) {
                    return;
                }
                final String name = mEditName.toString();
                if (mEditName.isString()) {
                    return;
                }
                final String idNumber = idEditIdNumber.toString();
                if (idEditIdNumber.isString()) {
                    return;
                }
                itSubmit(phone, name, idNumber);
            }
        });
    }


    protected void itSubmit(String phone, String name, String idNumber) {

    }

}
