package com.zz.yt.lib.ui.base.bottom.array;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;


/**
 * 纯列表类处理
 *
 * @author qf
 * @version 1.0
 **/
public abstract class LatteBottomArrayDelegate extends LatteBottomArray {

    /**
     * 获得数据处理
     *
     * @return 数据处理
     */
    protected abstract BaseDataConverter setDataConverter();

    @Override
    public Object setLayout() {
        return R.layout.a_ui_base_array_recycler;
    }

}
