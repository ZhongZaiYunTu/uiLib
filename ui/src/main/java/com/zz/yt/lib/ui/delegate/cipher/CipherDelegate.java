package com.zz.yt.lib.ui.delegate.cipher;

import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.delegate.code.BaseCodeDelegate;
import com.zz.yt.lib.ui.item.EditLayoutBar;


/**
 * 更新密码
 *
 * @author qf
 * @version 1.0
 * @date 2020/6/8
 **/
public class CipherDelegate extends BaseCodeDelegate {

    private final static int MIN_PASSWORD = 6;
    private EditLayoutBar mEditPassword = null;
    private EditLayoutBar mEditNewPassword = null;
    private EditLayoutBar mEditConfirmPassword = null;

    private TextView hintPass = null;

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_cipher;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("密码管理");
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        mEditPassword = rootView.findViewById(R.id.id_edit_old_pass);
        mEditNewPassword = rootView.findViewById(R.id.id_edit_new_pass);
        mEditConfirmPassword = rootView.findViewById(R.id.id_edit_repeat_pass);

        //密码不能小于6位
        hintPass = rootView.findViewById(R.id.id_text_hint_pass);


        initView();
    }

    private void initView() {
        initHintView(hintPass);
        inPassword(mEditPassword);
        inPassword(mEditNewPassword);
        inPassword(mEditConfirmPassword);
    }

    /**
     * @param codeLayout:密码强度提示
     */
    protected void initHintView(TextView codeLayout) {

    }

    @Override
    protected void onSubmit() {
        String password = mEditPassword.toString();
        String newPassword = mEditNewPassword.toString();
        String confirmPassword = mEditConfirmPassword.toString();
        if (isVerification(password, newPassword, confirmPassword)) {
            submit(password, newPassword);
        }
    }

    protected void submit(String password, String newPassword) {

    }

    protected boolean isVerification(String password, String newPassword, String confirmPassword) {
        if (mEditPassword.isString()) {
            return false;
        }
        if (mEditNewPassword.isString()) {
            return false;
        }
        if (RegexUtils.isZh(newPassword)) {
            ToastUtils.showShort("新密码不能有汉字");
            return false;
        }
        if (newPassword.length() < MIN_PASSWORD) {
            ToastUtils.showShort("新密码不能小于6位");
            return false;
        }
        if (password.equals(newPassword)) {
            ToastUtils.showShort("新密码不能与旧密码相同");
            return false;
        }
        if (mEditConfirmPassword.isString()) {
            return false;
        }
        if (!newPassword.equals(confirmPassword)) {
            ToastUtils.showShort("重复密码与新密码不相同");
            return false;
        }
        return isCode();
    }

    /**
     * @param mPassword:密码显示隐藏
     */
    protected void inPassword(final EditLayoutBar mPassword) {
        if (mPassword != null) {
            mPassword.getRightView().setImageResource(R.drawable.ic_visibility_off);
            mPassword.getValueView().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mPassword.getRightView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPassword.isSelected()) {
                        mPassword.setSelected(false);
                        mPassword.getRightView().setImageResource(R.drawable.ic_visibility_off);
                        mPassword.getValueView().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    } else {
                        mPassword.setSelected(true);
                        mPassword.getRightView().setImageResource(R.drawable.ic_visibility_on);
                        mPassword.getValueView().setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    }
                    mPassword.getValueView().setSelection(mPassword.getValueView().getText().length());
                }
            });
        }
    }
}
