package com.zz.yt.lib.ui.base.builder;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.base.bottom.item.BaseBottomItemDelegate;
import com.zz.yt.lib.ui.recycler.data.TabEntity;

import java.util.LinkedHashMap;

/**
 * @author qf
 * @version 1.0
 **/
public final class HomeTabBuilder {

    private final LinkedHashMap<TabEntity, BaseBottomItemDelegate> ITEMS = new LinkedHashMap<>();

    @NonNull
    public static HomeTabBuilder builder() {
        return new HomeTabBuilder();
    }

    public final HomeTabBuilder addItem(TabEntity bean, BaseBottomItemDelegate delegate) {
        ITEMS.put(bean, delegate);
        return this;
    }

    public final HomeTabBuilder addItems(LinkedHashMap<TabEntity, BaseBottomItemDelegate> items) {
        ITEMS.putAll(items);
        return this;
    }

    public final LinkedHashMap<TabEntity, BaseBottomItemDelegate> build() {
        return ITEMS;
    }
}
