package com.zz.yt.lib.ui.base.ui.array;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.recycler.data.BaseDataConverter;


/**
 * 纯列表类处理
 *
 * @author qf
 * @version 1.0.4.24
 **/
public abstract class LatteArrayDelegate extends LatteArray {

    /**
     * 获得数据处理
     *
     * @return 数据处理
     */
    protected abstract BaseDataConverter setDataConverter();

    @Override
    public Object setLayout() {
        return R.layout.a_ui_base_array_recycler;
    }

    @Override
    protected void initRecyclerView(RecyclerView recyclerView) {
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
    }


}
