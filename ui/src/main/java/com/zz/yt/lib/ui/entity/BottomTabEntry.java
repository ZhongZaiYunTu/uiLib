package com.zz.yt.lib.ui.entity;

import androidx.annotation.Nullable;

/**
 * Created by 傅令杰
 * @author 傅令杰
 */

public final class BottomTabEntry {

    private final CharSequence ICON;
    private final CharSequence TITLE;

    public BottomTabEntry(CharSequence icon, CharSequence title) {
        this.ICON = icon;
        this.TITLE = title;
    }

    public CharSequence getIcon() {
        return ICON;
    }


    public CharSequence getTitle() {
        return TITLE;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj);
    }


}
