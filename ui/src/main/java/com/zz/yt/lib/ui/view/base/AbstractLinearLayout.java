package com.zz.yt.lib.ui.view.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ResourceUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.ui.callback.OnSelectDateStrCallback;
import com.zz.yt.lib.ui.listener.OnClickStringListener;
import com.zz.yt.lib.ui.popup.view.CalendarEasyPopup;
import com.zz.yt.lib.ui.popup.view.CitySelectPopup;
import com.zz.yt.lib.ui.popup.view.TipsPopup;
import com.zz.yt.lib.ui.utils.AddressUtils;
import com.zz.yt.lib.ui.utils.DateKeysUtils;
import com.zz.yt.lib.ui.utils.DatesUtils;
import com.zz.yt.lib.ui.view.mothOrYearSelect.MothOrYearSelectBuilder;
import com.zz.yt.lib.ui.view.mothOrYearSelect.OnMothOrYearSelectListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 默认LinearLayout
 *
 * @author qf
 * @version 1.0
 **/
public abstract class AbstractLinearLayout extends RelativeLayout implements IListener {

    protected Context mContext;
    protected String cache = "";

    public AbstractLinearLayout(Context context) {
        super(context);
        mContext = context;
        inCache();
    }

    public AbstractLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        inCache();
    }

    //****************************** 缓存  *****************************************/

    protected void inCache() {
        cache = cache();
    }

    /**
     * 获取缓存值
     */
    protected final String cache() {
        return LattePreference.getCustomAppProfile(getClass().getCanonicalName());
    }

    /**
     * @param cache:开始缓存
     */
    protected final void setCache(String cache) {
        this.cache = cache;
        LattePreference.addCustomAppProfile(getClass().getCanonicalName(), cache);
    }

    //****************************** 获取控件  *****************************************/

    protected View getView(@IdRes int viewId) {
        return findViewById(viewId);
    }

    protected TextView getTextView(@IdRes int viewId) {
        return findViewById(viewId);
    }

    protected ImageView getImageView(@IdRes int viewId) {
        return findViewById(viewId);
    }

    protected EditText getEditText(@IdRes int viewId) {
        return findViewById(viewId);
    }

    protected Button getButton(@IdRes int viewId) {
        return findViewById(viewId);
    }

    protected RadioButton getRadioButton(@IdRes int viewId) {
        return findViewById(viewId);
    }

    /************************************* 获取布局控件  *****************************************/


    protected int dp2px(float dp) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    protected int sp2px(float sp) {
        final float scale = this.mContext.getResources().getDisplayMetrics().scaledDensity;
        return (int) (sp * scale + 0.5f);
    }

    /************************************* 获取布局控件  *****************************************/
    protected ViewGroup rootView() {
        return Latte.getActivity()
                .getWindow()
                .getDecorView()
                .findViewById(android.R.id.content);
    }

    /**
     * 默认为单例列表
     */
    protected RecyclerView.LayoutManager layoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    /**
     * 设置为瀑布列表
     */
    @NonNull
    protected RecyclerView.LayoutManager layoutManagerFlexbox(Context context) {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        return layoutManager;
    }

    /************************************* 设置获取值  *****************************************/

    protected String getTagStr(int viewId) {
        if (getTextView(viewId) == null) {
            return "";
        }
        if (getTextView(viewId).getTag() == null) {
            return "";
        }
        return String.valueOf(getTextView(viewId).getTag()).trim();
    }

    protected String getHintStr(int viewId) {
        if (getTextView(viewId) != null && getTextView(viewId).getHint() != null) {
            return String.valueOf(getTextView(viewId).getHint()).trim();
        }
        return "请选择正确内容";
    }

    protected String getTextStr(int viewId) {
        if (getTextView(viewId) != null) {
            return getTextView(viewId).getText().toString().trim();
        }
        return "";
    }

    /***
     * 判断数据是否为空
     * @param viewId：
     */
    protected boolean isEmpty(int viewId) {
        return isEmpty(viewId, getHintStr(viewId));
    }

    /***
     * 判断数据是否为空
     * @param viewId：
     * @param text：
     */
    protected boolean isEmpty(int viewId, final CharSequence text) {
        TextView depText = getTextView(viewId);
        if (depText != null && depText.getText().toString().trim().isEmpty()) {
            ToastUtils.showShort(text);
            return true;
        }
        return false;
    }

    /***
     * 判断数据是否为空
     * @param viewId：
     */
    protected boolean isTextEmpty(int viewId) {
        return isTextEmpty(viewId, getHintStr(viewId));
    }

    /**
     * 判断数据是否为空
     *
     * @param viewId：
     * @param text：
     */
    protected boolean isTextEmpty(int viewId, final CharSequence text) {
        TextView depText = getTextView(viewId);
        if (depText != null && depText.getText().toString().trim().isEmpty()) {
            ToastUtils.showShort(text);
            return true;
        }
        return false;
    }

    /************************************* 设置值 *****************************************/

    protected final void setText(@IdRes int viewId, CharSequence text) {
        TextView textView = getTextView(viewId);
        if (textView != null) {
            textView.setText(text);
        }
    }

    protected final void setTextHint(int viewId, CharSequence hint) {
        TextView textView = getTextView(viewId);
        if (textView != null) {
            textView.setHint(hint);
        }
    }

    protected final void setInputType(int viewId, int type) {
        TextView view = findViewById(viewId);
        if (view != null) {
            view.setInputType(type);
            view.setSingleLine(false);
        }
    }

    /***
     * 设置字体大小(代码设置 如14f)
     * @param viewId：
     * @param size：
     */
    protected final void setTextSize(@IdRes int viewId, float size) {
        setTextSizePx(viewId, sp2px(size));
    }

    /***
     * 设置字体大小
     * @param viewId：
     * @param size：
     */
    protected final void setTextSizePx(@IdRes int viewId, float size) {
        TextView textView = getTextView(viewId);
        if (textView != null) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
    }

    protected final void setTextStyle(@IdRes int viewId, int style) {
        //Typeface.NORMAL Typeface.BOLD Typeface.ITALIC
        TextView textView = getTextView(viewId);
        if (textView != null) {
            textView.setTypeface(null, style);
        }
    }

    protected final void setTextTag(@IdRes int viewId, CharSequence text) {
        TextView textView = getTextView(viewId);
        if (textView != null) {
            textView.setTag(text);
        }
    }

    protected final void setTextColor(@IdRes int viewId, int textColor) {
        TextView textView = getTextView(viewId);
        if (textView != null) {
            textView.setTextColor(textColor);
        }
    }

    protected final void setRadioButton(@IdRes int viewId, CharSequence text) {
        RadioButton textView = findViewById(viewId);
        if (textView != null) {
            textView.setText(text);
        }
    }

    protected final void setBackgroundResource(@IdRes int viewId, @DrawableRes int resId) {
        if (findViewById(viewId) != null) {
            findViewById(viewId).setBackgroundResource(resId);
        }
    }

    protected final void setBackgroundColor(@IdRes int viewId, @DrawableRes int resId) {
        if (findViewById(viewId) != null) {
            findViewById(viewId).setBackgroundColor(resId);
        }
    }

    protected final void setImageResource(int viewId, @DrawableRes int text) {
        ImageView view = getImageView(viewId);
        if (view != null) {
            view.setImageResource(text);
        }
    }

    /**
     * @param viewId:控件选中情况交换
     */
    protected final void setSelect(int viewId) {
        setSelected(viewId, !isSelected(viewId));
    }

    /**
     * @param viewId:设置控件是否选中
     * @param text:设置控件是否选中
     */
    protected final void setSelected(int viewId, boolean text) {
        View view = getView(viewId);
        if (view != null) {
            view.setSelected(text);
        }
    }

    /**
     * @param viewId:设置控件是否編輯
     * @param text:设置控件是否編輯
     */
    protected final void setEnabled(int viewId, boolean text) {
        TextView view = getTextView(viewId);
        if (view != null) {
            view.setEnabled(text);
        }
    }

    /**
     * @param viewId:获得控件是否选中
     */
    protected boolean isSelected(int viewId) {
        View view = getView(viewId);
        if (view != null) {
            return view.isSelected();
        }
        return false;
    }

    /************************************* 设置是否可以编辑  *****************************************/

    protected final void setViewEdit(int viewId, boolean isEdit) {
        TextView view = getTextView(viewId);
        if (view != null) {
            //去掉点击时编辑框下面横线:
            view.setEnabled(isEdit);
            //不可编辑
            view.setFocusable(isEdit);
            //不可编辑
            view.setFocusableInTouchMode(isEdit);
        }
    }

    protected final void setViewOnClick(int viewId, boolean isEdit) {
        View view = findViewById(viewId);
        if (view != null) {
            //去掉点击时编辑框下面横线:
            view.setEnabled(isEdit);
            //不可编辑
            view.setFocusable(isEdit);
        }
    }

    //************************************* 设置控件显示  *****************************************/

    /**
     * 是否显示(true显示；false隐藏)。
     */
    public final boolean isShow() {
        return isShow(this);
    }

    /**
     * @param viewId:该控件是否显示(true显示；false隐藏)。
     */
    protected final boolean isShow(int viewId) {
        View view = findViewById(viewId);
        return isShow(view);
    }

    /**
     * @param view:该控件是否显示(true显示；false隐藏)。
     */
    protected final boolean isShow(View view) {
        if (view != null) {
            return view.getVisibility() == VISIBLE;
        }
        return false;
    }


    /**
     * @param is:true显示；false隐藏
     */
    public final void setShow(boolean is) {
        setShow(this, is);
    }

    /**
     * @param viewId:需要设置的控件。
     * @param is:true显示；false隐藏
     */
    protected final void setShow(@IdRes int viewId, boolean is) {
        View view = findViewById(viewId);
        setShow(view, is);
    }

    /**
     * @param view:需要设置的控件。
     * @param is:true显示；false隐藏
     */
    protected final void setShow(View view, boolean is) {
        if (view != null) {
            view.setVisibility(is ? VISIBLE : GONE);
        }
    }

    /**
     * @param viewId:需要设置的控件。
     * @param is:true显示；false不显示
     */
    @Deprecated
    protected final void setVisible(@IdRes int viewId, boolean is) {
        View view = findViewById(viewId);
        setHide(view, is);
    }

    /**
     * @param is:true显示；false不显示
     */
    public final void setHide(boolean is) {
        setHide(this, is);
    }

    /**
     * @param viewId:需要设置的控件。
     * @param is:true显示；false不显示
     */
    protected final void setHide(@IdRes int viewId, boolean is) {
        View view = findViewById(viewId);
        setHide(view, is);
    }

    /**
     * @param view:需要设置的控件。
     * @param is:true显示；false不显示
     */
    protected final void setHide(View view, boolean is) {
        if (view != null) {
            view.setVisibility(is ? VISIBLE : INVISIBLE);
        }
    }

    /************************************* 设置默认数据 ****************************************/

    protected List<String> data() {
        return data(5);
    }

    protected List<String> data(int size) {
        List<String> data = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            data.add("");
        }
        return data;
    }

    protected List<Map<String, Object>> dataNet() {
        return dataNet(5);
    }

    protected List<Map<String, Object>> dataNet(int size) {
        List<Map<String, Object>> data = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            data.add(new HashMap<String, Object>());
        }
        return data;
    }

    /************************************* 设置事件  *****************************************/

    protected final void setOnClickListener(@IdRes int viewId, View.OnClickListener listener) {
        View view = findViewById(viewId);
        if (view != null) {
            view.setOnClickListener(listener);
        }
    }

    /**
     * @param viewId:设置长按监听事件
     * @param listener:监听焦点
     */
    protected final void setOnLongClickListener(int viewId, View.OnLongClickListener listener) {
        View view = findViewById(viewId);
        if (view != null) {
            view.setOnLongClickListener(listener);
        }
    }

    /**
     * @param viewId:设置输入监听事件
     * @param listener:监听焦点
     */
    protected final void setOnFocusChangeListener(int viewId, View.OnFocusChangeListener listener) {
        View view = findViewById(viewId);
        if (view != null) {
            view.setOnFocusChangeListener(listener);
        }
    }

    /**
     * @param viewId:设置输入监听事件
     * @param watcher:
     */
    protected final void addTextChangedListener(int viewId, TextWatcher watcher) {
        TextView view = findViewById(viewId);
        if (view != null) {
            view.addTextChangedListener(watcher);
        }
    }

    /**
     * @param viewId:设置输入完成监听事件
     * @param watcher:
     */
    protected final void setOnEditorActionListener(int viewId, TextView.OnEditorActionListener watcher) {
        TextView view = findViewById(viewId);
        if (view != null) {
            view.setOnEditorActionListener(watcher);
        }
    }

    /**
     * @param viewId:设置输入完成监听事件
     * @param watcher:
     */
    protected final void setOnEditorActionListener(final int viewId, final OnClickStringListener watcher) {
        setOnEditorActionListener(viewId, new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                        actionId == EditorInfo.IME_ACTION_DONE ||
                        event != null &&
                                event.getAction() == KeyEvent.ACTION_DOWN &&
                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (event == null || !event.isShiftPressed()) {
                        if (watcher != null) {
                            watcher.onClick(getTextStr(viewId));
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    /**
     * @param viewId:设置判断控件监听事件
     * @param listener:
     */
    protected final void setOnCheckedSwitchListener(int viewId, CompoundButton.OnCheckedChangeListener listener) {
        @SuppressLint("UseSwitchCompatOrMaterialCode")
        Switch swtTest = findViewById(viewId);
        if (swtTest != null) {
            //给开关按钮设置监听状态改变事件
            swtTest.setOnCheckedChangeListener(listener);
        }
    }

    /**
     * @param viewId:设置RadioGroup监听事件
     * @param watcher:
     */
    protected final void setOnCheckedChangeListener(int viewId, RadioGroup.OnCheckedChangeListener watcher) {
        RadioGroup view = findViewById(viewId);
        if (view != null) {
            view.setOnCheckedChangeListener(watcher);
        }
    }

    /**
     * @param viewId:控件
     * @param max:限制最大输入字符个数
     */
    protected final void setFilters(int viewId, int max) {
        TextView view = findViewById(viewId);
        if (view != null) {
            view.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});
        }
    }

    /*** 选择时间(默认日期<年月日>) */
    protected final void onClickTime(OnClickStringListener listener) {
        onClickTime(DateKeysUtils.DATE, listener);
    }

    /*** 选择时间 */
    protected final void onClickTime(@DateKeysUtils.Type int patternType, OnClickStringListener listener) {
        DatesUtils.create()
                .setType(patternType)
                .setOnClickStringListener(listener)
                .onClick();
    }


    /*** 选择时间 */
    protected final void onClickTimeSingle(OnSelectDateStrCallback callback) {
        onClickTimeSingle(false, callback);
    }

    /**
     * @param callback:选择时间
     * @param toDay:true(只能选择今天及以后的时间)
     */
    protected final void onClickTimeSingle(boolean toDay, OnSelectDateStrCallback callback) {
        CalendarEasyPopup
                .create(true, toDay)
                .setOnClickListener(callback)
                .popup();
    }

    /*** 选择时间段 */
    protected final void onClickTimeMultiple(OnSelectDateStrCallback callback) {
        onClickTimeMultiple(false, callback);
    }

    /**
     * @param callback:选择时间段
     * @param toDay:true(只能选择今天及以后的时间)
     */
    protected final void onClickTimeMultiple(boolean toDay, OnSelectDateStrCallback callback) {
        CalendarEasyPopup
                .create(false, toDay)
                .setOnClickListener(callback)
                .popup();
    }

    /**
     * @param listener:城市选择及回调
     */
    protected final void onClickCity(OnClickStringListener listener) {
        CitySelectPopup.create()
                .setOnClickListener(listener)
                .popup();
    }

    /**
     * @param listener:省市区选择及回调
     */
    protected final void onClickAreas(OnClickStringListener listener) {
        AddressUtils.getInstance()
                .onClickListener(listener)
                .popup();
    }

    /*** 选择日期段 */
    protected final void onClickDate(final boolean isMonth, final OnSelectDateStrCallback callback) {
        Calendar selectedDate = Calendar.getInstance();

        Calendar startDate = Calendar.getInstance();
        startDate.set(2000, 0, 1);

        Calendar endDate = Calendar.getInstance();
        endDate.set(selectedDate.getWeekYear() + 5, 11, 1);

        new MothOrYearSelectBuilder(getContext(),
                new OnMothOrYearSelectListener() {
                    @Override
                    public void onMothOrYearSelect(Date date, Date date2) {
                        SimpleDateFormat pattern = TimeUtils.getSafeDateFormat("yyyy");
                        if (isMonth) {
                            pattern = TimeUtils.getSafeDateFormat("yyyy-MM");
                        }
                        String startTime = TimeUtils.date2String(date, pattern);
                        String endTime = TimeUtils.date2String(date2, pattern);
                        if (callback != null) {
                            callback.selectDate(startTime, endTime);
                        }
                    }
                })
                .setMothOrYear(isMonth)
                .setDividerColor(Color.DKGRAY)
                .setContentTextSize(16)
                .setDecorView(rootView())
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setOutSideColor(0x00000000)
                .setOutSideCancelable(true)
                .build()
                .show();
    }

    /****************************** 设置点击频繁大于0.6秒一次  *********************************/

    private long lastClickTime = 0;

    /*** 判断点击频繁是否大于0.6秒一次 */
    protected synchronized boolean isFastClick() {
        return isFastClick(600);
    }

    /*** 判断点击频繁是否大于0.6秒一次 */
    protected synchronized boolean isFastClick(int gap) {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < gap) {
            return true;

        }
        lastClickTime = time;
        return false;

    }

    /****************************** 左、右两边图标点击事件  *********************************/

    protected final void setOnTouchListenerLeft(int vieId, final OnClickListener listener) {
        //TextView左边图标点击事件
        setOnTouchListener(vieId, 0, listener);
    }

    /**
     * @param vieId:TextView右边图标点击事件
     * @param listener:点击事件处理
     */
    protected final void setOnTouchListenerRight(int vieId, final OnClickListener listener) {
        setOnTouchListener(vieId, 2, listener);
    }

    /**
     * @param vieId:TextView四边图标点击事件
     * @param listener:点击事件处理
     */
    @SuppressLint("ClickableViewAccessibility")
    protected final void setOnTouchListener(int vieId, final int i, final OnClickListener listener) {
        final TextView view = getTextView(vieId);
        if (view != null) {
            final int width = view.getWidth() - view.getPaddingRight();
            view.setOnTouchListener(new OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {
                    // 得到一个长度为4的数组，分别表示左上右下四张图片(0.1.2.3)
                    Drawable drawable = getViewDrawables(view, i);
                    //如果右边没有图片，不再处理
                    if (drawable == null)
                        return false;
                    //如果不是按下事件，不再处理
                    if (event.getAction() != MotionEvent.ACTION_UP)
                        return false;
                    if (event.getX() > width - drawable.getIntrinsicWidth()) {
                        if (listener != null) {
                            listener.onClick(view);
                        }
                    }
                    return false;
                }
            });
        }
    }

    //****************************** 设置左边图标  *********************************/

    /**
     * @param image:左边按钮图片
     */
    protected final void setLeftImage(int vieId, int image) {
        try {
            setLeftImage(vieId, ResourceUtils.getDrawable(image));
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
    }

    /**
     * @param image:左边按钮图片
     */
    protected final void setLeftImage(TextView textView, int image) {
        try {
            setLeftImage(textView, ResourceUtils.getDrawable(image));
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
    }

    /**
     * @param image:左边按钮图片
     */
    protected final void setLeftImage(int vieId, Drawable image) {
        setLeftImage(getTextView(vieId), image);
    }

    /**
     * @param image:左边按钮图片
     */
    protected final void setLeftImage(TextView textView, Drawable image) {
        if (textView != null) {
            try {
                textView.setCompoundDrawablesWithIntrinsicBounds(image,
                        getViewDrawables(textView, 1),
                        getViewDrawables(textView, 2),
                        getViewDrawables(textView, 3));
            } catch (Exception e) {
                LatteLogger.e(e.getMessage());
            }
        }
    }

    //****************************** 设置右边图标  *********************************/

    /**
     * @param image:右边按钮图片
     */
    protected final void setRightImage(int vieId, int image) {
        try {
            setRightImage(vieId, ResourceUtils.getDrawable(image));
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
    }

    /**
     * @param image:右边按钮图片
     */
    protected final void setRightImage(int vieId, Drawable image) {
        try {
            setRightImage(getTextView(vieId), image);
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
    }

    /**
     * @param image:右边按钮图片
     */
    protected final void setRightImage(TextView textView, int image) {
        try {
            setRightImage(textView, ResourceUtils.getDrawable(image));
        } catch (Exception e) {
            LatteLogger.e(e.getMessage());
        }
    }

    /**
     * @param image:右边按钮图片
     */
    protected final void setRightImage(TextView textView, Drawable image) {
        if (textView != null) {
            try {
                textView.setCompoundDrawablesWithIntrinsicBounds(
                        getViewDrawables(textView, 0),
                        getViewDrawables(textView, 1),
                        image,
                        getViewDrawables(textView, 3));
            } catch (Exception e) {
                LatteLogger.e(e.getMessage());
            }
        }
    }


    /**
     * @param view:得到一个长度为4的数组，分别表示左上右下四张图片(0.1.2.3)
     * @param i:左上右下四张图片(0.1.2.3)
     */
    protected Drawable getViewDrawables(TextView view, int i) {
        if (view != null) {
            return view.getCompoundDrawables()[i];
        }
        return null;
    }

    //****************************** 拨打电话、发短信  *********************************/

    /**
     * 拨打电话（跳转到拨号界面，用户手动点击拨打）
     *
     * @param phoneNum 电话号码
     */
    protected final void callPhone(String phoneNum) {
        final Intent intent = new Intent(Intent.ACTION_DIAL);
        final Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        TipsPopup.create().setDate("确定拨打电话：" + phoneNum + " 吗？").setOnClickListener(new OnClickDecisionListener() {
            @Override
            public void onConfirm(Object o) {
                ContextCompat.startActivity(Latte.getActivity(), intent, null);
            }

            @Override
            public void onCancel() {

            }
        }).popup();
    }

    /**
     * 发短信
     *
     * @param uri：跳转到发短信界面
     */
    protected final void callShortLetter(String uri) {
        //打电话
        final Intent intent = new Intent(Intent.ACTION_SENDTO);
        final Uri data = Uri.parse(uri);
        intent.setData(data);
        ContextCompat.startActivity(Latte.getActivity(), intent, null);
    }

    /**
     * 发短信
     *
     * @param phone：跳转到发短信界面
     */
    protected final void callShortLetter(String phone, String body) {
        //发短信
        final String sms = "sms";
        final Uri data = Uri.parse(sms + "to:" + phone);
        final Intent intent = new Intent(Intent.ACTION_SENDTO, data);
        intent.putExtra(sms + "_body", body);
        ContextCompat.startActivity(Latte.getActivity(), intent, null);
    }

    //****************************** 手机号、身份证号等星号显示  *********************************/

    /**
     * @param viewId:控件
     * @param text:设置‘手机号’显示为隐藏
     */
    protected final void setTextPhone(@IdRes int viewId, String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        setText(viewId, text.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));
    }

    /**
     * @param viewId:控件
     * @param text:设置‘身份证号’显示为隐藏
     */
    protected final void setTextIdCard(@IdRes int viewId, String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        setText(viewId, text.replaceAll("(\\d{4})\\d{10}(\\w{4})", "$1****$2"));
    }

    /**
     * @param viewId:控件
     * @param text:设置‘邮件’显示为隐藏
     */
    protected final void setTextEmail(@IdRes int viewId, String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        setText(viewId, text.replaceAll("(\\w?)(\\w+)(\\w)(@\\w+\\.[a-z]+(\\.[a-z]+)?)", "$1****$3$4"));
    }

}
