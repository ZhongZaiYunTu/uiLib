package com.zz.yt.lib.ui.base.builder;

import androidx.annotation.NonNull;

import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.recycler.data.TabEntity;

import java.util.LinkedHashMap;

/**
 * @author qf
 * @version 1.0
 **/
public final class ItemTabBuilder {

    private final LinkedHashMap<TabEntity, LatteDelegate> ITEMS = new LinkedHashMap<>();

    @NonNull
    public static ItemTabBuilder builder() {
        return new ItemTabBuilder();
    }

    public final ItemTabBuilder addItem(TabEntity bean, LatteDelegate delegate) {
        ITEMS.put(bean, delegate);
        return this;
    }

    public final ItemTabBuilder addItems(LinkedHashMap<TabEntity, LatteDelegate> items) {
        ITEMS.putAll(items);
        return this;
    }

    public final LinkedHashMap<TabEntity, LatteDelegate> build() {
        return ITEMS;
    }
}
