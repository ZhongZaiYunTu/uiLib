package com.zz.yt.lib.ui.base.ui.tab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.base.builder.ItemTabBuilder;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.recycler.data.TabEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import me.yokeyword.fragmentation.ISupportFragment;

/**
 * tab处理
 *
 * @author qf
 * @version 1.0
 **/
public abstract class LatteTopTabDelegate extends LatteTitleDelegate implements OnTabSelectListener {

    private CommonTabLayout mTabLayout = null;

    private final ArrayList<CustomTabEntity> TAB_BEANS = new ArrayList<>();
    private final ArrayList<LatteDelegate> ITEM_DELEGATES = new ArrayList<>();
    private final LinkedHashMap<CustomTabEntity, LatteDelegate> ITEMS = new LinkedHashMap<>();
    private int mCurrentDelegate = 0;
    private int mIndexDelegate = 0;

    /**
     * 第一次默认显示的 tab
     */
    public int setIndexDelegate() {
        return 0;
    }

    /**
     * 初始化界面
     *
     * @param builder:
     * @return 界面
     */
    public abstract LinkedHashMap<TabEntity, LatteDelegate> setItems(ItemTabBuilder builder);

    @Override
    public Object setLayout() {
        return R.layout.a_ui_base_latte_tab_layout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIndexDelegate = setIndexDelegate();
        final ItemTabBuilder builder = ItemTabBuilder.builder();
        final LinkedHashMap<TabEntity, LatteDelegate> items = setItems(builder);
        ITEMS.putAll(items);
        for (Map.Entry<CustomTabEntity, LatteDelegate> item : ITEMS.entrySet()) {
            final CustomTabEntity key = item.getKey();
            final LatteDelegate value = item.getValue();
            TAB_BEANS.add(key);
            ITEM_DELEGATES.add(value);
        }
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        final LinearLayoutCompat bottomBar = findViewById(R.id.bottom_bar);
        LayoutInflater.from(getContext()).inflate(R.layout.bottom_item_common_tab_layout, bottomBar);
        mTabLayout = rootView.findViewById(R.id.id_tab_layout);
        mTabLayout.setTabData(TAB_BEANS);
        mTabLayout.setOnTabSelectListener(this);
        mTabLayout.setCurrentTab(mIndexDelegate);
        final int size = ITEMS.size();
        final ISupportFragment[] delegateArray = ITEM_DELEGATES.toArray(new ISupportFragment[size]);
        getSupportDelegate().loadMultipleRootFragment(R.id.id_layout_frame, mIndexDelegate, delegateArray);
        mCurrentDelegate = mIndexDelegate;
    }


    @Override
    public void onTabSelect(int position) {
        mTabLayout.setCurrentTab(position);
        getSupportDelegate().showHideFragment(ITEM_DELEGATES.get(position), ITEM_DELEGATES.get(mCurrentDelegate));
        //注意先后顺序
        mCurrentDelegate = position;
    }

    public int getCurrentDelegate() {
        return mCurrentDelegate;
    }

    @Override
    public void onTabReselect(int position) {

    }

}
