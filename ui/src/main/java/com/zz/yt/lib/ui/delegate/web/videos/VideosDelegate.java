package com.zz.yt.lib.ui.delegate.web.videos;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.UtilsTransActivity;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.web.WebDelegate;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.delegate.web.WebUiDelegate;

import java.util.List;


public class VideosDelegate extends WebUiDelegate {

    @NonNull
    public static VideosDelegate create(String title, String url) {
        final Bundle args = new Bundle();
        args.putString(BundleKey.TITLE.name(), title);
        args.putString(BundleKey.URL.name(), url);
        final VideosDelegate delegate = new VideosDelegate();
        delegate.setArguments(args);
        return delegate;
    }

    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        final Bundle args = getArguments();
        if (args != null) {
            PermissionUtils
                    .permission(PermissionConstants.CAMERA, PermissionConstants.MICROPHONE)
                    .rationale(new PermissionUtils.OnRationaleListener() {
                        @Override
                        public void rationale(@NonNull UtilsTransActivity activity, @NonNull ShouldRequest shouldRequest) {
                            shouldRequest.again(true);
                        }
                    })
                    .callback(new PermissionUtils.FullCallback() {
                        @Override
                        public void onGranted(@NonNull List<String> permissionsGranted) {
                            LatteLogger.d("权限获取成功", permissionsGranted);
                            final WebDelegate delegate = loadRootDelegate();
                            getSupportDelegate().loadRootFragment(R.id.id_layout_frame, delegate);
                        }

                        @Override
                        public void onDenied(@NonNull List<String> permissionsDeniedForever, @NonNull List<String> permissionsDenied) {
                            LatteLogger.d("权限永久拒绝", permissionsDeniedForever);
                            LatteLogger.d("权限拒绝", permissionsDenied);
                        }
                    })
                    .request();
        }
    }

    protected WebDelegate loadRootDelegate() {
        VideosDelegateImpl delegate = VideosDelegateImpl.create(url);
        delegate.setPageLoadListener(this);
        delegate.setTopDelegate(this);
        return delegate;
    }

}

