package com.zz.yt.lib.ui.delegate.edition.about;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.zz.yt.lib.ui.BundleKey;
import com.zz.yt.lib.ui.ButtonListLayout;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;

import java.util.ArrayList;
import java.util.List;

/**
 * 版本信息
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/10/19
 **/
public class EditionAboutDelegate extends LatteTitleDelegate {

    PackageManager mPackageManager;
    private static final List<String> mPackageNames = new ArrayList<>();
    private static final String GAODE_PACKAGE_NAME = "com.autonavi.minimap";
    private static final String BAIDU_PACKAGE_NAME = "com.baidu.BaiduMap";

    private String title = "中再云图";
    private double currLocationX = 0.0;
    private double currLocationY = 0.0;
    private final double locationX = 29.602552;
    private final double locationY = 106.313509;
    private final String storeName = "中再云图技术有限公司";


    @NonNull
    public static EditionAboutDelegate create() {
        final Bundle args = new Bundle();
        EditionAboutDelegate fragment = new EditionAboutDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    public static EditionAboutDelegate create(Bundle args) {
        EditionAboutDelegate fragment = new EditionAboutDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void getData(Bundle bundle) {
        super.getData(bundle);
        title = bundle.getString(BundleKey.TITLE.name(), "中再云图");
        currLocationX = bundle.getDouble(BundleKey.LONGITUDE.name());
        currLocationY = bundle.getDouble(BundleKey.LATITUDE.name());
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_ui_agree;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText(title);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);

        mPackageManager = _mActivity.getPackageManager();

        ButtonListLayout phone = rootView.findViewById(R.id.id_bll_phone);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:400-966-1988");
                intent.setData(data);
                startActivity(intent);
            }
        });

        ButtonListLayout address = rootView.findViewById(R.id.id_bll_address);
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (haveGaodeMap()) {
                    // 如果安装高德APP
                    openGaodeMapToGuide();//打开高德APP
                } else {
                    //否则 打开浏览器
                    openBrowserToGuide();
                }
            }
        });
    }

    /**
     * 打开高德地图
     * https://lbs.amap.com/api/amap-mobile/guide/android/route
     */
    private void openGaodeMapToGuide() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        LogUtils.i("高德定位：", "经度：" + currLocationY + " ,纬度：" + currLocationX);
        String url;
        if (currLocationX == 0.0 || currLocationY == 0.0) {
            url = "androidamap://route?sourceApplication=amap&dlat=" + locationX + "&dlon=" + locationY + "&dname=" + storeName + "&dev=0&t=0";
        } else {
            url = "androidamap://route?sourceApplication=amap&slat=" + currLocationX + "&slon=" + currLocationY
                    + "&dlat=" + locationX + "&dlon=" + locationY + "&dname=" + storeName + "&dev=0&t=0";
        }

        Uri uri = Uri.parse(url);
        //将功能Scheme以URI的方式传入data
        intent.setData(uri);
        //启动该页面即可
        startActivity(intent);
    }

    /**
     * 打开浏览器
     */
    private void openBrowserToGuide() {
        String url = "http://uri.amap.com/navigation?to=" + locationY + "," + locationX + "," +
                storeName + "&mode=car&policy=1&src=mypage&coordinate=gaode&callnative=0";
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void initPackageManager() {
        List<PackageInfo> packageInfos = mPackageManager.getInstalledPackages(0);
        if (packageInfos != null) {
            for (int i = 0; i < packageInfos.size(); i++) {
                mPackageNames.add(packageInfos.get(i).packageName);
            }
        }
    }

    /**
     * 判断有无安装高德
     */
    public boolean haveGaodeMap() {
        initPackageManager();
        return mPackageNames.contains(GAODE_PACKAGE_NAME);
    }
}