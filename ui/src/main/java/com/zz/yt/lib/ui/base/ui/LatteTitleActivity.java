package com.zz.yt.lib.ui.base.ui;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.SearchView;

import androidx.annotation.NonNull;

import com.whf.android.jar.base.latte.BaseActivity;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.R;


/***
 * 界面
 * @author qf
 * @version 2.3.23
 */
public abstract class LatteTitleActivity extends BaseActivity {

    private CustomTitleBar mTitleBar = null;

    /***
     * 设置标题
     * @param titleBar:数据处理
     */
    protected abstract void setCustomTitleBar(CustomTitleBar titleBar);


    /***
     * 设置标题
     * @param title:标题
     */
    protected void setTitle(String title) {
        if (mTitleBar != null) {
            mTitleBar.setText(title);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //1
        View viewTop = findViewById(R.id.id_view_top);
        if (viewTop != null && isTopView()) {
            addMarginTopEqualStatusBarHeight(viewTop);
        }
        //2
        mTitleBar = findViewById(R.id.id_title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitleClickLeftListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            mTitleBar.getTitleBarLeftBtn().setTextSize(18);
            mTitleBar.getTitleBarLeftBtn().setCompoundDrawablePadding(16);
            mTitleBar.getTitleBarTitle().setTextSize(18);
            setCustomTitleBar(mTitleBar);
        }
        //3
        final SearchView mSearchView = findViewById(R.id.search_view);
        if (mSearchView != null) {
            setSearchView(mSearchView);
        }
    }

    /**
     * @return 是否是全屏
     */
    protected boolean isTopView() {
        return true;
    }


    /***
     * 搜索控件
     * @param search：
     */
    protected void setSearchView(@NonNull SearchView search) {
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return inSearch(newText);
            }
        });
    }

    /***
     * 进行搜索
     * @param query：
     */
    protected boolean inSearch(String query) {
        LatteLogger.i(query);
        return false;
    }


}
